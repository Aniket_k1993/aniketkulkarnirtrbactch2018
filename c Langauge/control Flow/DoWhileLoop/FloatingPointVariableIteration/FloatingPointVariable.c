#include <stdio.h>
int main(void)
{
	//variable declarations
	float f;
	float f_num = 1.7f;
	//code
	printf("\n");
	printf("Printing Numbers %f to %f : \n", f_num, (f_num * 10.0f));
	f = f_num;
	do
	{
		printf("\t%f\n", f);
		f = f + f_num;
	}
	while (f <= (f_num * 10.0f));
	printf("\n");
	return(0);
}

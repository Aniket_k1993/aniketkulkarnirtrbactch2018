#import <UIKit/UIKit.h>

enum{
    ANK_ATTRIBUTE_POSITION = 0,
    ANK_ATTRIBUTE_COLOR,
    ANK_ATTRIBUTE_NORMAL,
    ANK_ATTRIBUTE_TEXTURE0
};
@interface GLESView : UIView <UIGestureRecognizerDelegate>
-(void)startAnimation;
-(void)stopAnimation;
@end

#include <windows.h>
#include <gl/GL.h>
#include<gl/GLU.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLfloat angle_x_Light = 0;
GLfloat angle_y_Light = 0;
GLfloat angle_z_Light = 0;

//int x_rotation = 0;
//int y_rotation = 0;
//int z_rotation = 0;

bool gbLighting = false;
GLUquadric *quadric[24];
GLfloat KeyisPress = 0.0f;
//Lighting arrays:
//LIGHT0: GLOBAL WHITE LIGHT
GLfloat light_ambient[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };	//White colored light
//GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position[] = { 1.0f, 1.0f, 1.0f, 0.0f };	//Position of light

GLfloat light_model_ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
GLfloat light_model_local_viewer[] = { 0.0f };

//MATERIALS OF THE SPHERES:
//1. EMERALD:

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	int xPos, yPos;
	MONITORINFO mi;
	mi = { sizeof(MONITORINFO) };

	//code

	//initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Registering Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Materials"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi);
	xPos = (mi.rcMonitor.right - mi.rcMonitor.left) / 2;
	yPos = (mi.rcMonitor.bottom - mi.rcMonitor.top) / 2;
	SetWindowPos(hwnd, HWND_TOP, xPos - 400, yPos - 300, 800, 600, SWP_NOZORDER | SWP_FRAMECHANGED);



	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update(); 
				display();
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	//void display(void);
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
			case 'l':	//For 'l' or 'L'
			case 'L':
			if (gbLighting == false)
			{
				gbLighting = true;
				glEnable(GL_LIGHTING);
			}
			else
			{
				gbLighting = false;
				glDisable(GL_LIGHTING);
			}
			break;
			case 'x':
			case 'X':
				KeyisPress = 1;
				angle_x_Light = 0.0f;
				break;

			case 'y':
			case 'Y':
				KeyisPress = 2;
				angle_y_Light = 0.0f;
				break;

			case 'z':
			case 'Z':
				KeyisPress = 3;
				angle_z_Light = 0.0f;
				break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);	//Dark gray background

	glClearDepth(1.0f);			//clear depth buffer
	glEnable(GL_DEPTH_TEST);	//enable the depth
	glDepthFunc(GL_LEQUAL);		//less than or equal to 1.0f in far. Uses ray tracing algorithm

	//Optional Calls
	glShadeModel(GL_SMOOTH);							//to remove aliasing
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	//to remove distortion

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

	//Lighting calls
	//LIGHT0:
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	//glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	//Enable LIGHT0
	glEnable(GL_LIGHT0);

	for (int i = 0; i <= 24; i++)
	{
		quadric[i] = gluNewQuadric();
	}

	//Create a quadric for the sphere
	//quadric = gluNewQuadric();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width <= height)
	{
		glOrtho(0.0f, 15.5f, 0, (10.0f *(GLfloat)height / width), -10.0f, 15.f);
	}
	else
	{
		glOrtho(0.0f, (-15.5f * (GLfloat)width / height), 0.0f, 15.5f, -10.0f, 15.f);
	}
}


void display(void)
{
	void draw24sphare(void);
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	draw24sphare();

	SwapBuffers(ghdc);
}

void draw24sphare(void)
{

	if (KeyisPress == 1)
	{
		glPushMatrix();
		glRotatef(angle_x_Light, 1.0f, 0.0f, 0.0f);		//Light rotating about X-axis
		light_position[2] = angle_x_Light;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		glPopMatrix();
	}
	else if (KeyisPress == 2)
	{
		glPushMatrix();
		glRotatef(angle_y_Light, 0.0f, 1.0f, 0.0f);		//Light rotating about Y-axis
		light_position[0] = angle_y_Light;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		glPopMatrix();
	}
	else if (KeyisPress == 3)
	{
		glPushMatrix();
		glRotatef(angle_z_Light, 0.0f, 0.0f, 1.0f);		//Light rotating about Z-axis
		light_position[1] = angle_z_Light;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		glPopMatrix();
	}

	GLfloat material_ambiant[4];
	GLfloat material_Diffuse[4];
	GLfloat material_Specular[4];
	GLfloat material_shininess[1];
	//COLUMN - 1
	//1st sphere on 1st column
	//Material:

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	material_ambiant[0] = 0.0215f;
	material_ambiant[1] = 0.1745f;
	material_ambiant[2] = 0.215f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.7568f;
	material_Diffuse[1] = 0.61424f;
	material_Diffuse[2] = 0.7568f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.633f;
	material_Specular[1] = 0.727811f;
	material_Specular[2] = 0.633f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.6 * 128.0f;

	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);


	glPushMatrix();
	glTranslatef(-7.5f, 12.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[0], 0.75f, 30, 30);
	glPopMatrix();

	//2nd sphere on 1st column
	//Material:
	material_ambiant[0] = 0.135f;
	material_ambiant[1] = 0.2225f;
	material_ambiant[2] = 0.1575f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.54f;
	material_Diffuse[1] = 0.89f;
	material_Diffuse[2] = 0.63f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.316228f;
	material_Specular[1] = 0.316228f;
	material_Specular[2] = 0.316228f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.1 * 128.0f;

	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);


	glPushMatrix();
	glTranslatef(-7.5f, 10.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[1], 0.75f, 30, 30);
	glPopMatrix();

	//3rd sphere on 1st column
	//Material:
	material_ambiant[0] = 0.05375f;
	material_ambiant[1] = 0.05f;
	material_ambiant[2] = 0.06625f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.18257f;
	material_Diffuse[1] = 0.17f;
	material_Diffuse[2] = 0.22525f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.332741f;
	material_Specular[1] = 0.328634f;
	material_Specular[2] = 0.346435f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.3 * 128.0f;

	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-7.5f, 8.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[2], 0.75f, 30, 30);
	glPopMatrix();

	//4th sphere on 1st column
	//Material:
	material_ambiant[0] = 0.25f;
	material_ambiant[1] = 0.20725f;
	material_ambiant[2] = 0.20725f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 1.0f;
	material_Diffuse[1] = 0.829f;
	material_Diffuse[2] = 0.829f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.296648f;
	material_Specular[1] = 0.296648f;
	material_Specular[2] = 0.296648f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.088 * 128.0f;

	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-7.5f, 6.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[3], 0.75f, 30, 30);
	glPopMatrix();

	//5th sphere on 1st column
	//Material:
	material_ambiant[0] = 0.1745f;
	material_ambiant[1] = 0.01175f;
	material_ambiant[2] = 0.01175f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.61424f;
	material_Diffuse[1] = 0.04136f;
	material_Diffuse[2] = 0.04136f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.727811f;
	material_Specular[1] = 0.626959f;
	material_Specular[2] = 0.626959f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.6 * 128.0f;

	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-7.5f, 4.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[4], 0.75f, 30, 30);
	glPopMatrix();

	//6th sphere on 1st column
	//Material:
	material_ambiant[0] = 0.1f;
	material_ambiant[1] = 0.18725;
	material_ambiant[2] = 0.1745f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.396f;
	material_Diffuse[1] = 0.74151f;
	material_Diffuse[2] = 0.69102f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.297254;
	material_Specular[1] = 0.30829f;
	material_Specular[2] = 0.306678f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.1 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-7.5f, 2.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[5], 0.75f, 30, 30);
	glPopMatrix();

	//COLUMN - 2
	//1st sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.329412f;
	material_ambiant[1] = 0.223529f;
	material_ambiant[2] = 0.027451f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.780392f;
	material_Diffuse[1] = 0.568627f;
	material_Diffuse[2] = 0.113725f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.992157f;
	material_Specular[1] = 0.941176f;
	material_Specular[2] = 0.807843f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.21794872 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 12.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[6], 0.75f, 30, 30);
	glPopMatrix();

	//2nd sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.2125f;
	material_ambiant[1] = 0.1275f;
	material_ambiant[2] = 0.054f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.714f;
	material_Diffuse[1] = 0.4284f;
	material_Diffuse[2] = 0.18144f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.393549f;
	material_Specular[1] = 0.271906f;
	material_Specular[2] = 0.166721f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.2 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 10.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[7], 0.75f, 30, 30);
	glPopMatrix();

	//3rd sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.25f;
	material_ambiant[1] = 0.25f;
	material_ambiant[2] = 0.25f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.4f;
	material_Diffuse[1] = 0.4f;
	material_Diffuse[2] = 0.4f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.774597f;
	material_Specular[1] = 0.774597f;
	material_Specular[2] = 0.774597f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.6 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 8.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[8], 0.75f, 30, 30);
	glPopMatrix();

	//4th sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.19125f;
	material_ambiant[1] = 0.0735f;
	material_ambiant[2] = 0.0225f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.7038f;
	material_Diffuse[1] = 0.27048f;
	material_Diffuse[2] = 0.0828f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.256777f;
	material_Specular[1] = 0.137622f;
	material_Specular[2] = 0.086014f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.1 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 6.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[9], 0.75f, 30, 30);
	glPopMatrix();

	//5th sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.24752f;
	material_ambiant[1] = 0.1995f;
	material_ambiant[2] = 0.0745f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.75164f;
	material_Diffuse[1] = 0.60648f;
	material_Diffuse[2] = 0.22648f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.628281f;
	material_Specular[1] = 0.555802f;
	material_Specular[2] = 0.366065f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.4 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 4.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[10], 0.75f, 30, 30);
	glPopMatrix();

	//6th sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.19225f;
	material_ambiant[1] = 0.19225f;
	material_ambiant[2] = 0.19225f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.50754f;
	material_Diffuse[1] = 0.50754f;
	material_Diffuse[2] = 0.50754f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.508273f;
	material_Specular[1] = 0.508273f;
	material_Specular[2] = 0.508273f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.4 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 2.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[11], 0.75f, 30, 30);
	glPopMatrix();

	//COLUMN - 3
	//1st sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.01f;
	material_Diffuse[1] = 0.01f;
	material_Diffuse[2] = 0.01f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.50f;
	material_Specular[1] = 0.50f;
	material_Specular[2] = 0.50f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 12.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[12], 0.75f, 30, 30);
	glPopMatrix();

	//2nd sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.1f;
	material_ambiant[2] = 0.06f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.0f;
	material_Diffuse[1] = 0.50980392f;
	material_Diffuse[2] = 0.50980392f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.50196078f;
	material_Specular[1] = 0.50196078f;
	material_Specular[2] = 0.50196078f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 10.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[13], 0.75f, 30, 30);
	glPopMatrix();

	//3rd sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.1f;
	material_Diffuse[1] = 0.35f;
	material_Diffuse[2] = 0.1f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.45f;
	material_Specular[1] = 0.55f;
	material_Specular[2] = 0.45f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 8.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[14], 0.75f, 30, 30);
	glPopMatrix();

	//4th sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.5f;
	material_Diffuse[1] = 0.0f;
	material_Diffuse[2] = 0.0f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.7f;
	material_Specular[1] = 0.6f;
	material_Specular[2] = 0.6f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 6.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[15], 0.75f, 30, 30);
	glPopMatrix();

	//5th sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.55f;
	material_Diffuse[1] = 0.55f;
	material_Diffuse[2] = 0.55f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.70f;
	material_Specular[1] = 0.70f;
	material_Specular[2] = 0.70f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 4.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[16], 0.75f, 30, 30);
	glPopMatrix();

	//6th sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.5f;
	material_Diffuse[1] = 0.5f;
	material_Diffuse[2] = 0.0f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.60f;
	material_Specular[1] = 0.60f;
	material_Specular[2] = 0.50f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 2.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[17], 0.75f, 30, 30);
	glPopMatrix();

	//COLUMN - 4
	//1st sphere on 4th column
	//Material:
	material_ambiant[0] = 0.02f;
	material_ambiant[1] = 0.02f;
	material_ambiant[2] = 0.02f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.01f;
	material_Diffuse[1] = 0.01f;
	material_Diffuse[2] = 0.01f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.4f;
	material_Specular[1] = 0.4f;
	material_Specular[2] = 0.4f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.078125 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 12.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[18], 0.75f, 30, 30);
	glPopMatrix();

	//2nd sphere on 4th column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.05f;
	material_ambiant[2] = 0.05f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.4f;
	material_Diffuse[1] = 0.5f;
	material_Diffuse[2] = 0.5f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.04f;
	material_Specular[1] = 0.7f;
	material_Specular[2] = 0.7f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.078125 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 10.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[19], 0.75f, 30, 30);
	glPopMatrix();

	//3rd sphere on 4th column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.05f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.4f;
	material_Diffuse[1] = 0.5f;
	material_Diffuse[2] = 0.4f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.04f;
	material_Specular[1] = 0.7f;
	material_Specular[2] = 0.04f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.1 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 8.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[20], 0.75f, 30, 30);
	glPopMatrix();

	//4th sphere on 4th column
	//Material:
	material_ambiant[0] = 0.05f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.5f;
	material_Diffuse[1] = 0.4f;
	material_Diffuse[2] = 0.4f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.7f;
	material_Specular[1] = 0.04f;
	material_Specular[2] = 0.04f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.078125 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 6.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[21], 0.75f, 30, 30);
	glPopMatrix();

	//5th sphere on 4th column
	//Material:
	material_ambiant[0] = 0.05f;
	material_ambiant[1] = 0.05f;
	material_ambiant[2] = 0.05f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.5f;
	material_Diffuse[1] = 0.5f;
	material_Diffuse[2] = 0.5f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.7f;
	material_Specular[1] = 0.7f;
	material_Specular[2] = 0.7f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.78125 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 4.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[22], 0.75f, 30, 30);
	glPopMatrix();

	//6th sphere on 4th column
	//Material:
	material_ambiant[0] = 0.05f;
	material_ambiant[1] = 0.05f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.5f;
	material_Diffuse[1] = 0.5f;
	material_Diffuse[2] = 0.4f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.7f;
	material_Specular[1] = 0.7f;
	material_Specular[2] = 0.04f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.1 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 2.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[23], 0.75f, 30, 30);
	glPopMatrix();

}

void uninitialize(void)
{
	//UNINITIALIZATION CODE

	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	for (int i = 0; i < 24; i++)
	{
		gluDeleteQuadric(quadric[i]);	//Delete the quadric
	}
}


void update(void)
{

	//code	
	//Rotate the light about x-axis
	angle_x_Light = ((angle_x_Light - 0.5) - 360);

	//Rotate the light about y-axis
	angle_y_Light = ((angle_y_Light - 0.5) - 360);
	
	//Rotate the light about z-axis
	angle_z_Light = ((angle_z_Light - 0.5) - 360);
	
}

﻿#include<windows.h>
#include <gl/GL.h>
#include<stdio.h>
#define WIN_WIDTH 800
#define WIN_HEIGHT 600


#pragma comment(lib,"opengl32.lib")


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HDC ghdc = NULL;
HGLRC ghrc = NULL;
HDC hdc;
FILE *gpFile = NULL;
HWND ghwnd=NULL;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;
bool gbActiveWindow = false;
bool BFullScreen = false;
void ToggleScreen(void);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) 
{
        //Function Declaration
        void Display(void);
        int intialize(int);
        int iRet = NULL;
        bool bDone = false;
        WNDCLASSEX wndclass;
        MSG msg;
        HWND hwnd;
        TCHAR szAppName[] = TEXT("MyApp");


        wndclass.cbSize = sizeof(WNDCLASSEX);
        wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
        wndclass.cbClsExtra = 0;
        wndclass.cbWndExtra = 0;
        wndclass.lpfnWndProc = WndProc;
        wndclass.hInstance = hInstance;
        wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
        wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
        wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
        wndclass.lpszClassName = szAppName;
        wndclass.lpszMenuName = NULL;
        wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);


        RegisterClassEx(&wndclass);


        hwnd = CreateWindowEx(WS_EX_APPWINDOW,
                szAppName,
                TEXT("My Application"),
                WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
                0,
                0,
                WIN_WIDTH,
                WIN_HEIGHT,
                NULL,
                NULL,
                hInstance,
                NULL);


        ghwnd =hwnd;


        iRet=intialize(iRet);


        if (fopen_s(&gpFile, "Aniket.txt", "w") != 0)
        {
                MessageBox(NULL, TEXT("The Created Aniket.txt File"), TEXT("Message"), MB_OK);
        }
        else


        {
                fprintf(gpFile, "File Is Created Sucessfully /n");
        }
        ShowWindow(hwnd, SW_SHOW);
        SetForegroundWindow(hwnd);
        SetFocus(hwnd);


        
        //Message Loop
        while (bDone == false)
        {
                if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
                {
                        if (msg.message == WM_QUIT)
                                bDone = true;
                        else
                        {
                                TranslateMessage(&msg);
                                DispatchMessage(&msg);
                        }
                }
                else
                {
                        if (gbActiveWindow == true)
                        {
                                //Hear Can Call update
                        }
                        //Hear Call Display
                        Display();
                }
        }
        return((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) 
{
        //int intialize(int);
        void Resize(int, int);
        void uninitialize(void);
        void ToggleScreen(void);
        switch (iMsg)
        {
        case WM_SETFOCUS:
                gbActiveWindow = true;
                break;
        case WM_KILLFOCUS:
                gbActiveWindow = false;
                break;
        case WM_SIZE:
                Resize(LOWORD(lParam), HIWORD(lParam));
                break;
        case WM_ERASEBKGND:
                return(0);
        case WM_CLOSE:
                DestroyWindow(hwnd);
                break;     
        case WM_KEYDOWN:
                switch (wParam)
                {
                case VK_ESCAPE:
                        DestroyWindow(hwnd);
                        break;
                case 0x46: //for 'f' or 'F'
                        ToggleScreen();
                        break;
                default:
                        break;
                }
                break;
        case WM_DESTROY:
                uninitialize();
                PostQuitMessage(0);
                break;
        default:
                break;
        }
        return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleScreen(void)
{
        MONITORINFO mi;
        if (BFullScreen == FALSE)
        {
                mi = { sizeof(MONITORINFO) };
                if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY),&mi))
                {
                        SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
                        SetWindowPos(ghwnd,
                                HWND_TOP,
                                mi.rcMonitor.left,
                                mi.rcMonitor.top,
                                mi.rcMonitor.right - mi.rcMonitor.left,
                                mi.rcMonitor.bottom - mi.rcMonitor.top,
                                SWP_NOZORDER | SWP_FRAMECHANGED);
                }
                ShowCursor(FALSE);
                BFullScreen = true;
        }
        else
        {
                SetWindowLong(ghwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);
                SetWindowPlacement(ghwnd, &wpPrev);
                SetWindowPos(ghwnd,
                        HWND_TOP,
                        0,
                        0,
                        0,
                        0,
                        SWP_NOZORDER | 
                        SWP_FRAMECHANGED |
                        SWP_NOMOVE | 
                        SWP_NOSIZE | 
                        SWP_NOOWNERZORDER);
                ShowCursor(TRUE);
                BFullScreen = false;
        }
}


int intialize(int iRet)
{
        //code
        void Resize(int, int);


        //variable declarations
        PIXELFORMATDESCRIPTOR pfd;
        int iPixelFormatIndex;


        //code


        memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
        //Initialization of structure 'PIXELFORMATDESCRIPTOR'
        pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
        pfd.nVersion = 1;
        pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
        pfd.iPixelType = PFD_TYPE_RGBA;
        pfd.cColorBits = 32;
        pfd.cRedBits = 8;
        pfd.cGreenBits = 8;
        pfd.cBlueBits = 8;
        pfd.cAlphaBits = 8;


        ghdc = GetDC(ghwnd);


        if (iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd) == 0)
        {
                return(-1);
        }


        if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
        {
                return (-2);
        }


        ghrc = wglCreateContext(ghdc);
        if (ghrc == NULL)
        {
                return(-3);
        }


        if (wglMakeCurrent(ghdc, ghrc) == FALSE)
        {
                return(-4);
        }
        else if (iRet == -1)
        {
        fprintf(gpFile, "ChosePixelFormat Failed /n");
        DestroyWindow(ghwnd);
        }
        else if (iRet == -2)
        {
        fprintf(gpFile, "SetixelFormat Failed /n");
        DestroyWindow(ghwnd);
        }
        else if (iRet == -3)
        {
        fprintf(gpFile, "wglcreateContext Failed /n");
        DestroyWindow(ghwnd);
        }
        else if (iRet == -4)
        {
        fprintf(gpFile, "wglMakeCurrent Failed /n");
        DestroyWindow(ghwnd);
        }
        else
        {
        fprintf(gpFile, "All File Arr Sucessfull");
        }
        glClearColor(0.0f, 1.0f, 1.0f, 0.0f);
        // worm up ro call Resize WinWidth And winHight
        Resize(WIN_WIDTH, WIN_HEIGHT);
        return iRet;
}
void Resize(int width, int height)
{
        //code
        glViewport(0, 0, (GLsizei)width, (GLsizei)height);


}
void Display(void)
{
        //code
        glClear(GL_COLOR_BUFFER_BIT);


        SwapBuffers(ghdc);
}
void uninitialize(void)
{
        //UNINITIALIZATION CODE


        if (BFullScreen == true)
        {
                dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
                SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
                SetWindowPlacement(ghwnd, &wpPrev);
                SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);


                ShowCursor(TRUE);


        }


        if (wglGetCurrentContext() == ghrc)
        {
                wglMakeCurrent(NULL, NULL);
        }
        if (ghrc)
        {
                wglDeleteContext(ghrc);
                ghrc = NULL;
        }


        if (ghdc)
        {
                ReleaseDC(ghwnd, hdc);
                ghdc = NULL;
        }
        if (gpFile)
        {
                fprintf(gpFile, "File Is Close Secessfull /n");
                fclose(gpFile);
        }
}
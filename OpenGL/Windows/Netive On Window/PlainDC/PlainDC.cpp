#include<windows.h>
bool BFullScreen = false;
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
HWND ghwnd = NULL;
WINDOWPLACEMENT wpprev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;
void ToggleScreen(void);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {

	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);
	hwnd = CreateWindow(szAppName,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW,
		WS_OVERLAPPED,
		WS_OVERLAPPED,
		WS_OVERLAPPED,
		WS_OVERLAPPED,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0)) {

		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	RECT rc;
	HDC hdc;
	TCHAR str[] = TEXT("Hellow World!!!");
	void ToggleScreen(void);
	switch (iMsg)
	{
	case WM_CREATE:
		GetClientRect(hwnd, &rc);
		hdc = GetDC(hwnd);
		SetBkColor(hdc, RGB(0, 255, 0));
		DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		ReleaseDC(hwnd, hdc);
		break;
	case WM_CHAR:
	{
		switch (LOWORD(wParam))
		{
		case 'f':
			ToggleScreen();
			break;
		case 'l':
			GetClientRect(hwnd, &rc);
			hdc = GetDC(hwnd);
			SetTextColor(hdc, RGB(0, 255, 0));
			SetBkColor(hdc, RGB(255, 0, 0));
			SetBkMode(hdc, TRANSPARENT);
			DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
			ReleaseDC(hwnd, hdc);
			break;
		}
	}
	break;
	/*case WM_PAINT:
		PAINTSTRUCT ps;
		GetClientRect(hwnd, &rc);
		BeginPaint(hwnd, &ps);
		hdc = GetDC(hwnd);
		SetBkColor(hdc, RGB(1, 0, 0));
		DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		ReleaseDC(hwnd, hdc);
		EndPaint(hwnd, &ps);
		break;*/  
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleScreen(void)
{
	MONITORINFO mi;
	if (BFullScreen == FALSE)
	{
		mi = { sizeof(MONITORINFO) };
		if (GetWindowPlacement(ghwnd, &wpprev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(ghwnd,
				HWND_TOP,
				mi.rcMonitor.left,
				mi.rcMonitor.top,
				mi.rcMonitor.right - mi.rcMonitor.left,
				mi.rcMonitor.bottom - mi.rcMonitor.top,
				SWP_NOZORDER | SWP_FRAMECHANGED);
		}
		ShowCursor(FALSE);
		BFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpprev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER |
			SWP_FRAMECHANGED |
			SWP_NOMOVE |
			SWP_NOSIZE |
			SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		BFullScreen = false;
	}
}



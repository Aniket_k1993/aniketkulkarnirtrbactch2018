#include<Windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<stdio.h>
#include<windowsx.h>
#include "resource.h"
#define _USE_MATH_DEFINES
#include<math.h>
//#define PI 3.1415926535898
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"Winmm.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT  600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
GLfloat x_For_fi = -3.0, y_For_fa = 5.0, a_For_fn = -7.0, b_For_si = 10.0, r = 0.0f, s = 0.0f, t = 0.0f, i = 0.0f, d = 0.0f, k = 0.0f, f = 12;
GLfloat m = -13.0f, n = -14.5f;
//GLfloat v_For_Airoplain = -13.0, z_For_Airoplain = -14.5 /*, m = -13.0, n = -14.5, o = -2.5f, p = -15.2f*/;
//GLfloat v = -3.0, z = -4.5, v_For_Bottom = 0.0f, z_For_Bottom = 0.0f, z_For_xMove = -2.f;
//GLfloat v_For_Bottom = 0.0f, z_For_Bottom = 0.0f;
GLfloat v = 0.0f, y = 0.0f, a = -2.0f;
GLfloat angle_For_Top = M_PI;
GLfloat v_For_Bottom = 0.0f, y_For_Bottom = 0.0f, a_For_Sec_Mov = -2.0f;
GLfloat angle_For_Bottom = M_PI;
GLfloat angle = 45.0f;
GLfloat angle_exit = 0.0f;

long int Ver = 1;
GLfloat orange = -3.5f;
GLfloat white = -3.5f;
GLfloat green = -3.5f;
GLfloat D_fade = 0.0f;
GLfloat Red = 0.0f;
GLfloat Green1 = 0.0f;
GLfloat Green2 = 0.0f;
GLfloat Black = 1.0f;
GLfloat Black1 = 0.5f;
GLfloat Black2 = 0.0f;
GLfloat orange_for_tiranga = 0.0f;
GLfloat white_for_tiranga = 0.0f;
GLfloat green_for_tiranga = 0.0f;
GLfloat x_move_tiranga = 0.0f;

GLfloat tricolor_movement = -3.0f;
//GLfloat angle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool bFullScreen = false;
DWORD dwStyle;
void airoplain(void);
void tiranga(void);
void First_I(void);
void For_N(void);
void For_D(void);
void Second_I(void);
void For_A(void);

HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
FILE *gpFile = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpzsCmdLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("DoubleBuffer OGL App");

	bool bDone = false;
	int iRet = 0;
	int iRe = 0;
	int initialize(int);
	void uninitialize(void);
	void display(void);
	void update(void);
	void tiranga(void);
	if (fopen_s(&gpFile, "Aniket.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File can not be created...\n"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File created successfully \n");
	}

	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("DoubleBuffer Windowing OpenGL"),
		WS_OVERLAPPEDWINDOW | CDS_FULLSCREEN | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_VISIBLE,
		100, 100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize(iRe);



	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//call update here ..
			}
			update();
			display();
	
		}
	}

	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void toggleFullScreen(void);
	void resize(int, int);


	//code
	switch (iMsg)
	{
		//case WM_CREATE:
		//	toggleFullScreen();
		//	break;
	case WM_CREATE:
		//Playing National Anthem Song on window creation
		PlaySound(MAKEINTRESOURCE(IDR_WAVE1), NULL, SND_RESOURCE| SND_ASYNC);
		//exit(0);
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			toggleFullScreen();
			break;
		}
		break;
	case WM_DESTROY:
		//MessageBox(hwnd, TEXT("By Darling!!!!"), TEXT("WM DESTROY"), MB_OK);
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void toggleFullScreen(void)
{
	MONITORINFO MI;
	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			MI = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev)
				&& GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &MI))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);


				SetWindowPos(ghwnd,
					HWND_TOPMOST,
					MI.rcMonitor.left,
					MI.rcMonitor.top,
					MI.rcMonitor.right - MI.rcMonitor.left,
					MI.rcMonitor.bottom - MI.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(false);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle & WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0, 0, 0, 0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(true);
		bFullScreen = false;
	}
}

int initialize(int iRe)
{
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	if (iRe == -1)
	{
		fprintf(gpFile, "Failed -ChoosePixelFormat");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "Success -ChoosePixelFormat");
	}
	if (iRe == -2)
	{
		fprintf(gpFile, "Failed -SetPixelFormat");
		DestroyWindow(ghwnd);
	}
	else if (iRe == -3)
	{
		fprintf(gpFile, "Failed -wglCreateContext");
		DestroyWindow(ghwnd);
	}
	else if (iRe == -4)
	{
		fprintf(gpFile, "Failed -wglMakeCurrent");
		DestroyWindow(ghwnd);
	}

	else
	{
		fprintf(gpFile, "Initialization is Successful \n");
	}

	//toggleFullScreen();
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	resize(WIN_WIDTH, WIN_HEIGHT);
	toggleFullScreen();
	return(iRe);


}

void uninitialize()
{
	SetWindowLong(ghwnd, GWL_STYLE, dwStyle & WS_OVERLAPPEDWINDOW);
	SetWindowPlacement(ghwnd, &wpPrev);
	SetWindowPos(ghwnd,
		HWND_TOP,
		0, 0, 0, 0,
		SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log Close Successfully");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void resize(int width, int height)
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	//float angle;
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if(Ver >= 1)
	{ 
		glTranslatef(x_For_fi, 0.0f, -4.2f);
		First_I();
		if (x_For_fi <= -1.5f)
			Ver = 2;
	}
	////////////// FOR A
	if (Ver >= 2)
	{ 
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(y_For_fa, 0.0f, -4.2f);
		For_A();
		if (y_For_fa >= 1.5f)
			Ver = 3;
	}
	////////////// for N 
	if(Ver >= 3)
	{ 
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-1.2f, -a_For_fn, -4.2f);
		For_N();
		if (a_For_fn <= 0.0f)
			Ver = 4;
	}
	/////////////// FOR I
	if(Ver >= 4)
	{ 
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.02f, -b_For_si, -4.2f);
		Second_I();
		if (b_For_si >= 0.0f)
			Ver = 5;
	}
	///////////// FOR D
	if(Ver >= 5)
	{ 
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.2f, 0.0f, -4.2f);
		For_D();
		if (Red >= 1.0f)	//To initiate next animation of I after fade in effect sets in
			Ver = 6;
	}
	///////////////////Tirang and Airoplain
	//if (vary >= 6)
	//{
	if (Ver >= 6)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPointSize(7.0f);
		glTranslatef(-3.0f, 4.5f, -3.0f);
		if (angle_For_Top < (3 * (M_PI / 2)))
		{
			v = 4.5f * (cos(angle_For_Top));
			y = 4.5f * (sin(angle_For_Top));
			glTranslatef(v, y, -6.0f);
			glRotatef(angle, 0.0f, 0.0f, -1.0f);
			airoplain();
			tiranga();

		}

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPointSize(7.0f);
		glTranslatef(-3.0f, -4.5f, -3.0f);
		if (angle_For_Top < (3 * (M_PI / 2)))
		{
			v = 4.5f * (cos(angle_For_Top));
			y = 4.5f * (sin(-angle_For_Top));
			glTranslatef(v, y, -6.0f);
			glRotatef(angle, 0.0f, 0.0f, 1.0f);
			airoplain();
			tiranga();
		}

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPointSize(7.0f);
		glTranslatef(-3.0f, -0.0f, -3.0f);
		if (v <= 10.0f)
		{
			glTranslatef(v, 0.0f, -6.0f);
			airoplain();
		}
		tiranga();
		//if (Black <= 0.0f)
		//time = 7;
		if (v >= 7.7)
		{
			Ver = 7;
		}
	}
	
	if (Ver >= 7)
	{
		/////////////////// For Right Top


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPointSize(7.0f);
		glTranslatef(6.7f, 0.0f, -3.0f);
		if (angle_For_Bottom < (3 * (M_PI / 2)))
		{
			v_For_Bottom = 4.0f * (cos(angle_For_Bottom) / 2);
			y_For_Bottom = 4.0f * (-sin(angle_For_Bottom) / 2);
			//glRotatef(angle, v_For_Bottom, y_For_Bottom, 0.0f);
			glTranslatef(v_For_Bottom, y_For_Bottom, -6.0f);
			glRotatef(angle_exit, 0.0f, 0.0f, 1.0f);
			airoplain();
			//tiranga();
			//tiranga();
		}
		//glTranslatef(v_For_Bottom, y_For_Bottom, -6.0f);


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPointSize(7.0f);
		glTranslatef(6.7f, 0.0f, -3.0f);
		if (angle_For_Bottom < (3 * (M_PI / 2)))
		{
			v_For_Bottom = 4.0f * (cos(angle_For_Bottom)/2);
			y_For_Bottom = 4.0f * (sin(angle_For_Bottom)/2);
			//glRotatef(angle, v_For_Bottom, y_For_Bottom, 0.0f);
			glTranslatef(v_For_Bottom, y_For_Bottom, -6.0f);
			glRotatef(angle_exit, 0.0f, 0.0f, -1.0f);
			airoplain();
			//tiranga();
		}
		if (angle_exit <= 45.0f)
			Ver = 8;
	}
	SwapBuffers(ghdc);
}

void update(void)
{
////////  MOVIG FIRST I	
	if (Ver >= 1 && x_For_fi <= -1.5)
	{
		x_For_fi = x_For_fi + 0.0002f;
	}
//////// FOR MOVING A
	if (Ver >= 2 && y_For_fa >= 1.5f)
	{
		y_For_fa = y_For_fa - 0.0002f;
	}
/////// FOR MOVING 
	if (Ver >= 3 && a_For_fn <= 0.0f)
	{
		a_For_fn = a_For_fn + 0.0002f;
	}
////// FOR MOVING SECOND I
	if (Ver >= 4 && b_For_si >= 0.0f)
	{
		b_For_si = b_For_si - 0.0002f;

	}
	else
	{ 
		if (time >= 5)						//For animation of D
		{
			if (Red <= 1.0f)		//For Orange fade in
				Red = Red + 0.0007f;
			if (Green1 <= 1.0f)		//For Green fade in
				Green1 = Green1 + 0.0007f;
			if (Green2 <= 0.5f)		//For Orange fade in
				Green2 = Green2 + 0.0007f;
		}
	}
	
	/////////////// FOR AiroPlain

	if (Ver >= 6)
	{
		if (angle >= 0.0f)
		{
			angle = angle - 0.007f;
		}


		angle_For_Top = angle_For_Top + 0.0002;
		//v = v + 0.0002f;
		if (v <= 0.0)
		{
			v = v + 0.0005;
			//airoplain();
		}
		else if (v <= 10.7f)
		{
			v = v + 0.0005;
		}
		//if (angle_For_Bottom <= 45)
		//{
		//	if (Black <= 1.0f)
		//		Black = 1.0f;
		//	if (Black1 <= 0.5f)
		//		Black1 = 0.0f;
		//	if (Black2 <= 1.0f)
		//		Black2 = 1.0f;
		//}
		//else
		//{
		//	if (Black >= 0.0f)
		//		Black = Black - 0.0007f;
		//	if (Black1 >= 0.0f)
		//		Black1 = Black1 - 0.0007f;
		//	if (Black2 >= 0.0f)
		//		Black2 = Black2 - 0.0007f;
		//}
		if (tricolor_movement <= -2.175f)
			tricolor_movement = tricolor_movement + 0.0005f;
		if (tricolor_movement >= 1.175f)
		{
			if (orange <= -0.35f)
				orange = orange + 0.0007f;
			if (white <= -0.435f)
				white = white + 0.0007f;
			if (green <= -0.36f)
				green = green + 0.0007f;
		}

	}
	//GLfloat v = 0.0f, y = 0.0f, a = -2.0f;
	//GLfloat angle_For_Top = M_PI;
	if (Ver >= 7)
	{
		angle_For_Bottom = angle_For_Bottom + 0.0002;
		if (angle_exit <= 45.0f)
		{
			angle_exit = angle_exit + 0.007f;
		}
	}
	if (Ver >= 8)
	{
		if (Black >= 0.0f)
			Black = Black - 0.0007f;
		if (Black1 >= 0.0f)
			Black1 = Black1 - 0.0007f;
		if (Black2 >= 0.0f)
			Black2 = Black2 - 0.0007f;

		if (orange_for_tiranga <= 1.0f)
		{
			orange_for_tiranga = orange_for_tiranga + 0.0007f;
		}
		if (white_for_tiranga <= 0.5f)
		{
			white_for_tiranga = white_for_tiranga + 0.0007f;
		}
		if (x_move_tiranga <= 15.0f)
		{
			x_move_tiranga = x_move_tiranga + 0.005f;
		}
	}


}

void airoplain(void)
{
	//glTranslatef(0.0f, 0.0f, -4.4f);
	glColor3f(0.128f, 0.226f, 0.238f);
	glBegin(GL_TRIANGLES);
	glVertex3f(0.6f, 0.0f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.1f, -0.1f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(-0.3f, -0.1f, 0.0f);
	glVertex3f(0.1f, -0.1f, 0.0f);
	glEnd();
	////////////// UP PAN
	glBegin(GL_TRIANGLES);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(-0.2f, 0.3f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glEnd();
	////////////// DOWN PAN
	glBegin(GL_TRIANGLES);
	glVertex3f(0.1f, -0.0f, 0.0f);
	glVertex3f(-0.2f, -0.3f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glEnd();
	///////// BACK PANAL
	glBegin(GL_TRIANGLES);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glVertex3f(-0.4f, -0.2f, 0.0f);
	glEnd();

	//////////   IAF
	///////////////FOR I
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(-0.3f, -0.1f, 0.0f);
	glEnd();

	glBegin(GL_LINE_STRIP);
	glVertex3f(-0.2f, -0.1f, 0.0f);
	glVertex3f(-0.1f, 0.1f, 0.0f);
	glVertex3f(0.0f, -0.1f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.1f, -0.1f, 0.0f);
	glVertex3f(0.1f, 0.08f, 0.0f);
	glVertex3f(0.2f, 0.08f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();
	
}
//
void tiranga(void)
{
	glLineWidth(5.0f);
	glBegin(GL_LINES);

	if (tricolor_movement >= 1.375f)
	{
		//
		//Center Line of A
		//Orange line
		//glColor3f(1.0f, 0.5f, 0.0f);		//Orange color
		glColor3f(Black, Black1, 0.0f);		//Orange color
											//glVertex3f(0.528f, 0.02f, 0.0f);	//Original co-ordinates
											//glVertex3f(0.974, 0.02f, 0.0f);
		glVertex3f(orange, 0.05f, 0.0f);
		glVertex3f(-1.7f, 0.05f, 0.0f);

		//White line
		//glColor3f(1.0f, 1.0f, 1.0f);		//White
		glColor3f(Black, Black, Black);		//Orange color
											//glVertex3f(0.52f, 0.0f, 0.0f);	//Original co-ordinates
											//glVertex3f(0.98f, 0.0f, 0.0f);
		glVertex3f(white, 0.02f, 0.0f);
		glVertex3f(-1.7f, 0.02f, 0.0f);

		//Green line
		//glColor3f(0.0f, 1.0f, 0.0f);		//Green color
		glColor3f(0.0f, Black, 0.0f);		//Orange color
											//glVertex3f(0.513f, -0.02f, 0.0f);	//Original Co-ordinates
											//glVertex3f(0.985f, -0.02f, 0.0f);
		glVertex3f(green, -0.01f, 0.0f);
		glVertex3f(-1.7f, -0.01f, 0.0f);
	}
	else
	{
		//Center Line of A
		//Orange line
		//glColor3f(1.0f, 0.5f, 0.0f);		//Orange color
		glColor3f(Black, Black1, 0.0f);
		//glVertex3f(0.528f, 0.02f, 0.0f);	//Original co-ordinates
		//glVertex3f(0.974, 0.02f, 0.0f);
		glVertex3f(-9.5f, 0.05f, 0.0f);
		glVertex3f(-0.4f, 0.05f, 0.0f);

		//White line
		//glColor3f(1.0f, 1.0f, 1.0f);		//White
		glColor3f(Black, Black, Black);		//White
											//glVertex3f(0.52f, 0.0f, 0.0f);	//Original co-ordinates
											//glVertex3f(0.98f, 0.0f, 0.0f);
		glVertex3f(-9.5f, 0.02f, 0.0f);
		glVertex3f(-0.4f, 0.02f, 0.0f);

		//Green line
		//glColor3f(0.0f, 1.0f, 0.0f);		//Green color
		glColor3f(0.0f, Black, 0.0f);		//Green color
											//glVertex3f(0.513f, -0.02f, 0.0f);	//Original Co-ordinates
											//glVertex3f(0.985f, -0.02f, 0.0f);
		glVertex3f(-9.5f, -0.01f, 0.0f);
		glVertex3f(-0.4f, -0.01f, 0.0f);
	}
	glEnd();

}

void First_I(void)
{
	///////////////// FOR I  
	glLineWidth(5.5);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.5f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.5f, -1.0f, 0.0f);
	glEnd();

}

void For_N(void)
{
	glLineWidth(5.5);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.3f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.3f, -1.0f, 0.0f);
	
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.3f, -1.0f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.3f, 1.0f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.3f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.3f, -1.0f, 0.0f);

	glEnd();

}

void For_D(void)
{
	glLineWidth(5.5);
	glBegin(GL_LINES);
	glColor3f(Red, Green2, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(0.4f, 1.0f, 0.0f);
	glColor3f(Red, Green2, 0.0f);
	glVertex3f(0.4f, 1.0f, 0.0f);
	glColor3f(0.0f, Green1, 0.0f);
	glVertex3f(0.4f, -1.0f, 0.0f);
	glColor3f(0.0f, Green1, 0.0f);
	glVertex3f(0.4f, -1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glColor3f(0.0f, Green1, 0.0f);
	glVertex3f(-0.3f, -1.0f, 0.0f);
	glColor3f(Red, Green2, 0.0f);
	glVertex3f(-0.3f, 1.0f, 0.0f);
	glEnd();
}

void Second_I(void)
{
	glLineWidth(5.5);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.5f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.5f, -1.0f, 0.0f);
	glEnd();
}

void For_A(void)
{
	glLineWidth(5.5);
	glBegin(GL_LINE_STRIP);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.5f, -1.0f, 0.0f);
	glEnd();


	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(orange_for_tiranga, white_for_tiranga, green_for_tiranga);		//Orange color
	glVertex3f(-0.22, 0.05f, 0.0f);
	glVertex3f(0.22, 0.05f, 0.0f);

	glColor3f(orange_for_tiranga, orange_for_tiranga, orange_for_tiranga);		//White
	glVertex3f(-0.22, 0.02f, 0.0f);
	glVertex3f(0.22, 0.02f, 0.0f);

	glColor3f(0.0f, orange_for_tiranga, 0.0f);		//Green color
	glVertex3f(-0.22, 0.01f, 0.0f);
	glVertex3f(0.22, 0.01f, 0.0f);
	glEnd();

}





/*
		//if (v <= 0.0)
		//{
		//	v = v + 0.0002;
		//}
		//if (z <= 0.0)
		//{
		//	z = z + 0.0002;
		//}
		//else if (z <= 6.5)
		//{
		//	z = z + 0.0002;
		//}

		//angle_For_Top = angle_For_Top + 0.0007f;
		//if (z_For_xMove <= 0.0f)
		//{
		//	z_For_xMove = z_For_xMove + 0.0006f;
		//}
		//else if (z_For_xMove <= 6.5f)
		//{
		//	z_For_xMove = z_For_xMove + 0.0006f;
		//}*/


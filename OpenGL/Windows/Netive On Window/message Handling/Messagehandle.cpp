#include<windows.h>
bool BFullScreen = false;
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {

	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);
	hwnd = CreateWindow(szAppName,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW,
		WS_OVERLAPPED,
		WS_OVERLAPPED,
		WS_OVERLAPPED,
		WS_OVERLAPPED,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);
	
	while (GetMessage(&msg, NULL, 0, 0)) 
	{

		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) 
{
	void ToggleScreen(void);
	switch (iMsg)
	{
	case WM_CREATE:
		MessageBox (hwnd, TEXT("WM_CREATE message is sent"), TEXT("Messages"),
			MB_OK|MB_ICONEXCLAMATION|MB_TOPMOST);
		break;
	case WM_MOVE:
		MessageBox (hwnd, TEXT("WM_MOVE message is sent"), TEXT("Messages"),
			MB_OK|MB_ICONINFORMATION|MB_TOPMOST);
		break;
	case WM_SIZE:
		MessageBox(hwnd, TEXT("WM_KEYDOWN message is sent"), TEXT("Messages"),
			MB_OK|MB_ICONASTERISK|MB_TOPMOST);
		break;
	case WM_KEYDOWN:
		{
		MessageBox(hwnd, TEXT("WM_KEYDOWN message is sent"), TEXT("Messages"), MB_OK);
		}
		break;
case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


#include <windows.h>
#include <gl/GL.h>
#include <gl/Glu.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#define WIN_WIDTH 			800
#define WIN_HEIGHT 			800
#define TRUE 				1
#define FALSE				0

#define BUFFER_SIZE			256
#define S_EQUAL				0

#define WIN_INIT_X			100
#define WIN_INIT_y			100


#define NR_POINT_COORDS			3
#define NR_TEXTURE_COORDS		3
#define MIN_NR_FACE_TOKENS		3
#define MAX_NR_FACE_TOKENS		4

#define FOY_ANGLE 			45
#define ZNEAR				0.1
#define ZFAR				200.0

#define VIEWPORT_BOTTOMLEFT_X 		0
#define VIEWPORT_BOTTOMLEFT_Y		0

#define TEAPOT_X_TRANSLATE		0.0f
#define TEAPOT_Y_TRANSLATE	       	-25.0f
#define TEAPOT_Z_TRANSLATE		-125.0f

#define TEAPOT_X_SCALE_FACTOR		0.75f
#define TEAPOT_Y_SCALE_FACTOR		0.75f
#define TEAPOT_Z_SCALE_FACTOR		0.75f

#define START_ANGLE_POS			0.0f
#define END_ANGLE_POS			360.0f
#define TEAPOT_ANGLE_INCREMENT	0.01f



#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"kernel32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"user32.lib")
#pragma comment(lib,"gdi32.lib")

bool bLight = false;
GLfloat LightAmbient[] = { 0.1f, 0.1f, 0.1f, 1.0f };
GLfloat LightDiffuse[] = { 0.6f, 0.6f, 0.6f, 1.0f };
GLfloat LightPosition[] = {0.0f,2.0f,1.0f,0.0f};
GLfloat LightSpecular[] = { 0.7f, 0.7f, 0.3f, 1.0f };
GLfloat matarialSpecular[] ={1.0f,1.0f,1.0f,1.0f};  
GLfloat matarialShineness[] ={50.0f,50.0f,50.0f,50.0f};
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLfloat g_rotate;


void mydraw(void);

std::vector<std::vector<float>> g_vertices;

std::vector<std::vector<float>> g_texture;

std::vector<std::vector<float>> g_normals;

std::vector<std::vector<int>> g_face_tri, g_face_texture, g_face_normals;

FILE *g_fp_meshfile = NULL;
FILE *g_fp_logfile = NULL;
char line[BUFFER_SIZE];



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	void initialize(void);
	void uninitialize(void);
	void update(void);
	void uninitialize(void);		
	void display(void);
	WNDCLASSEX wndclass;
	HWND hWnd = NULL;
	MSG msg;
	HBRUSH hBrush = NULL;
	HCURSOR hCursor = NULL;
	HICON hIcon = NULL;
	HICON hIconSm = NULL;
	static TCHAR szClassName[] = TEXT("Mesh loading version 2");
	
	bool bDone = false;
	hBrush = (HBRUSH)GetStockObject(BLACK_BRUSH);
	
	hCursor = LoadCursor((HINSTANCE)NULL, IDC_ARROW);
	
	hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	
	hIconSm = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW |CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hbrBackground = hBrush;
	wndclass.hCursor = hCursor;
	wndclass.hIcon 	= hIcon;
	wndclass.hIconSm = hIconSm;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,szClassName, TEXT("OpenGL Fixed Function Pipeline using Native Windowing: First Window"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, 0, 0, WIN_WIDTH, WIN_HEIGHT, NULL, NULL, hInstance, NULL );
	if(!hWnd)
		MessageBox(NULL,TEXT("NO_HWND"),TEXT("MAG"),MB_OK);
	ghwnd = hWnd;
	
	initialize();
	ShowWindow(hWnd,SW_SHOW);
	void display(void);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	while(bDone == false)
	{
		if(PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ))
		{
			if( msg.message == WM_QUIT )
				bDone = true ;
			else
			{
				TranslateMessage ( &msg ) ;
				DispatchMessage ( &msg ) ;
			}
		}
		else
		{
			if( gbActiveWindow == true )
			{
				if( gbEscapeKeyIsPressed == true )
					bDone = true;
				else
				{
					update();
					display();
				}
			}
		}
	}

	uninitialize();
	return ((int)msg.wParam);
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize( int, int );
	void ToggleFullscreen(void);
	void uninitialize(void);

	switch(iMsg)
	{
		case WM_ACTIVATE:
			if(HIWORD(wParam) == 0 )
				gbActiveWindow = true;
			else
				gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return(0);

		case WM_SIZE:
			resize(LOWORD(lParam), HIWORD(lParam));
			break;
		case WM_KEYDOWN:
			switch(wParam)
			{
				case VK_ESCAPE:
					if(gbEscapeKeyIsPressed == false)
						gbEscapeKeyIsPressed = true;
					break;
				case 'l':
				case 'L':
					if(bLight == false)
					{
						bLight = true;
						glEnable(GL_LIGHTING);
					}
					else
					{
						bLight = false;
						glDisable(GL_LIGHTING);
					}
					break;
				case 0x46:  //for 'f' or 'F'
					if(gbFullscreen == false)
					{
						ToggleFullscreen();
						gbFullscreen = true;
					}
					else
					{
						ToggleFullscreen();
						gbFullscreen = false;
					}
					break;
				default:
					break;
			}
			break;
		case WM_LBUTTONDOWN:
			break;
		case WM_PAINT:
			break;
		case WM_CLOSE:
			uninitialize();
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
void ToggleFullscreen(void)
{
	MONITORINFO mi;
	if(gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if(dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO)};
			if(GetWindowPlacement(ghwnd,&wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong (ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos( ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED );

		ShowCursor(TRUE);
	}
}
void initialize(void)
{
	void resize(int, int);
	void LoadMeshData(void);
	void uninitialize(void);

	g_fp_logfile = fopen("MONKEYHEADLOADER.LOG","w");
	if(!g_fp_logfile)
		uninitialize();

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = -1;

	ZeroMemory((void*)&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	
	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if(iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}	
	if(SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) ==FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	ghrc = wglCreateContext(ghdc);
	if(ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if( wglMakeCurrent(ghdc, ghrc) == FALSE )
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
	//glEnable(GL_TEXTURE_2D);
	LoadMeshData();
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,LightAmbient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,LightDiffuse);
	//glLightfv(GL_LIGHT0,GL_POSITION,LightPosition);
	glLightfv(GL_LIGHT0,GL_AMBIENT,LightSpecular);
	//glMaterialfv(GL_FRONT,GL_SPECULAR,matarialSpecular);
	//glMaterialfv(GL_FRONT,GL_SHININESS,matarialShineness);
	glEnable(GL_LIGHT0);
	glEnable( GL_COLOR_MATERIAL );
	glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE );
	resize( WIN_WIDTH, WIN_HEIGHT );
}

void LoadMeshData(void)
{
	void uninitialize(void);
	
	g_fp_meshfile = fopen("TEAPOT_852.obj" , "r");
	if(!g_fp_meshfile)
		uninitialize();

	char *sep_space = " ";

	char *sep_fslash = "/";

	char *first_token = NULL;

	char *token = NULL;


	char *face_token[MAX_NR_FACE_TOKENS];
	
	int nr_tokens;

	char *token_vertex_index = NULL;
	
	char *token_texture_index = NULL;
	
	//char *token_normal_index = NULL;


	while((fgets(line, BUFFER_SIZE, g_fp_meshfile) != NULL) && !feof(g_fp_meshfile))
	{
		first_token = strtok(line, sep_space);

		if(strcmp(first_token, "v") == S_EQUAL)
		{
			std::vector<float> vec_point_coord(NR_POINT_COORDS);

			for(int i = 0; i !=NR_POINT_COORDS; i++)
				vec_point_coord[i] = atof(strtok(NULL,sep_space));
			g_vertices.push_back(vec_point_coord);
		}
		else if(strcmp(first_token, "vt") == S_EQUAL)
		{
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);

			for(int i = 0; i != NR_TEXTURE_COORDS; i++)
				vec_texture_coord[i] = atof(strtok(NULL, sep_space));
			g_texture.push_back(vec_texture_coord);
		}
		/*else if(strcmp(first_token, "vn") == S_EQUAL)
		{
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);

			for(int i = 0; i != NR_NORMAL_COORDS; i++)
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			g_normals.push_back(vec_normal_coord);
		}*/
		else if(strcmp(first_token, "f") == S_EQUAL)
		{
			std::vector<int> triangle_vertex_indices(3),texture_vertex_indices(3);

			memset((void*)face_token, 0, MAX_NR_FACE_TOKENS);

			nr_tokens = 0;

			while(token = strtok(NULL,sep_space))
			{
				if(strlen(token) < 3)
					break;
				face_token[nr_tokens] = token;
				nr_tokens++;
			}

			for(int i = 0; i != MIN_NR_FACE_TOKENS; ++i)
			{
				token_vertex_index = strtok(face_token[i], sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				
				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
			}
			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);

			if(nr_tokens == MAX_NR_FACE_TOKENS)
			{
				triangle_vertex_indices[1] = triangle_vertex_indices[2];
				triangle_vertex_indices[2] = atoi(token_vertex_index);
				texture_vertex_indices[1] = texture_vertex_indices[2];
				texture_vertex_indices[2] = atoi(token_texture_index);
				token_vertex_index = strtok(face_token[MAX_NR_FACE_TOKENS - 1 ], sep_fslash);
				token_texture_index = strtok(NULL,sep_fslash);
				g_face_tri.push_back(triangle_vertex_indices);
				g_face_texture.push_back(texture_vertex_indices);
			}
		}

		memset((void*)line,(int)'\0',BUFFER_SIZE);
	}	
	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	fprintf(g_fp_logfile,TEXT("g_vertices:%llu g_texture:%llu g_face_tri:%llu \n"),g_vertices.size(),g_texture.size(),g_face_tri.size());
}
void update(void)
{
	g_rotate = g_rotate + TEAPOT_ANGLE_INCREMENT;

	if(g_rotate >= END_ANGLE_POS)
		g_rotate = START_ANGLE_POS;
}
void display(void)
{
	void uninitialize(void);
	mydraw();
	SwapBuffers(ghdc);
}
void resize(int width, int height)
{
	if(height == 0)
		height = 1;
	glViewport( 0, 0, (GLsizei)width, (GLsizei)height );
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(FOY_ANGLE,(GLfloat)width/(GLfloat)height,ZNEAR,ZFAR);
}
void uninitialize(void)
{
	if(gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
	}
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	DestroyWindow(ghwnd);
	ghwnd = NULL;
}
void mydraw(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(TEAPOT_X_TRANSLATE, TEAPOT_Y_TRANSLATE,TEAPOT_Z_TRANSLATE);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);
	glScalef(TEAPOT_X_SCALE_FACTOR, TEAPOT_Y_SCALE_FACTOR, TEAPOT_Z_SCALE_FACTOR);

	glFrontFace(GL_CCW);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	for(int i = 0; i != g_face_tri.size(); ++i)
	{
		glBegin(GL_TRIANGLES);
		for(int j = 0; j != g_face_tri[i].size(); j++)
		{
			int vi = g_face_tri[i][j] - 1;
			glVertex3f(g_vertices[vi][0], g_vertices[vi][1], g_vertices[vi][2]);
		}
		glEnd();
	}
	
}

#include<Windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include<stdio.h>

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT  600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int width, height;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool bFullScreen = false;
DWORD dwStyle;
//int width, height;
int flag = 0;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
HWND ghwnd = NULL;
FILE *gpFile = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpzsCmdLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("DoubleBuffer OGL App");

	bool bDone = false;
	int iRet = 0;
	int iRe = 0;
	int initialize(int);
	void uninitialize(void);
	void display(void);

	if (fopen_s(&gpFile, "Aniket.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File can not be created...\n"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File created successfully \n");
	}

	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("DoubleBuffer Windowing OpenGL"),
		WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_VISIBLE,
		100, 100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;
	iRet = initialize(iRe);



	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//call update here ..
			}
			display();
		}
	}

	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void toggleFullScreen(void);
	void resize(int, int);


	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
			toggleFullScreen();
			break;
		case 0x30:
			flag = 0;
			resize(WIN_WIDTH, WIN_HEIGHT);
			break;
		case 0x31:
			flag = 1;
			resize(WIN_WIDTH, WIN_HEIGHT);
			break;
		case 0x32:
			flag = 2;
			resize(WIN_WIDTH, WIN_HEIGHT);
			break;
		case 0x33:
			flag = 3;
			resize(WIN_WIDTH, WIN_HEIGHT);
			break;
		case 0x34:
			flag = 4;
			resize(WIN_WIDTH, WIN_HEIGHT);
			break;
		case 0x35:
			flag = 5;
			resize(WIN_WIDTH, WIN_HEIGHT);
			break;
		case 0x36:
			flag = 6;
			resize(WIN_WIDTH, WIN_HEIGHT);
			break;
		case 0x37:
			flag = 7;
			resize(WIN_WIDTH, WIN_HEIGHT);
			break;
		case 0x38:
			flag = 8;
			resize(WIN_WIDTH, WIN_HEIGHT);
			break;
		}
		break;
	case WM_DESTROY:
		MessageBox(hwnd, TEXT("By Darling!!!!"), TEXT("WM DESTROY"), MB_OK);
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void toggleFullScreen(void)
{
	MONITORINFO MI;
	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			MI = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev)
				&& GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &MI))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);


				SetWindowPos(ghwnd,
					HWND_TOPMOST,
					MI.rcMonitor.left,
					MI.rcMonitor.top,
					MI.rcMonitor.right - MI.rcMonitor.left,
					MI.rcMonitor.bottom - MI.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(false);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle & WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0, 0, 0, 0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(true);
		bFullScreen = false;
	}
}

int initialize(int iRe)
{
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return -1;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		return -2;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return -3;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return -4;
	}

	if (iRe == -1)
	{
		fprintf(gpFile, "Failed -ChoosePixelFormat");
		DestroyWindow(ghwnd);
	}
	else
	{
		fprintf(gpFile, "Success -ChoosePixelFormat");
	}
	if (iRe == -2)
	{
		fprintf(gpFile, "Failed -SetPixelFormat");
		DestroyWindow(ghwnd);
	}
	else if (iRe == -3)
	{
		fprintf(gpFile, "Failed -wglCreateContext");
		DestroyWindow(ghwnd);
	}
	else if (iRe == -4)
	{
		fprintf(gpFile, "Failed -wglMakeCurrent");
		DestroyWindow(ghwnd);
	}

	else
	{
		fprintf(gpFile, "Initialization is Successful \n");
	}


	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(iRe);


}

void uninitialize()
{
	SetWindowLong(ghwnd, GWL_STYLE, dwStyle & WS_OVERLAPPEDWINDOW);
	SetWindowPlacement(ghwnd, &wpPrev);
	SetWindowPos(ghwnd,
		HWND_TOP,
		0, 0, 0, 0,
		SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (gpFile)
	{
		fprintf(gpFile, "Log Close Successfully");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void resize(int width, int height /*UINT msg, WPARAM wParam*/)
{
	int msg;
	WPARAM wParam;
	if (height == 0)
		height = 1;
	/////////For Left Bottom
	if (flag == 0)
	{
		glViewport(0,0,(float)width,(float)height);
	}
	else if (flag == 1)
	{
		glViewport(0, 0, width/2, height/2);
	}
	///////// For Right Bottom
	else if (flag == 2)
	{
		glViewport(width/2, 0, width/2, height/2);
	}
	///////// For Left Top 
	else if (flag == 3)
	{
		glViewport(0, height/2, width/2, height/2);
	}
	///////// For Right Top
	else if (flag == 4)
	{
		glViewport(width/2, height/2, width/2, height/2);
	}
	///////////
	else if (flag == 5)
	{
		glViewport(0, 0, width/2, height);
	}
	else if (flag == 6)
	{
		glViewport(width/2, 0, width/2, height);
	}
	else if (flag == 7)
	{
		glViewport(0, height/2, width/2, height);
	}
	else if (flag == 8)
	{
		glViewport(width, height/2, width/2, height/2);
	}
	else if (flag == 9)
	{
		glViewport(width*0.5, height*0.5, width*0.5, height*0.5);
	}	
	//if (flag == 0)
////	{
	//	
	//}
//	else if (flag == 1)
//	{
		
//	}
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBegin(GL_TRIANGLES);
	//////////////////////////////////////////////////////////////////
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.5f, -0.5f, 0.0f);
		glVertex3f(0.5f, -0.5f, 0.0f);
	glEnd();

	SwapBuffers(ghdc);
}



























#include <GL/freeglut.h>


#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
bool bFullscreen = false; //variable to toggle for fullscreen

int main(int argc, char** argv)
{
	//function prototypes
	void display(void);
	void resize(int, int);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void initialize(void);
	void uninitialize(void);

	//code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	glutInitWindowSize(800, 600); //to declare initial window size
	glutInitWindowPosition(100, 100); //to declare initial window position
	glutCreateWindow("OpenGL First Window : Hello World !!!"); //open the window with "OpenGL First Window : Hello World" in the title bar

	initialize();

	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	//	return(0); 
}

void display(void)
{
	GLfloat x, y;
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f,1.0f,0.0f);
	glVertex3f(0.0f,1.0f,0.0f);
	glVertex3f(0.0f,-1.0f,0.0f);
	glEnd();
	glColor3f(0.0f,0.0f,1.0f);
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	for( x = 0.05f; x <= 1.0f; x = x + 0.05f )
	{
		glVertex3f(x,1.0f,0.0f);
		glVertex3f(x,-1.0f,0.0f);
		glVertex3f(-x,1.0f,0.0f);
		glVertex3f(-x,-1.0f,0.0f);
	}	
	glEnd();
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f,1.0f,0.0f);
	glVertex3f(-1.0f,0.0f,0.0f);
	glVertex3f(1.0f,0.0f,0.0f);
	glEnd();
	glColor3f(0.0f,0.0f,1.0f);
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	for(y = 0.05f; y <= 1.0f; y = y + 0.05f)
	{
		glVertex3f(-1.0f,y,0.0f);
		glVertex3f(1.0f,y,0.0f);
		glVertex3f(-1.0f,-y,0.0f);
		glVertex3f(1.0f,-y,0.0f);
	}
	glEnd();
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f,0.0f,0.0f);
	glVertex2f(0.6f,0.6f);
	glVertex2f(0.6f,-0.6f);
	glVertex2f(-0.6f,-0.6f);
	glVertex2f(-0.6f,0.6f);
	glEnd();	

glFlush();
}

void initialize(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //blue 
}

void keyboard(unsigned char key, int x, int y)
{
	//code
	switch (key)
	{
	case 27: // Escape
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bFullscreen == false)
		{
			glutFullScreen();
			bFullscreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bFullscreen = false;
		}
		break;
	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	//code
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;
	case GLUT_RIGHT_BUTTON:
		glutMainLoop();
		break;
	default:
		break;
	}
}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
}

void uninitialize(void)
{
	//code
}


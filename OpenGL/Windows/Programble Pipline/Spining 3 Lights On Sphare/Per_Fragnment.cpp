#include <windows.h>
#include <stdio.h>		// For file I/O

#include <gl\glew.h>	// For GLSL extensions. It must be included before <gl\GL.h>
#include <gl\GL.h>

#include "vmath.h"		// Header file from Red Book (For maths) - (v-vermilion)
#include "Sphere.h"		// For the sphere object

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

// Properties of the vertex
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
FILE *gpFile = NULL;		// Global pointer, so that log file can be opened anywhere

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

// Key press:
bool Animation = false;
bool LkeyIsPressed = false;

GLuint VertexShaderObject;
GLuint FragmentShaderObject;
GLuint ShaderProgramObject;

// Sphere variables:
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gNumVertices;
GLuint gNumElements;

// Sphere Vao and Vbo:
GLuint vao_Sphare;
GLuint vbo_Sphere_Position;
GLuint vbo_Sphere_Normal;
GLuint vbo_Sphere_Element;

// Uniforms:
GLuint Model_Matrix_Uniform;
GLuint View_Matrix_Uniform;
GLuint Projection_Matrix_Uniform;

// Light uniforms For Red:
GLuint La_Uniform;				// Uniform for ambient component of light
GLuint Ld_Uniform;				// Uniform for diffuse component of light
GLuint Ls_Uniform;				// Uniform for specular component of light
GLuint Light_Position_Uniform;	// Uniform for light position

// Light uniforms For Red:
GLuint La_UniformBlue;				// Uniform for ambient component of light
GLuint Ld_UniformBlue;				// Uniform for diffuse component of light
GLuint Ls_UniformBlue;				// Uniform for specular component of light
GLuint Light_Position_UniformBlue;	// Uniform for light position

									// Light uniforms For Red:
GLuint La_UniformGreen;				// Uniform for ambient component of light
GLuint Ld_UniformGreen;				// Uniform for diffuse component of light
GLuint Ls_UniformGreen;				// Uniform for specular component of light
GLuint Light_Position_UniformGreen;	// Uniform for light position


// Matrial uniforms:
GLuint Ka_Uniform;				// Uniform for ambient component of material
GLuint Kd_Uniform;				// Uniform for diffuse component of material
GLuint Ks_Uniform;				// Uniform for specular component of material
GLuint Material_Shininess_Uniform;	// Uniform for shininess component of material

GLuint LKeyPressedUniform;

mat4 PerspectiveProjectionMatrix;

// Animate and Light variables:
bool gbAnimate;
bool gbLight;

//For Red
float angleLight = 0.0f;
GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat lightDiffuse[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat lightSpecular[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat lightPosition[] = { 0.0f,0.0f,0.0f,0.0f };

//For Blue
float lightBlue = 0.0f;
GLfloat lightAmbientBlue[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat lightDiffuseBlue[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat lightSpecularBlue[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat lightPositionBlue[] = { 0.0f,0.0f,0.0f,0.0f };

//For Green
float lightGreen = 0.0f;
GLfloat lightAmbientGreen[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat lightDiffuseGreen[] = { 0.0f, 1.0f,0.0f,0.0f };
GLfloat lightSpecularGreen[] = { 0.0f,1.0f,0.0f,0.0f };
GLfloat lightPositionGreen[] = { 0.0f,0.0f,0.0f,0.0f };


GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat materialShininess = 128.0f;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void Update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	//code
	// Create log file for debugging
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable Pipeline : Lights on Sphere (Per Vertex lighting)"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Initialize
	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Update for rotation:
			//if(gbAnimate == true)
				//update();
			// Drawing / rendering function
			//display();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
			Update();
			display();
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// Variable declarations:
	static WORD xMouse = NULL;
	static WORD yMouse = NULL;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)		// If the window is active
			gbActiveWindow = true;
		else							// If non-zero, the window is inactive
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C: // for 'L' or 'l'
			if (LkeyIsPressed == false)
			{
				gbLight = true;
				LkeyIsPressed = true;
			}
			else
			{
				gbLight = false;
				LkeyIsPressed = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);

	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// GLEW initializtion code for GLSL.
	// It must be here, ie, after creating OpenGL context but before using and OpenGL functions
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}


	// *** VERTEX SHADER ***
	// 1. Create the shader (object is created and assigned)
	VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);


	// PER VERTEX LIGHTING:
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform vec4 u_light_positionBlue;" \
		"uniform vec4 u_light_positionGreen;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 light_directionBlue;" \
		"out vec3 light_directionGreen;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"light_directionBlue = vec3(u_light_positionBlue) - eye_coordinates.xyz;" \
		"light_directionGreen = vec3(u_light_positionGreen) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// 4. Compile the shader - Dynamic compilation by the driver
	glCompileShader(VertexShaderObject);

	// ERROR CHECKING:
	GLint iInfoLogLength = 0;			// Length of the log created on failure to compile
	GLint iShaderCompiledStatus = 0;	// Stores shader compilation status
	char *szInfoLog = NULL;				// String to store the log

	glGetShaderiv(VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// Create the shader
	FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Pass the source code to shader
	// FragColor is our name for the Fragment
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core"	\
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 light_directionBlue;" \
		"in vec3 light_directionGreen;" \
		"in vec3 viewer_vector;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_LaBlue;" \
		"uniform vec3 u_LdBlue;" \
		"uniform vec3 u_LsBlue;" \
		"uniform vec3 u_LaGreen;" \
		"uniform vec3 u_LdGreen;" \
		"uniform vec3 u_LsGreen;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \

		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		
		"vec3 normalized_light_directionBlue=normalize(light_directionBlue);" \
		"vec3 ambientBlue = u_LaBlue * u_Ka;" \
		"float tn_dot_ldBlue = max(dot(normalized_transformed_normals, normalized_light_directionBlue),0.0);" \
		"vec3 diffuseBlue = u_LdBlue * u_Kd * tn_dot_ldBlue;" \
		"vec3 reflection_vectorBlue = reflect(-normalized_light_directionBlue, normalized_transformed_normals);" \
		"vec3 specularBlue = u_Ls * u_Ks * pow(max(dot(reflection_vectorBlue, normalized_viewer_vector), 0.0), u_material_shininess);" \

		"vec3 normalized_light_directionGreen=normalize(light_directionGreen);" \
		"vec3 ambientGreen = u_LaGreen * u_Ka;" \
		"float tn_dot_ldGreen = max(dot(normalized_transformed_normals, normalized_light_directionGreen),0.0);" \
		"vec3 diffuseGreen = u_LdGreen * u_Kd * tn_dot_ldGreen;" \
		"vec3 reflection_vectorGreen = reflect(-normalized_light_directionGreen, normalized_transformed_normals);" \
		"vec3 specularGreen = u_LsGreen * u_Ks * pow(max(dot(reflection_vectorGreen, normalized_viewer_vector), 0.0), u_material_shininess);" \

		"phong_ads_color=ambient + diffuse + specular + ambientBlue + diffuseBlue + specularBlue + ambientGreen + diffuseGreen + specularGreen;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	
	glShaderSource(FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compiler the shader
	glCompileShader(FragmentShaderObject);

	// Error checking for compilation done here
	glGetShaderiv(FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	// *** SHADER PROGRAM ***
	// Create
	ShaderProgramObject = glCreateProgram();		// It can link ALL the shaders. Hence, no parameter

													// Attach vertex shader to the shader program
	glAttachShader(ShaderProgramObject, VertexShaderObject);

	// Attach fragment shader to the shader program
	glAttachShader(ShaderProgramObject, FragmentShaderObject);

	// Pre link binding of shader program object with vertex shader position attribute
	// This shader object is recognized by out AMC_ATTRIBUTE_POSITION
	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	// Pre link binding of shader program object with fragment shader position attribute
	// This shader object is recognized by out AMC_ATTRIBUTE_NORMAL
	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link the shader
	glLinkProgram(ShaderProgramObject);

	// Error checking for linking
	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(ShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)	// Failure to link
	{
		glGetProgramiv(ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetProgramInfoLog(ShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program shader link log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// Get uniform locations:
	// Uniforms for Model, View and Projection matrices:
	Model_Matrix_Uniform = glGetUniformLocation(ShaderProgramObject, "u_model_matrix");
	View_Matrix_Uniform = glGetUniformLocation(ShaderProgramObject, "u_view_matrix");
	Projection_Matrix_Uniform = glGetUniformLocation(ShaderProgramObject, "u_projection_matrix");

	// If L key is Pressed or not:
	LKeyPressedUniform = glGetUniformLocation(ShaderProgramObject, "u_lighting_enabled");

	// Uniforms for Light:
	// Ambient color intensity of light:
	La_Uniform = glGetUniformLocation(ShaderProgramObject, "u_La");
	La_UniformBlue = glGetUniformLocation(ShaderProgramObject, "u_LaBlue");
	La_UniformGreen = glGetUniformLocation(ShaderProgramObject, "u_LaGreen");
	// Diffuse color intensity of light:
	Ld_Uniform = glGetUniformLocation(ShaderProgramObject, "u_Ld");
	Ld_UniformBlue = glGetUniformLocation(ShaderProgramObject, "u_LdBlue");
	Ld_UniformGreen = glGetUniformLocation(ShaderProgramObject, "u_LdGreen");
	// Specular color intensity of light:
	Ls_Uniform = glGetUniformLocation(ShaderProgramObject, "u_Ls");
	Ls_UniformBlue = glGetUniformLocation(ShaderProgramObject, "u_LsBlue");
	Ls_UniformGreen = glGetUniformLocation(ShaderProgramObject, "u_LsGreen");
	// Position of light:
	Light_Position_Uniform = glGetUniformLocation(ShaderProgramObject, "u_light_position");
	Light_Position_UniformBlue = glGetUniformLocation(ShaderProgramObject, "u_light_positionBlue");
	Light_Position_UniformGreen = glGetUniformLocation(ShaderProgramObject, "u_light_positionGreen");

	// Uniforms for material:
	// ambient reflective color intensity of material
	Ka_Uniform = glGetUniformLocation(ShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_Uniform = glGetUniformLocation(ShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_Uniform = glGetUniformLocation(ShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	Material_Shininess_Uniform = glGetUniformLocation(ShaderProgramObject, "u_material_shininess");;

	// Vertices, Colors, Shader attributes, vbo, vao initializations:
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// BLOCK FOR SPHERE:
	glGenVertexArrays(1, &vao_Sphare);
	glBindVertexArray(vao_Sphare);

	// A. BUFFER BLOCK FOR VERTICES:
	glGenBuffers(1, &vbo_Sphere_Position);					// Buffer to store vertex position
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Sphere_Position);		// Find that named object in memory
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);	// Takes data from CPU to GPU

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	// Release the buffer for vertices:
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// B. BUFFER BLOCK FOR NORMALS:
	glGenBuffers(1, &vbo_Sphere_Normal);					// Buffer to store vertex normals
	glBindBuffer(GL_ARRAY_BUFFER, vbo_Sphere_Normal);		// Find that named object in memory
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);	// Takes data from CPU to GPU

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	// C. BUFFER BLOCK FOR ELEMENTS:
	glGenBuffers(1, &vbo_Sphere_Element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// Release the buffer for colors:
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);				// Unbind

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glEnable(GL_CULL_FACE);			// Turn off culling for rotation

	// Set the background color to black:
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Set PerspectiveMatrix to identity matrix
	PerspectiveProjectionMatrix = mat4::identity();

	// Warm up call. (Not required for Windows as resize is called before WM_PAINT
	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Start to use the OpenGL program object
	glUseProgram(ShaderProgramObject);		// Compiled and linked program goes to the driver


	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 RotationMatrix = mat4::identity();
	modelMatrix = translate(0.0f, 0.0f, -2.0f);

	if (gbLight == true)
	{
		// Set the 'u_lighting_enabled' uniform:
		glUniform1i(LKeyPressedUniform, 1);

		// Set the light's properties:
		glUniform3fv(La_Uniform, 1, lightAmbient);
		glUniform3fv(Ld_Uniform, 1, lightDiffuse);
		glUniform3fv(Ls_Uniform, 1, lightSpecular);

		glUniform3fv(La_UniformBlue, 1, lightAmbientBlue);
		glUniform3fv(Ld_UniformBlue, 1, lightDiffuseBlue);
		glUniform3fv(Ls_UniformBlue, 1, lightSpecularBlue);

		glUniform3fv(La_UniformGreen, 1, lightAmbientGreen);
		glUniform3fv(Ld_UniformGreen, 1, lightDiffuseGreen);
		glUniform3fv(Ls_UniformGreen, 1, lightSpecularGreen);

		lightPosition[0] = 0.0f;
		lightPosition[1] = cos(angleLight) * 100.0f ;
		lightPosition[2] = sin(angleLight) * 100.0f - 3.0f;
		glUniform4fv(Light_Position_Uniform, 1, lightPosition);
		
		lightPositionBlue[0] = cos(lightBlue) * 100.0f;
		lightPositionBlue[1] = -3.0f;
		lightPositionBlue[2] = sin(lightBlue) * 100.0f - 3.0f;
		glUniform4fv(Light_Position_UniformBlue, 1, lightPositionBlue);

		lightPositionGreen[0] = cos(lightGreen) * 100.0f;
		lightPositionGreen[1] = sin(lightGreen) * 100.0f;
		lightPositionGreen[2] = 0.0f;
		glUniform4fv(Light_Position_UniformGreen, 1, lightPositionGreen);
		
		//angleBlue
		// Set the material's properties:
		glUniform3fv(Ka_Uniform, 1, materialAmbient);
		glUniform3fv(Kd_Uniform, 1, materialDiffuse);
		glUniform3fv(Ks_Uniform, 1, materialSpecular);
		glUniform1f(Material_Shininess_Uniform, materialShininess);
	}
	else
	{
		// Set the 'u_lighting_enabled' uniform:
		glUniform1i(LKeyPressedUniform, 0);
	}

	// OpenGL Drawing

	// CUBE:
	// Set the modelview, rotation, scale and modelviewprojection matrices to identity 

	// Translate the modelViewMatrix along the z axis


	glUniformMatrix4fv(Model_Matrix_Uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(View_Matrix_Uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(Projection_Matrix_Uniform, 1, GL_FALSE, PerspectiveProjectionMatrix);

	// BIND vao
	glBindVertexArray(vao_Sphare);

	// Draw either by glDrawTraingles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_Sphere_Element);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// Unbind vao
	glBindVertexArray(0);

	// Stop using the OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void Update(void)
{
	angleLight = angleLight + 0.001f;
	if (angleLight >= 360.0f)
	{
		angleLight = 0.0f;
	}

	lightBlue = lightBlue + 0.001f;
	if (lightBlue >= 360.0f)
	{
		lightBlue = 0.0f;
	}

	lightGreen = lightGreen + 0.001f;
	if (lightGreen >= 360.0f)
	{
		lightGreen = 0.0f;
	}
}


void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	// glPerspective(FOV, aspect ratio, near, far)
	PerspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	// Destroy Sphere vao 
	if (vao_Sphare)
	{
		glDeleteVertexArrays(1, &vao_Sphare);
		vao_Sphare = 0;
	}

	// Destroy vbo for Sphere Position:
	if (vbo_Sphere_Position)
	{
		glDeleteBuffers(1, &vbo_Sphere_Position);
		vbo_Sphere_Position = 0;
	}

	// Destroy vbo for Sphere Normal:
	if (vbo_Sphere_Normal)
	{
		glDeleteBuffers(1, &vbo_Sphere_Normal);
		vbo_Sphere_Normal = 0;
	}

	// Destroy vbo for Sphere Element:
	if (vbo_Sphere_Element)
	{
		glDeleteBuffers(1, &vbo_Sphere_Element);
		vbo_Sphere_Element = 0;
	}

	// Detach the shaders first before deleting
	// Detach vertex shader from shader program object
	glDetachShader(ShaderProgramObject, VertexShaderObject);
	// Detach fragment shader from shader program object
	glDetachShader(ShaderProgramObject, FragmentShaderObject);

	// Delete vertex shader object
	glDeleteShader(VertexShaderObject);
	VertexShaderObject = 0;
	// Delete fragment shader object
	glDeleteShader(FragmentShaderObject);
	FragmentShaderObject = 0;

	// Delete shader program object. It has no objects attached
	glDeleteProgram(ShaderProgramObject);
	ShaderProgramObject = 0;

	// Unlink shader program
	glUseProgram(0);		// Stray call

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)			// Closing the log file here
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

#include<Windows.h>
#include<stdio.h>

#include<gl\glew.h>
#include<gl\GL.h>

#include"vmath.h"

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"glew32.lib")

//Constants
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Vmath namespace
using namespace vmath;

//Global Variables
HWND ghwnd = NULL;
DWORD dwStyle;
bool bFullScreen = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;

void Color_Circle(void);

//Shader Program Objects
GLint gShaderProgramObject;

//Addition
GLuint mvpUniform;
GLuint samplerUniform;

int windowWidth;
int windowHeight;

enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// For Circle
GLuint vao_Circle;
GLuint vbo_position_Circle;
GLuint vbo_color_Circle;
static int noofpoints = 0;

// For Cemi Circle
GLuint vao_cemiCircle;
GLuint vbo_position_cemiCircle;
GLuint vbo_color_cemiCircle;
static int noofpointsSecond = 0;

// fOR Rectangle
GLuint vao_Rectangle;
GLuint vbo_position_Rectangle;
GLuint vbo_color_Rectangle;

// fOR Trangle
GLuint vao_Trangle;
GLuint vbo_position_Trangle;
GLuint vbo_color_Trangle;

// For Horizental line
GLuint vao_Horizentalline;
GLuint vao_HorizentallineWithLoop;

GLfloat HorizentalLine_Vertices_WLoop[12];

GLuint vbo_position_HorizentalLineL;
GLuint vbo_color_HorizentalL;
GLuint vbo_position_HorizentalLine;
GLuint vbo_color_Horizentalline;


// For Verticle
GLuint vao_Verticleline;
GLuint vao_VerticlelineWithLoop;

GLuint vbo_position_VerticlL;
GLuint vbo_color_VerticleL;
GLuint vbo_position_VerticleLine;
GLuint vbo_color_Verticleline;

GLfloat Verticle_Vertices_WLoop[12];

mat4 PrespectiveGraphicsProjectionMatrix;


//Method Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
//void PrintTime();
//WinMain Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevinstance, LPSTR lpszCmdLine, int iCmdShow) {
	//Method Declaration
	int Initialize(void);
	void Display(void);
	void Update(void);
	//Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OGL Window");
	int iRet = 0;
	bool bDone = false;
	//Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can't Be Created"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else {
		//PrintTime();
		fprintf_s(gpFile, "Log File Created Successfully\n");
	}
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Ortho Graphics Rectangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	iRet = Initialize();
	if (iRet == -1) {
		fprintf_s(gpFile, "Choose Pixel Format Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -2) {
		fprintf_s(gpFile, "Set Pixel Format Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -3) {
		fprintf_s(gpFile, "wgl Create Context Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -4) {
		fprintf_s(gpFile, "wgl Make Current Failed\n");
		DestroyWindow(0);
	}
	else {
		fprintf_s(gpFile, "Initialization Succeeded\n");
	}
	ShowWindow(hwnd, iCmdShow);
	//Do not call update window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	//Game Loop
	//SendMessage(hwnd, WM_PAINT, 0, 0);
	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow == true) {
			}
			Display();
			//Here Call Display
		}
	}
	return (int)msg.wParam;
}
//WndProc Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	//Function Declaration
	void ToggleFullScreen(void);
	void Resize(int, int);
	void UnInitialize(void);
	void Update(void);
	//Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		windowWidth = LOWORD(lParam);
		windowHeight = HIWORD(lParam);
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(0);
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
//ToggleFullScreen Function
void ToggleFullScreen(void) {
	//Variable Declaration
	MONITORINFO mi;
	if (bFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				bFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}
//Initialize OpenGl
int Initialize(void) {
	//Function Declaration
	void Resize(int, int);
	void UnInitialize(void);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;
	//Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW |
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		return -3;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	result = glewInit();
	if (result != GLEW_OK) {
		fprintf_s(gpFile, "glewInit() failed\n");
		UnInitialize();
		DestroyWindow(0);
	}

	//Addition
	GLint gVertexShaderObject;
	GLint gFragementShaderObject;

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Vertex Shader Code
	const GLchar* vertextShaderSourceCode =
		"#version 330 core " \
		"\n " \
		"in vec4 vPosition;"	\
		"in vec4 vColor;"	\
		"out vec4 out_color;"	\
		"uniform mat4 u_mvp_matrix;"	\
		"void main(void)"	\
		"{"	\
		"gl_Position = u_mvp_matrix * vPosition;"
		"out_color = vColor;"	\
		"}";

	//"in vec4 vColor;" \
	//"out vec4 out_color;" \
		//"out_color=vColor;"\

	//Shader object and source code mapping
	glShaderSource(gVertexShaderObject, 1, &vertextShaderSourceCode, NULL);

	//Compile Shader
	glCompileShader(gVertexShaderObject);

	//Error Checking
	GLint iShaderCompileStatusN = 0;
	GLint iInfoLogLength = 0;
	GLchar * szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatusN);

	if (iShaderCompileStatusN == GL_FALSE) {
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf_s(gpFile, "Vertex Shader Compilation Log %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//Fragement Shader
	//Define Shader Object
	gFragementShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Fragement Shader Code
	const GLchar* fragementShaderSourceCode =
		"#version 330 core " \
		"\n " \
		"in vec4 out_color;"
		"out vec4 FragColor;"
		"void main(void)" \
		"{" \
		"FragColor = out_color;"	\
		"} ";

		
	//Shader object and source code mapping
	glShaderSource(gFragementShaderObject, 1, &fragementShaderSourceCode, NULL);

	//Compile Shader
	glCompileShader(gFragementShaderObject);

	//Error Checking
	iShaderCompileStatusN = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(gFragementShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatusN);

	if (iShaderCompileStatusN == GL_FALSE) {
		glGetShaderiv(gFragementShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gFragementShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf_s(gpFile, "Fragement Shader Compilation Log %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//Program Object
	gShaderProgramObject = glCreateProgram();
	//Attach Shaders
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragementShaderObject);

		//Link Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error Checking
	GLint iShaderLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderLinkStatus);

	if (iShaderLinkStatus == GL_FALSE) {
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf_s(gpFile, "Program Link Log %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//Post Linking
	//Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject,
		"u_mvp_matrix");

	// For Vertical
	const float Verticle_Color[] =
	{
		1.0,0.0,0.0,
		1.0,0.0,0.0
	};

	const float Line_Vertices[] =
	{
		0.0,1.0,0.0,
		0.0,-1.0,0.0,
	};

	const float Line_Color[] =
	{
		1.0,0.0,0.0,
		1.0,0.0,0.0
	};

	//Verticle Line
	glGenVertexArrays(1, &vao_Verticleline);
	glBindVertexArray(vao_Verticleline);

	//Generate Buffer
	glGenBuffers(1, &vbo_position_VerticlL);
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_position_VerticlL);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(Line_Vertices),
		Line_Vertices,
		GL_STATIC_DRAW);
	
//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// COLOER
	glGenBuffers(1, &vbo_color_VerticleL);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_color_VerticleL);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(Line_Color),
		Line_Color,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Unbind array
	glBindVertexArray(0);


	//Create vao
	//Create Verticle Vline With Loop
	glGenVertexArrays(1, &vao_VerticlelineWithLoop);
	glBindVertexArray(vao_VerticlelineWithLoop);

	// VERTICES
	//Generate Buffer
	glGenBuffers(1, &vbo_position_VerticleLine);
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_position_VerticleLine);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		6 * 2 * sizeof(GL_FLOAT),
		NULL,
		GL_DYNAMIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// COLOR		
	glGenBuffers(1, &vbo_color_Verticleline);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_color_Verticleline);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(Verticle_Color),
		Verticle_Color,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Unbind array
	glBindVertexArray(0);

	// For Horizental
	const float Horizental_Color[] =
	{
		0.0,1.0,0.0,
		0.0,1.0,0.0,
		0.0,1.0,0.0,
		0.0,1.0,0.0
	};

	const float HorizentalLine_Vertices[] =
	{
		-1.0,0.0,0.0,
		1.0,0.0,0.0,
	};

	const float HorizentalLine_Color_Single[] =
	{
		0.0,1.0,0.0,
		0.0,1.0,0.0,
		0.0,1.0,0.0,
		0.0,1.0,0.0
	};
		//Horizental Line
		glGenVertexArrays(1, &vao_Horizentalline);
		glBindVertexArray(vao_Horizentalline);

		//Generate Buffer
		glGenBuffers(1, &vbo_position_HorizentalLineL);
		glBindBuffer(GL_ARRAY_BUFFER,
		vbo_position_HorizentalLineL);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
			sizeof(HorizentalLine_Vertices),
			HorizentalLine_Vertices,
			GL_STATIC_DRAW);
		
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// COLOER
		glGenBuffers(1, &vbo_color_HorizentalL);
		//Bind Generated Buffer
		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_color_HorizentalL);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
			sizeof(HorizentalLine_Color_Single),
			HorizentalLine_Color_Single,
			GL_STATIC_DRAW);
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
			2,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		//Unbind array
		glBindVertexArray(0);


		//Create vao
		//Create Horizental Vline With Loop
		glGenVertexArrays(1, &vao_HorizentallineWithLoop);
		glBindVertexArray(vao_HorizentallineWithLoop);
		
		// VERTICES
		//Generate Buffer
		glGenBuffers(1, &vbo_position_HorizentalLine);
		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_position_HorizentalLine);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
			6 *2* sizeof(GL_FLOAT),
			NULL,
			GL_DYNAMIC_DRAW);
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	
		// COLOR		
		glGenBuffers(1, &vbo_color_Horizentalline);
		//Bind Generated Buffer
		glBindBuffer(GL_ARRAY_BUFFER,
		vbo_color_Horizentalline);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
		sizeof(Horizental_Color),
		Horizental_Color,
		GL_STATIC_DRAW);
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		//Unbind array
		glBindVertexArray(0);

		
		
		//***************************  For Rectangle
		const float Rectangle_Vertices[] =
		{
			1.0,1.0,0.0,
			-1.0,1.0,0.0,
			-1.0,-1.0,0.0,
			1.0,-1.0,0.0
		};
		const float Rectangle_Color[] =
		{
			1.0,1.0,0.0,
			1.0,1.0,0.0,
			1.0,1.0,0.0,
			1.0,1.0,0.0
		};
		
		glGenVertexArrays(1, &vao_Rectangle);
		glBindVertexArray(vao_Rectangle);


		//TRIANGLE
		//Generate Buffer
		glGenBuffers(1, &vbo_position_Rectangle);
		//Bind Generated Buffer
		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_position_Rectangle);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
			sizeof(Rectangle_Vertices),
			Rectangle_Vertices,
			GL_STATIC_DRAW);
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &vbo_color_Rectangle);
		//Bind Generated Buffer
		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_color_Rectangle);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
			sizeof(Rectangle_Color),
			Rectangle_Color,
			GL_STATIC_DRAW);
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		//Unbind array
		glBindVertexArray(0);

		// ************************************ For Traingle
		const float Trangle_Vertices[] =
		{
			0.0,1.0,0.0,
			-1.0,-1.0,0.0,
			1.0,-1.0,0.0
		};
		const float Trangle_Color[] =
		{
			1.0,1.0,0.0,
			1.0,1.0,0.0,
			1.0,1.0,0.0
		};

		glGenVertexArrays(1, &vao_Trangle);
		glBindVertexArray(vao_Trangle);


		//TRIANGLE
		//Generate Buffer
		glGenBuffers(1, &vbo_position_Trangle);
		//Bind Generated Buffer
		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_position_Trangle);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
			sizeof(Trangle_Vertices),
			Trangle_Vertices,
			GL_STATIC_DRAW);
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &vbo_color_Trangle);
		//Bind Generated Buffer
		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_color_Trangle);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
			sizeof(Trangle_Color),
			Trangle_Color,
			GL_STATIC_DRAW);
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		//Unbind array
		glBindVertexArray(0);

		// For Big Circle In All Shape's
			//Create vao
	//Save everying in single set
		glGenVertexArrays(1, &vao_Circle);
		glBindVertexArray(vao_Circle);

		//Generate Buffer
		glGenBuffers(1, &vbo_position_Circle);
		//Bind Generated Buffer
		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_position_Circle);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
			6 * sizeof(float),
			NULL,
			GL_DYNAMIC_DRAW);
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// Color For Circle
		glGenBuffers(1, &vbo_color_Circle);
		//Bind Generated Buffer
		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_color_Circle);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
			6 * sizeof(float),
			NULL,
			GL_DYNAMIC_DRAW);
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		//Unbind array
		glBindVertexArray(0);

		//Unbind array
		glBindVertexArray(0);
		/*GLuint vao_cemiCircle;
GLuint vbo_position_cemiCircle;
GLuint vbo_color_cemiCircle;*/
		// ********** Cemi Circle
		glGenVertexArrays(1, &vao_cemiCircle);
		glBindVertexArray(vao_cemiCircle);

		//Generate Buffer
		glGenBuffers(1, &vbo_position_cemiCircle);
		//Bind Generated Buffer
		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_position_cemiCircle);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
			6 * sizeof(float),
			NULL,
			GL_DYNAMIC_DRAW);
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		// Color For Circle
		glGenBuffers(1, &vbo_color_cemiCircle);
		//Bind Generated Buffer
		glBindBuffer(GL_ARRAY_BUFFER,
			vbo_color_cemiCircle);
		//Fill Buffer
		glBufferData(GL_ARRAY_BUFFER,
			6 * sizeof(float),
			NULL,
			GL_DYNAMIC_DRAW);
		//Set Vertex Attrib Pointer
		glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			NULL);
		//Enable Vertex Attrib Array
		glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
		//Unbind Buffer
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		//Unbind array
		glBindVertexArray(0);

		//Unbind array
		glBindVertexArray(0);

	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

	PrespectiveGraphicsProjectionMatrix = mat4::identity();

	Resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}
void Color_Circle(void)
{

}

//Function Resize
void Resize(int width, int height) 
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	PrespectiveGraphicsProjectionMatrix=perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);

}

//Function Display
void Display(void) 
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	//Declaration of Matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 TrabslationMatrix;


	// For Verticaly
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix 
	modelViewMatrix = translate(0.0f, 0.0f, -3.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao
	glBindVertexArray(vao_Verticleline);
	glLineWidth(4.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);


	// Line's With Loop
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix 
	modelViewMatrix = translate(0.0f, 0.0f, -3.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao
	glBindVertexArray(vao_VerticlelineWithLoop);
	GLfloat x, y;
	for (x = -1.0f; x <= 1.0f; x = x + 0.05f)
	{
		Verticle_Vertices_WLoop[0] = x;
		Verticle_Vertices_WLoop[1] = 1.0;
		Verticle_Vertices_WLoop[2] = 0.0;
		Verticle_Vertices_WLoop[3] = x;
		Verticle_Vertices_WLoop[4] = -1.0;
		Verticle_Vertices_WLoop[5] = 0.0;

		glBindBuffer(GL_ARRAY_BUFFER, vbo_position_VerticleLine);						// Find that named object in memory
		glBufferData(GL_ARRAY_BUFFER, sizeof(Verticle_Vertices_WLoop), Verticle_Vertices_WLoop, GL_DYNAMIC_DRAW);	// Takes data from CPU to GPU
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		Color_Circle();
		glLineWidth(1.0f);
		glDrawArrays(GL_LINES, 0, 2);
	}
	glBindVertexArray(0);

	//For Horizental line's
	// First Line

	// Line's With Loop
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix 
	modelViewMatrix = translate(0.0f, 0.0f, -3.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao
	glBindVertexArray(vao_Horizentalline);
	glLineWidth(4.0f);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);


	// Line's With Loop
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix 
	modelViewMatrix = translate(0.0f, 0.0f, -3.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms
	
	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao
	glBindVertexArray(vao_HorizentallineWithLoop);
	//GLfloat x, y;
	for (x = -1.0f; x <= 1.0f; x = x + 0.05f)
	{
		HorizentalLine_Vertices_WLoop[0] = 1.0;
		HorizentalLine_Vertices_WLoop[1] = x;
		HorizentalLine_Vertices_WLoop[2] = 0.0;
		HorizentalLine_Vertices_WLoop[3] = -1.0;
		HorizentalLine_Vertices_WLoop[4] = x;
		HorizentalLine_Vertices_WLoop[5] = 0.0;

		glBindBuffer(GL_ARRAY_BUFFER, vbo_position_HorizentalLine);						// Find that named object in memory
		glBufferData(GL_ARRAY_BUFFER, sizeof(HorizentalLine_Vertices_WLoop), HorizentalLine_Vertices_WLoop, GL_DYNAMIC_DRAW);	// Takes data from CPU to GPU
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glLineWidth(1.0f);
		glDrawArrays(GL_LINES, 0, 2);
	}
	glBindVertexArray(0);


	//**************************** For Rectangle By Line LOOP
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix 
	modelViewMatrix = translate(0.0f, 0.0f, -3.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao
	glBindVertexArray(vao_Rectangle);
	glLineWidth(4.0f);
	glDrawArrays(GL_LINE_LOOP, 0, 4);
	glBindVertexArray(0);

	//**************************** For Small Rectangle By Line LOOP
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix 
	modelViewMatrix = translate(0.0f, 0.0f, -5.2f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao
	glBindVertexArray(vao_Rectangle);
	glLineWidth(4.0f);
	glDrawArrays(GL_LINE_LOOP, 0, 4);
	glBindVertexArray(0);


	//***************************** For Circle By Points
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix Multiplication

	modelViewMatrix = translate(0.0f, 0.0f, -4.0f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao

	glBindVertexArray(vao_Circle);
	//Bind with textures if any
	static float circleangle[9000];
	int circle_points = 3000;	//Vertices
	float angle = 0.0f;
	Color_Circle();
	static float circleColor[9000];

	for (noofpoints = 0; noofpoints < circle_points; noofpoints += 3)
	{
		//angle = 2 * PI * i / circle_points;
		angle = 2 * M_PI * noofpoints / circle_points;
		circleangle[noofpoints++] = cos(angle)*1.1;
		circleangle[noofpoints++] = sin(angle)*1.1;
		circleangle[noofpoints++] = 0.0f;
		
		//circleColor[0] = 1.0f;
		//circleColor[1] = 1.0f;
		//circleColor[2] = 0.0f;
		//circleColor[3] = 1.0f;
		//circleColor[4] = 1.0f;
		//circleColor[5] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, vbo_position_Circle);						// Find that named object in memory
		glBufferData(GL_ARRAY_BUFFER, sizeof(circleangle), circleangle, GL_DYNAMIC_DRAW);	// Takes data from CPU to GPU
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		//glBindBuffer(GL_ARRAY_BUFFER, vbo_color_Circle);						// Find that named object in memory
		//glBufferData(GL_ARRAY_BUFFER, sizeof(circleColor), circleColor, GL_DYNAMIC_DRAW);	// Takes data from CPU to GPU
		//glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	for (noofpoints = 0; noofpoints <= circle_points; noofpoints += 3)
	{
		angle = 2 * M_PI * noofpoints / circle_points;
		circleColor[noofpoints++] = 1.0f;
		circleColor[noofpoints++] = 1.0f;
		circleColor[noofpoints++] = 0.0f;

		glBindBuffer(GL_ARRAY_BUFFER, vbo_color_Circle);						// Find that named object in memory
		glBufferData(GL_ARRAY_BUFFER, sizeof(circleColor), circleColor, GL_DYNAMIC_DRAW);	// Takes data from CPU to GPU
		glBindBuffer(GL_ARRAY_BUFFER, 0);

	}
		//Draw
		//glLineWidth(3.0f);
		glPointSize(5.0f);
		glDrawArrays(GL_POINTS,
			0,
			circle_points++);
	glBindVertexArray(0);

	//**************************** For Rectangle By Line LOOP
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix 
	modelViewMatrix = translate(0.0f, 0.0f, -3.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao
	glBindVertexArray(vao_Rectangle);
	glLineWidth(4.0f);
	glDrawArrays(GL_LINE_LOOP, 0, 4);
	glBindVertexArray(0);

	//**************************** For Small trangle By Line LOOP
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix 
	modelViewMatrix = translate(0.0f, 0.0f, -5.2f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao
	glBindVertexArray(vao_Trangle);
	glLineWidth(4.0f);
	glDrawArrays(GL_LINE_LOOP, 0, 3);
	glBindVertexArray(0);

	//***************************** For Small Circle 
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix Multiplication

	modelViewMatrix = translate(0.0f, -0.6f, -9.0f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao

	glBindVertexArray(vao_Circle);
	//Bind with textures if any
		glBindBuffer(GL_ARRAY_BUFFER, vbo_position_Circle);						// Find that named object in memory
		glBufferData(GL_ARRAY_BUFFER, sizeof(circleangle), circleangle, GL_DYNAMIC_DRAW);	// Takes data from CPU to GPU
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		//glBindBuffer(GL_ARRAY_BUFFER, vbo_color_Circle);						// Find that named object in memory
		//glBufferData(GL_ARRAY_BUFFER, sizeof(circleColor), circleColor, GL_DYNAMIC_DRAW);	// Takes data from CPU to GPU
		//glBindBuffer(GL_ARRAY_BUFFER, 0);

		//Draw
		//glLineWidth(3.0f);
		glPointSize(5.0f);
		glDrawArrays(GL_POINTS,
			0,
			circle_points++);
	glBindVertexArray(0);


	glUseProgram(0);

	SwapBuffers(ghdc);

}
void UnInitialize(void) {
	if (bFullScreen == true) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		//ShowCursor(TRUE);
	}


	if (vbo_position_VerticleLine) {
		glDeleteBuffers(1, &vbo_position_VerticleLine);
		vbo_position_VerticleLine = 0;
	}

	if (vbo_color_Verticleline) {
		glDeleteBuffers(1, &vbo_color_Verticleline);
		vbo_color_Verticleline = 0;
	}

	if (vao_VerticlelineWithLoop) {
		glDeleteVertexArrays(1, &vao_VerticlelineWithLoop);
		vao_VerticlelineWithLoop = 0;
	}

	if (vao_VerticlelineWithLoop) {
		glDeleteVertexArrays(1, &vao_VerticlelineWithLoop);
		vao_VerticlelineWithLoop = 0;
	}

	if (vbo_position_HorizentalLine) {
		glDeleteBuffers(1, &vbo_position_HorizentalLine);
		vbo_position_HorizentalLine= 0;
	}

	if (vbo_color_Horizentalline) {
		glDeleteBuffers(1, &vbo_color_Horizentalline);
		vbo_color_Horizentalline = 0;
	}

	if (vao_HorizentallineWithLoop) {
		glDeleteVertexArrays(1, &vao_HorizentallineWithLoop);
		vao_HorizentallineWithLoop= 0;
	}

	if (vao_HorizentallineWithLoop) {
		glDeleteVertexArrays(1, &vao_HorizentallineWithLoop);
		vao_HorizentallineWithLoop = 0;
	}

	if (gShaderProgramObject) {
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,
			&shaderCount);

		GLuint * pShaders = (GLuint *)malloc(shaderCount * sizeof(GLuint));

		if (pShaders) {
			glGetAttachedShaders(gShaderProgramObject,
				shaderCount,
				&shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++) {
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (gpFile) {
		fprintf_s(gpFile, "Log File Closed Successfully");
		fclose(gpFile);
		gpFile = NULL;
	}
}



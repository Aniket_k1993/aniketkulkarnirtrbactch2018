#include<Windows.h>
#include<stdio.h>

#include<GL\glew.h>
#include<gl\GL.h>

#include"vmath.h"

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"glew32.lib")

//Constants
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Vmath namespace
using namespace vmath;

//Global Variables
HWND ghwnd = NULL;
DWORD dwStyle;
bool bFullScreen = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;

//Shader Program Objects
GLint gShaderProgramObject;

//Addition
GLuint mvpUniform;
GLuint samplerUniform;

int windowWidth;
int windowHeight;

enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint vao_Pyramid;

//Traingle VBO
GLuint vbo_position_Pyramid;
GLuint vbo_color_Pyramid;

//Cube VBO

mat4 PrespectiveGraphicsProjectionMatrix;

// Declaration For Rotatation
GLfloat anglePyramid = 0.0;

//************************************************************
// Uniforms:
bool LkeyIsPressed = false;
GLuint Model_Matrix_Uniform;
GLuint View_Matrix_Uniform;
GLuint Projection_Matrix_Uniform;

// Light uniforms:
GLuint La_Uniform;				// Uniform for ambient component of light
GLuint Ld_Uniform;				// Uniform for diffuse component of light
GLuint Ls_Uniform;				// Uniform for specular component of light
GLuint Light_Position_Uniform;	// Uniform for light position

// Matrial uniforms:
GLuint Ka_Uniform;				// Uniform for ambient component of material
GLuint Kd_Uniform;				// Uniform for diffuse component of material
GLuint Ks_Uniform;				// Uniform for specular component of material
GLuint Material_Shininess_Uniform;	// Uniform for shininess component of material

// Light uniforms:
GLuint La_UniformBlue;				// Uniform for ambient component of light
GLuint Ld_UniformBlue;				// Uniform for diffuse component of light
GLuint Ls_UniformBlue;				// Uniform for specular component of light
GLuint Light_Position_UniformBlue;	// Uniform for light position

GLuint LKeyPressedUniform;

// Animate and Light variables:
bool gbAnimate;
bool gbLight;

GLfloat lightAmbientRed[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat lightDiffuseRed[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightSpecularRed[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightPositionRed[] = { 2.0f,0.0f,0.0f,1.0f };
//GLfloat lightDiffuseRed[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat materialShininess= 128.0f;


GLfloat lightAmbientBlue[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat lightDiffuseBlue[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightSpecularBlue[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightPositionBlue[] = { -2.0f,0.0f,0.0f,1.0f };


//Method Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
//void PrintTime();
//WinMain Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevinstance, LPSTR lpszCmdLine, int iCmdShow) {
	//Method Declaration
	int Initialize(void);
	void Display(void);
	void Update(void);
	//Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OGL Window");
	int iRet = 0;
	bool bDone = false;
	//Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can't Be Created"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else {
		//PrintTime();
		fprintf_s(gpFile, "Log File Created Successfully\n");
	}
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Ortho Graphics Traingle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	iRet = Initialize();
	if (iRet == -1) {
		fprintf_s(gpFile, "Choose Pixel Format Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -2) {
		fprintf_s(gpFile, "Set Pixel Format Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -3) {
		fprintf_s(gpFile, "wgl Create Context Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -4) {
		fprintf_s(gpFile, "wgl Make Current Failed\n");
		DestroyWindow(0);
	}
	else {
		fprintf_s(gpFile, "Initialization Succeeded\n");
	}
	ShowWindow(hwnd, iCmdShow);
	//Do not call update window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	//Game Loop
	//SendMessage(hwnd, WM_PAINT, 0, 0);
	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow == true) 
			{
				Update();
			}
			Display();
			//Here Call Display
		}
	}
	return (int)msg.wParam;
}
//WndProc Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	//Function Declaration
	void ToggleFullScreen(void);
	void Resize(int, int);
	void UnInitialize(void);
	void Update(void);
	//Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		windowWidth = LOWORD(lParam);
		windowHeight = HIWORD(lParam);
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(0);
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 'l': // for 'L' or 'l'
		case 'L':
			if (LkeyIsPressed == false)
			{
				gbLight = true;
				LkeyIsPressed = true;
			}
			else
			{
				gbLight = false;
				LkeyIsPressed = false;
			}
			break;
		}
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
//ToggleFullScreen Function
void ToggleFullScreen(void) {
	//Variable Declaration
	MONITORINFO mi;
	if (bFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				bFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}
//Initialize OpenGl
int Initialize(void) {
	//Function Declaration
	void Resize(int, int);
	void UnInitialize(void);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;
	//Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW |
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		return -3;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	result = glewInit();
	if (result != GLEW_OK) {
		fprintf_s(gpFile, "glewInit() failed\n");
		UnInitialize();
		DestroyWindow(0);
	}

	//Addition
	GLint gVertexShaderObject;
	GLint gFragementShaderObject;

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Vertex Shader Code
	const GLchar *vertextShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_positionRed;" \
		"uniform vec4 u_light_positionBlue;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_directionRed;" \
		"out vec3 light_directionBlue;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_directionRed = vec3(u_light_positionRed) - eye_coordinates.xyz;" \
		"light_directionBlue = vec3(u_light_positionBlue) - eye_coordinates.xyz;" \

		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";


	//"in vec4 vColor;" \
	//"out vec4 out_color;" \
		//"out_color=vColor;"\

	//Shader object and source code mapping
	glShaderSource(gVertexShaderObject, 1, &vertextShaderSourceCode, NULL);

	//Compile Shader
	glCompileShader(gVertexShaderObject);

	//Error Checking
	GLint iShaderCompileStatusN = 0;
	GLint iInfoLogLength = 0;
	GLchar * szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatusN);

	if (iShaderCompileStatusN == GL_FALSE) {
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf_s(gpFile, "Vertex Shader Compilation Log %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//Fragement Shader
	//Define Shader Object
	gFragementShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Fragement Shader Code	const GLchar *fragmentShaderSourceCode =
	const GLchar *fragmentShaderSourceCode =
	"#version 450 core"	\
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_directionRed;" \
		"in vec3 light_directionBlue;" \
		"in vec3 viewer_vector;" \
		"uniform vec3 u_LaRed;" \
		"uniform vec3 u_LdRed;" \
		"uniform vec3 u_LsRed;" \
		"uniform vec3 u_LaBlue;" \
		"uniform vec3 u_LdBlue;" \
		"uniform vec3 u_LsBlue;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \

		"vec3 normalized_light_directionRed=normalize(light_directionRed);" \
		"vec3 ambientRed = u_LaRed * u_Ka;" \
		"float tn_dot_ldRed = max(dot(normalized_transformed_normals, normalized_light_directionRed),0.0);" \
		"vec3 diffuseRed = u_LdRed * u_Kd * tn_dot_ldRed;" \
		"vec3 reflection_vectorRed = reflect(-normalized_light_directionRed, normalized_transformed_normals);" \
		"vec3 specularRed = u_LsRed * u_Ks * pow(max(dot(reflection_vectorRed, normalized_viewer_vector), 0.0), u_material_shininess);" \

		"vec3 normalized_light_directionBlue=normalize(light_directionBlue);" \
		"float tn_dot_ldBlue = max(dot(normalized_transformed_normals, normalized_light_directionBlue),0.0);" \
		"vec3 ambientBlue = u_LaBlue * u_Ka;" \
		"vec3 diffuseBlue = u_LdBlue * u_Kd * tn_dot_ldBlue;" \
		"vec3 reflection_vectorBlue = reflect(-normalized_light_directionBlue, normalized_transformed_normals);" \
		"vec3 specularBlue = u_LsBlue * u_Ks * pow(max(dot(reflection_vectorBlue, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambientRed + diffuseRed + specularRed + ambientBlue + diffuseBlue + specularBlue;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	//"in vec4 out_color;"\
	//"fragColor=out_color;" \
		
	//Shader object and source code mapping
	glShaderSource(gFragementShaderObject, 1, &fragmentShaderSourceCode, NULL);

	//Compile Shader
	glCompileShader(gFragementShaderObject);

	//Error Checking
	iShaderCompileStatusN = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(gFragementShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatusN);

	if (iShaderCompileStatusN == GL_FALSE) {
		glGetShaderiv(gFragementShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gFragementShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf_s(gpFile, "Fragement Shader Compilation Log %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//Program Object
	gShaderProgramObject = glCreateProgram();
	//Attach Shaders
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragementShaderObject);


	// Pre link binding of shader program object with vertex shader position attribute
// This shader object is recognized by out AMC_ATTRIBUTE_POSITION
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	// Pre link binding of shader program object with fragment shader position attribute
	// This shader object is recognized by out AMC_ATTRIBUTE_NORMAL
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error Checking
	GLint iShaderLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderLinkStatus);

	if (iShaderLinkStatus == GL_FALSE) {
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf_s(gpFile, "Program Link Log %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	Model_Matrix_Uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	View_Matrix_Uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	Projection_Matrix_Uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	/*Model_Matrix_Uniform 
	View_Matrix_Uniform
	Projection_Matrix_Uniform
	*/
	// If L key is Pressed or not:
	LKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	// Uniforms for Light:
	// Ambient color intensity of light:
	La_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LaRed");
	// Diffuse color intensity of light:
	Ld_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LdRed");
	// Specular color intensity of light:
	Ls_Uniform = glGetUniformLocation(gShaderProgramObject, "u_LsRed");

	La_UniformBlue = glGetUniformLocation(gShaderProgramObject, "u_LaBlue");
	// Diffuse color intensity of light:
	Ld_UniformBlue = glGetUniformLocation(gShaderProgramObject, "u_LdBlue");
	// Specular color intensity of light:
	Ls_UniformBlue = glGetUniformLocation(gShaderProgramObject, "u_LsBlue");
	// Position of light:
	Light_Position_Uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	Light_Position_UniformBlue = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	// Uniforms for material:
	// ambient reflective color intensity of material
	Ka_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_Uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	Material_Shininess_Uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");;


	//Vertices
	const float Pyramid_Vertices[] =
	{
		0.0f, 1.0f, 0.0f,		// Apex
		-1.0f, -1.0f, 1.0f,		// Left bottom
		1.0f, -1.0f, 1.0f,		// Right bottom
		// Perspective triangle (Right face)
		0.0f, 1.0f, 0.0f,		// Apex
		1.0f, -1.0f, 1.0f,		// Left bottom
		1.0f, -1.0f, -1.0f,		// Right bottom
		// Perspective triangle (Back face)
		0.0f, 1.0f, 0.0f,		// Apex
		1.0f, -1.0f, -1.0f,		// Left bottom
		-1.0f, -1.0f, -1.0f,	// Right bottom
		// Perspective triangle (Left face)
		0.0f, 1.0f, 0.0f,		// Apex
		-1.0f, -1.0f, -1.0f,	// Left bottom
		-1.0f, -1.0f, 1.0f		// Right bottom
	};

	const float Pyramid_Color[] =
	{
		0.0f, 0.447214f, 0.894427f,		// Red apex
		0.0f, 0.447214f, 0.894427f,		// Green left bottom
		0.0f, 0.447214f, 0.894427f,		// Blue right bottom

		0.894427f, 0.447214f, 0.0f,		// Red apex
		0.894427f, 0.447214f, 0.0f,		// Blue right bottom
		0.894427f, 0.447214f, 0.0f,		// Green left bottom

		0.0f, 0.447214f, -0.894427f,		// Red apex
		0.0f, 0.447214f, -0.894427f,		// Green left bottom
		0.0f, 0.447214f, -0.894427f,		// Blue right bottom

		-0.894427f, 0.447214f, 0.0f,		// Red apex
		-0.894427f, 0.447214f, 0.0f,		// Blue right bottom
		-0.894427f, 0.447214f, 0.0f,		// Green left bottom

	};
	//Create vao
//Create Traingle
	glGenVertexArrays(1, &vao_Pyramid);
	glBindVertexArray(vao_Pyramid);
	//Pyramid
	//Generate Buffer
	glGenBuffers(1, &vbo_position_Pyramid);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_position_Pyramid);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(Pyramid_Vertices),
		Pyramid_Vertices,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_color_Pyramid);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_color_Pyramid);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(Pyramid_Color),
		Pyramid_Color,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind array
	glBindVertexArray(0);


	//Now from here onward add Frame Buffer Facility
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	PrespectiveGraphicsProjectionMatrix = mat4::identity();

	Resize(WIN_WIDTH, WIN_HEIGHT);
	return 0;
}
//Function Resize
void Resize(int width, int height) 
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	PrespectiveGraphicsProjectionMatrix=perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);

}

//Function Display
void Display(void) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	if (gbLight == true)
	{
		// Set the 'u_lighting_enabled' uniform:
		glUniform1i(LKeyPressedUniform, 1);

		// Set the light's properties:
		glUniform3fv(La_Uniform, 1, lightAmbientRed);
		glUniform3fv(Ld_Uniform, 1, lightDiffuseRed);
		glUniform3fv(Ls_Uniform, 1, lightSpecularRed);
		// Set the light's properties:
		glUniform3fv(La_UniformBlue, 1, lightAmbientBlue);
		glUniform3fv(Ld_UniformBlue, 1, lightDiffuseBlue);
		glUniform3fv(Ls_UniformBlue, 1, lightSpecularBlue);

		glUniform4fv(Light_Position_Uniform, 1, lightPositionRed);
		glUniform4fv(Light_Position_UniformBlue, 1, lightPositionBlue);

		// Set the material's properties:
		glUniform3fv(Ka_Uniform, 1, materialAmbient);
		glUniform3fv(Kd_Uniform, 1, materialDiffuse);
		glUniform3fv(Ks_Uniform, 1, materialSpecular);
		glUniform1f(Material_Shininess_Uniform, materialShininess);
	}
	else
	{
		// Set the 'u_lighting_enabled' uniform:
		glUniform1i(LKeyPressedUniform, 0);
	}


	//Declaration of Matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 TrabslationMatrix;
	mat4 RotationMatrix;
	mat4 ScaleMatrix;
	mat4 modelMatrix;
	mat4 ViewMatrix;
	mat4 ProjectionMatrix;

	//For Cube
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	RotationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	ViewMatrix = mat4::identity();
	ProjectionMatrix = mat4::identity();
	//ScaleMatrix = mat4::identity();

	////Matrix Multiplication

	//ScaleMatrix = scale(0.75f, 0.75f, 0.75f);

	modelViewMatrix = translate(0.0f, 0.0f, -6.0f);

	RotationMatrix = rotate(anglePyramid, 0.0f, 1.0f, 0.0f);
	modelViewMatrix = modelViewMatrix * RotationMatrix;


	modelMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;
	ViewMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;
	ProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(Model_Matrix_Uniform, 1, GL_FALSE, modelViewMatrix);

	glUniformMatrix4fv(View_Matrix_Uniform, 1, GL_FALSE, ViewMatrix);

	glUniformMatrix4fv(Projection_Matrix_Uniform, 1, GL_FALSE, PrespectiveGraphicsProjectionMatrix);


	////Send necessary matrices to shader in resp. Uniforms
	//
	//	
	//	
	//glUniformMatrix4fv(Model_Matrix_Uniform,//Changed
	//	1,
	//	GL_FALSE,
	//	modelMatrix);

	//glUniformMatrix4fv(View_Matrix_Uniform,//Changed
	//	1,
	//	GL_FALSE,
	//	ViewMatrix);

	//glUniformMatrix4fv(Projection_Matrix_Uniform,//Changed
	//	1,
	//	GL_FALSE,
	//	ProjectionMatrix);

	//Bind with vao

	glBindVertexArray(vao_Pyramid);
	//Draw

	glDrawArrays(GL_TRIANGLES, 0, 12);

	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(ghdc);

}


void Update(void)
{
	anglePyramid = anglePyramid + 0.1f;
	if (anglePyramid >= 360.0f)
		anglePyramid = 0.0f;
}



void UnInitialize(void) {
	if (bFullScreen == true) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		//ShowCursor(TRUE);
	}


	if (vbo_position_Pyramid) {
		glDeleteBuffers(1, &vbo_position_Pyramid);
		vbo_position_Pyramid= 0;
	}

	if (vbo_color_Pyramid) {
		glDeleteBuffers(1, &vbo_color_Pyramid);
		vbo_color_Pyramid= 0;
	}

	if (gShaderProgramObject) {
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,
			&shaderCount);

		GLuint * pShaders = (GLuint *)malloc(shaderCount * sizeof(GLuint));

		if (pShaders) {
			glGetAttachedShaders(gShaderProgramObject,
				shaderCount,
				&shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++) {
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (gpFile) {
		fprintf_s(gpFile, "Log File Closed Successfully");
		fclose(gpFile);
		gpFile = NULL;
	}
}



#include <windows.h>
#include <stdio.h>		// For file I/O

#include <gl\glew.h>	// For GLSL extensions. It must be included before <gl\GL.h>
#include <gl\GL.h>

#include "vmath.h"		// Header file from Red Book (For maths) - (v-vermilion)
#include "MSOGLWindow.h"  // Header File Is For BitMap

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

// Properties of the vertex
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
FILE *gpFile = NULL;		// Global pointer, so that log file can be opened anywhere

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint VertexShaderObject;
GLuint FragmentShaderObject;
GLuint ShaderProgramObject;

GLuint Vao_Cube;
GLuint Vbo_Cube_Position;
GLuint Vbo_Cube_Texture;

GLuint MVPUniform;
GLuint TextureSamplerUniform;	// to catch uniform of sampler

GLuint TextureKundali;			// Kundali texture object

mat4 PerspectiveProjectionMatrix;


// For Rotate a  Shapes
bool gbLight;
GLfloat Cube_Angle = 0.0f;

// For InterLeved Array Only
bool bIsLKeyPressed = false;
GLuint modelMatrixUniform = 0;
GLuint viewMatrixUniform = 0;
GLuint projectionMatrixUniform = 0;
GLuint laUniform = 0;
GLuint ldUniform = 0;
GLuint lsUniform = 0;
GLuint kaUniform = 0;
GLuint kdUniform = 0;
GLuint ksUniform = 0;
GLuint materialShininessUniform = 0;
GLuint lightPositionUniform = 0;
GLuint isLightingEnabledUniform = 0;
GLuint textureSamplerUniform = 0;
GLuint textureMarble = 0;

GLfloat lightAmbient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialShininess = 128.0f;


//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	//code
	// Create log file for debugging
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable Pipeline : Multicolored Triangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Initialize
	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Drawing / rendering function
			update();
			display();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// Variable declarations:
	static WORD xMouse = NULL;
	static WORD yMouse = NULL;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)		// If the window is active
			gbActiveWindow = true;
		else							// If non-zero, the window is inactive
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x4c:
			if (bIsLKeyPressed == false) 
			{
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else 
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;
		case 0x46:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);
	int LoadGLTextures(GLuint *, TCHAR[]);


	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// GLEW initializtion code for GLSL.
	// It must be here, ie, after creating OpenGL context but before using and OpenGL functions
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}


	// *** VERTEX SHADER ***
	// 1. Create the shader (object is created and assigned)
	VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	//fprintf(gpFile, "Vertex Shader Is Created.\n");

	// vPosition and u_mvp_matrix are user defined names for matrices for transformation
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vertexPosition;" \
		"in vec3 vertexNormal;" \
		"in vec4 vertexColor;" \
		"in vec2 vertexTextureCoordinate0;" \
		"\n" \
		"out vec4 outVertexColor;" \
		"out vec3 tNormal;" \
		"out vec3 source;" \
		"out vec3 viewVector;" \
		"out vec2 outVertexTextureCoordinate0;" \
		"\n" \
		"uniform mat4 modelMatrix;" \
		"uniform mat4 viewMatrix;" \
		"uniform mat4 projectionMatrix;" \
		"uniform vec4 lightPosition;" \
		"uniform int isLightingEnabled;" \
		"\n" \
		"void main(void)" \
		"{" \
		"   if(isLightingEnabled == 1)" \
		"   {" \
		"       vec4 eyeCoordinates = viewMatrix * modelMatrix * vertexPosition;" \
		"       tNormal = mat3(viewMatrix * modelMatrix) * vertexNormal;" \
		"       source = vec3(lightPosition) - eyeCoordinates.xyz;" \
		"       viewVector = -eyeCoordinates.xyz;" \
		"   }" \
		"\n" \
		"   gl_Position = projectionMatrix * viewMatrix * modelMatrix * vertexPosition;" \
		"   outVertexColor = vertexColor;" \
		"   outVertexTextureCoordinate0 = vertexTextureCoordinate0;" \
		"}";


	// (const GLchar **) - Pass the address of the program by casting (as array is used)
	glShaderSource(VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	//fprintf(gpFile, "In VertexShaderSource.\n");

	// 4. Compile the shader - Dynamic compilation by the driver
	glCompileShader(VertexShaderObject);
	//fprintf(gpFile, "In Vertex Shader Compiled.\n");

	// ERROR CHECKING:
	GLint iInfoLogLength = 0;			// Length of the log created on failure to compile
	GLint iShaderCompiledStatus = 0;	// Stores shader compilation status
	char *szInfoLog = NULL;				// String to store the log

	glGetShaderiv(VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// Create the shader
	FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	fprintf(gpFile, "Fragment Shader Is Created.\n");

	// Pass the source code to shader
	// FragColor is our name for the Fragment
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 outVertexColor;" \
		"in vec3 tNormal;" \
		"in vec3 source;" \
		"in vec3 viewVector;" \
		"in vec2 outVertexTextureCoordinate0;" \
		"\n" \
		"out vec4 fragmentColor;" \
		"\n" \
		"uniform int isLightingEnabled;" \
		"uniform vec3 la;" \
		"uniform vec3 ld;" \
		"uniform vec3 ls;" \
		"uniform vec3 ka;" \
		"uniform vec3 kd;" \
		"uniform vec3 ks;" \
		"uniform float materialShininess;" \
		"\n" \
		"uniform sampler2D textureSampler0;"
		"\n" \
		"void main(void)" \
		"{" \
		"   vec3 phongAdsColor;" \
		"   if(isLightingEnabled == 1)" \
		"   {" \
		"       vec3 normalizedTNormal = normalize(tNormal);" \
		"       vec3 normalizedSource = normalize(source);" \
		"       vec3 normalizedViewVector = normalize(viewVector);" \
		"       float tNormalDotLightDirection = max(dot(normalizedTNormal, normalizedSource), 0.0);" \
		"       vec3 ambient = la * ka;" \
		"       vec3 diffuse = ld * kd * tNormalDotLightDirection;" \
		"       vec3 reflectionVector = reflect(-normalizedSource, normalizedTNormal);" \
		"       vec3 specular = ls * ks * pow(max(dot(reflectionVector, normalizedViewVector), 0.0), materialShininess);" \
		"       phongAdsColor = ambient + diffuse + specular;"
		"   }" \
		"   else" \
		"   {" \
		"       phongAdsColor = vec3(1.0, 1.0, 1.0);" \
		"   }" \
		"\n" \
		"   fragmentColor = texture(textureSampler0, outVertexTextureCoordinate0) * outVertexColor * vec4(phongAdsColor, 1.0);" \
		"}";

	glShaderSource(FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	//fprintf(gpFile, "Fragment Shader Source .\n");

	// compiler the shader
	glCompileShader(FragmentShaderObject);
	//fprintf(gpFile, "Fragment Shader Compiled .\n");

	// Error checking for compilation done here
	glGetShaderiv(FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	//fprintf(gpFile, "Get IV Shader is Success .\n");

	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	//				*** SHADER PROGRAM ***
	// Create
	ShaderProgramObject = glCreateProgram();		
	glAttachShader(ShaderProgramObject, VertexShaderObject);

	//It can link ALL the shaders. Hence, no parameter
	// Attach fragment shader to the shader program
	glAttachShader(ShaderProgramObject, FragmentShaderObject);
	//fprintf(gpFile, "Attechted The Fragment Shader To Shader Program Code .\n");

	// This shader object is recognized by out YSG_ATTRIBUTE_POSITION
	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vertexNormal");
	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vertexColor");
	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUTE_TEXTURE0, "vertexTextureCoordinate0");

	// Link the shader
	glLinkProgram(ShaderProgramObject);

	// Error checking for linking
	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(ShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)	
	{
		glGetProgramiv(ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		
				glGetProgramInfoLog(ShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program shader link log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// Get MVP uniform location
	modelMatrixUniform = glGetUniformLocation(ShaderProgramObject, "modelMatrix");
	viewMatrixUniform = glGetUniformLocation(ShaderProgramObject, "viewMatrix");
	projectionMatrixUniform = glGetUniformLocation(ShaderProgramObject, "projectionMatrix");

	isLightingEnabledUniform = glGetUniformLocation(ShaderProgramObject, "isLightingEnabled");

	laUniform = glGetUniformLocation(ShaderProgramObject, "la");
	ldUniform = glGetUniformLocation(ShaderProgramObject, "ld");
	lsUniform = glGetUniformLocation(ShaderProgramObject, "ls");
	kaUniform = glGetUniformLocation(ShaderProgramObject, "ka");
	kdUniform = glGetUniformLocation(ShaderProgramObject, "kd");
	ksUniform = glGetUniformLocation(ShaderProgramObject, "ks");
	lightPositionUniform = glGetUniformLocation(ShaderProgramObject, "lightPosition");
	materialShininessUniform = glGetUniformLocation(ShaderProgramObject, "materialShininess");

	// Vertices, colors, shader attributes, vbo, vao initializations
	const GLfloat rectangleVertices[] =
	{
		-1.0f, 1.0f, 1.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 0.0f, 0.0f,
		-1.0f, -1.0f, 1.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 0.0f,1.0f,0.0f, 1.0f,0.0f,0.0f, 0.0f, 0.0f,
		1.0f, -1.0f, 1.0f, 0.0f,1.0f,0.0f, 1.0f,0.0f,0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f, 0.0f,1.0f,0.0f, 1.0f,0.0f,0.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f, 0.0f,1.0f,0.0f, 1.0f,0.0f,0.0f, 0.0f, 1.0f,
		1.0f, 1.0f, -1.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f,-1.0f, 0.0f, 0.0f,
		1.0f, -1.0f, -1.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f,-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f,-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f,-1.0f, 0.0f, 1.0f,
		-1.0f, 1.0f, -1.0f, 0.0f,1.0f,1.0f, -1.0f,0.0f,0.0f, 0.0f, 0.0f,
		-1.0f, -1.0f, -1.0f, 0.0f,1.0f,1.0f, -1.0f,0.0f,0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f, 0.0f,1.0f,1.0f, -1.0f,0.0f,0.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f, 0.0f,1.0f,1.0f, -1.0f,0.0f,0.0f, 0.0f, 1.0f,
		-1.0f, 1.0f, -1.0f, 1.0f,0.5f,0.0f, 0.0f,1.0f,0.0f, 0.0f, 0.0f,
		-1.0f, 1.0f, 1.0f, 1.0f,0.5f,0.0f, 0.0f,1.0f,0.0f,1.0f, 0.0f,
		1.0f, 1.0f, 1.0f, 1.0f,0.5f,0.0f, 0.0f,1.0f,0.0f, 	1.0f, 1.0f,
		1.0f, 1.0f, -1.0f, 1.0f,0.5f,0.0f, 0.0f,1.0f,0.0f, 0.0f, 1.0f,
		-1.0f, -1.0f, -1.0f, 0.0f,0.7f,1.0f, 0.0f,-1.0f,0.0f, 0.0f, 0.0f,
		-1.0f, -1.0f, 1.0f, 0.0f,0.7f,1.0f, 0.0f,-1.0f,0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f, 0.0f,0.7f,1.0f, 0.0f,-1.0f,0.0f, 1.0f, 1.0f,
		1.0f, -1.0f, -1.0f, 0.0f,0.7f,1.0f, 0.0f,-1.0f,0.0f, 0.0f, 1.0f
	};
	glGenVertexArrays(1, &Vao_Cube);
	glBindVertexArray(Vao_Cube);

	// A. BUFFER BLOCK FOR VERTICES:
	glGenBuffers(1, &Vbo_Cube_Position);				
	glBindBuffer(GL_ARRAY_BUFFER, Vbo_Cube_Position);
	glBufferData(GL_ARRAY_BUFFER, 24 * 11 * sizeof(float), rectangleVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);


	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(9 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);

	// Release the buffer for vertices:
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);				// Unbind 


	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	//call the LoadTexture Function
	LoadGLTextures(&TextureKundali, MAKEINTRESOURCE(IDBITMAP_KUNDALI));

	// Set the background color to black:
	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

	// Set PerspectiveMatrix to identity matrix
	PerspectiveProjectionMatrix = mat4::identity();

	// Warm up call. (Not required for Windows as resize is called before WM_PAINT
	resize(WIN_WIDTH, WIN_HEIGHT);
}

int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[])
{
	//variable declarations
	HBITMAP hBitmap = NULL;
	BITMAP bmp;
	int iStatus = FALSE;

	//code
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	glGenTextures(1, texture);

	if (hBitmap)
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);	
	
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		//Generate mipmapped texture
		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			bmp.bmWidth,
			bmp.bmHeight,
			0,
			GL_BGR,
			GL_UNSIGNED_BYTE,
			bmp.bmBits);

		// Create mipmaps for this texture for better image quality:
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap);
	}
	return(iStatus);
}

void update(void)
{
	Cube_Angle = Cube_Angle - 0.1f;
	if (Cube_Angle <= -360.0f)
		Cube_Angle = 0.0f;
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	fprintf(gpFile, "Enter a Display is Sucess.\n");

	// Start to use the OpenGL program object
	glUseProgram(ShaderProgramObject);		

	if (gbLight == true) 
	{	
		glUniform1i(isLightingEnabledUniform, 1);
		//setting light's properties
		glUniform3fv(laUniform, 1, lightAmbient);
		glUniform3fv(ldUniform, 1, lightDiffuse);
		glUniform3fv(lsUniform, 1, lightSpecular);

		glUniform4fv(lightPositionUniform, 1, lightPosition);
		
		//set material properties
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);
		glUniform1f(materialShininessUniform, materialShininess);

		//setting materia properties
	}
	else {
		glUniform1i(isLightingEnabledUniform, 0);
	}

	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	modelMatrix = translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = rotate(Cube_Angle, 1.0f, 0.0f, 0.0f);
	rotationMatrix *= rotate(Cube_Angle, 0.0f, 1.0f, 0.0f);
	rotationMatrix *= rotate(Cube_Angle, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	// Pass above matrices to shaders
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, PerspectiveProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);					
	glBindTexture(GL_TEXTURE_2D, TextureKundali);	
	glUniform1i(TextureSamplerUniform, 0);			


	// BIND vao
	glBindVertexArray(Vao_Cube);

	// Draw either by glDrawTraingles() or glDrawArrays() or glDrawElements()
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);	
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);	
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);	
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);	
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);	

	glBindVertexArray(0);


	// Stop using the OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}
	
void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	// glPerspective(FOV, aspect ratio, near, far)
	PerspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	if (TextureKundali)
	{
		glDeleteTextures(1, &TextureKundali);
		TextureKundali = 0;
	}

	// Destroy Rectangle
	if (Vao_Cube)
	{
		glDeleteVertexArrays(1, &Vao_Cube);
		Vao_Cube = 0;
	}

	// Destroy vbo for vertices:
	if (Vbo_Cube_Position)
	{
		glDeleteBuffers(1, &Vbo_Cube_Position);
		Vbo_Cube_Position = 0;
	}

	// Destroy vbo for colors:
	if (Vbo_Cube_Texture)
	{
		glDeleteBuffers(1, &Vbo_Cube_Texture);
		Vbo_Cube_Texture = 0;
	}

	// Detach vertex shader from shader program object
	glDetachShader(ShaderProgramObject, VertexShaderObject);
	// Detach fragment shader from shader program object
	glDetachShader(ShaderProgramObject, FragmentShaderObject);

	// Delete vertex shader object
	glDeleteShader(VertexShaderObject);
	VertexShaderObject = 0;
	// Delete fragment shader object
	glDeleteShader(FragmentShaderObject);
	FragmentShaderObject = 0;

	// Delete shader program object. It has no objects attached
	glDeleteProgram(ShaderProgramObject);
	ShaderProgramObject = 0;

	// Unlink shader program
	glUseProgram(0);		// Stray call

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)			// Closing the log file here
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}



#include <windows.h>
#include <stdio.h>		// For file I/O

#include <gl\glew.h>	// For GLSL extensions. It must be included before <gl\GL.h>
#include <gl\GL.h>

#include "vmath.h"		// Header file from Red Book (For maths) - (v-vermilion)
#include "MSOGLWindow.h"  // Header File Is For BitMap

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define checkImageWidth 64
#define checkImageHeight 64

using namespace vmath;

// Properties of the vertex
enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0
};

int isKeyPressed = 0;
GLfloat smileyTextureVertices[8];
//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable declarations
FILE *gpFile = NULL;		// Global pointer, so that log file can be opened anywhere

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gVao_Twicked_Smily;

GLuint gVbo_Smily_Twicked_Position;
GLuint gVbo_Smily_Twicked_Texture;

GLuint gMVPUniform;
GLuint gTextureSamplerUniform;	// to catch uniform of sampler

GLuint texture_Smiley_Tweaked;
GLuint texture;
GLubyte checkImage[checkImageHeight][checkImageWidth][4];


mat4 gPerspectiveProjectionMatrix;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	//code
	// Create log file for debugging
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Programmable Pipeline : Multicolored Triangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Initialize
	initialize();

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Drawing / rendering function
			display();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

//WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// Variable declarations:
	static WORD xMouse = NULL;
	static WORD yMouse = NULL;

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)		// If the window is active
			gbActiveWindow = true;
		else							// If non-zero, the window is inactive
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x31:
			isKeyPressed = 1;
			break;
		case 0x32:
			isKeyPressed = 2;
			break;
		case 0x33:
			isKeyPressed = 3;
			break;
		case 0x34:
			isKeyPressed = 4;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void uninitialize(void);
	void resize(int, int);
	int LoadGLTextures(GLuint *, TCHAR[]);


	//variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// GLEW initializtion code for GLSL.
	// It must be here, ie, after creating OpenGL context but before using and OpenGL functions
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}


	// *** VERTEX SHADER ***
	// 1. Create the shader (object is created and assigned)
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	//fprintf(gpFile, "Vertex Shader Is Created.\n");

	// vPosition and u_mvp_matrix are user defined names for matrices for transformation
	const GLchar *vertexShaderSourceCode =
		"#version 330 core"	\
		"\n"	\
		"in vec4 vPosition;"	\
		"in vec2 vTexture0_Coord;"	\
		"out vec2 out_texture0_coord;"	\
		"uniform mat4 u_mvp_matrix;"	\
		"void main(void)"	\
		"{"	\
		"gl_Position = u_mvp_matrix * vPosition;"
		"out_texture0_coord = vTexture0_Coord;"	\
		"}";
	//fprintf(gpFile, "In Vertex ShaderCode.\n");

	// (const GLchar **) - Pass the address of the program by casting (as array is used)
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	//fprintf(gpFile, "In VertexShaderSource.\n");

	// 4. Compile the shader - Dynamic compilation by the driver
	glCompileShader(gVertexShaderObject);
	//fprintf(gpFile, "In Vertex Shader Compiled.\n");

	// ERROR CHECKING:
	GLint iInfoLogLength = 0;			// Length of the log created on failure to compile
	GLint iShaderCompiledStatus = 0;	// Stores shader compilation status
	char *szInfoLog = NULL;				// String to store the log

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// Create the shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	fprintf(gpFile, "Fragment Shader Is Created.\n");

	// Pass the source code to shader
	// FragColor is our name for the Fragment
	const GLchar *fragmentShaderSourceCode =
		"#version 330 core"	\
		"\n"	\
		"in vec2 out_texture0_coord;"
		"out vec4 FragColor;"	\
		"uniform sampler2D u_texture0_sampler;"
		"void main(void)"	\
		"{"	\
		"FragColor = texture(u_texture0_sampler, out_texture0_coord);"	\
		"}";
	//fprintf(gpFile, "Fragment Shader Source Code.\n");

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	//fprintf(gpFile, "Fragment Shader Source .\n");

	// compiler the shader
	glCompileShader(gFragmentShaderObject);
	//fprintf(gpFile, "Fragment Shader Compiled .\n");

	// Error checking for compilation done here
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	//fprintf(gpFile, "Get IV Shader is Success .\n");

	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	//				*** SHADER PROGRAM ***
	// Create
	gShaderProgramObject = glCreateProgram();		
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	//It can link ALL the shaders. Hence, no parameter
	// Attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);
	//fprintf(gpFile, "Attechted The Fragment Shader To Shader Program Code .\n");

	// This shader object is recognized by out YSG_ATTRIBUTE_POSITION
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	// This shader object is recognized by out YSG_ATTRIBUTE_TEXTURE0
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");

	// Link the shader
	glLinkProgram(gShaderProgramObject);

	// Error checking for linking
	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)	
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program shader link log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// Get MVP uniform location
	gMVPUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	gTextureSamplerUniform = glGetUniformLocation(gShaderProgramObject, "u_texture0_sampler");

	// Vertices, colors, shader attributes, vbo, vao initializations

	const GLfloat Smily_Vertices[] =
	{
		-1.0f, -1.0f, 0.0f,		// left-bottom
		-1.0f, 1.0f, 0.0f,		// left-top
		1.0f, 1.0f, 0.0f,		// right-top
		1.0f, -1.0f, 0.0f		// right-bottom
	};


	glGenVertexArrays(1, &gVao_Twicked_Smily);
	glBindVertexArray(gVao_Twicked_Smily);
	fprintf(gpFile, "Genrete And Bind Pyramid Vao is Success .\n");

	// A. BUFFER BLOCK FOR VERTICES:
	glGenBuffers(1, &gVbo_Smily_Twicked_Position);				
	fprintf(gpFile, "Genrete Pyramid Position is Success .\n");
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Smily_Twicked_Position);

	glBufferData(GL_ARRAY_BUFFER, sizeof(Smily_Vertices), Smily_Vertices, GL_STATIC_DRAW);
	fprintf(gpFile, "Bind Pyramid Position is Success .\n");

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	// Release the buffer for vertices:
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &gVbo_Smily_Twicked_Texture);					

	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Smily_Twicked_Texture);
	glBufferData(GL_ARRAY_BUFFER, 4*2*sizeof(GL_FLOAT), NULL, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXTURE0);

	// Release the buffer for colors:
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);				

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	//call the LoadTexture Function
	LoadGLTextures(&texture_Smiley_Tweaked, MAKEINTRESOURCE(IDBITMAP_SMILEY));

	// Set the background color to black:
	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

	// Set PerspectiveMatrix to identity matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	// Warm up call. (Not required for Windows as resize is called before WM_PAINT
	resize(WIN_WIDTH, WIN_HEIGHT);
}

int LoadGLTextures(GLuint *texture, TCHAR imageResourceId[])
{
	//variable declarations
	HBITMAP hBitmap = NULL;
	BITMAP bmp;
	int iStatus = FALSE;

	//code
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageResourceId, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	glGenTextures(1, texture);

	if (hBitmap)
	{
		iStatus = TRUE;
		GetObject(hBitmap, sizeof(bmp), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);	// Set 1 rather than default 4 for better performance

												//Texture steps:
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		//Generate mipmapped texture
		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB,
			bmp.bmWidth,
			bmp.bmHeight,
			0,
			GL_BGR,
			GL_UNSIGNED_BYTE,
			bmp.bmBits);

		// Create mipmaps for this texture for better image quality:
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(hBitmap);
	}
	return(iStatus);
}


void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Start to use the OpenGL program object
	glUseProgram(gShaderProgramObject);		

	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();
		
	// Translate the modelViewMatrix along the z axis
	modelViewMatrix = translate(0.0f, 0.0f, -4.0f);

	// Multiply the ModelView and Perspective Matrix to get modelviewprojection matrix
	modelViewProjectionMatrix = gPerspectiveProjectionMatrix * modelViewMatrix;	

	glUniformMatrix4fv(gMVPUniform, 1, GL_FALSE, modelViewProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);					
	glBindTexture(GL_TEXTURE_2D, texture_Smiley_Tweaked);	
	glUniform1i(gTextureSamplerUniform, 0);
	//glEnable(GL_TEXTURE_2D);

	glBindVertexArray(gVao_Twicked_Smily);

	smileyTextureVertices[0] = 0.0f;
	smileyTextureVertices[1] = 0.0f;
	smileyTextureVertices[2] = 0.0f;
	smileyTextureVertices[3] = 1.0f;
	smileyTextureVertices[4] = 1.0f;
	smileyTextureVertices[5] = 1.0f;
	smileyTextureVertices[6] = 1.0f;
	smileyTextureVertices[7] = 0.0f;


	if (isKeyPressed == 1)
	{
		// BIND with quad Texture (3 steps):
		glActiveTexture(GL_TEXTURE0);								// 1. 0th texture (corresponds to YSG_ATTRIBUTE_TEXTURE0)
		glBindTexture(GL_TEXTURE_2D, texture_Smiley_Tweaked);			// 2. bind to the texture
		glUniform1i(gTextureSamplerUniform, 0);						// 3. 0th sampler enabled (as we have only 1 texture sampler in frag shader

		smileyTextureVertices[0] = 0.0f;
		smileyTextureVertices[1] = 0.0f;
		smileyTextureVertices[2] = 0.0f;
		smileyTextureVertices[3] = 0.5f;
		smileyTextureVertices[4] = 0.5f;
		smileyTextureVertices[5] = 0.5;
		smileyTextureVertices[6] = 0.5;
		smileyTextureVertices[7] = 0.0f;
	}
	else if (isKeyPressed == 2)
	{
		// BIND with quad Texture (3 steps):
		glActiveTexture(GL_TEXTURE0);								// 1. 0th texture (corresponds to YSG_ATTRIBUTE_TEXTURE0)
		glBindTexture(GL_TEXTURE_2D, texture_Smiley_Tweaked);			// 2. bind to the texture
		glUniform1i(gTextureSamplerUniform, 0);						// 3. 0th sampler enabled (as we have only 1 texture sampler in frag shader

		smileyTextureVertices[0] = 0.0f;
		smileyTextureVertices[1] = 0.0f;
		smileyTextureVertices[2] = 0.0f;
		smileyTextureVertices[3] = 1.0f;
		smileyTextureVertices[4] = 1.0f;
		smileyTextureVertices[5] = 1.0f;
		smileyTextureVertices[6] = 1.0f;
		smileyTextureVertices[7] = 0.0f;
	}
	else if (isKeyPressed == 3)
	{
		// BIND with quad Texture (3 steps):
		glActiveTexture(GL_TEXTURE0);								// 1. 0th texture (corresponds to YSG_ATTRIBUTE_TEXTURE0)
		glBindTexture(GL_TEXTURE_2D, texture_Smiley_Tweaked);			// 2. bind to the texture
		glUniform1i(gTextureSamplerUniform, 0);						// 3. 0th sampler enabled (as we have only 1 texture sampler in frag shader

		smileyTextureVertices[0] = 0.0f;
		smileyTextureVertices[1] = 0.0f;
		smileyTextureVertices[2] = 0.0f;
		smileyTextureVertices[3] = 2.0f;
		smileyTextureVertices[4] = 2.0f;
		smileyTextureVertices[5] = 2.0f;
		smileyTextureVertices[6] = 2.0f;
		smileyTextureVertices[7] = 0.0f;
	}
	else if (isKeyPressed == 4)
	{
		// BIND with quad Texture (3 steps):
		glActiveTexture(GL_TEXTURE0);								// 1. 0th texture (corresponds to YSG_ATTRIBUTE_TEXTURE0)
		glBindTexture(GL_TEXTURE_2D, texture_Smiley_Tweaked);			// 2. bind to the texture
		glUniform1i(gTextureSamplerUniform, 0);						// 3. 0th sampler enabled (as we have only 1 texture sampler in frag shader

		smileyTextureVertices[0] = 0.5f;
		smileyTextureVertices[1] = 0.5f;
		smileyTextureVertices[2] = 0.5;
		smileyTextureVertices[3] = 0.5f;
		smileyTextureVertices[4] = 0.5f;
		smileyTextureVertices[5] = 0.5f;
		smileyTextureVertices[6] = 0.5f;
		smileyTextureVertices[7] = 0.5;
	}

	glBindBuffer(GL_ARRAY_BUFFER,  gVbo_Smily_Twicked_Texture);						// Find that named object in memory
	glBufferData(GL_ARRAY_BUFFER, sizeof(smileyTextureVertices), smileyTextureVertices, GL_DYNAMIC_DRAW);	// Takes data from CPU to GPU
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Draw either by glDrawTraingles() or glDrawArrays() or glDrawElements()
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	// 3(x, y, z) vertices in the smileyVertices array


	glBindVertexArray(0);

	// Stop using the OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}
	
void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	// glPerspective(FOV, aspect ratio, near, far)
	gPerspectiveProjectionMatrix = perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	// Destroy vao
	if (gVao_Twicked_Smily)
	{
		glDeleteVertexArrays(1, &gVao_Twicked_Smily);
		gVao_Twicked_Smily = 0;
	}

	// Destroy vbo for vertices:
	if (gVbo_Smily_Twicked_Position)
	{
		glDeleteBuffers(1, &gVbo_Smily_Twicked_Position);
		gVbo_Smily_Twicked_Position = 0;
	}

	if (gVbo_Smily_Twicked_Texture)
	{
		glDeleteBuffers(1, &gVbo_Smily_Twicked_Texture);
		gVbo_Smily_Twicked_Texture = 0;
	}

	if (texture_Smiley_Tweaked)
	{
		glDeleteTextures(1, &texture_Smiley_Tweaked);
		texture_Smiley_Tweaked = 0;
	}

	// Destroy vbo for colors:
	
	// Detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// Detach fragment shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// Delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	// Delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// Delete shader program object. It has no objects attached
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	// Unlink shader program
	glUseProgram(0);		// Stray call

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);
	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)			// Closing the log file here
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}


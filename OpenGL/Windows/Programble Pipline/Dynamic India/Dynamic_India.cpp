#include<Windows.h>
#include<stdio.h>

#include<gl\glew.h>
#include<gl\GL.h>

#include"vmath.h"
#include"resource.h"
#pragma comment(lib,"Winmm.lib")


#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"glew32.lib")

//Constants
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Vmath namespace
using namespace vmath;
// Line Variables
const GLint iLinePoints = 2;

//Global Variables
HWND ghwnd = NULL;
DWORD dwStyle;
bool bFullScreen = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;

GLfloat anglePyramid = 0.0;
GLfloat angleCube = 0.0;

//Shader Program Objects
GLint gShaderProgramObject;

//Addition
GLuint mvpUniform;
GLuint samplerUniform;

int windowWidth;
int windowHeight;

enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint vao_FORI;
GLuint vbo_FORI_POSITION;
GLuint vbo_FORI_COLOR;
GLuint vao_FORN;
GLuint vbo_FORN_CROSS_POSITION;
GLuint vbo_FORN_CROSS_COLOR;
GLuint vao_FORD_UP;
GLuint vbo_FORD_UP_POSITION;
GLuint vbo_FORD_UP_COLOR;
GLuint vao_FORD_DOWN;
GLuint vbo_FORD_DOWN_POSITION;
GLuint vbo_FORD_DOWN_COLOR;
GLuint vao_FORA;
GLuint vbo_FORA_POSITION;
GLuint vbo_FORA_COLOR;
GLuint vao_FORA_CENTER;
GLuint vbo_FORA_POSITION_CENTER;
GLuint vbo_FORA_COLOR_CENTER;
GLuint vao_AIROPLAIN;
GLuint vbo_AIROPLAIN;
GLuint vbo_COLOR_AIROPLAIN;
void DrawAiroplain(void);

// For Animation To Dynamic India
GLfloat x_For_fi = 2.0f;
GLfloat x_For_fsi = 8.6f;
GLfloat a_For_fn = 5.3f;
GLfloat y_For_fa = 4.4f;
GLfloat ForWhite = 10.6f;
GLfloat ForOrange = 10.6f;
GLfloat ForGreen = 10.6f;
GLfloat Colr_D = 0.0f;

//AIROPLAIN
// Animating Variables Plane
GLfloat Airoplain_Upper_X = -4.0f;
GLfloat Airoplain_Upper_Y = 3.0f;
GLfloat Airoplain_Middle_X = -4.0f;
GLfloat Airoplain_Middle_Y = 0.0f;
GLfloat Airoplain_Lower_X = -4.0f;
GLfloat Airoplain_Lower_Y = -3.0f;

GLfloat gfUFlagOX = -1.0f;
GLfloat gfUFlagOY = 0.03f;
GLfloat gfUFlagWX = -1.0f;
GLfloat gfUFlagWY = 0.0f;
GLfloat gfUFlagGX = -1.0f;
GLfloat gfUFlagGY = -0.03f;

GLfloat gfMFlagOX = -1.0f;
GLfloat gfMFlagOY = 0.03f;
GLfloat gfMFlagWX = -1.0f;
GLfloat gfMFlagWY = 0.0f;
GLfloat gfMFlagGX = -1.0f;
GLfloat gfMFlagGY = -0.03f;

GLfloat gfLFlagOX = -1.0f;
GLfloat gfLFlagOY = 0.03f;
GLfloat gfLFlagWX = -1.0f;
GLfloat gfLFlagWY = 0.0f;
GLfloat gfLFlagGX = -1.0f;
GLfloat gfLFlagGY = -0.03f;
GLfloat Flag = 0.0f;


GLfloat plainDown = 4.0f;

mat4 PrespectiveGraphicsProjectionMatrix;

//Method Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
//void PrintTime();
//WinMain Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevinstance, LPSTR lpszCmdLine, int iCmdShow) {
	//Method Declaration
	int Initialize(void);
	void Display(void);
	void Update(void);
	//Variable Declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("OGL Window");
	int iRet = 0;
	bool bDone = false;
	//Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can't Be Created"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else {
		//PrintTime();
		fprintf_s(gpFile, "Log File Created Successfully\n");
	}
	wndclass.cbClsExtra = 0;
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbWndExtra = 0;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Ortho Graphics Traingle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;
	iRet = Initialize();
	if (iRet == -1) {
		fprintf_s(gpFile, "Choose Pixel Format Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -2) {
		fprintf_s(gpFile, "Set Pixel Format Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -3) {
		fprintf_s(gpFile, "wgl Create Context Failed\n");
		DestroyWindow(0);
	}
	else if (iRet == -4) {
		fprintf_s(gpFile, "wgl Make Current Failed\n");
		DestroyWindow(0);
	}
	else {
		fprintf_s(gpFile, "Initialization Succeeded\n");
	}
	ShowWindow(hwnd, iCmdShow);
	//Do not call update window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	//Game Loop
	//SendMessage(hwnd, WM_PAINT, 0, 0);
	while (bDone == false) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			if (gbActiveWindow == true) {
			}
			Update();
			Display();
			//Here Call Display
		}
	}
	return (int)msg.wParam;
}
//WndProc Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	//Function Declaration
	void ToggleFullScreen(void);
	void Resize(int, int);
	void UnInitialize(void);
	void Update(void);
	//Code
	switch (iMsg)
	{
	case WM_CREATE:
		PlaySound(MAKEINTRESOURCE(IDR_WAVE1), NULL, SND_RESOURCE | SND_ASYNC);
		break;
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_SIZE:
		windowWidth = LOWORD(lParam);
		windowHeight = HIWORD(lParam);
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(0);
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_DESTROY:
		UnInitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
//ToggleFullScreen Function
void ToggleFullScreen(void) {
	//Variable Declaration
	MONITORINFO mi;
	if (bFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				bFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}
//Initialize OpenGl
int Initialize(void) {
	//Function Declaration
	void Resize(int, int);
	void UnInitialize(void);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;
	//Code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW |
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	ghdc = GetDC(ghwnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) {
		return -1;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return -2;
	}
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL) {
		return -3;
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE) {
		return -4;
	}

	result = glewInit();
	if (result != GLEW_OK) {
		fprintf_s(gpFile, "glewInit() failed\n");
		UnInitialize();
		DestroyWindow(0);
	}

	//Addition
	GLint gVertexShaderObject;
	GLint gFragementShaderObject;

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Vertex Shader Code
	const GLchar* vertextShaderSourceCode =
		"#version 440 core " \
		"\n " \
		"in vec4 vPosition;"	\
		"in vec4 vColor;"	\
		"out vec4 out_color;"	\
		"uniform mat4 u_mvp_matrix;"	\
		"void main(void)"	\
		"{"	\
		"gl_Position = u_mvp_matrix * vPosition;"
		"out_color = vColor;"	\
		"}";

	//"in vec4 vColor;" \
	//"out vec4 out_color;" \
		//"out_color=vColor;"\

	//Shader object and source code mapping
	glShaderSource(gVertexShaderObject, 1, &vertextShaderSourceCode, NULL);

	//Compile Shader
	glCompileShader(gVertexShaderObject);

	//Error Checking
	GLint iShaderCompileStatusN = 0;
	GLint iInfoLogLength = 0;
	GLchar * szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatusN);

	if (iShaderCompileStatusN == GL_FALSE) {
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf_s(gpFile, "Vertex Shader Compilation Log %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//Fragement Shader
	//Define Shader Object
	gFragementShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Fragement Shader Code
	const GLchar* fragementShaderSourceCode =
		"#version 440 core " \
		"\n " \
		"in vec4 out_color;"
		"out vec4 FragColor;"
		"void main(void)" \
		"{" \
		"FragColor = out_color;"	\
		"} ";


	//"in vec4 out_color;"\
	//"fragColor=out_color;" \
		
	//Shader object and source code mapping
	glShaderSource(gFragementShaderObject, 1, &fragementShaderSourceCode, NULL);

	//Compile Shader
	glCompileShader(gFragementShaderObject);

	//Error Checking
	iShaderCompileStatusN = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(gFragementShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatusN);

	if (iShaderCompileStatusN == GL_FALSE) {
		glGetShaderiv(gFragementShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gFragementShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf_s(gpFile, "Fragement Shader Compilation Log %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//Program Object
	gShaderProgramObject = glCreateProgram();
	//Attach Shaders
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragementShaderObject);

		//Link Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error Checking
	GLint iShaderLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderLinkStatus);

	if (iShaderLinkStatus == GL_FALSE) {
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf_s(gpFile, "Program Link Log %s\n", szInfoLog);
				free(szInfoLog);
				UnInitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	//Post Linking
	//Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject,
		"u_mvp_matrix");
	//mvpUniform = glGetUniformLocation(gShaderProgramObject,
	//	"u_mvp_matrix");

	//Vertices
	const float ForI_Vertices[] =
	{
		-1.0f, 1.0f,0.0,
		-1.0f, -1.0f,0.0
	};

	const float ForI_Color[] =
	{
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f
	};

	const float ForN_Vertices[] =
	{
		-0.5, -1.0, 0.0,		// Apex
		-0.5, 1.0, 0.0,		// Let bottom
		-0.5, 1.0, 0.0,		// Let bottom
		0.5, -1.0, 0.0,		// Apex
		0.5, -1.0, 0.0,
		0.5, 1.0, 0.0
	};

	const float ForN_Color[] =
	{
		0.0, 1.0, 0.0,		// Apex
		1.0, 0.0, 0.0,		// Let bottom
		1.0, .0, 0.0,		// Apex
		0.0, 1.0, 0.0,		// Let bottom
		0.0, 1.0, 0.0,		// Apex
		1.0, 0.0, 0.0 
	};

	const float ForD_Vertices[] =
	{
		-0.5, 1.0, 0.0,		// Apex
		0.5, 1.0, 0.0,		// Let bottom
		0.5, 1.0, 0.0,		// Let bottom
		0.5, -1.0, 0.0,		// Apex
		0.5, -1.0, 0.0,
		-0.5, -1.0, 0.0,
		-0.4, -1.0, 0.0,
		-0.4, 1.0, 0.0 };
		
	const float ForA_Vertices[]=
	{
		0.5f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		-0.5f, -1.0f, 0.0f
	};

	const float ForA_Color[] =
	{
		0.0f, 1.0f, 0.0f,
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		
	};
	//******************************************** CENTER
	const float ForA_Center_Color[] =
	{
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f
	};

	const float Orange_Color[] =
	{
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0
	};

	const float Green_Color[] =
	{
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f
	};

	//Create vao
	//For First "I"  
	glGenVertexArrays(1, &vao_FORI);
	glBindVertexArray(vao_FORI);

	//For First "I" Vetices
	//Generate Buffer
	glGenBuffers(1, &vbo_FORI_POSITION);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_FORI_POSITION);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForI_Vertices),
		ForI_Vertices,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// For Creating "i" Color
	glGenBuffers(1, &vbo_FORI_COLOR);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_FORI_COLOR);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForI_Color),
		ForI_Color,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind array
	glBindVertexArray(0);


	//Create vao
	//For First "N"  
	glGenVertexArrays(1, &vao_FORN);
	glBindVertexArray(vao_FORN);
	
	//For First "N" Vetices
	//Generate Buffer
	glGenBuffers(1, &vbo_FORN_CROSS_POSITION);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_FORN_CROSS_POSITION);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForN_Vertices),
		ForN_Vertices,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// For Creating "i" Color
	glGenBuffers(1, &vbo_FORN_CROSS_COLOR);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_FORN_CROSS_COLOR);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForN_Color),
		ForN_Color,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind array
	glBindVertexArray(0);


	//Create vao
	//For First "D UP"  
	glGenVertexArrays(1, &vao_FORD_UP);
	glBindVertexArray(vao_FORD_UP);

	//For First "I" Vetices
	//Generate Buffer
	glGenBuffers(1, &vbo_FORD_UP_POSITION);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_FORD_UP_POSITION);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForD_Vertices),
		ForD_Vertices,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// For Creating "i" Color
	glGenBuffers(1, &vbo_FORD_UP_COLOR);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_FORD_UP_COLOR);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		4*2*sizeof(float),
		NULL,
		GL_DYNAMIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Unbind array
	glBindVertexArray(0);

	// For Creating A
	//Create vao
	glGenVertexArrays(1, &vao_FORA);
	glBindVertexArray(vao_FORA);

	//For First "I" Vetices
	//Generate Buffer
	glGenBuffers(1, &vbo_FORA_POSITION);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_FORA_POSITION);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForA_Vertices),
		ForA_Vertices,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// For Creating "i" Color
	glGenBuffers(1, &vbo_FORA_COLOR);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_FORA_COLOR);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForA_Color),
		ForA_Color,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind array
	glBindVertexArray(0);


	// For Creating A Center
	//Create vao
	glGenVertexArrays(1, &vao_FORA_CENTER);
	glBindVertexArray(vao_FORA_CENTER);

	//For First "A Center" Vetices
	//Generate Buffer
	glGenBuffers(1, &vbo_FORA_POSITION_CENTER);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_FORA_POSITION_CENTER);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForD_Vertices),
		ForD_Vertices,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// For Creating "A" Color
	glGenBuffers(1, &vbo_FORA_COLOR_CENTER);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_FORA_COLOR_CENTER);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForA_Center_Color),
		ForA_Center_Color,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind array
	glBindVertexArray(0);


	// For AIROPLAIN
	//Create vao
	glGenVertexArrays(1, &vao_AIROPLAIN);
	glBindVertexArray(vao_AIROPLAIN);

	//For First "A Center" Vetices
	//Generate Buffer
	glGenBuffers(1, &vbo_AIROPLAIN);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		4*2*sizeof(float),
		NULL,
		GL_DYNAMIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// For Creating "A" Color
	glGenBuffers(1, &vbo_COLOR_AIROPLAIN);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_COLOR_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		4*2*sizeof(float),
		NULL,
		GL_DYNAMIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind array
	glBindVertexArray(0);



	//Now from here onward add Frame Buffer Facility
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1.0f);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	PrespectiveGraphicsProjectionMatrix = mat4::identity();

	Resize(WIN_WIDTH, WIN_HEIGHT);
	ToggleFullScreen();
	return 0;
}
//Function Resize
void Resize(int width, int height) 
{
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	PrespectiveGraphicsProjectionMatrix=perspective(45.0f, ((GLfloat)width / (GLfloat)height), 0.1f, 100.0f);

}

//Function Display
void Display(void) {
	void drawLine(float x1, float y1, float x2, float y2, float r, float g, float b);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 TrabslationMatrix;
	

	// For Drawing "I"
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix Multiplication

	modelViewMatrix = translate(-x_For_fi, 0.0f, -4.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao

	glBindVertexArray(vao_FORI);

	//Bind with textures if any
	//Draw
	glLineWidth(7.0f);
	glDrawArrays(GL_LINES,
		0,
		3);

	glBindVertexArray(0);

	// For N Front Line
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix Multiplication

	modelViewMatrix = translate(-1.5f, a_For_fn, -4.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao

	glBindVertexArray(vao_FORN);

	//Bind with textures if any

	//Draw

	glDrawArrays(GL_LINE_STRIP,
		0,
		6);

	glBindVertexArray(0);

	// For D Front
	// For Drawing N CROSS
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix Multiplication

	modelViewMatrix = translate(-0.1f, 0.0f, -4.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao

	glBindVertexArray(vao_FORD_UP);

	const float ForD_Color[] =
	{
		Colr_D, 0.0, 0.0,		// Apex
		Colr_D, 0.0, 0.0,		// Let bottom
		Colr_D, 0.0, 0.0,		// Apex
		0.0, Colr_D, 0.0,		// Let bottom
		0.0, Colr_D, 0.0,		// Apex
		0.0, Colr_D, 0.0,		// Let bottom
		0.0, Colr_D, 0.0,		// Apex
		Colr_D, 0.0, 0.0
	};

	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_FORD_UP_COLOR);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForD_Color),
		ForD_Color,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,
		0);

	glDrawArrays(GL_LINE_STRIP,
		0,
		8);

	glBindVertexArray(0);

	//////////////   FOR SECOND I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix Multiplication

	modelViewMatrix = translate(2.0f, -x_For_fsi, -4.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao

	glBindVertexArray(vao_FORI);

	//Bind with textures if any

	//Draw

	glDrawArrays(GL_LINES,
		0,
		3);

	glBindVertexArray(0);

	/////////////////////  FOR A
	//////////////   FOR SECOND I
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix Multiplication

	modelViewMatrix = translate(y_For_fa, 0.0f, -4.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	//Bind with vao

	glBindVertexArray(vao_FORA);

	//Bind with textures if any

	//Draw

	glDrawArrays(GL_LINE_LOOP,
		0,
		3);

	glBindVertexArray(0);
	
	// Red Line For A
	glBindVertexArray(vao_AIROPLAIN);
	drawLine(-0.255f, 0.0f, 0.255f, 0.0f, Flag, 0.0f, 0.0f);
	glLineWidth(6.0f);
	glDrawArrays(GL_LINES,
		0,
		2);

	glBindVertexArray(0);
	
	// White Line For A
	glBindVertexArray(vao_AIROPLAIN);
	drawLine(-0.255f, -0.02f, 0.255f, -0.02f, Flag, Flag, Flag);
	glLineWidth(6.0f);
	glDrawArrays(GL_LINES,
		0,
		2);

	glBindVertexArray(0);

	// Green Line For A
	glBindVertexArray(vao_AIROPLAIN);
	drawLine(-0.255f, -0.04f, 0.255f, -0.04f, 0.0f, Flag, 0.0f);
	glLineWidth(6.0f);
	glDrawArrays(GL_LINES,
		0,
		2);

	glBindVertexArray(0);



		//******** AIROPLAIN *************
		// Top AIROPLAIN MOVING
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix Multiplication

	modelViewMatrix = translate(Airoplain_Upper_X,
		Airoplain_Upper_Y,
		-4.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	DrawAiroplain();

			// MID AIROPLAIN MOVING
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix Multiplication

	modelViewMatrix = translate(Airoplain_Middle_X,
		Airoplain_Middle_Y,
		-4.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	DrawAiroplain();

			// DOWN AIROPLAIN MOVING
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	TrabslationMatrix = mat4::identity();
	//Matrix Multiplication

	modelViewMatrix = translate(Airoplain_Lower_X,
		Airoplain_Lower_Y,
		-4.5f);
	modelViewProjectionMatrix = PrespectiveGraphicsProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	DrawAiroplain();

	glUseProgram(0);

	SwapBuffers(ghdc);

}
void DrawAiroplain(void)
{
	void drawLine(float x1, float y1, float x2, float y2, float r, float g, float b);

	glBindVertexArray(vao_AIROPLAIN);

	const float Airoplain_Vertices[] =
	{
		0.6f, 0.0f, 0.0f,
		0.1f, 0.1f, 0.0f,
		0.1f, -0.1f, 0.0f,
	};

	const float Airoplain_Color[] =
	{
	0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
	};

	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_COLOR_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(Airoplain_Color),
		Airoplain_Color,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,
		0);

	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(Airoplain_Vertices),
		Airoplain_Vertices,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,
		0);
	glLineWidth(6.0f);
	glDrawArrays(GL_TRIANGLES,
		0,
		3);

	glBindVertexArray(0);

	// *********************************   SECOND PART *******************************
	glBindVertexArray(vao_AIROPLAIN);

	const float ForDAN_Vertices[] =
	{
		0.1f, 0.1f, 0.0f,
		-0.3f, 0.1f, 0.0f,
		-0.3f, -0.1f, 0.0f,
		0.1f, -0.1f, 0.0f,
	};

	const float ForDAN_Color[] =
	{
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
	};

	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_COLOR_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForDAN_Color),
		ForDAN_Color,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,
		0);

	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForDAN_Vertices),
		ForDAN_Vertices,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,
		0);
	glLineWidth(6.0f);
	glDrawArrays(GL_TRIANGLE_FAN,
		0,
		4);

	glBindVertexArray(0);

	//********************************************** FOR THIRD PART ***********************
	glBindVertexArray(vao_AIROPLAIN);

	const float ForDTWO_Vertices[] =
	{
		0.1f, 0.0f, 0.0f,
		-0.2f, 0.3f, 0.0f,
		-0.1f, 0.0f, 0.0f,
	};

	const float ForDTWO_Color[] =
	{
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
	};

	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_COLOR_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForDTWO_Color),
		ForDTWO_Color,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,
		0);

	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForDTWO_Vertices),
		ForDTWO_Vertices,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,
		0);
	glLineWidth(6.0f);
	glDrawArrays(GL_TRIANGLES,
		0,
		3);

	glBindVertexArray(0);

	//***************************************************** FORTH PART
	glBindVertexArray(vao_AIROPLAIN);

	const float ForDTHREE_Vertices[] =
	{
	0.1f, -0.0f, 0.0f,
	-0.2f, -0.3f, 0.0f,
	-0.1f, 0.0f, 0.0f,
	};

	const float ForDTHREE_Color[] =
	{
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
	};

	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_COLOR_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForDTHREE_Color),
		ForDTHREE_Color,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,
		0);

	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForDTHREE_Vertices),
		ForDTHREE_Vertices,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,
		0);
	glLineWidth(6.0f);
	glDrawArrays(GL_TRIANGLES,
		0,
		3);

	glBindVertexArray(0);

	//***************************************  FIFTH PART
	glBindVertexArray(vao_AIROPLAIN);

	const float ForDFOUR_Vertices[] =
	{
	-0.1f, 0.0f, 0.0f,
	-0.4f, 0.2f, 0.0f,
	-0.4f, -0.2f, 0.0f,
	};

	const float ForDFOUR_Color[] =
	{
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
		0.128f, 0.226f, 0.238f,		// Apex
	};

	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_COLOR_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForDFOUR_Color),
		ForDFOUR_Color,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,
		0);

	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_AIROPLAIN);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(ForDFOUR_Vertices),
		ForDFOUR_Vertices,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,
		0);
	glLineWidth(6.0f);
	glDrawArrays(GL_TRIANGLES,
		0,
		3);

	glBindVertexArray(0);

	//Flag
	glBindVertexArray(vao_AIROPLAIN);
	glLineWidth(6.0);
	drawLine(gfMFlagOX, gfMFlagOY, -0.6f, gfMFlagOY, 1.0f, 0.6f, 0.2f);
	glDrawArrays(GL_LINES,
		0,
		iLinePoints);
	glBindVertexArray(0);

	glBindVertexArray(vao_AIROPLAIN);
	glLineWidth(6.0);
	drawLine(gfMFlagWX, gfMFlagWY, -0.6f, gfMFlagWY, 1.0f, 1.0f, 1.0f);
	glDrawArrays(GL_LINES,
		0,
		iLinePoints);
	glBindVertexArray(0);

	glBindVertexArray(vao_AIROPLAIN);
	glLineWidth(6.0);
	drawLine(gfMFlagGX, gfMFlagGY, -0.6f, gfMFlagGY, 0.07f, 0.53f, 0.02f);
	glDrawArrays(GL_LINES,
		0,
		iLinePoints);
	glBindVertexArray(0);



}

void drawLine(float x1, float y1, float x2, float y2, float r, float g, float b) {
	// function declaration

	// Variable declaration
	GLfloat fLineVertices[3 * 2];
	GLfloat fLineColor[3 * 2];

	// Vertices
	fLineVertices[0] = x1;
	fLineVertices[1] = y1;
	fLineVertices[2] = 0.0f;

	fLineVertices[3] = x2;
	fLineVertices[4] = y2;
	fLineVertices[5] = 0.0f;

	// Color
	fLineColor[0] = r;
	fLineColor[1] = g;
	fLineColor[2] = b;

	fLineColor[3] = r;
	fLineColor[4] = g;
	fLineColor[5] = b;

	// Line Vertices
	glBindBuffer(GL_ARRAY_BUFFER, vbo_AIROPLAIN);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fLineVertices),
		fLineVertices,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Circle Color
	glBindBuffer(GL_ARRAY_BUFFER, vbo_COLOR_AIROPLAIN);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fLineColor),
		fLineColor,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void Update(void)
{
	if (x_For_fi >= 1.5)
	{
		x_For_fi = x_For_fi - 0.0005f;
	}
	//////// FOR MOVING A
	if (y_For_fa >= 2.01f)
	{
		y_For_fa = y_For_fa - 0.0005f;
	}
	/////// FOR MOVING 
	if (a_For_fn >= 0.0f)
	{
		a_For_fn = a_For_fn - 0.0005f;
	}
	////// FOR MOVING SECOND I
	if (x_For_fsi >= 0.0f)
	{
		x_For_fsi = x_For_fsi - 0.0005f;
	}
	else
	{
		if (Colr_D <= 1.0) 
		{
			Colr_D = Colr_D + 0.0005f;
		}
		else
		{
			if (Airoplain_Upper_X <= 2.0f)
			{
				Airoplain_Upper_X += 0.0005f;
				gfUFlagOX -= 0.0005f;
				gfUFlagWX -= 0.0005f;
				gfUFlagGX -= 0.0005f;
			}
			if (Airoplain_Upper_Y >= 0.0f)
			{
				Airoplain_Upper_Y -= 0.0005f;
			}
			else if (Airoplain_Upper_X >= 2.0f)
			{

				if (Airoplain_Upper_X >= -1.5f)
				{
					Airoplain_Upper_X += 0.0005f;
					Airoplain_Upper_Y -= 0.0005f;
				}
			}

			if (Airoplain_Middle_X <= 2.0f)
			{
				Airoplain_Middle_X += 0.0005f;
				gfMFlagOX -= 0.0005f;
				gfMFlagWX -= 0.0005f;
				gfMFlagGX -= 0.0005f;
			}
			if (Airoplain_Lower_X <= 2.0f)
			{
				Airoplain_Lower_X += 0.0005f;
				gfLFlagOX -= 0.0005f;
				gfLFlagWX -= 0.0005f;
				gfLFlagGX -= 0.0005f;
			}
			else 
			{
					gfMFlagOX += 0.0005f;
					gfMFlagWX += 0.0005f;
					gfMFlagGX += 0.0005f;

			}
			if (Airoplain_Lower_Y <= 0.0f)
			{
				Airoplain_Lower_Y += 0.0005f;
			}
			else if (Airoplain_Lower_X >= 2.0f)
			{
				if (Airoplain_Middle_X <= 4.0f)
				{
					Airoplain_Middle_X += 0.0005f;
					gfMFlagOX += 0.0005f;
					gfMFlagWX += 0.0005f;
					gfMFlagGX += 0.0005f;
				}
				else
				{
					if (Flag <= 1.0)
					{
						Flag = Flag + 0.0005f;
					}

				}
				if (Airoplain_Lower_Y <= 2.0f)
				{
					Airoplain_Lower_X += 0.0005f;
					Airoplain_Lower_Y += 0.0005f;
				}
			}
		}
	}
}

void UnInitialize(void) {
	if (bFullScreen == true) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		//ShowCursor(TRUE);
	}


	if (vbo_FORI_POSITION) 
	{
		glDeleteBuffers(1, &vbo_FORI_POSITION);
		vbo_FORI_POSITION= 0;
	}

	if (gShaderProgramObject) {
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,
			&shaderCount);

		GLuint * pShaders = (GLuint *)malloc(shaderCount * sizeof(GLuint));

		if (pShaders) {
			glGetAttachedShaders(gShaderProgramObject,
				shaderCount,
				&shaderCount,
				pShaders);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++) {
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (wglGetCurrentContext() == ghrc) {
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc) {
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc) {
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (gpFile) {
		fprintf_s(gpFile, "Log File Closed Successfully");
		fclose(gpFile);
		gpFile = NULL;
	}
}



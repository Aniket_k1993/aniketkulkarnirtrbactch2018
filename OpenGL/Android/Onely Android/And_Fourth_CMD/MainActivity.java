package com.rtr.EventWithText;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Window;
import android.content.pm.ActivityInfo;
import android.view.WindowManager;


public class MainActivity extends AppCompatActivity 
{
	private MyView myView;
 
	@Override
    protected void onCreate(Bundle savedInstanceState) 
	{
       // setContentView(R.layout.activity_main);
	   // There are two step to hide our titlebar 
	   //1. requestWindowFeature(Window.FEATURE_NO_TITLE);
	   this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

	   //FullScreen
	   MainActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	   myView = new MyView(this);

	   // Set Background Color 
	   //this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);

	   setContentView(myView);
    }

	@Override
	protected void onPause()
	{
		super.onPause();

	}

	@Override
	protected void onResume()
	{
		super.onResume();

	}
}

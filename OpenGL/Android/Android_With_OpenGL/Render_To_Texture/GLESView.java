package com.rtr.Render_To_Texture;						// For Multicolored Perspective triangle

import android.content.Context;									// For "Context" class

import android.opengl.GLSurfaceView;							// For "GLSurfaceView" class
import android.opengl.GLES32;									// For OpenGL ES 32

import javax.microedition.khronos.opengles.GL10;				// For OpenGLES 1.0 needed as param
import javax.microedition.khronos.egl.EGLConfig;				// For EGLConfig needed as param

import android.view.MotionEvent;								// For "MotionEvent" class
import android.view.GestureDetector;							// For "GestureDetector" class
import android.view.GestureDetector.OnGestureListener;			// For "OnGestureListener" class
import android.view.GestureDetector.OnDoubleTapListener;		// For "OnDoubleTapListener" class

//java.nio.Buffer For vbo:
import java.nio.ByteOrder;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import android.opengl.Matrix;									// For Matrix math;

// A view for OpenGL ES 3 graphics which also receives touch events:
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context myContext;
	private GestureDetector myGestureDetector;
	String AMC;
	// OpenGL objects:
	private int VertexShaderObject;
	private int FragmentShaderObject;
	private int ShaderProgramObject;
	
	private int VertexShaderObject_fbo;
	private int FragmentShaderObject_fbo;
	private int ShaderProgramObject_fbo;
	
	// vao and vbo:
	private int[] vao_Pyramid = new int[1];
	private int[] vbo_Pyramid_Position = new int[1];
	private int[] vbo_Pyramid_Texture = new int[1];
	
	private int[] vao_Cube = new int[1];
	private int[] vbo_Cube_Position = new int[1];
	private int[] vbo_Cube_Texture = new int[1];

	private int mvpUniform;
//	private int mvpUniform_fbo;
	private float PerspectiveProjectionMatrix[] = new float[16];		// 4x4 matrix
	int width;
	int height;
	// For rotationshapes
	private float anglepyramid = 0.0f;
	private float angleCube = 0.0f;
	
	//For a Texture to Render Globle variable's
	private int gShaderProgramObject_FBO;
	private int mvpUniform_FBO;
	private int samplerUniform_FBO;
	private int[] vbo_texCoord_Cube = new int[1];
	private int[] FBO = new int[1];
	private int[] texture_FBO = new int[1];
	private int[] fbo_depth = new int[1];
	private int TextureSamplerUniform;	// to catch uniform of sampler


	// Constructor:
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		myContext = drawingContext;
		
		// Set the EGLContext to current supported version of OpenGL-ES:
		setEGLContextClientVersion(3);
		
		// Set the renderer:
		setRenderer(this);
		
		// Set the render mode to render only when there is change in the drawing data:
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		myGestureDetector = new GestureDetector(myContext, this, null, false);
		myGestureDetector.setOnDoubleTapListener(this);
	}
	
	// Initialize function:
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// Print OpenGL ES version:
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("AMC : OpenGL ES Version : " + glesVersion);
		
		// Print OpenGL Shading Language version:
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("AMC : OpenGL Shading Language Version : " + glslVersion);
		
		// Print supported OpenGL extensions:
		//String glesExtensions = gl.glGetString(GL10.GL_EXTENSIONS);
		//System.out.println("AMC : OpenGL Extensions supported on this device : " + glesExtensions);
		
		initialize(gl);
	}
	
	// Resize funtion:
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	// Display function:
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	// 0. Handle 'onTouchEvent' because it triggers all gesture and tap events:
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventAction = e.getAction();
		if(!myGestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}
	
	// 1. Double Tap:
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}
	
	// 2. Double Tap event:
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	// 3. Single Tap:
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}
	
	// 4. On Down:
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	// 5. On Single Tap UP:
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	// 6. Fling:
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	// 7. Scroll: (Exit the program)
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	
	// 8. Long Press:
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	
	// 9. Show Press:
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	private void initialize(GL10 gl)
	{
		System.out.println("AMC : In initialize : " + AMC);

		ColorShaders();
		fboShaders();
		
		for_bindfbo();

		Draw_Pyramid_Cube();

		// Enable depth testing and specify the depth test to perform:
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);

		// Set the background color to Black:
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		
		// Set projectionMatrix and Identity matrix:
		Matrix.setIdentityM(PerspectiveProjectionMatrix, 0);
		System.out.println("AMC : Out initialize : " + AMC);

	}

	private void Draw_Pyramid_Cube()
	{
			final float pyramidVertices[] = new float[]
										{	// Perspective triangle (Front face)
											0.0f, 1.0f, 0.0f,		// Apex
											-1.0f, -1.0f, 1.0f,		// Left bottom
											1.0f, -1.0f, 1.0f,		// Right bottom
											// Perspective triangle (Right face)
											0.0f, 1.0f, 0.0f,		// Apex
											1.0f, -1.0f, 1.0f,		// Left bottom
											1.0f, -1.0f, -1.0f,		// Right bottom
											// Perspective triangle (Back face)
											0.0f, 1.0f, 0.0f,		// Apex
											1.0f, -1.0f, -1.0f,		// Left bottom
											-1.0f, -1.0f, -1.0f,	// Right bottom
											// Perspective triangle (Left face)
											0.0f, 1.0f, 0.0f,		// Apex
											-1.0f, -1.0f, -1.0f,		// Left bottom
											-1.0f, -1.0f, 1.0f		// Right bottom
										};
		System.out.println("AMC : Initialization Create Pyramid Vertices Array: " + AMC);

		final	float pyramidTexcoords[]= new float[]
										{
										0.5f, 1.0f,0.0f, // front-top
										0.0f, 0.0f,0.0f, // front-left
										1.0f, 0.0f, 0.0f,// front-right
            
										0.5f, 1.0f,0.0f, // right-top
										1.0f, 0.0f,0.0f, // right-left
										0.0f, 0.0f,0.0f, // right-right
            
										0.5f, 1.0f,0.0f, // back-top
										1.0f, 0.0f, 0.0f,// back-left
										0.0f, 0.0f, 0.0f,// back-right
            
										0.5f, 1.0f, 0.0f,// left-top
										0.0f, 0.0f, 0.0f,// left-left
										1.0f, 0.0f, 0.0f// left-right
										};
		System.out.println("AMC : Initialization Create Pyramid Color Array: " + AMC);

																				
		// A. BLOCK FOR PYRAMID:
		GLES32.glGenVertexArrays(1, vao_Pyramid, 0);								// Generate vertex array object names (here, pyramid)
		GLES32.glBindVertexArray(vao_Pyramid[0]);								// Bind a 'single' vertex array object
		System.out.println("AMC : Initialization Bind Vao_Pyramid: " + AMC);
		
		// 1. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_Pyramid_Position, 0);							// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Pyramid_Position[0]);		// Bind a named buffer object to the specified binding point
		System.out.println("AMC : Initialization Bind vbo_Pyramid_Position: " + AMC);
		
		ByteBuffer PyramidPosByteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
		PyramidPosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer PyramidVerticesBuffer = PyramidPosByteBuffer.asFloatBuffer();
		PyramidVerticesBuffer.put(pyramidVertices);
		PyramidVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							pyramidVertices.length * 4,		// size in bytes of the buffer object's new data store
							PyramidVerticesBuffer,			// pointer to the data that will be copied into the data store
							GLES32.GL_STATIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		System.out.println("AMC : Initialization UnBind vbo_Pyramid_Position: " + AMC);
		
		// 2. BUFFER BLOCK FOR TEXTURE:
		GLES32.glGenBuffers(1, vbo_Pyramid_Texture, 0);								// Generate buffer object names (here, for texture);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Pyramid_Texture[0]);		// Bind a named buffer object to the specified binding point
		System.out.println("AMC : Initialization Bind vbo_Pyramid_Color: " + AMC);

		ByteBuffer PyramidTextureByteBuffer = ByteBuffer.allocateDirect(pyramidTexcoords.length * 4);
		PyramidTextureByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer PyramidTextureBuffer = PyramidTextureByteBuffer.asFloatBuffer();
		PyramidTextureBuffer.put(pyramidTexcoords);
		PyramidTextureBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		
							pyramidTexcoords.length * 4,	
							PyramidTextureBuffer,			
							GLES32.GL_STATIC_DRAW);			
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
									3, 									
									GLES32.GL_FLOAT,					
									false, 0, 0);						
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		System.out.println("AMC : Initialization UnBind vbo_Pyramid_Color: " + AMC);
		
		// Release vao:
		GLES32.glBindVertexArray(0);
		System.out.println("AMC : Initialization UnBind Vao_Pyramid: " + AMC);
		

				final float cubeVertices[] = new float[]
										{	// Perspective square (Top face)
											1.0f, 1.0f, -1.0f,		// Right top
											-1.0f, 1.0f, -1.0f, 	// Left top
											-1.0f, 1.0f, 1.0f,		// Left bottom
											1.0f, 1.0f, 1.0f,		// Right bottom
											// Perspective square (Bottom face)
											1.0f, -1.0f, -1.0f,		// Right top
											-1.0f, -1.0f, -1.0f, 	// Left top
											-1.0f, -1.0f, 1.0f,		// Left bottom
											1.0f, -1.0f, 1.0f,		// Right bottom
											// Perspective square (Front face)
											1.0f, 1.0f, 1.0f,		// Right top
											-1.0f, 1.0f, 1.0f,		// Left top
											-1.0f, -1.0f, 1.0f, 	// Left bottom
											1.0f, -1.0f, 1.0f,		// Right bottom
											// Perspective square (Back face)
											1.0f, 1.0f, -1.0f,		// Right top											
											-1.0f, 1.0f, -1.0f,		// Left top
											-1.0f, -1.0f, -1.0f, 	// Left bottom
											1.0f, -1.0f, -1.0f,		// Right bottom
											// Perspective square (Right face)
											1.0f, 1.0f, -1.0f,		// Right top											
											1.0f, 1.0f, 1.0f,		// Left top
											1.0f, -1.0f, 1.0f, 		// Left bottom
											1.0f, -1.0f, -1.0f,		// Right bottom
											// Perspective square (Left face)
											-1.0f, 1.0f, 1.0f,		// Right top																						
											-1.0f, 1.0f, -1.0f,		// Left top
											-1.0f, -1.0f, -1.0f, 	// Left bottom
											-1.0f, -1.0f, 1.0f		// Right bottom
										};
		System.out.println("AMC : Initialization Created a Cube Vertices Array: " + AMC);
		

        final float cubeTexcoords[]= new float[]
										{
											0.0f,0.0f,
											1.0f,0.0f,
											1.0f,1.0f,
											0.0f,1.0f,
            
											0.0f,0.0f,
											1.0f,0.0f,
											1.0f,1.0f,
											0.0f,1.0f,
            
											0.0f,0.0f,
											1.0f,0.0f,
											1.0f,1.0f,
											0.0f,1.0f,
            
											0.0f,0.0f,
											1.0f,0.0f,
											1.0f,1.0f,
											0.0f,1.0f,
            
											0.0f,0.0f,
											1.0f,0.0f,
											1.0f,1.0f,
											0.0f,1.0f,
            
											0.0f,0.0f,
											1.0f,0.0f,
											1.0f,1.0f,
											0.0f,1.0f,
										};

		System.out.println("AMC : Initialization Created a Cube texture Array: " + AMC);
	
		// B. BLOCK FOR CUBE:
		GLES32.glGenVertexArrays(1, vao_Cube, 0);					
		GLES32.glBindVertexArray(vao_Cube[0]);						
		System.out.println("AMC : Initialization Bind Vao_Cube: " + AMC);
		
		// 1.. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_Cube_Position, 0);								
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Cube_Position[0]);		
		System.out.println("AMC : Initialization Bind Vbo_Cube_Position: " + AMC);

		ByteBuffer CubePosByteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4);
		CubePosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer CubeVerticesBuffer = CubePosByteBuffer.asFloatBuffer();
		CubeVerticesBuffer.put(cubeVertices);
		CubeVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		
							cubeVertices.length * 4,		
							CubeVerticesBuffer,				
							GLES32.GL_STATIC_DRAW);			
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	
									3, 									
									GLES32.GL_FLOAT,					
									false, 0, 0);						
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		System.out.println("AMC : Initialization UnBind Vbo_Cube_Position: " + AMC);
		
		
		// 2. BUFFER BLOCK FOR TEXTURE:
		GLES32.glGenBuffers(1, vbo_Cube_Texture, 0);						
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Cube_Texture[0]);	
		System.out.println("AMC : Initialization Bind Vbo_Cube_Texcord: " + AMC);
		
		ByteBuffer CubeTextureByteBuffer = ByteBuffer.allocateDirect(cubeTexcoords.length * 4);
		CubeTextureByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer CubeTextureBuffer = CubeTextureByteBuffer.asFloatBuffer();
		CubeTextureBuffer.put(cubeTexcoords);
		CubeTextureBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		
							cubeTexcoords.length * 4,		
							CubeTextureBuffer,				
							GLES32.GL_STATIC_DRAW);			
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXTURE0,	
									2, 									
									GLES32.GL_FLOAT,					
									false, 0, 0);						
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXTURE0);
		
		// Release the buffer for texture:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		System.out.println("AMC : Initialization UnBind Vbo_Cube_Texcord: " + AMC);
		
		// Release vao:
		GLES32.glBindVertexArray(0);
		System.out.println("AMC : Initialization UnBind Vao_Cube: " + AMC);

	}

	private void for_bindfbo()
	{
		System.out.println("AMC : In For_BindFBO: " + AMC);

		ByteBuffer PositionOneByteBuffer = ByteBuffer.allocateDirect(4 * 2 * 4);
		PositionOneByteBuffer.order(ByteOrder.nativeOrder());
		IntBuffer draw_Buffers = PositionOneByteBuffer.asIntBuffer();
		draw_Buffers .put(GLES32.GL_COLOR_ATTACHMENT0 );
		
		System.out.println("AMC : Create draw_buffers successfully " + AMC);

//		IntBuffer draw_Buffers = ByteBuffer.IntBuffer; 
	//	 final int[] drawBuffers = new int[] { GLES32.GL_COLOR_ATTACHMENT0 };

		// ******************************** naw from changes to try and Error
			GLES32.glGenFramebuffers(1,FBO,0);
			GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER,FBO[0]);//Read/Draw
				System.out.println("AMC : gen And Bind FrameBuffer successfully " + AMC);
	

			GLES32.glGenTextures(1,texture_FBO,0);
			GLES32.glBindTexture(GLES32.GL_TEXTURE_2D,texture_FBO[0]);
				System.out.println("AMC : Created fbo_texture successfully " + AMC);

			GLES32.glTexStorage2D(GLES32.GL_TEXTURE_2D,1,GLES32.GL_RGBA8,1024,1024);

			GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
			GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);
			GLES32.glFramebufferTexture(GLES32.GL_FRAMEBUFFER,GLES32.GL_COLOR_ATTACHMENT0 ,texture_FBO[0],0);
			//Turn Off Mipmaps 4th 0
				System.out.println("AMC : Convert into data TexParam.i & FrameBufferTexture successfully " + AMC);

			//public const enum  =   ;
//			 private enum draw_Buffers[] { GLES32.gl_COLOR_ATTACHMENT0 };

			//Changes
			GLES32.glGenBuffers(1, fbo_depth,0);
			GLES32.glBindRenderbuffer(GLES32.GL_RENDERBUFFER, fbo_depth[0]);
			GLES32.glRenderbufferStorage(GLES32.GL_RENDERBUFFER, GLES32.GL_DEPTH24_STENCIL8, 1024, 1024);
			GLES32.glFramebufferRenderbuffer(GLES32.GL_FRAMEBUFFER, GLES32.GL_DEPTH_ATTACHMENT, GLES32.GL_RENDERBUFFER, fbo_depth[0]);
				System.out.println("AMC : gen and bind FBO_Depth successfully " + AMC);


			GLES32.glDrawBuffers(1,draw_Buffers);

			GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);
			GLES32.glBindRenderbuffer(GLES32.GL_RENDERBUFFER, 0);
			GLES32.glBindRenderbuffer(GLES32.GL_RENDERBUFFER, 0);
				System.out.println("AMC : unbind all successfully " + AMC);

		System.out.println("AMC : Out For_BindFBO: " + AMC);


	}


	private void fboShaders()
	{
		System.out.println("AMC : In FBO_SHADERS " + AMC);

		VertexShaderObject_fbo = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		// Vertex shader source code:
		final String vertexShaderSourceCodeN = String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec2 vTexture0_Coord;"+
			"out vec2 out_texture0_coord;"+
			"uniform mat4 u_mvp_matrix;"+
			"void main(void)"+
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;"+
			"out_texture0_coord = vTexture0_Coord;"+
			"}"
			);
		
		// Provide source code to the vertex shader:
		// Specify the shader object and the corresponding vertex shader text (ie source code)
		GLES32.glShaderSource(VertexShaderObject_fbo, vertexShaderSourceCodeN);
		
		// Compile the shader and check for any errors:
		GLES32.glCompileShader(VertexShaderObject_fbo);
		
		// Error checking:
		int[] iShaderCompilationStatus = new int[1];	// 1 member integer array
		int[] iInfoLogLength = new int[1];				// 1 member integer array
		String szInfoLog = null;						// String to store the log
		
		// As there are no addresses in Java, we use an array of size 1
		GLES32.glGetShaderiv(VertexShaderObject_fbo, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
		if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)	// Compilation failed
		{
			GLES32.glGetShaderiv(VertexShaderObject_fbo, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(VertexShaderObject_fbo);
				System.out.println("AMC : Vertex Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
				System.out.println("AMC : fbo_vertexShader Compiled successfully " + AMC);
		
		/*<-- FRAGMENT SHADER -->*/
		
		// Create Fragment Shader:
		FragmentShaderObject_fbo = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		// Fragment shader source code:
		final String FragmentShaderSourceCodeN = String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec2 out_texture0_coord;"+
			"uniform highp sampler2D u_texture0_sampler;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor = texture(u_texture0_sampler, out_texture0_coord);"+
			"}"
		);
		
		// Provide source code to the Fragment shader:
		// Specify the shader object and the corresponding fragment shader text (ie source code)
		GLES32.glShaderSource(FragmentShaderObject_fbo, FragmentShaderSourceCodeN);
		
		// Compile the shader and check for any errors:
		GLES32.glCompileShader(FragmentShaderObject_fbo);
		
		// Error checking:
		iShaderCompilationStatus[0] = 0;			// Re initialize
		iInfoLogLength[0] = 0;						// Re initialize
		szInfoLog = null;							// Re initialize
		
		GLES32.glGetShaderiv(FragmentShaderObject_fbo, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
		if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(FragmentShaderObject_fbo, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(FragmentShaderObject_fbo);
				System.out.println("AMC : Fragment shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
				System.out.println("AMC : fbo_Fragment Compiled successfully " + AMC);
		
		// Create the Shader Program:
		ShaderProgramObject_fbo = GLES32.glCreateProgram();
		
		// Attach Vertex shader to the shader program:
		GLES32.glAttachShader(ShaderProgramObject_fbo, VertexShaderObject_fbo);
		
		// Attach the Fragment shader to the shader program:
		GLES32.glAttachShader(ShaderProgramObject_fbo, FragmentShaderObject_fbo);
		
		// Pre-link binding of shader program object with vertex shader attributes:
		GLES32.glBindAttribLocation(ShaderProgramObject_fbo, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
		
		// Pre-link binding of shader program object with fragment shader attributes:
		GLES32.glBindAttribLocation(ShaderProgramObject_fbo, GLESMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
		
		// Link the two shaders together to shader program object to get a single executable and check for errors:
		GLES32.glLinkProgram(ShaderProgramObject);
		
		// Error checking:
		int[] iShaderProgramLinkStatus = new int[1];		// Linking check
		iInfoLogLength[0] = 0;								// Re initialize
		szInfoLog = null;									// Re initialize
		
		GLES32.glGetProgramiv(ShaderProgramObject_fbo, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(ShaderProgramObject_fbo, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(ShaderProgramObject_fbo);
				System.out.println("AMC : Shader Program link log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
				System.out.println("AMC : fbo_ProgramShaderObject Compiled successfully " + AMC);
		
		// Get MVP Uniform location:
		// The actual locations assigned to uniform variables are not known until the program object is linked successfully.
		mvpUniform_FBO = GLES32.glGetUniformLocation(ShaderProgramObject_fbo, "u_mvp_matrix");	

		samplerUniform_FBO = GLES32.glGetUniformLocation(ShaderProgramObject_fbo, "u_texture0_sampler");	
		System.out.println("AMC : Out FBO_SHADERS " + AMC);

	}
	private void ColorShaders()
	{
		System.out.println("AMC : In COLOR SHADERS " + AMC);

	/*<-- VERTEX SHADER -->*/
		
		// Create vertex shader
		VertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		// Vertex shader source code:
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec4 vColor;"+
			"uniform mat4 u_mvp_matrix;"+
			"out vec4 out_color;"+
			"void main(void)"+
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;"+
			"out_color = vColor;"+
			"}"
		);
		
		// Provide source code to the vertex shader:
		// Specify the shader object and the corresponding vertex shader text (ie source code)
		GLES32.glShaderSource(VertexShaderObject, vertexShaderSourceCode);
		
		// Compile the shader and check for any errors:
		GLES32.glCompileShader(VertexShaderObject);
		
		// Error checking:
		int[] iShaderCompilationStatus = new int[1];	// 1 member integer array
		int[] iInfoLogLength = new int[1];				// 1 member integer array
		String szInfoLog = null;						// String to store the log
		
		// As there are no addresses in Java, we use an array of size 1
		GLES32.glGetShaderiv(VertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
		if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)	// Compilation failed
		{
			GLES32.glGetShaderiv(VertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(VertexShaderObject);
				System.out.println("AMC : Vertex Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
				System.out.println("AMC : Vertex Shaders  Compiled successfully " + AMC);
		
		/*<-- FRAGMENT SHADER -->*/
		
		// Create Fragment Shader:
		FragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		// Fragment shader source code:
		final String FragmentShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec4 out_color;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor = out_color;"+
			"}"
		);
		
		// Provide source code to the Fragment shader:
		// Specify the shader object and the corresponding fragment shader text (ie source code)
		GLES32.glShaderSource(FragmentShaderObject, FragmentShaderSourceCode);
		
		// Compile the shader and check for any errors:
		GLES32.glCompileShader(FragmentShaderObject);
		
		// Error checking:
		iShaderCompilationStatus[0] = 0;			// Re initialize
		iInfoLogLength[0] = 0;						// Re initialize
		szInfoLog = null;							// Re initialize
		
		GLES32.glGetShaderiv(FragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
		if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(FragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(FragmentShaderObject);
				System.out.println("AMC : Fragment shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
				System.out.println("AMC : Fragment Shader  Compiled successfully " + AMC);

		// Create the Shader Program:
		ShaderProgramObject = GLES32.glCreateProgram();
		
		// Attach Vertex shader to the shader program:
		GLES32.glAttachShader(ShaderProgramObject, VertexShaderObject);
		
		// Attach the Fragment shader to the shader program:

		GLES32.glAttachShader(ShaderProgramObject, FragmentShaderObject);
		
		// Pre-link binding of shader program object with vertex shader attributes:
		GLES32.glBindAttribLocation(ShaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
		
		// Pre-link binding of shader program object with fragment shader attributes:
		GLES32.glBindAttribLocation(ShaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
		
		// Link the two shaders together to shader program object to get a single executable and check for errors:
		GLES32.glLinkProgram(ShaderProgramObject);
		
		// Error checking:
		int[] iShaderProgramLinkStatus = new int[1];		// Linking check
		iInfoLogLength[0] = 0;								// Re initialize
		szInfoLog = null;									// Re initialize
		
		GLES32.glGetProgramiv(ShaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(ShaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(ShaderProgramObject);
				System.out.println("AMC : Shader Program link log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
				System.out.println("AMC : Program ShaderProgramObject  Compiled successfully " + AMC);
		
		// Get MVP Uniform location:
		// The actual locations assigned to uniform variables are not known until the program object is linked successfully.
		mvpUniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_mvp_matrix");	
		System.out.println("AMC : Out COLOR SHADERS " + AMC);

	}

	private void resize(int width, int height)
	{
		//Code:
		// Set the viewport:
		GLES32.glViewport(0, 0, width, height);
		
		// Perspective Projection = FOV, aspect ratio, near, far:
		Matrix.perspectiveM(PerspectiveProjectionMatrix, 0, 45.0f, 
							((float)width/(float)height), 0.1f, 100.0f);
	}
	
	public void display()
	{
		// Code:
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
			// Use shader program:

		GLES32.glUseProgram(ShaderProgramObject);
				System.out.println("AMC : Draw Use Programm ShaderProgramObject " + AMC);		
		
		For_Pyramid();

		GLES32.glUseProgram(0);
		
		GLES32.glUseProgram(ShaderProgramObject_fbo);

		For_Cube();

		GLES32.glUseProgram(0);

		// call update
		update();

		// Render/Flush:
		requestRender();
	}

	private void For_Pyramid()
	{
	
		float ModelViewMatrix[] = new float[16];				// Eye space 
		float ModelViewProjectionMatrix[] = new float[16];		// Clip space
		float RotationMatrix[] = new float[16];   //RotationMatrix
		float ScaleMatrix[] = new float[16];					// Scaling matrix

		GLES32.glClearColor(0.0f, 1.0f, 1.0f, 0.0f);
		GLES32.glViewport(0, 0, 1024, 1024);
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		// Changes
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, FBO[0]);
		System.out.println("AMC : draw a FrameBuffer successfully " + AMC);


		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);

		Matrix.scaleM(ScaleMatrix, 0, ScaleMatrix, 0, 0.75f, 0.75f, 0.75f);					// Scale the cube by 0.75 on all sides

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, -1.5f, 0.0f, -4.0f);	// Translate back by -4.0f
		//		Matrix.setIdentityM(RotationMatrix,0);
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		Matrix.rotateM(RotationMatrix, 0, RotationMatrix, 0, angleCube, 1.0f, 0.0f, 0.0f);	// Rotate cube about the x axis
		Matrix.rotateM(RotationMatrix, 0, RotationMatrix, 0, angleCube, 0.0f, 1.0f, 0.0f);	// Rotate cube about the y axis
		Matrix.rotateM(RotationMatrix, 0, RotationMatrix, 0, angleCube, 0.0f, 0.0f, 1.0f);	// Rotate cube about the z axis

		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						RotationMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		// Pass the above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable, 
		// whose position we have already calculated in Initialize() by using glGetUniformLocation():
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);

		// Bind with pyramid texture:
		// Bind vao:
		GLES32.glBindVertexArray(vao_Pyramid[0]);
		System.out.println("AMC : draw Bind Vao_Pyramid " + AMC);

		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);		// 3 (each with x,y,z) vertices in triangleVertices array
			
			// Unbind vao:
		GLES32.glBindVertexArray(0);
		System.out.println("AMC : draw UNBind Vao_Pyramid " + AMC);
		
		GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);
				System.out.println("AMC : draw a FrameBuffer successfully " + AMC);
		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
		
		System.out.println("AMC : Draw UNUSE Programm ShaderProgramObject " + AMC);

	}

	private void For_Cube()
	{
		float ModelViewMatrix[] = new float[16];				// Eye space 
		float ModelViewProjectionMatrix[] = new float[16];		// Clip space
		float RotationMatrix[] = new float[16];   //RotationMatrix
		float ScaleMatrix[] = new float[16];					// Scaling matrix

		System.out.println("AMC : Draw Use Programm ShaderProgramObject_fbo " + AMC);
		GLES32.glClearColor(0.0f, 1.0f, 1.0f, 0.0f);
		GLES32.glViewport(0, 0, this.width, this.height);
//		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		//pyramid
		Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(RotationMatrix,0);
		System.out.println("AMC : All identity  " + AMC);
		
		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, 1.5f, 0.0f, -4.0f);	// Translate back by -4.0f
		
		Matrix.rotateM(RotationMatrix, 0, RotationMatrix, 0,anglepyramid, 0.0f, 1.0f, 0.0f);	// Translate back by -4.0f
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						RotationMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		GLES32.glUniformMatrix4fv(mvpUniform_FBO, 1, false, ModelViewProjectionMatrix, 0);
		// Bind vao:
		GLES32.glBindVertexArray(vao_Cube[0]);
		System.out.println("AMC : Bind Vao Cube " + AMC);
				
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_FBO[0]);
		GLES32.glUniform1i(samplerUniform_FBO, 0); // 0th sampler enable (as we have only 1 texture sampler in fragment shader)
		System.out.println("AMC : ABU Texture_fbo  " + AMC);
				
//		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 12);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_STRIP,0,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_STRIP,4,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_STRIP,8,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_STRIP,12,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_STRIP,16,4);
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_STRIP,20,4);	
		// Unbind vao:
		GLES32.glBindVertexArray(0);
				System.out.println("AMC : UnBind Vao Cube " + AMC);
		
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
				System.out.println("AMC : Draw unbind Texture " + AMC);	
	}
	private void update()
	{
		anglepyramid= anglepyramid + 1.0f;
		if (anglepyramid>= 360.0f)
			anglepyramid = 0.0f;

		angleCube = angleCube - 1.0f;
		if (angleCube <= -360.0f)
			angleCube = 0.0f;			
	}

	
	void uninitialize()
	{
		// Code:
		// Destroy vao:
		if(vao_Pyramid[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_Pyramid, 0);
			vao_Pyramid[0] = 0;
		}
		
		// Destroy vbo for vertices:
		if(vbo_Pyramid_Position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_Pyramid_Position, 0);
			vbo_Pyramid_Position[0] = 0;
		}

		// Destroy vbo for colors:
		if(vbo_Pyramid_Texture[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_Pyramid_Texture, 0);
			vbo_Pyramid_Texture[0] = 0;
		}
			
		if(vao_Cube[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_Cube, 0);
			vao_Cube[0] = 0;
		}
		
		// Destroy vbo for vertices:
		if(vbo_Cube_Position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_Cube_Position, 0);
			vbo_Cube_Position[0] = 0;
		}

		// Destroy vbo for colors:
		if(vbo_Cube_Texture[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_Cube_Texture, 0);
			vbo_Cube_Texture[0] = 0;
		}
		
		// Detach and delete the shaders one by one that are attached to the the shader program object
		if(ShaderProgramObject != 0)
		{
			if(VertexShaderObject != 0)
			{
				// Detach vertex shader from the shader program and then delete it:
				GLES32.glDetachShader(ShaderProgramObject, VertexShaderObject);
				GLES32.glDeleteShader(VertexShaderObject);
				VertexShaderObject = 0;
			}
			
			if(FragmentShaderObject != 0)
			{
				// Detach fragment shader from the shader program and then delete it:
				GLES32.glDetachShader(ShaderProgramObject, FragmentShaderObject);
				GLES32.glDeleteShader(FragmentShaderObject);
				FragmentShaderObject = 0;
			}
		}
		
		// Delete the shader program object
		if(ShaderProgramObject != 0)
		{
			GLES32.glDeleteProgram(ShaderProgramObject);
			ShaderProgramObject = 0;
		}
	}
}
		
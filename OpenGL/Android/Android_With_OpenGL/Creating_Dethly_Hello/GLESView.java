package com.rtr.Creating_DethlyHello;						// For Multicolored Perspective triangle

import android.content.Context;									// For "Context" class

import android.opengl.GLSurfaceView;							// For "GLSurfaceView" class
import android.opengl.GLES32;									// For OpenGL ES 32

import javax.microedition.khronos.opengles.GL10;				// For OpenGLES 1.0 needed as param
import javax.microedition.khronos.egl.EGLConfig;				// For EGLConfig needed as param

import android.view.MotionEvent;								// For "MotionEvent" class
import android.view.GestureDetector;							// For "GestureDetector" class
import android.view.GestureDetector.OnGestureListener;			// For "OnGestureListener" class
import android.view.GestureDetector.OnDoubleTapListener;		// For "OnDoubleTapListener" class

// For vbo:
import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import android.opengl.Matrix;									// For Matrix math;
// A view for OpenGL ES 3 graphics which also receives touch events:
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context myContext;
	private GestureDetector myGestureDetector;

	// OpenGL objects:
	private int VertexShaderObject;
	private int FragmentShaderObject;
	private int ShaderProgramObject;
	
//	FloatBuffer CircleColorsBuffer;
//	FloatBuffer CircleVerticesBuffer; 

	// Trangle 
	private int[] vao_Trangle = new int[1];
	private int[] vbo_Position_Trangle = new int[1];
	private int[] vbo_Color_Trangle = new int[1];
	
	FloatBuffer SquareColorsBuffer;
	FloatBuffer SquareVerticesBuffer;

	// line
	private int[] vao_Line = new int[1];
	private int[] vbo_Position_Line = new int[1];
	private int[] vbo_Color_Line = new int[1];
	
	FloatBuffer LineColorsBuffer;
	FloatBuffer LineVerticesBuffer;


	//Circle 
	
	ByteBuffer CirclePosByteBuffer;
	ByteBuffer CircleColByteBuffer;
	FloatBuffer CircleVerticesBuffer;
	FloatBuffer CircleColorsBuffer;

	private int[] vao_Circle = new int[1];
	private int[] vbo_Position_Circle = new int[1];							// For the vertices
	private int[] vbo_Color_Circle = new int[1];								// For the colors

	private float circleangle[] = new float[4500];
	private float circleColor[] = new float[4500];

	private float Angle_Circle = 0.0f;
	private float angle_Triangle = 0.0f; 
	private float angle_line = 0.0f;
	private float x = -2.0f; 
	private float y = -2.0f; 
	private float z = -2.0f;
	private float z1 = -2.0f;

	private int mvpUniform;
	private float PerspectiveProjectionMatrix[] = new float[16];		// 4x4 matrix
		
	//vaoCube
	// Constructor:
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		myContext = drawingContext;
		
		// Set the EGLContext to current supported version of OpenGL-ES:
		setEGLContextClientVersion(3);
		
		// Set the renderer:
		setRenderer(this);
		
		// Set the render mode to render only when there is change in the drawing data:
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		myGestureDetector = new GestureDetector(myContext, this, null, false);
		myGestureDetector.setOnDoubleTapListener(this);
	}
	
	// Initialize function:
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// Print OpenGL ES version:
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("AMC : OpenGL ES Version : " + glesVersion);
		
		// Print OpenGL Shading Language version:
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("AMC : OpenGL Shading Language Version : " + glslVersion);
		
		// Print supported OpenGL extensions:
		//String glesExtensions = gl.glGetString(GL10.GL_EXTENSIONS);
		//System.out.println("AMC : OpenGL Extensions supported on this device : " + glesExtensions);
		
		initialize(gl);
	}
	
	// Resize funtion:
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	// Display function:
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	// 0. Handle 'onTouchEvent' because it triggers all gesture and tap events:
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventAction = e.getAction();
		if(!myGestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}
	
	// 1. Double Tap:
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}
	
	// 2. Double Tap event:
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	// 3. Single Tap:
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}
	
	// 4. On Down:
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	// 5. On Single Tap UP:
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	// 6. Fling:
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	// 7. Scroll: (Exit the program)
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	
	// 8. Long Press:
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	
	// 9. Show Press:
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	private void initialize(GL10 gl)
	{
		/*<-- VERTEX SHADER -->*/
		
		// Create vertex shader
		VertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		// Vertex shader source code:
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec4 vColor;"+
			"uniform mat4 u_mvp_matrix;"+
			"out vec4 out_color;"+
			"void main(void)"+
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;"+
			"out_color = vColor;"+
			"}"
		);
		
		// Provide source code to the vertex shader:
		// Specify the shader object and the corresponding vertex shader text (ie source code)
		GLES32.glShaderSource(VertexShaderObject, vertexShaderSourceCode);
		
		// Compile the shader and check for any errors:
		GLES32.glCompileShader(VertexShaderObject);
		
		// Error checking:
		int[] iShaderCompilationStatus = new int[1];	// 1 member integer array
		int[] iInfoLogLength = new int[1];				// 1 member integer array
		String szInfoLog = null;						// String to store the log
		
		// As there are no addresses in Java, we use an array of size 1
		GLES32.glGetShaderiv(VertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
		if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)	// Compilation failed
		{
			GLES32.glGetShaderiv(VertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(VertexShaderObject);
				System.out.println("AMC : Vertex Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		/*<-- FRAGMENT SHADER -->*/
		
		// Create Fragment Shader:
		FragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		// Fragment shader source code:
		final String FragmentShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec4 out_color;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor = out_color;"+
			"}"
		);
		
		// Provide source code to the Fragment shader:
		// Specify the shader object and the corresponding fragment shader text (ie source code)
		GLES32.glShaderSource(FragmentShaderObject, FragmentShaderSourceCode);
		
		// Compile the shader and check for any errors:
		GLES32.glCompileShader(FragmentShaderObject);
		
		// Error checking:
		iShaderCompilationStatus[0] = 0;			// Re initialize
		iInfoLogLength[0] = 0;						// Re initialize
		szInfoLog = null;							// Re initialize
		
		GLES32.glGetShaderiv(FragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
		if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(FragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(FragmentShaderObject);
				System.out.println("AMC : Fragment shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		// Create the Shader Program:
		ShaderProgramObject = GLES32.glCreateProgram();
		
		// Attach Vertex shader to the shader program:
		GLES32.glAttachShader(ShaderProgramObject, VertexShaderObject);
		
		// Attach the Fragment shader to the shader program:
		GLES32.glAttachShader(ShaderProgramObject, FragmentShaderObject);
		
		// Pre-link binding of shader program object with vertex shader attributes:
		GLES32.glBindAttribLocation(ShaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
		
		// Pre-link binding of shader program object with fragment shader attributes:
		GLES32.glBindAttribLocation(ShaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
		
		// Link the two shaders together to shader program object to get a single executable and check for errors:
		GLES32.glLinkProgram(ShaderProgramObject);
		
		// Error checking:
		int[] iShaderProgramLinkStatus = new int[1];		// Linking check
		iInfoLogLength[0] = 0;								// Re initialize
		szInfoLog = null;									// Re initialize
		
		GLES32.glGetProgramiv(ShaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(ShaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(ShaderProgramObject);
				System.out.println("AMC : Shader Program link log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		// Get MVP Uniform location:
		// The actual locations assigned to uniform variables are not known until the program object is linked successfully.
		mvpUniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_mvp_matrix");
		
		// *********************************** Trangle
		GLES32.glGenVertexArrays(1, vao_Trangle, 0);					// Generate vertex array object names
		GLES32.glBindVertexArray(vao_Trangle[0]);						// Bind a 'single' vertex array object
		
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_Position_Trangle, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_Trangle[0]);	// Bind a named buffer object to the specified binding point
		
		/*ByteBuffer SquarePosByteBuffer = ByteBuffer.allocateDirect(vline.length * 4);
		SquarePosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer SquareVerticesBuffer = SquarePosByteBuffer.asFloatBuffer();
		SquareVerticesBuffer.put(vline);
		SquareVerticesBuffer.position(0);
		*/
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							4 * 2 * 4,	// size in bytes of the buffer object's new data store
							null,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// B. BUFFER BLOCK FOR COLORS:
		GLES32.glGenBuffers(1, vbo_Color_Trangle, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Color_Trangle[0]);		// Bind a named buffer object to the specified binding point

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							4 * 2 * 4,		// size in bytes of the buffer object's new data store
							null,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// Release vao:
		GLES32.glBindVertexArray(0);

		// *********************************** LINES
		GLES32.glGenVertexArrays(1, vao_Line, 0);					// Generate vertex array object names
		GLES32.glBindVertexArray(vao_Line[0]);						// Bind a 'single' vertex array object
		
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_Position_Line, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_Line[0]);	// Bind a named buffer object to the specified binding point
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							4 * 2 * 4,	// size in bytes of the buffer object's new data store
							null,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// B. BUFFER BLOCK FOR COLORS:
		GLES32.glGenBuffers(1, vbo_Color_Line, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Color_Line[0]);		// Bind a named buffer object to the specified binding point
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							4 * 2 * 4,		// size in bytes of the buffer object's new data store
							null,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// Release vao:
		GLES32.glBindVertexArray(0);



		// ******************************* Circle

		GLES32.glGenVertexArrays(1, vao_Circle, 0);					// Generate vertex array object names
		GLES32.glBindVertexArray(vao_Circle[0]);						// Bind a 'single' vertex array object
		
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_Position_Circle, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_Circle[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer SquarePosByteBuffer = ByteBuffer.allocateDirect(circleangle.length * 4);
		SquarePosByteBuffer.order(ByteOrder.nativeOrder());
		SquareVerticesBuffer = SquarePosByteBuffer.asFloatBuffer();
		SquareVerticesBuffer.put(circleangle);
		SquareVerticesBuffer.position(0);

		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							4 * 2 * 4,	// size in bytes of the buffer object's new data store
							null,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// B. BUFFER BLOCK FOR COLORS:
		GLES32.glGenBuffers(1, vbo_Color_Circle, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Color_Circle[0]);		// Bind a named buffer object to the specified binding point
		
		ByteBuffer SquareColByteBuffer = ByteBuffer.allocateDirect(circleColor.length * 4);
		SquareColByteBuffer.order(ByteOrder.nativeOrder());
		SquareColorsBuffer= SquareColByteBuffer.asFloatBuffer();
		SquareColorsBuffer.put(circleColor);
		SquareColorsBuffer.position(0);

		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							4 * 2 * 4,		// size in bytes of the buffer object's new data store
							null,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// Release vao:
		GLES32.glBindVertexArray(0);
		
		// Enable depth testing and specify the depth test to perform:
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		// Set the background color to Black:
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		
		// Set projectionMatrix and Identity matrix:
		Matrix.setIdentityM(PerspectiveProjectionMatrix, 0);
	}
	
	private void resize(int width, int height)
	{
		//Code:
		// Set the viewport:
		GLES32.glViewport(0, 0, width, height);
		
		// Perspective Projection = FOV, aspect ratio, near, far:
		Matrix.perspectiveM(PerspectiveProjectionMatrix, 0, 45.0f, 
							((float)width/(float)height), 0.1f, 100.0f);
	}
	
	public void display()
	{
		// Code:
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		// Use shader program:
		GLES32.glUseProgram(ShaderProgramObject);
		
		// OpenGL ES drawing:
		float ModelViewMatrix[] = new float[16];				// Eye space 
		float ModelViewProjectionMatrix[] = new float[16];		// Clip space
		float ScaleMatrix[] = new float[16];					// Scaling matrix

		//********************* TRANGLE******************
		Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(ScaleMatrix,0);

		Matrix.scaleM(ScaleMatrix, 0, ScaleMatrix, 0, 0.75f, 0.75f, 0.75f);					// Scale the cube by 0.75 on all sides

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, z, z, -2.260f);	// Translate back by -4.0f
		//		Matrix.setIdentityM(RotationMatrix,0);
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);
		
		Trangle();

		//******************************LINE************************
				Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(ScaleMatrix,0);

		Matrix.scaleM(ScaleMatrix, 0, ScaleMatrix, 0, 0.75f, 0.75f, 0.75f);					// Scale the cube by 0.75 on all sides

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, 0.0f, -x, -2.260f);	// Translate back by -4.0f
		//		Matrix.setIdentityM(RotationMatrix,0);
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);
		
		Line();
		
		//*****************************CIRCLE**********************
		Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(ScaleMatrix,0);

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, -z, z1, -5.9f);	// Translate back by -4.0f
		//		Matrix.setIdentityM(RotationMatrix,0);
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);
		drawCircle();		


		// Stop using the shader program object:
		GLES32.glUseProgram(0);

		// call update
		Update();

		// Render/Flush:
		requestRender();
	}

	public void Update()
	{
		Angle_Circle = Angle_Circle + 2.0f;
		if (Angle_Circle >= 360.0f)
			Angle_Circle = 0.0f;
		angle_Triangle = angle_Triangle+ 2.0f;
		if (angle_Triangle >= 360.0f)
			angle_Triangle = 0.0f;
		angle_line = angle_line+ 2.0f;
		if (angle_line >= 360.0f)
			angle_line = 0.0f;

		if (x <= 0.015f)
		{
			x = x + 0.007f;
		}
		if (y <= 0.050f)
		{
			y = y + 0.007f;
		}
		if (z <= 0.0f)
		{
			z = z + 0.007f;
		}

		if (z1 <= -0.5f)
		{
			z1 = z1 + 0.007f;
		}

		if (x >= 0.050f)
		{
			Angle_Circle = 0.0f;
			angle_Triangle = 0.0f;
			angle_line = 0.0f;
		}
		if (y >= 0.050f)
		{
			Angle_Circle = 0.0f;
			angle_Triangle = 0.0f;
			angle_line = 0.0f;
		}
		if (z >= 0.0)
		{
			Angle_Circle = 0.0f;
			angle_Triangle = 0.0f;
			angle_line = 0.0f;

		}
	}

	public void Trangle()	
	{
		// ******************************************************* Trangle
		GLES32.glBindVertexArray(vao_Trangle[0]);
		
		float Trangle_Vertices[] = new float[9];
		float trangle_Color[] = new float[9];
						Trangle_Vertices[0] = 0.0f;
						Trangle_Vertices[1] = 0.850f;
						Trangle_Vertices[2] = 0.0f;
						Trangle_Vertices[3] = -0.850f;
						Trangle_Vertices[4] = -0.850f;
						Trangle_Vertices[5] = 0.0f;
						Trangle_Vertices[6] = 0.850f;
						Trangle_Vertices[7] = -0.850f;
						Trangle_Vertices[8] = 0.0f;

						
						trangle_Color[0] = 1.0f;
						trangle_Color[1] = 1.0f;
						trangle_Color[2] = 0.0f;
						trangle_Color[3] = 1.0f;
						trangle_Color[4] = 1.0f;
						trangle_Color[5] = 0.0f;
						trangle_Color[6] = 1.0f;
						trangle_Color[7] = 1.0f;
						trangle_Color[8] = 0.0f;

						
		GLES32.glGenBuffers(1, vbo_Position_Trangle, 0);							// Generate buffer object names (here, for texture);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_Trangle[0]);		// Bind a named buffer object to the specified binding point
		
		ByteBuffer TrangleByteBuffer = ByteBuffer.allocateDirect(Trangle_Vertices.length * 4);
		TrangleByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer TrangleBuffer = TrangleByteBuffer.asFloatBuffer();
		TrangleBuffer.put(Trangle_Vertices);
		TrangleBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							Trangle_Vertices.length * 4,		// size in bytes of the buffer object's new data store
							TrangleBuffer,			// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// **************************** color
		GLES32.glGenBuffers(1, vbo_Color_Trangle, 0);							// Generate buffer object names (here, for texture);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Color_Trangle[0]);		// Bind a named buffer object to the specified binding point
		
		ByteBuffer TrangleColorByteBuffer = ByteBuffer.allocateDirect(trangle_Color.length * 4);
		TrangleColorByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer TrangleColorBuffer = TrangleColorByteBuffer.asFloatBuffer();
		TrangleColorBuffer.put(trangle_Color);
		TrangleColorBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							trangle_Color.length * 4,		// size in bytes of the buffer object's new data store
							TrangleColorBuffer,			// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glLineWidth(7.5f);

		// Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
		GLES32.glDrawArrays(GLES32.GL_LINE_LOOP, 0, 3);		// 4 for the triangle fan to draw a quad

		// Unbind vao:
		GLES32.glBindVertexArray(0);	
	}

	public void Line()	
	{
		// ******************************************************* Line
		GLES32.glBindVertexArray(vao_Line[0]);
		
		float Line_Vertices[] = new float[6];
		float Line_Color[] = new float[6];
						Line_Vertices[0] = 0.0f;
						Line_Vertices[1] = 0.850f;
						Line_Vertices[2] = 0.0f;
						Line_Vertices[3] = 0.0f;
						Line_Vertices[4] = -0.850f;
						Line_Vertices[5] = 0.0f;

						
						Line_Color[0] = 1.0f;
						Line_Color[1] = 1.0f;
						Line_Color[2] = 0.0f;
						Line_Color[3] = 1.0f;
						Line_Color[4] = 1.0f;
						Line_Color[5] = 0.0f;

						
		GLES32.glGenBuffers(1, vbo_Position_Line, 0);							// Generate buffer object names (here, for texture);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_Line[0]);		// Bind a named buffer object to the specified binding point
		
		ByteBuffer LineByteBuffer = ByteBuffer.allocateDirect(Line_Vertices.length * 4);
		LineByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer LineBuffer = LineByteBuffer.asFloatBuffer();
		LineBuffer.put(Line_Vertices);
		LineBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							Line_Vertices.length * 4,		// size in bytes of the buffer object's new data store
							LineBuffer,			// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		// **************************** color
		GLES32.glGenBuffers(1, vbo_Color_Line, 0);							// Generate buffer object names (here, for texture);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Color_Line[0]);		// Bind a named buffer object to the specified binding point
		
		ByteBuffer LineColorByteBuffer = ByteBuffer.allocateDirect(Line_Color.length * 4);
		LineColorByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer LineColorBuffer = LineColorByteBuffer.asFloatBuffer();
		LineColorBuffer.put(Line_Color);
		LineColorBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							Line_Color.length * 4,		// size in bytes of the buffer object's new data store
							LineColorBuffer,			// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glLineWidth(7.5f);

		// Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 4 for the triangle fan to draw a quad

		// Unbind vao:
		GLES32.glBindVertexArray(0);	
	}


	private void drawCircle()
	{
		for (int noofpoints = 0; noofpoints < 1500; noofpoints +=3)
		{
				double angle = 2 * Math.PI * noofpoints / (float) 1500;
				circleangle[noofpoints] = (float)Math.cos(angle);
				circleangle[noofpoints+1] = (float)Math.sin(angle)-0.19f;
				circleangle[noofpoints+2] = (float)0.0f;


		// **************************** color
			
				circleColor[noofpoints]= 1.0f;
				circleColor[noofpoints+1]=1.0f;
				circleColor[noofpoints+2]=0.0f;

		}			
		GLES32.glBindVertexArray(vao_Circle[0]);

		GLES32.glGenBuffers(1, vbo_Position_Circle, 0);							// Generate buffer object names (here, for texture);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position_Circle[0]);		// Bind a named buffer object to the specified binding point
			
		SquareVerticesBuffer.put(circleangle);
		SquareVerticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							circleangle.length * 4,		// size in bytes of the buffer object's new data store
							SquareVerticesBuffer,			// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		GLES32.glGenBuffers(1, vbo_Color_Circle, 0);							// Generate buffer object names (here, for texture);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Color_Circle[0]);		// Bind a named buffer object to the specified binding point
		

		SquareColorsBuffer.put(circleColor);
		SquareColorsBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							circleColor.length * 4,		// size in bytes of the buffer object's new data store
							SquareColorsBuffer,			// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

//		GLES32.glpointsize(3.0f);
		GLES32.glDrawArrays(GLES32.GL_POINTS, 0, 4500);	
		
		
		GLES32.glBindVertexArray(0);	

	}

	void uninitialize()
	{
		// Detach and delete the shaders one by one that are attached to the the shader program object
		if(ShaderProgramObject != 0)
		{
			if(VertexShaderObject != 0)
			{
				// Detach vertex shader from the shader program and then delete it:
				GLES32.glDetachShader(ShaderProgramObject, VertexShaderObject);
				GLES32.glDeleteShader(VertexShaderObject);
				VertexShaderObject = 0;
			}
			
			if(FragmentShaderObject != 0)
			{
				// Detach fragment shader from the shader program and then delete it:
				GLES32.glDetachShader(ShaderProgramObject, FragmentShaderObject);
				GLES32.glDeleteShader(FragmentShaderObject);
				FragmentShaderObject = 0;
			}
		}
		
		// Delete the shader program object
		if(ShaderProgramObject != 0)
		{
			GLES32.glDeleteProgram(ShaderProgramObject);
			ShaderProgramObject = 0;
		}
	}
}
		
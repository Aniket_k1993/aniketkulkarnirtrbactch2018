package com.rtr.Dynamic_India;						// For Multicolored Perspective triangle

import android.content.Context;		
// For "Context" class
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;							// For "GLSurfaceView" class
import android.opengl.GLES32;									// For OpenGL ES 32

import javax.microedition.khronos.opengles.GL10;				// For OpenGLES 1.0 needed as param
import javax.microedition.khronos.egl.EGLConfig;				// For EGLConfig needed as param

import android.view.MotionEvent;								// For "MotionEvent" class
import android.view.GestureDetector;							// For "GestureDetector" class
import android.view.GestureDetector.OnGestureListener;			// For "OnGestureListener" class
import android.view.GestureDetector.OnDoubleTapListener;		// For "OnDoubleTapListener" class

// For vbo:
import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import android.opengl.Matrix;									// For Matrix math;

// A view for OpenGL ES 3 graphics which also receives touch events:
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context myContext;
	private GestureDetector myGestureDetector;

	// OpenGL objects:
	private int VertexShaderObject;
	private int FragmentShaderObject;
	private int ShaderProgramObject;
		String AMC;
	
	// vao and vbo:
	//For I
	private int[] vao = new int[1];
	private int[] vbo_Position = new int[1];
	private int[] vbo_Color = new int[1];
	  
	//For N
	private int[] vaoN = new int[1];
	private int[] vboN = new int[1];
	private int[] vboColorN = new int[1];

	//For D
	private int[] vaoD = new int[1];
	private int[] vboD = new int[1];
	private int[] vboColorD = new int[1];
	FloatBuffer ForDColorsBuffer;
	final float forDColor[] = new float[26];
	
	// For Second I
	private int[] vaoSecondI = new int[1];
	private int[] vboSecondI = new int[1];
	private int[] vboColorSecondI = new int[1];

	// For A
	private int[] vaoA = new int[1];
	private int[] vboA = new int[1];
	private int[] vboColorA = new int[1];

	//AIROPLAIN
	private int[] vao_AIROPLAIN = new int[1];
	private int[] vbo_AIROPLAIN = new int[1];
	private int[] vbo_COLOR_AIROPLAIN = new int[1];



	// For Animation
	private float x_For_fi = 3.0f;
	private float x_For_fsi = 8.6f;
	private float a_For_fn = 6.3f;
	private float y_For_fa = 4.4f;
	private float ForWhite = 10.6f;
	private float ForOrange = 10.6f;
	private float ForGreen = 10.6f;
	private float Colr_D = 0.0f;


	//AIROPLAIN
	// Animating Variables Plane
	private float Airoplain_Upper_X = -4.0f;
	private float Airoplain_Upper_Y = 3.0f;
	private float Airoplain_Middle_X = -4.0f;
	private float Airoplain_Middle_Y = 0.0f;
	private float Airoplain_Lower_X = -4.0f;
	private float Airoplain_Lower_Y = -3.0f;

	private float gfUFlagOX = -1.0f;
	private float gfUFlagOY = 0.03f;
	private float gfUFlagWX = -1.0f;
	private float gfUFlagWY = 0.0f;
	private float gfUFlagGX = -1.0f;
	private float gfUFlagGY = -0.03f;

	private float gfMFlagOX = -1.0f;
	private float gfMFlagOY = 0.03f;
	private float gfMFlagWX = -1.0f;
	private float gfMFlagWY = 0.0f;
	private float gfMFlagGX = -1.0f;
	private float gfMFlagGY = -0.03f;

	private float gfLFlagOX = -1.0f;
	private float gfLFlagOY = 0.03f;
	private float gfLFlagWX = -1.0f;
	private float gfLFlagWY = 0.0f;
	private float gfLFlagGX = -1.0f;
	private float gfLFlagGY = -0.03f;
	private float Flag = 0.0f;
	private MediaPlayer mediaPlayer;

	 private double startTime = 1;
   private double finalTime = 1;

	private float plainDown = 4.0f;


	private int mvpUniform;
	private float PerspectiveProjectionMatrix[] = new float[16];		// 4x4 matrix
		
	//vaoCube
	// Constructor:
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		
		myContext = drawingContext;
		mediaPlayer = MediaPlayer.create(drawingContext, R.raw.song);
		
		// Set the EGLContext to current supported version of OpenGL-ES:
		setEGLContextClientVersion(3);
		
		// Set the renderer:
		setRenderer(this);
		
		// Set the render mode to render only when there is change in the drawing data:
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		myGestureDetector = new GestureDetector(myContext, this, null, false);
		myGestureDetector.setOnDoubleTapListener(this);
	}
	
	// Initialize function:
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// Print OpenGL ES version:''
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("AMC : OpenGL ES Version : " + glesVersion);
		
		// Print OpenGL Shading Language version:
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("AMC : OpenGL Shading Language Version : " + glslVersion);

		initialize(gl);
	}
	
	// Resize funtion:
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	// Display function:
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();

	}
	
	// 0. Handle 'onTouchEvent' because it triggers all gesture and tap events:
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventAction = e.getAction();
		if(!myGestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}
	
	// 1. Double Tap:
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}
	
	// 2. Double Tap event:
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	// 3. Single Tap:
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}
	
	// 4. On Down:
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	// 5. On Single Tap UP:
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	// 6. Fling:
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	// 7. Scroll: (Exit the program)
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	
	// 8. Long Press:
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	
	// 9. Show Press:
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	private void initialize(GL10 gl)
	{
		/*<-- VERTEX SHADER -->*/
		
		// Create vertex shader
		VertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		
		// Vertex shader source code:
		final String vertexShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"in vec4 vColor;"+
			"uniform mat4 u_mvp_matrix;"+
			"out vec4 out_color;"+
			"void main(void)"+
			"{"+
			"gl_Position = u_mvp_matrix * vPosition;"+
			"out_color = vColor;"+
			"}"
		);
		
		// Provide source code to the vertex shader:
		// Specify the shader object and the corresponding vertex shader text (ie source code)
		GLES32.glShaderSource(VertexShaderObject, vertexShaderSourceCode);
		
		// Compile the shader and check for any errors:
		GLES32.glCompileShader(VertexShaderObject);
		
		// Error checking:
		int[] iShaderCompilationStatus = new int[1];	// 1 member integer array
		int[] iInfoLogLength = new int[1];				// 1 member integer array
		String szInfoLog = null;						// String to store the log
		
		// As there are no addresses in Java, we use an array of size 1
		GLES32.glGetShaderiv(VertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
		if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)	// Compilation failed
		{
			GLES32.glGetShaderiv(VertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(VertexShaderObject);
				System.out.println("AMC : Vertex Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		/*<-- FRAGMENT SHADER -->*/
		
		// Create Fragment Shader:
		FragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		
		// Fragment shader source code:
		final String FragmentShaderSourceCode = String.format
		(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec4 out_color;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"FragColor = out_color;"+
			"}"
		);
		
		// Provide source code to the Fragment shader:
		// Specify the shader object and the corresponding fragment shader text (ie source code)
		GLES32.glShaderSource(FragmentShaderObject, FragmentShaderSourceCode);
		
		// Compile the shader and check for any errors:
		GLES32.glCompileShader(FragmentShaderObject);
		
		// Error checking:
		iShaderCompilationStatus[0] = 0;			// Re initialize
		iInfoLogLength[0] = 0;						// Re initialize
		szInfoLog = null;							// Re initialize
		
		GLES32.glGetShaderiv(FragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
		if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(FragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(FragmentShaderObject);
				System.out.println("AMC : Fragment shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		// Create the Shader Program:
		ShaderProgramObject = GLES32.glCreateProgram();
		
		// Attach Vertex shader to the shader program:
		GLES32.glAttachShader(ShaderProgramObject, VertexShaderObject);
		
		// Attach the Fragment shader to the shader program:
		GLES32.glAttachShader(ShaderProgramObject, FragmentShaderObject);
		
		// Pre-link binding of shader program object with vertex shader attributes:
		GLES32.glBindAttribLocation(ShaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
		
		// Pre-link binding of shader program object with fragment shader attributes:
		GLES32.glBindAttribLocation(ShaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
		
		// Link the two shaders together to shader program object to get a single executable and check for errors:
		GLES32.glLinkProgram(ShaderProgramObject);
		
		// Error checking:
		int[] iShaderProgramLinkStatus = new int[1];		// Linking check
		iInfoLogLength[0] = 0;								// Re initialize
		szInfoLog = null;									// Re initialize
		
		GLES32.glGetProgramiv(ShaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(ShaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(ShaderProgramObject);
				System.out.println("AMC : Shader Program link log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		// Get MVP Uniform location:
		// The actual locations assigned to uniform variables are not known until the program object is linked successfully.
		mvpUniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_mvp_matrix");
		
		// Vertices, Colors, Shader attributes, vbo, vao initializations:
		final float forIVertices[] = new float[]
										{							// Perspective triangle
											-1.0f, 1.0f, 0.0f,		// Apex
											-1.0f, -1.0f, 0.0f,		// Left bottom
										};
		final float forIColor[] = new float[]
										{							// Perspective triangle
											1.0f, 0.0f, 0.0f,		// Apex
											0.0f, 1.0f, 0.0f,		// Left bottom
										};
		final float forNVertices[] = new float[]
										{					
											-0.5f, -1.0f, 0.0f,		// Apex
											-0.5f, 1.0f, 0.0f,		// Left bottom
											-0.5f, 1.0f, 0.0f,		// Left bottom
											0.5f, -1.0f, 0.0f,		// Apex
											0.5f,-1.0f,0.0f,
											0.5f,1.0f,0.0f,
										};
		final float forNColor[] = new float[]
										{							// Perspective triangle
											0.0f, 1.0f, 0.0f,		// Apex
											1.0f, 0.0f, 0.0f,		// Left bottom
											1.0f, .0f, 0.0f,		// Apex
											0.0f, 1.0f, 0.0f,		// Left bottom
											0.0f, 1.0f, 0.0f,		// Apex
											1.0f, 0.0f, 0.0f,		// Left bottom
										};
		final float forDVertices[] = new float[]
										{					
											-0.5f, 1.0f, 0.0f,		// Apex
											0.5f, 1.0f, 0.0f,		// Left bottom
											0.5f, 1.0f, 0.0f,		// Left bottom
											0.5f, -1.0f, 0.0f,		// Apex
											0.5f,-1.0f,0.0f,
											-0.5f,-1.0f,0.0f,
											-0.4f,-1.0f,0.0f,
											-0.4f,1.0f,0.0f,
										};
		final float forSecondIVertices[] = new float[]
										{					
											1.0f, 1.0f, 0.0f,		// Apex
											1.0f, -1.0f, 0.0f,		// Left bottom
										};
		final float forSecondIColor[] = new float[]
										{							// Perspective triangle
											1.0f, 0.0f, 0.0f,		// Apex
											0.0f, 1.0f, 0.0f,		// Left bottom
										};

		final float forAVertices[] = new float[]
										{							// Perspective triangle
										0.5f, -1.0f, 0.0f,
										0.0f, 1.0f, 0.0f,
										-0.5f, -1.0f, 0.0f
										};	
		final float forAColor[] = new float[]
										{							// Perspective triangle
											0.0f, 1.0f, 0.0f,
											1.0f, 0.0f, 0.0f,
											0.0f, 1.0f, 0.0f,
										};	



		// For Cube 
		GLES32.glGenVertexArrays(1, vao, 0);					// Generate vertex array object names
		GLES32.glBindVertexArray(vao[0]);						// Bind a 'single' vertex array object
		
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_Position, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Position[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer SquarePosByteBuffer = ByteBuffer.allocateDirect(forIVertices.length * 4);
		SquarePosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer SquareVerticesBuffer = SquarePosByteBuffer.asFloatBuffer();
		SquareVerticesBuffer.put(forIVertices);
		SquareVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							forIVertices.length * 4,	// size in bytes of the buffer object's new data store
							SquareVerticesBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_STATIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// B. BUFFER BLOCK FOR COLORS:
		GLES32.glGenBuffers(1, vbo_Color, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Color[0]);		// Bind a named buffer object to the specified binding point
		/*vbo_Position  
		vbo_Color*/
		ByteBuffer SquareColByteBuffer = ByteBuffer.allocateDirect(forIColor.length * 4);
		SquareColByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer SquareColorsBuffer= SquareColByteBuffer.asFloatBuffer();
		SquareColorsBuffer.put(forIColor);
		SquareColorsBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							forIColor.length * 4,		// size in bytes of the buffer object's new data store
							SquareColorsBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_STATIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// Release vao:
		GLES32.glBindVertexArray(0);


		//*****************************************************************
		GLES32.glGenVertexArrays(1, vaoN, 0);					// Generate vertex array object names
		GLES32.glBindVertexArray(vaoN[0]);						// Bind a 'single' vertex array object
		
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vboN, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboN[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer ForNPosByteBuffer = ByteBuffer.allocateDirect(forNVertices.length * 4);
		ForNPosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer ForNVerticesBuffer = ForNPosByteBuffer.asFloatBuffer();
		ForNVerticesBuffer.put(forNVertices);
		ForNVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							forNVertices.length * 4,	// size in bytes of the buffer object's new data store
							ForNVerticesBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_STATIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// B. BUFFER BLOCK FOR COLORS:
		GLES32.glGenBuffers(1, vboColorN, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColorN[0]);		// Bind a named buffer object to the specified binding point
		
		ByteBuffer ForNColByteBuffer = ByteBuffer.allocateDirect(forNColor.length * 4);
		ForNColByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer ForNColorsBuffer= ForNColByteBuffer.asFloatBuffer();
		ForNColorsBuffer.put(forNColor);
		ForNColorsBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							forNColor.length * 4,		// size in bytes of the buffer object's new data store
							ForNColorsBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_STATIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// Release vao:
		GLES32.glBindVertexArray(0);
		
	//************************************************************************
		GLES32.glGenVertexArrays(1, vaoD, 0);					// Generate vertex array object names
		GLES32.glBindVertexArray(vaoD[0]);						// Bind a 'single' vertex array object
		
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vboD, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboD[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer ForDPosByteBuffer = ByteBuffer.allocateDirect(forDVertices.length * 4);
		ForDPosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer ForDVerticesBuffer = ForDPosByteBuffer.asFloatBuffer();
		ForDVerticesBuffer.put(forDVertices);	
		ForDVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							forDVertices.length * 4,	// size in bytes of the buffer object's new data store
							ForDVerticesBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_STATIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// B. BUFFER BLOCK FOR COLORS:
		GLES32.glGenBuffers(1, vboColorD, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColorD[0]);		// Bind a named buffer object to the specified binding point
		
		ByteBuffer ForDColByteBuffer = ByteBuffer.allocateDirect(forDColor.length * 4);
		ForDColByteBuffer.order(ByteOrder.nativeOrder());
		ForDColorsBuffer= ForDColByteBuffer.asFloatBuffer();
		ForDColorsBuffer.put(forDColor);
		ForDColorsBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							4 * 4 * 4,		// size in bytes of the buffer object's new data store
							null,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// Release vao:
		GLES32.glBindVertexArray(0);
		
		
		//*****************************************************************************
		GLES32.glGenVertexArrays(1, vaoSecondI, 0);					// Generate vertex array object names
		GLES32.glBindVertexArray(vaoSecondI[0]);						// Bind a 'single' vertex array object
		
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vboSecondI, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboSecondI[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer SecondIPosByteBuffer = ByteBuffer.allocateDirect(forSecondIVertices.length * 4);
		SecondIPosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer SecondIVerticesBuffer = SecondIPosByteBuffer.asFloatBuffer();
		SecondIVerticesBuffer.put(forSecondIVertices);
		SecondIVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							forSecondIVertices.length * 4,	// size in bytes of the buffer object's new data store
							SecondIVerticesBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_STATIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// B. BUFFER BLOCK FOR COLORS:
		GLES32.glGenBuffers(1, vbo_Color, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_Color[0]);		// Bind a named buffer object to the specified binding point
		
		ByteBuffer SecondIColByteBuffer = ByteBuffer.allocateDirect(forSecondIColor.length * 4);
		SecondIColByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer SecondIColorsBuffer= SecondIColByteBuffer.asFloatBuffer();
		SecondIColorsBuffer.put(forSecondIColor);
		SecondIColorsBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							forSecondIColor.length * 4,		// size in bytes of the buffer object's new data store
							SecondIColorsBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_STATIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// Release vao:
		GLES32.glBindVertexArray(0);
		
		
		//********************************************************************************************
		GLES32.glGenVertexArrays(1, vaoA, 0);					// Generate vertex array object names
		GLES32.glBindVertexArray(vaoA[0]);						// Bind a 'single' vertex array object
		
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vboA, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboA[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer ForAPosByteBuffer = ByteBuffer.allocateDirect(forAVertices.length * 4);
		ForAPosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer ForAVerticesBuffer = ForAPosByteBuffer.asFloatBuffer();
		ForAVerticesBuffer.put(forAVertices);
		ForAVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							forAVertices.length * 4,	// size in bytes of the buffer object's new data store
							ForAVerticesBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_STATIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// B. BUFFER BLOCK FOR COLORS:
		GLES32.glGenBuffers(1, vboColorA, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColorA[0]);		// Bind a named buffer object to the specified binding point
		
		ByteBuffer ForAColByteBuffer = ByteBuffer.allocateDirect(forAColor.length * 4);
		ForAColByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer ForAColorsBuffer= ForAColByteBuffer.asFloatBuffer();
		ForAColorsBuffer.put(forAColor);
		ForAColorsBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							forAColor.length * 4,		// size in bytes of the buffer object's new data store
							ForAColorsBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_STATIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// Release vao:
		GLES32.glBindVertexArray(0);


		// ************************************** Draw Airoplain *********************************
		GLES32.glGenVertexArrays(1, vao_AIROPLAIN, 0);					// Generate vertex array object names
		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);						// Bind a 'single' vertex array object
		System.out.println("AMC : Bind Airoplain Vao: " + AMC);
		
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_AIROPLAIN, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_AIROPLAIN[0]);	// Bind a named buffer object to the specified binding point
		System.out.println("AMC : Bind VBO Airoplain: " + AMC);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							4 * 2 * 4,	// size in bytes of the buffer object's new data store
							null,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		System.out.println("AMC : unBind VBO Airoplain: " + AMC);
		
		// B. BUFFER BLOCK FOR COLORS:
		GLES32.glGenBuffers(1, vbo_COLOR_AIROPLAIN, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_COLOR_AIROPLAIN[0]);		// Bind a named buffer object to the specified binding point
		System.out.println("AMC : unBind VBO Color Airoplain: " + AMC);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							4 * 2 * 4,		// size in bytes of the buffer object's new data store
							null,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	
		System.out.println("AMC : unBind VBO color Airoplain: " + AMC);
		
		// Release vao:
		GLES32.glBindVertexArray(0);
		System.out.println("AMC : unBind vao Airoplain: " + AMC);
		
		
		// Enable depth testing and specify the depth test to perform:
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		// We will always cull back faces for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);
		
		// Set the background color to Black:
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		
		// Set projectionMatrix and Identity matrix:
		Matrix.setIdentityM(PerspectiveProjectionMatrix, 0);
	}
	
	private void resize(int width, int height)
	{
		//Code:
		// Set the viewport:
		GLES32.glViewport(0, 0, width, height);
		
		// Perspective Projection = FOV, aspect ratio, near, far:
		Matrix.perspectiveM(PerspectiveProjectionMatrix, 0, 45.0f, 
							((float)width/(float)height), 0.1f, 100.0f);
	}
	
	public void display()
	{
		// Code:
		mediaPlayer.start();

		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		// Use shader program:
		GLES32.glUseProgram(ShaderProgramObject);
		
		// OpenGL ES drawing:
		float ModelViewMatrix[] = new float[16];				// Eye space 
		float ModelViewProjectionMatrix[] = new float[16];		// Clip space
		float ScaleMatrix[] = new float[16];					// Scaling matrix


		// Set modelview and modelviewprojection matrices to the identity matrix
		Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(ScaleMatrix,0);

		Matrix.scaleM(ScaleMatrix, 0, ScaleMatrix, 0, 0.75f, 0.75f, 0.75f);					// Scale the cube by 0.75 on all sides

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, -x_For_fi, 0.0f, -4.0f);	// Translate back by -4.0f
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);
		
		// Bind vao:
		GLES32.glBindVertexArray(vao[0]);
		
		GLES32.glLineWidth(4.0f);
		// Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 4 for the triangle fan to draw a quad
		
		// Unbind vao:
		GLES32.glBindVertexArray(0);

		//**************************************************************************************
		Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(ScaleMatrix,0);

		Matrix.scaleM(ScaleMatrix, 0, ScaleMatrix, 0, 0.75f, 0.75f, 0.75f);					// Scale the cube by 0.75 on all sides

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, -1.399f, a_For_fn, -4.0f);	// Translate back by -4.0f
		//		Matrix.setIdentityM(RotationMatrix,0);
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);
		
		// Bind vao:
		GLES32.glBindVertexArray(vaoN[0]);
		
		// Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
		GLES32.glDrawArrays(GLES32.GL_LINE_STRIP, 0, 6);		// 4 for the triangle fan to draw a quad
		
		// Unbind vao:
		GLES32.glBindVertexArray(0);

		//***************************************************************************
		Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(ScaleMatrix,0);

		Matrix.scaleM(ScaleMatrix, 0, ScaleMatrix, 0, 0.75f, 0.75f, 0.75f);					// Scale the cube by 0.75 on all sides

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, 0.0f, 0.0f, -4.0f);	// Translate back by -4.0f
		//		Matrix.setIdentityM(RotationMatrix,0);
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		// Pass the above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable, 
		// whose position we have already calculated in Initialize() by using glGetUniformLocation():
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);
		
		// Bind vao:
		GLES32.glBindVertexArray(vaoD[0]);
		

		forDColor[0] = Colr_D;
		forDColor[1] = 0.0f;
		forDColor[2] = 0.0f;

		forDColor[3] = Colr_D;
		forDColor[4] = 0.0f;
		forDColor[5] = 0.0f;

		forDColor[6] = Colr_D;
		forDColor[7] = 0.0f;
		forDColor[8] = 0.0f;


		forDColor[9] = 0.0f;
		forDColor[10] = Colr_D;
		forDColor[11] = 0.0f;

		forDColor[12] = 0.0f;
		forDColor[13] = Colr_D;
		forDColor[14] = 0.0f;

		forDColor[15] = 0.0f;
		forDColor[16] = Colr_D;
		forDColor[17] = 0.0f;

		forDColor[18] = 0.0f;
		forDColor[19] = Colr_D;
		forDColor[20] = 0.0f;

		forDColor[21] = Colr_D;
		forDColor[22] = 0.0f;
		forDColor[23] = 0.0f;


		GLES32.glGenBuffers(1, vboColorD, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColorD[0]);		// Bind a named buffer object to the specified binding point

		ByteBuffer ForDColByteBuffer = ByteBuffer.allocateDirect(forDColor.length * 4);
		ForDColByteBuffer.order(ByteOrder.nativeOrder());
		ForDColorsBuffer = ForDColByteBuffer.asFloatBuffer();
		ForDColorsBuffer.put(forDColor);
		ForDColorsBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							forDColor.length * 4,		// size in bytes of the buffer object's new data store
							ForDColorsBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_STATIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		
		
		// Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
		GLES32.glDrawArrays(GLES32.GL_LINE_STRIP, 0, 8);		// 4 for the triangle fan to draw a quad
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		// Release vao:
		GLES32.glBindVertexArray(0);
		
		// Unbind vao:

		
		//******************************************************************************************
		Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(ScaleMatrix,0);

		Matrix.scaleM(ScaleMatrix, 0, ScaleMatrix, 0, 0.75f, 0.75f, 0.75f);					// Scale the cube by 0.75 on all sides

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, 0.1f, -x_For_fsi, -4.0f);	// Translate back by -4.0f
		//		Matrix.setIdentityM(RotationMatrix,0);
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		// Pass the above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable, 
		// whose position we have already calculated in Initialize() by using glGetUniformLocation():
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);
		
		// Bind vao:
		GLES32.glBindVertexArray(vaoSecondI[0]);
		
		// Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 4 for the triangle fan to draw a quad
		
		// Unbind vao:
		GLES32.glBindVertexArray(0);
		
		//******************************************************************************
		Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(ScaleMatrix,0);

		Matrix.scaleM(ScaleMatrix, 0, ScaleMatrix, 0, 0.75f, 0.75f, 0.75f);					// Scale the cube by 0.75 on all sides

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, y_For_fa, 0.0f, -4.0f);	// Translate back by -4.0f
		//		Matrix.setIdentityM(RotationMatrix,0);
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		// Pass the above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable, 
		// whose position we have already calculated in Initialize() by using glGetUniformLocation():
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);
		
		// Bind vao:
		GLES32.glBindVertexArray(vaoA[0]);
		
		// Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
		GLES32.glDrawArrays(GLES32.GL_LINE_STRIP, 0, 3);		// 4 for the triangle fan to draw a quad
		
		// Unbind vao:
		GLES32.glBindVertexArray(0);
		/*
		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);
		drawLine(-0.255f, 0.0f, 0.255f, 0.0f, Flag, 0.0f, 0.0f);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 4 for the triangle fan to draw a quad
		GLES32.glBindVertexArray(0);
		
		// white
		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);
		drawLine(-0.255f, -0.02f, 0.255f, -0.02f, Flag, Flag, Flag);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 4 for the triangle fan to draw a quad
		GLES32.glBindVertexArray(0);
		
		//green
		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);
		drawLine(-0.255f, -0.04f, 0.255f, -0.04f, 0.0f, Flag, 0.0f);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 4 for the triangle fan to draw a quad
		GLES32.glBindVertexArray(0);
		*/



		// ********************************************* Upper Aioplain
		Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(ScaleMatrix,0);

		Matrix.scaleM(ScaleMatrix, 0, ScaleMatrix, 0, 0.75f, 0.75f, 0.75f);					// Scale the cube by 0.75 on all sides

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, Airoplain_Upper_X, Airoplain_Upper_Y, -4.0f);	// Translate back by -4.0f
		//		Matrix.setIdentityM(RotationMatrix,0);
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		// Pass the above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable, 
		// whose position we have already calculated in Initialize() by using glGetUniformLocation():
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);
		
		DrawAiroplain();

		// ********************************************* Midd Aioplain
				Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(ScaleMatrix,0);

		Matrix.scaleM(ScaleMatrix, 0, ScaleMatrix, 0, 0.75f, 0.75f, 0.75f);					// Scale the cube by 0.75 on all sides

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, Airoplain_Middle_X, Airoplain_Middle_Y, -4.0f);	// Translate back by -4.0f
		//		Matrix.setIdentityM(RotationMatrix,0);
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		// Pass the above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable, 
		// whose position we have already calculated in Initialize() by using glGetUniformLocation():
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);
		
		DrawAiroplain();

		// ********************************************* Down Aioplain
		Matrix.setIdentityM(ModelViewMatrix, 0);
		Matrix.setIdentityM(ModelViewProjectionMatrix, 0);
		Matrix.setIdentityM(ScaleMatrix,0);

		Matrix.scaleM(ScaleMatrix, 0, ScaleMatrix, 0, 0.75f, 0.75f, 0.75f);					// Scale the cube by 0.75 on all sides

		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(ModelViewMatrix, 0, ModelViewMatrix, 0, Airoplain_Lower_X, Airoplain_Lower_Y, -4.0f);	// Translate back by -4.0f
		//		Matrix.setIdentityM(RotationMatrix,0);
		
		// Multiply the modelview and And Rotation Matrix:
		Matrix.multiplyMM(ModelViewMatrix, 0,
						ModelViewMatrix, 0,
						ScaleMatrix, 0);

		// Multiply the modelview and projection matrix to get the modelviewprojection matrix:
		Matrix.multiplyMM(ModelViewProjectionMatrix, 0,
						PerspectiveProjectionMatrix, 0,
						ModelViewMatrix, 0);
						
		// Pass the above modelviewprojection matrix to the vertex shader in "u_mvp_matrix" shader variable, 
		// whose position we have already calculated in Initialize() by using glGetUniformLocation():
		GLES32.glUniformMatrix4fv(mvpUniform, 1, false, ModelViewProjectionMatrix, 0);
		
		DrawAiroplain();

		GLES32.glBindVertexArray(0);
		
		// Stop using the shader program object:
		GLES32.glUseProgram(0);
		
		// call 
		update();

		// Render/Flush:
		requestRender();
	}
	
	private void DrawAiroplain()
	{
		// ************************** First Part of Airoplain *******************************
		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);
		final float Airoplain_Vertices[] = new float[]
		{
			0.6f, 0.0f, 0.0f,
			0.1f, 0.1f, 0.0f,
			0.1f, -0.1f, 0.0f,
		};
		final float Airoplain_Color[] = new float[]
		{
			0.128f, 0.226f, 0.238f,		// Apex
			0.128f, 0.226f, 0.238f,		// Apex
			0.128f, 0.226f, 0.238f,		// Apex
			0.128f, 0.226f, 0.238f,		// Apex
		};

	
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_AIROPLAIN, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_AIROPLAIN[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer AiroplainPosByteBuffer = ByteBuffer.allocateDirect(Airoplain_Vertices.length * 4);
		AiroplainPosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer AiroplainVerticesBuffer = AiroplainPosByteBuffer.asFloatBuffer();
		AiroplainVerticesBuffer.put(Airoplain_Vertices);
		AiroplainVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							Airoplain_Vertices.length * 4,	// size in bytes of the buffer object's new data store
							AiroplainVerticesBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

		GLES32.glGenBuffers(1, vbo_COLOR_AIROPLAIN, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_COLOR_AIROPLAIN[0]);		// Bind a named buffer object to the specified binding point
		
		ByteBuffer AiroplainColByteBuffer = ByteBuffer.allocateDirect(Airoplain_Color.length * 4);
		AiroplainColByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer AiroplainColorsBuffer= AiroplainColByteBuffer.asFloatBuffer();
		AiroplainColorsBuffer.put(Airoplain_Color);
		AiroplainColorsBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							Airoplain_Color.length* 4,		// size in bytes of the buffer object's new data store
							AiroplainColorsBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	

		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES, 0, 3);		// 4 for the triangle fan to draw a quad

		// Unbind vao:
		GLES32.glBindVertexArray(0);

		//**************************************** Second Part **********************************
		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);
		final float AiroplainF_Vertices[] = new float[]
		{
				0.1f, 0.1f, 0.0f,
		-0.3f, 0.1f, 0.0f,
		-0.3f, -0.1f, 0.0f,
		0.1f, -0.1f, 0.0f,

		};

	
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_AIROPLAIN, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_AIROPLAIN[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer AiroplainFPosByteBuffer = ByteBuffer.allocateDirect(AiroplainF_Vertices.length * 4);
		AiroplainFPosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer AiroplainFVerticesBuffer = AiroplainFPosByteBuffer.asFloatBuffer();
		AiroplainFVerticesBuffer.put(AiroplainF_Vertices);
		AiroplainFVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							AiroplainF_Vertices.length * 4,	// size in bytes of the buffer object's new data store
							AiroplainFVerticesBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		
		
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);		// 4 for the triangle fan to draw a quad
		
		// Unbind vao:
		GLES32.glBindVertexArray(0);

		//**************************************** Third Part Of Airoplain ****************************************
		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);
		final float AiroplainFI_Vertices[] = new float[]
		{
		0.1f, 0.0f, 0.0f,
		-0.2f, 0.3f, 0.0f,
		-0.1f, 0.0f, 0.0f,

		};
	
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_AIROPLAIN, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_AIROPLAIN[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer AiroplainFIPosByteBuffer = ByteBuffer.allocateDirect(AiroplainFI_Vertices.length * 4);
		AiroplainFIPosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer AiroplainFIVerticesBuffer = AiroplainFIPosByteBuffer.asFloatBuffer();
		AiroplainFIVerticesBuffer.put(AiroplainFI_Vertices);
		AiroplainFIVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							AiroplainFI_Vertices.length * 4,	// size in bytes of the buffer object's new data store
							AiroplainFIVerticesBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		

		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);		// 4 for the triangle fan to draw a quad
		
		// Unbind vao:
		GLES32.glBindVertexArray(0);

		//************************************** Forth Part Of Airoplain ************************

		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);
		final float AiroplainT_Vertices[] = new float[]
		{
			0.1f, -0.0f, 0.0f,
			-0.2f, -0.3f, 0.0f,
			-0.1f, 0.0f, 0.0f,
		};
	
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_AIROPLAIN, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_AIROPLAIN[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer AiroplainTPosByteBuffer = ByteBuffer.allocateDirect(AiroplainT_Vertices.length * 4);
		AiroplainTPosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer AiroplainTVerticesBuffer = AiroplainFPosByteBuffer.asFloatBuffer();
		AiroplainTVerticesBuffer.put(AiroplainT_Vertices);
		AiroplainTVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							AiroplainT_Vertices.length * 4,	// size in bytes of the buffer object's new data store
							AiroplainTVerticesBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		

		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);		// 4 for the triangle fan to draw a quad
		
		// Unbind vao:
		GLES32.glBindVertexArray(0);

		//************************************************ Fifth Part Of Airoplain *************************
		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);
		final float AiroplainFO_Vertices[] = new float[]
		{
			-0.1f, 0.0f, 0.0f,
			-0.4f, 0.2f, 0.0f,
			-0.4f, -0.2f, 0.0f,
		};
	
		// A. BUFFER BLOCK FOR VERTICES:
		GLES32.glGenBuffers(1, vbo_AIROPLAIN, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_AIROPLAIN[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer AiroplainFOPosByteBuffer = ByteBuffer.allocateDirect(AiroplainFO_Vertices.length * 4);
		AiroplainFOPosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer AiroplainFOVerticesBuffer = AiroplainFPosByteBuffer.asFloatBuffer();
		AiroplainFOVerticesBuffer.put(AiroplainFO_Vertices);
		AiroplainFOVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							AiroplainFO_Vertices.length * 4,	// size in bytes of the buffer object's new data store
							AiroplainFOVerticesBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		

		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN, 0, 4);		// 4 for the triangle fan to draw a quad
		
		// Unbind vao:
		GLES32.glBindVertexArray(0);
}	/*
	// ****************************** Flag *******************
		//Red
		
		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);
		drawLine(gfMFlagOX, gfMFlagOY, -0.6f, gfMFlagOY, 1.0f, 0.6f, 0.2f);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 4 for the triangle fan to draw a quad
		GLES32.glBindVertexArray(0);
		
		// white
		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);
		drawLine(gfMFlagWX, gfMFlagWY, -0.6f, gfMFlagWY, 1.0f, 1.0f, 1.0f);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 4 for the triangle fan to draw a quad
		GLES32.glBindVertexArray(0);
		
		//green
		GLES32.glBindVertexArray(vao_AIROPLAIN[0]);
		drawLine(gfMFlagGX, gfMFlagGY, -0.6f, gfMFlagGY, 0.07f, 0.53f, 0.02f);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 4 for the triangle fan to draw a quad
		GLES32.glBindVertexArray(0);		
	}
	
	private void drawLine(float x1, float y1, float x2, float y2, float r, float g, float b)
	{
        // function declaration
//		System.out.println("AMC : in Draw Line Function: " + AMC);

        // Variable declaration
       final float[] LinePosition = new float[] {  x1, y1, 0.0f, x2, y2, 0.0f };
       final float[] LineColor = new float[] {  r, g, b, r, g, b };

        // Vertices
 

	//	GLES32.glGenBuffers(1, vbo_AIROPLAIN, 0);						// Generate buffer object names (here, for vertices);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_AIROPLAIN[0]);	// Bind a named buffer object to the specified binding point
		
		ByteBuffer ForAPosByteBuffer = ByteBuffer.allocateDirect(LinePosition.length * 4);
		ForAPosByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer ForAVerticesBuffer = ForAPosByteBuffer.asFloatBuffer();
		ForAVerticesBuffer.put(LinePosition);
		ForAVerticesBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							LinePosition.length * 4,	// size in bytes of the buffer object's new data store
							ForAVerticesBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// Index of the generic vertex attribute to be modified (vertex)
									3, 									// Number of components per generic vertex attribute (vertex)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		// Release the buffers for vertices
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
		//System.out.println("AMC : unbind position sucessfull " + AMC);
		
		// B. BUFFER BLOCK FOR COLORS:
//		GLES32.glGenBuffers(1, vbo_COLOR_AIROPLAIN, 0);							// Generate buffer object names (here, for colors);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_COLOR_AIROPLAIN[0]);		// Bind a named buffer object to the specified binding point
		//System.out.println("AMC : bind Color sucessfull " + AMC);
		
		ByteBuffer AiroplainColByteBuffer = ByteBuffer.allocateDirect(LineColor.length * 4);
		AiroplainColByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer AiroplainColorsBuffer= AiroplainColByteBuffer.asFloatBuffer();
		AiroplainColorsBuffer.put(LineColor);
		AiroplainColorsBuffer.position(0);
		
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER, 		// target
							LineColor.length * 4,		// size in bytes of the buffer object's new data store
							AiroplainColorsBuffer,					// pointer to the data that will be copied into the data store
							GLES32.GL_DYNAMIC_DRAW);			// expected usage pattern of the data store
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// Index of the generic vertex attribute to be modified (color)
									3, 									// Number of components per generic vertex attribute (color)
									GLES32.GL_FLOAT,					// Data type of each component in the array
									false, 0, 0);						// normalized, stride, offset of the first component
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		// Release the buffer for colors:
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	
		//System.out.println("AMC : unbind Color sucessfull " + AMC);
		
    }
	*/

	private void update()
	{
		//System.out.println("AMC : update " + AMC);

		if (x_For_fi >= 1.5)
		{
			x_For_fi = x_For_fi - 0.005f;
		}
		//////// FOR MOVING A
		if (y_For_fa >= 2.0f)
		{
			y_For_fa = y_For_fa - 0.005f;
		}
		/////// FOR MOVING 
		if (a_For_fn >= 0.0f)
		{
			a_For_fn = a_For_fn - 0.005f;
		}
		////// FOR MOVING SECOND I
		if (x_For_fsi >= 0.0f)
		{
			x_For_fsi = x_For_fsi - 0.005f;
		}
		else
		{
		//System.out.println("AMC : update Second i else " + AMC);

			if (Colr_D <= 1.0f) 
			{
				Colr_D = Colr_D + 0.005f;
			}
				else
				{
					//System.out.println("AMC : for Airoplain " + AMC);

					if (Airoplain_Upper_X <= 2.0f)
					{
						Airoplain_Upper_X += 0.005f;
			//			gfUFlagOX -= 0.005f;
				//		gfUFlagWX -= 0.005f;
					//	gfUFlagGX -= 0.005f;
					}
					if (Airoplain_Upper_Y >= 0.0f)
					{
						Airoplain_Upper_Y -= 0.005f;
					}
						else if (Airoplain_Upper_X >= 2.0f)
						{

							if (Airoplain_Upper_X >= -1.5f)
							{
								Airoplain_Upper_X += 0.005f;
								Airoplain_Upper_Y -= 0.005f;
							}
						}

						else if (Airoplain_Middle_X <= 2.0f)
						{
							Airoplain_Middle_X += 0.005f;
		//					gfMFlagOX -= 0.005f;
	//						gfMFlagWX -= 0.005f;
//							gfMFlagGX -= 0.005f;
						}
						else if (Airoplain_Lower_X <= 2.0f)
						{
							Airoplain_Lower_X += 0.005f;
							//gfLFlagOX -= 0.005f;
						//	gfLFlagWX -= 0.005f;
					//		gfLFlagGX -= 0.005f;
						}
							else 
							{
							//		gfMFlagOX += 0.005f;
							//		gfMFlagWX += 0.005f;
							//		gfMFlagGX += 0.005f;
									
							}
							if (Airoplain_Lower_Y <= 0.0f)
							{
								Airoplain_Lower_Y += 0.005f;
							}
							else if (Airoplain_Lower_X >= 2.0f)
							{
								if (Airoplain_Middle_X <= 4.0f)
								{
									Airoplain_Middle_X += 0.005f;
						//			gfMFlagOX += 0.005f;
							//		gfMFlagWX += 0.005f;
							//		gfMFlagGX += 0.005f;
								}
				/*				else
								{
									if (Flag <= 1.0f)
									{
										Flag = Flag + 0.005f;
									}
								}
					*/			if (Airoplain_Lower_Y <= 2.0f)
								{
									Airoplain_Lower_X += 0.005f;
									Airoplain_Lower_Y += 0.005f;
								}
							}
				
				}
		}
	}
	
	void uninitialize()
	{
			mediaPlayer.stop();

		// Code:
		// Destroy vao:
		if(vao[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao, 0);
			vao[0] = 0;
		}
		
		// Destroy vbo for vertices:
		if(vbo_Position[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_Position, 0);
			vbo_Position[0] = 0;
		}

		// Destroy vbo for colors:
		if(vbo_Color[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_Color, 0);
			vbo_Color[0] = 0;
		}
		
		if(vaoN[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vaoN, 0);
			vaoN[0] = 0;
		}
		
		// Destroy vbo for vertices:
		if(vboN[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vboN, 0);
			vboN[0] = 0;
		}

		// Destroy vbo for colors:
		if(vboColorN[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vboColorN, 0);
			vboColorN[0] = 0;
		}
		
		if(vaoD[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vaoD, 0);
			vaoD[0] = 0;
		}
		
		// Destroy vbo for vertices:
		if(vboD[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vboD, 0);
			vboD[0] = 0;
		}

		// Destroy vbo for colors:
		if(vboColorD[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vboColorD, 0);
			vboColorD[0] = 0;
		}
		
		if(vaoSecondI[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vaoSecondI, 0);
			vaoSecondI[0] = 0;
		}
		
		// Destroy vbo for vertices:
		if(vboSecondI[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vboSecondI, 0);
			vboSecondI[0] = 0;
		}

		// Destroy vbo for colors:
		if(vboColorSecondI[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vboColorSecondI, 0);
			vboColorSecondI[0] = 0;
		}
		
		if(vaoA[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vaoA, 0);
			vaoA[0] = 0;
		}
		
		// Destroy vbo for vertices:
		if(vboA[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vboA, 0);
			vboA[0] = 0;
		}

		// Destroy vbo for colors:
		if(vboColorA[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vboColorA, 0);
			vboColorA[0] = 0;
		}
				
		if(vao_AIROPLAIN[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1, vao_AIROPLAIN, 0);
			vao_AIROPLAIN[0] = 0;
		}
		
		// Destroy vbo for vertices:
		if(vbo_COLOR_AIROPLAIN[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_COLOR_AIROPLAIN, 0);
			vbo_COLOR_AIROPLAIN[0] = 0;
		}

		// Destroy vbo for colors:
		if(vbo_AIROPLAIN[0] != 0)
		{
			GLES32.glDeleteBuffers(1, vbo_AIROPLAIN, 0);
			vbo_AIROPLAIN[0] = 0;
		}
		
		// Detach and delete the shaders one by one that are attached to the the shader program object
		if(ShaderProgramObject != 0)
		{
			if(VertexShaderObject != 0)
			{
				// Detach vertex shader from the shader program and then delete it:
				GLES32.glDetachShader(ShaderProgramObject, VertexShaderObject);
				GLES32.glDeleteShader(VertexShaderObject);
				VertexShaderObject = 0;
			}
			
			if(FragmentShaderObject != 0)
			{
				// Detach fragment shader from the shader program and then delete it:
				GLES32.glDetachShader(ShaderProgramObject, FragmentShaderObject);
				GLES32.glDeleteShader(FragmentShaderObject);
				FragmentShaderObject = 0;
			}
		}
		
		// Delete the shader program object
		if(ShaderProgramObject != 0)
		{
			GLES32.glDeleteProgram(ShaderProgramObject);
			ShaderProgramObject = 0;
		}
	}
}
		
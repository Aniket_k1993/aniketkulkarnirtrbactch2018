package com.rtr.Sphere_PerVertex;						// For Multicolored Perspective triangle

import android.content.Context;									// For "Context" class

import android.opengl.GLSurfaceView;							// For "GLSurfaceView" class
import android.opengl.GLES32;									// For OpenGL ES 32
import android.opengl.GLUtils;

import javax.microedition.khronos.opengles.GL10;				// For OpenGLES 1.0 needed as param
import javax.microedition.khronos.egl.EGLConfig;				// For EGLConfig needed as param

import android.graphics.BitmapFactory;
import android.graphics.Bitmap;

import android.view.MotionEvent;								// For "MotionEvent" class
import android.view.GestureDetector;							// For "GestureDetector" class
import android.view.GestureDetector.OnGestureListener;			// For "OnGestureListener" class
import android.view.GestureDetector.OnDoubleTapListener;		// For "OnDoubleTapListener" class

// For vbo:
import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.Matrix;									// For Matrix math;
//import com.rtr.sphere;
// A view for OpenGL ES 3 graphics which also receives touch events:
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener
{
	private final Context myContext;
	private GestureDetector myGestureDetector;

	// OpenGL objects:
	private int VertexShaderObject;
	private int FragmentShaderObject;
	private int ShaderProgramObject;
	
	private int La_Uniform;				// Uniform for ambient component of light
	private int Ld_Uniform;				// Uniform for diffuse component of light
	private int Ls_Uniform;				// Uniform for specular component of light
	private int Light_Position_Uniform;	// Uniform for light position
	private int Model_Matrix_Uniform;
	private int View_Matrix_Uniform;
	private int Projection_Matrix_Uniform;
	// Matrial uniforms:
	private int Ka_Uniform;				// Uniform for ambient component of material
	private int Kd_Uniform;				// Uniform for diffuse component of material
	private int Ks_Uniform;				// Uniform for specular component of material
	private int Material_Shininess_Uniform;	// Uniform for shininess component of material

	private int LKeyPressedUniform;

    private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];
	private int numVertices;
	private	int numElements;

	private boolean gbAnimate;
	private boolean gbLight;
	private boolean LkeyIsPressed = false;

    private float[] lightAmbient = { 0.0f,0.0f,0.0f,1.0f };
    private float[] lightDiffuse = { 1.0f,1.0f,1.0f,1.0f };
    private float[] lightSpecular = { 1.0f,1.0f,1.0f,1.0f };
    private float[] lightPosition = { 100.0f,100.0f,100.0f,1.0f };

    private float[] materialAmbient = { 0.0f,0.0f,0.0f,1.0f };
    private float[] materialDiffuse = { 1.0f,1.0f,1.0f,1.0f };
    private float[] materialSpecular = { 1.0f,1.0f,1.0f,1.0f };
    private float materialShininess = 50.0f;

	String aniket;

	Sphere sphere=new Sphere();
    float sphere_vertices[]=new float[1146];
    float sphere_normals[]=new float[1146];
    float sphere_textures[]=new float[764];
    short sphere_elements[]=new short[2280];
	

	private int mvpUniform;
	private float PerspectiveProjectionMatrix[] = new float[16];		// 4x4 matrix
	//vaoRectangle
	// Constructor:
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		myContext = drawingContext;
		
		// Set the EGLContext to current supported version of OpenGL-ES:
		setEGLContextClientVersion(3);
		
		// Set the renderer:
		setRenderer(this);
		
		// Set the render mode to render only when there is change in the drawing data:
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		myGestureDetector = new GestureDetector(myContext, this, null, false);
		myGestureDetector.setOnDoubleTapListener(this);
	}
	
	// Initialize function:
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		// Print OpenGL ES version:
		String glesVersion = gl.glGetString(GL10.GL_VERSION);
		System.out.println("AMC : OpenGL ES Version : " + glesVersion);
		
		// Print OpenGL Shading Language version:
		String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("AMC : OpenGL Shading Language Version : "  );
		
		// Print supported OpenGL extensions:
		//String glesExtensions = gl.glGetString(GL10.GL_EXTENSIONS);
		//System.out.println("AMC : OpenGL Extensions supported on this device : " + glesExtensions);
		
		initialize(gl);
	}
	
	// Resize funtion:
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height)
	{
		resize(width, height);
	}
	
	// Display function:
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	// 0. Handle 'onTouchEvent' because it triggers all gesture and tap events:
	@Override
	public boolean onTouchEvent(MotionEvent e)
	{
		int eventAction = e.getAction();
		if(!myGestureDetector.onTouchEvent(e))
			super.onTouchEvent(e);
		return(true);
	}
	
	// 1. Double Tap:
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}
	
	// 2. Double Tap event:
	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	
	// 3. Single Tap:
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		
		if (LkeyIsPressed == false)
			{
			System.out.println("AMC : in OnSingleTapInformed: " + aniket);
		
				gbLight = true;
				LkeyIsPressed = true;
			System.out.println("AMC : out OnSingleTapInformed: " + aniket);
			}
			else
			{
			gbLight = true;
				LkeyIsPressed = true;
				} 

		return(true);
	}
	
	// 4. On Down:
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	
	// 5. On Single Tap UP:
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	// 6. Fling:
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
	{
		return(true);
	}
	
	// 7. Scroll: (Exit the program)
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		uninitialize();
		System.exit(0);
		return(true);
	}
	
	// 8. Long Press:
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	
	// 9. Show Press:
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	
	private void initialize(GL10 gl)
	{
		/*<-- VERTEX SHADER -->*/
		
		// Create vertex shader
		VertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		System.out.println("AMC :  i am in intialization vertex Shader: "+aniket);
		
		// Vertex shader source code:
		final String vertexShaderSourceCode = String.format
		(
		    "#version 320 es"
            + "\n"
            + "in vec4 vPosition;"
            + "in vec3 vNormal;"
            + "\n"
            + "out vec3 phong_ads_color;"
            + "\n"
            + "uniform mat4 u_model_matrix;"
            + "uniform mat4 u_view_matrix;"
            + "uniform mat4 u_projection_matrix;"
            + "uniform mediump int u_lighting_enabled;"
            + "uniform vec3 u_la;"
            + "uniform vec3 u_ld;"
            + "uniform vec3 u_ls;"
            + "uniform vec3 u_ka;"
            + "uniform vec3 u_kd;"
            + "uniform vec3 u_ks;"
            + "uniform vec4 u_light_position;"
            + "uniform float u_material_shininess;"
            + "\n"
            + "void main(void)"
            + "{"
            + "   if(u_lighting_enabled == 1)"
            + "   {"
            + "       vec4 eye_Coordinates = u_view_matrix * u_model_matrix * vPosition;"
            + "       vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);"
            + "       vec3 light_direction = normalize(vec3(u_light_position) - eye_Coordinates.xyz);"
            + "       float tn_dot_ld  = max(dot(transformed_normals, light_direction), 0.0);"
            + "       vec3 ambient = u_la * u_ka;"
            + "       vec3 diffuse = u_ld * u_kd * tn_dot_ld ;"
            + "       vec3 reflection_vector = reflect(-light_direction, transformed_normals);"
            + "       vec3 viewer_vector = normalize(-eye_Coordinates.xyz);"
            + "       vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_material_shininess);"
            + "       phong_ads_color = ambient + diffuse + specular;"
            + "   }"
            + "   else"
            + "   {"
            + "       phong_ads_color = vec3(1.0, 1.0, 1.0);"
            + "   }"
            + "\n"
            + "   gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"
            + "}"
		);
		
		// Provide source code to the vertex shader:
		// Specify the shader object and the corresponding vertex shader text (ie source code)
		GLES32.glShaderSource(VertexShaderObject, vertexShaderSourceCode);
		
		// Compile the shader and check for any errors:
		GLES32.glCompileShader(VertexShaderObject);
		
		// Error checking:
		int[] iShaderCompilationStatus = new int[1];	// 1 member integer array
		int[] iInfoLogLength = new int[1];				// 1 member integer array
		String szInfoLog = null;						// String to store the log
		
		// As there are no addresses in Java, we use an array of size 1
		GLES32.glGetShaderiv(VertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
		if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)	// Compilation failed
		{
			GLES32.glGetShaderiv(VertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(VertexShaderObject);
				System.out.println("AMC : Vertex Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		/*<-- FRAGMENT SHADER -->*/
		
		// Create Fragment Shader:
		FragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		System.out.println("AMC :  i am in intialization fragment Shader: " + aniket );
		
		// Fragment shader source code:
		final String FragmentShaderSourceCode = String.format
		(
		    "#version 320 es"
            + "\n"
            + "precision highp float;"
            + "\n"
            + "in vec3 phong_ads_color;"
            + "\n"
            + "out vec4 FragColor;"
            + "\n"
            + "void main(void)"
            + "{"
            + "   FragColor = vec4(phong_ads_color, 1.0);"
            + "}"
			);
		
		// Provide source code to the Fragment shader:
		// Specify the shader object and the corresponding fragment shader text (ie source code)
		GLES32.glShaderSource(FragmentShaderObject, FragmentShaderSourceCode);
		
		// Compile the shader and check for any errors:
		GLES32.glCompileShader(FragmentShaderObject);
		
		// Error checking:
		iShaderCompilationStatus[0] = 0;			// Re initialize
		iInfoLogLength[0] = 0;						// Re initialize
		szInfoLog = null;							// Re initialize
		
		GLES32.glGetShaderiv(FragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
		if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(FragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(FragmentShaderObject);
				System.out.println("AMC : Fragment shader compilation log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		
		// Create the Shader Program:
		ShaderProgramObject = GLES32.glCreateProgram();
		
		// Attach Vertex shader to the shader program:
		GLES32.glAttachShader(ShaderProgramObject, VertexShaderObject);
		
		// Attach the Fragment shader to the shader program:
		GLES32.glAttachShader(ShaderProgramObject, FragmentShaderObject);
		
		// Pre-link binding of shader program object with vertex shader attributes:
		GLES32.glBindAttribLocation(ShaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
		
		// Pre-link binding of shader program object with fragment shader attributes:
		GLES32.glBindAttribLocation(ShaderProgramObject, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");
		
		// Link the two shaders together to shader program object to get a single executable and check for errors:
		GLES32.glLinkProgram(ShaderProgramObject);
		
		// Error checking:
		int[] iShaderProgramLinkStatus = new int[1];		// Linking check
		iInfoLogLength[0] = 0;								// Re initialize
		szInfoLog = null;									// Re initialize
		
		GLES32.glGetProgramiv(ShaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
		if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(ShaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(ShaderProgramObject);
				System.out.println("AMC : Shader Program link log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		System.out.println("AMC : in from model to light: " + aniket );
		
		// Get MVP Uniform location:
		// The actual locations assigned to uniform variables are not known until the program object is linked successfully.
		Model_Matrix_Uniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_model_matrix");
		View_Matrix_Uniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_view_matrix");
		Projection_Matrix_Uniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_projection_matrix");

		LKeyPressedUniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_lighting_enabled");
		
	// Uniforms for Light:
	// Ambient color intensity of light:
	La_Uniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_la");
	// Diffuse color intensity of light:
	Ld_Uniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_ld");
	// Specular color intensity of light:
	Ls_Uniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_ls");
	// Position of light:
	Light_Position_Uniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_light_position");

	// Uniforms for material:
	// ambient reflective color intensity of material
	Ka_Uniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_ka");
	// diffuse reflective color intensity of material
	Kd_Uniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_kd");
	// specular reflective color intensity of material
	Ks_Uniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	Material_Shininess_Uniform = GLES32.glGetUniformLocation(ShaderProgramObject, "u_material_shininess");
	System.out.println("AMC : out from model to light: " + aniket );
        
	//Sphere sphere = new Sphere();
	sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    numVertices = sphere.getNumberOfSphereVertices();
    numElements = sphere.getNumberOfSphereElements();	
		
		// Vertices, Colors, Shader attributes, vbo, vao initializations:
	    GLES32.glGenVertexArrays(1,vao_sphere,0);
		GLES32.glBindVertexArray(vao_sphere[0]);
		System.out.println("AMC : Create Vao successfully: " + aniket );
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
		System.out.println("AMC : Create vbo position successfully: " + aniket );
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
		System.out.println("AMC : Create vbo normal successfully: " + aniket);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);


		
		// Enable depth testing and specify the depth test to perform:
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		// We will always cull back faces for better performance
		GLES32.glEnable(GLES32.GL_CULL_FACE);
		
		// Set the background color to Black:
		GLES32.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		
		// Set projectionMatrix and Identity matrix:
		Matrix.setIdentityM(PerspectiveProjectionMatrix, 0);
		System.out.println("AMC : out intialization: " + aniket );

	}
	
	private void resize(int width, int height)
	{
		System.out.println("AMC : in resize: " + aniket );

		//Code:
		// Set the viewport:
		GLES32.glViewport(0, 0, width, height);
		
		// Perspective Projection = FOV, aspect ratio, near, far:
		Matrix.perspectiveM(PerspectiveProjectionMatrix, 0, 45.0f, 
							((float)width/(float)height), 0.1f, 100.0f);
		System.out.println("AMC : out resize: " + aniket );

	}
	
	public void display()
	{
		System.out.println("AMC : in resize: " + aniket );

		// Code:
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		
		// Use shader program:
		GLES32.glUseProgram(ShaderProgramObject);
		
		if (gbLight == true)
		{
		System.out.println("AMC : in Display If Condition: " + aniket );

			// Set the 'u_lighting_enabled' uniform:
			GLES32.glUniform1i(LKeyPressedUniform, 1);

			// Set the light's properties:
			GLES32.glUniform3fv(La_Uniform, 1, lightAmbient,0);
			GLES32.glUniform3fv(Ld_Uniform, 1, lightDiffuse,0);
			GLES32.glUniform3fv(Ls_Uniform, 1, lightSpecular,0);
			GLES32.glUniform4fv(Light_Position_Uniform, 1, lightPosition,0);

			// Set the material's properties:
			GLES32.glUniform3fv(Ka_Uniform, 1, materialAmbient,0);
			GLES32.glUniform3fv(Kd_Uniform, 1, materialDiffuse,0);
			GLES32.glUniform3fv(Ks_Uniform, 1, materialSpecular,0);
			GLES32.glUniform1f(Material_Shininess_Uniform, materialShininess);
		System.out.println("AMC : out Display If Condition: " + aniket );

		}
		else
		{
			// Set the 'u_lighting_enabled' uniform:
			GLES32.glUniform1i(LKeyPressedUniform, 0);
		}
		
		// OpenGL ES drawing:
		float modelMatrix[] = new float[16];				// Eye space 
		float viewMatrix[] = new float[16];		// Clip space
		
		// Set modelview and modelviewprojection matrices to the identity matrix
		Matrix.setIdentityM(modelMatrix, 0);
		Matrix.setIdentityM(viewMatrix, 0);
		
		// Translate the ModelViewMatrix and store back in the ModelViewMatrix:
		Matrix.translateM(modelMatrix, 0, 0.0f, 0.0f, -3.0f);	// Translate back by -4.0f
		
		GLES32.glUniformMatrix4fv(Model_Matrix_Uniform, 1, false, modelMatrix, 0);
		GLES32.glUniformMatrix4fv(View_Matrix_Uniform, 1, false, viewMatrix, 0);
		GLES32.glUniformMatrix4fv(Projection_Matrix_Uniform, 1, false, PerspectiveProjectionMatrix, 0);
		
		// Bind vao:
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES32.glBindVertexArray(0);
		
		// Stop using the shader program object:
		GLES32.glUseProgram(0);
		System.out.println("AMC : out resize: " + aniket );
		
		// Render/Flush:
		requestRender();
	}
	
	void uninitialize()
	{
		// Code:
        if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }

		
		// Detach and delete the shaders one by one that are attached to the the shader program object
		if(ShaderProgramObject != 0)
		{
			if(VertexShaderObject != 0)
			{
				// Detach vertex shader from the shader program and then delete it:
				GLES32.glDetachShader(ShaderProgramObject, VertexShaderObject);
				GLES32.glDeleteShader(VertexShaderObject);
				VertexShaderObject = 0;
			}
			
			if(FragmentShaderObject != 0)
			{
				// Detach fragment shader from the shader program and then delete it:
				GLES32.glDetachShader(ShaderProgramObject, FragmentShaderObject);
				GLES32.glDeleteShader(FragmentShaderObject);
				FragmentShaderObject = 0;
			}
		}
		
		// Delete the shader program object
		if(ShaderProgramObject != 0)
		{
			GLES32.glDeleteProgram(ShaderProgramObject);
			ShaderProgramObject = 0;
		}
	}
}
		
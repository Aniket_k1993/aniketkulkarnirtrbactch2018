//Onload Function
var canvas = null;
var context = null;
function main()
{
    //Get the Canvas Element
    canvas = document.getElementById("AMC");
    if (!canvas)
        console.log("Canvas Was Get Failed\n");
    else
        console.log("Canvase Was Get Sucessfully\n");

    console.log("Canvas Width : " + canvas.width + "And Canvas Height " + canvas.height);

    context = canvas.getContext("2d");
    if (!context)
        console.log("Context Are Get Failed\n");
    else
        console.log("Context Are Get Sucessfull\n");

    //fill the Canvas Backgroung Color With Black
    context.fillStyle = "black";
    context.fillRect(0, 0, canvas.width, canvas.height);

    drawText("Hello World !!!");

    window.addEventListener("keydown", onKeyDown, false);
    window.addEventListener("click", onMouseDown, false);
}

function drawText(text)
{
    //Center the Canvas in Allignment
    context.textAlign = "center";
    context.fillBaseline = "middle";

    //Text
//    var str = "Hellow World !!!";

    context.font = "48px sans-serif";

    // text color
    context.fillStyle = "white";

    //display the text in canvas center
    context.fillText(text, canvas.width / 2, canvas.height / 2);
}

function toggleFullScreen()
{
    //code
    var fullscreenElement =
        document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;

    if (!fullscreenElement)
    {
        if (canvas.requestFullscreen)
        {
            canvas.requestFullscreen();
        }
        else if (canvas.mozRequestFullScreen)
        {
            canvas.mozRequestFullScreen();
        }
        else if (canvas.webkitRequestFullscreen)
        {
            canvas.webkitRequestFullscreen();
        }
        else if (canvas.msRequestFullscreen)
        {
            canvas.msRequestFullscreen();
        }
    }
    else
    {
        if (document.exitFullscreen)
        {
            document.exitFullscreen();
        }
        else if (document.mozCancelFullScreen)
        {
            document.mozCancelFullScreen();
        }
        else if (document.webkitExitFullscreen)
        {
            document.webkitExitFullscreen();
        }
        else if (document.msExitFullscreen)
        {
            document.msExitFullscreen();
        }
    }
}
function onKeyDown(event)
{
    switch (event.key)
    {
        case "F":
        case "f":
            toggleFullScreen();
            //drawText("Hello World!!!");
            break;
    }
}


function mouseDown()
{
    //code
    
}


//global variables
var canvas = null;
var gl = null; //for webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
    AMC_ATTRIBUTE_VERTEX: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;


var RADIUS = 500;
var gAngle = 0.0;

var vbo_position;
var vbo_normal;
var vao_cube;
var mvpUniform;
var gAngle = 0.0;
var modelMatrixUniform, viewMatrixUniform, projectionMatrixUniform;
var lightPositionUniform, onLKeyUniform;
var laUniform, ldUniformRed, lsUniformRed,ldUniformGreen, lsUniformGreen,ldUniformBlue, lsUniformBlue;
var kaUniform, kdUniform, ksUniform, materialShininessUniform;
var gbOnLKeyLight1 = false;
var perspectiveProjectionMatrix;

var light_ambient = [0.0, 0.0, 0.0];

var light_diffuseRed = [1.0, 0.0, 0.0];
var light_specularRed = [1.0, 0.0, 0.0];

var light_diffuseGreen = [0.0, 1.0, 0.0];
var light_specularGreen = [0.0, 1.0, 0.0];

var light_diffuseBlue = [0.0, 0.0, 1.0];
var light_specularBlue = [0.0, 0.0, 1.0];

var material_ambient = [0.0, 0.0, 0.0];
var material_diffuse = [1.0, 1.0, 1.0];
var material_specular = [1.0, 1.0, 1.0];

var material_shininess = 128.0;
var sphere = null;
//To start animation
var requestAnimationFrame = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

//To stop animation
var cancelAnimationFrame =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame;

//on body load function
function main() {
    //get canvas elementFromPoint
    canvas = document.getElementById("AMC");
    if (!canvas)
        console.log("Obtaining canvas from main document failed\n");
    else
        console.log("Obtaining canvas from main document succeeded\n");
    //print obtained canvas width and height on console
    console.log("Canvas width:" + canvas.width + " height:" + canvas.height);
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;


    //register keyboard and mouse event with window class
    window.addEventListener("keydown", keydown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    init();
    resize();
    draw();
}

function init() {
    //Get OpenGL context
    gl = canvas.getContext("webgl2");
    if (gl == null)
        console.log("Obtaining 2D webgl2 failed\n");
    else
        console.log("Obtaining 2D webgl2 succeeded\n");

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex shaderProgramObject
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "uniform mediump int u_on_click;" +
        "uniform vec4 u_light_positionRed;" +
        "uniform vec4 u_light_positionBlue;" +
        "uniform vec4 u_light_positionGreen;" +
        "out vec3 transformed_normals;" +
        "out vec3 light_directionRed;" +
        "out vec3 light_directionGreen;" +
        "out vec3 light_directionBlue;" +
        "out vec3 viewer_vector;" +
        "void main(void)" +
        "{" +
        "if (u_on_click == 1)" +
        "{" +
        "vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" +
        "transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" +
        "light_directionRed = vec3(u_light_positionRed) - eye_coordinates.xyz;" +
        "light_directionGreen = vec3(u_light_positionGreen) - eye_coordinates.xyz;" +
        "light_directionBlue = vec3(u_light_positionBlue) - eye_coordinates.xyz;" +

        "viewer_vector = -eye_coordinates.xyz;" +
        "}" +
        "gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}";

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    //fragmentShader
    var fragmentShaderSource =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "uniform mediump int u_on_click;" +
        "in vec3 transformed_normals;" +

        "in vec3 light_directionRed;" +
        "in vec3 light_directionGreen;" +
        "in vec3 light_directionBlue;" +

        "in vec3 viewer_vector;" +

        "uniform vec3 u_La;" +

        "uniform vec3 u_LdRed;" +
        "uniform vec3 u_LsRed;" +
      
        "uniform vec3 u_LdGreen;" +
        "uniform vec3 u_LsGreen;" +

        "uniform vec3 u_LdBlue;" +
        "uniform vec3 u_LsBlue;" +

        "uniform vec3 u_Ka;" +
        "uniform vec3 u_Kd;" +
        "uniform vec3 u_Ks;" +

        "uniform float u_material_shininess;" +

        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "vec3 phong_ads_color;" +
        "if(u_on_click == 1)" +
        "{" +
        "vec3 normalized_transformed_normals = normalize(transformed_normals);" +
        "vec3 normalized_viewer_vector = normalize(viewer_vector);" +

        "vec3 normalized_light_directionRed = normalize(light_directionRed);" +
        "float tn_dot_ldRed = max(dot(normalized_transformed_normals, normalized_light_directionRed), 0.0);" +
        "vec3 ambient = u_La * u_Ka;" +
        "vec3 diffuseRed = u_LdRed * u_Kd * tn_dot_ldRed;" +
        "vec3 reflection_vectorRed = reflect(-normalized_light_directionRed, normalized_transformed_normals);" +
        "vec3 specularRed = u_LsRed * u_Ks * pow(max(dot(reflection_vectorRed, normalized_viewer_vector),0.0), u_material_shininess);" +

        "vec3 normalized_light_directionGreen = normalize(light_directionGreen);" +
        "float tn_dot_ldGreen = max(dot(normalized_transformed_normals, normalized_light_directionGreen), 0.0);" +
        "vec3 diffuseGreen = u_LdGreen * u_Kd * tn_dot_ldGreen;" +
        "vec3 reflection_vectorGreen = reflect(-normalized_light_directionGreen, normalized_transformed_normals);" +
        "vec3 specularGreen = u_LsGreen * u_Ks * pow(max(dot(reflection_vectorGreen, normalized_viewer_vector),0.0), u_material_shininess);" +

        "vec3 normalized_light_directionBlue = normalize(light_directionBlue);" +
        "float tn_dot_ldBlue = max(dot(normalized_transformed_normals, normalized_light_directionBlue), 0.0);" +
        "vec3 diffuseBlue = u_LdBlue * u_Kd * tn_dot_ldBlue;" +
        "vec3 reflection_vectorBlue = reflect(-normalized_light_directionBlue, normalized_transformed_normals);" +
        "vec3 specularBlue = u_LsBlue * u_Ks * pow(max(dot(reflection_vectorBlue, normalized_viewer_vector),0.0), u_material_shininess);" +

        "phong_ads_color = ambient + diffuseRed + specularRed + ambient + diffuseGreen + specularGreen + ambient + diffuseBlue + specularBlue;" +
        "}" +
        "else" +
        "{" +
        "phong_ads_color = vec3(1.0, 1.0, 1.0);" +
        "}" +
        "FragColor = vec4(phong_ads_color, 1.0);" +
        "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSource);
    gl.compileShader(fragmentShaderObject);
    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //shader program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    //pre-link binidng of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");
    //linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }


    modelMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
    viewMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
    projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");

    onLKeyUniform = gl.getUniformLocation(shaderProgramObject, "u_on_click");

    // ForRed
    laUniform = gl.getUniformLocation(shaderProgramObject, "u_La");

        /*ldUniformRed, lsUniformRed,ldUniformGreen, lsUniformGreen,ldUniformBlue, lsUniformBlue;*/

    ldUniformRed = gl.getUniformLocation(shaderProgramObject, "u_LdRed");
    lsUniformRed = gl.getUniformLocation(shaderProgramObject, "u_LsRed");

    lightRedPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_positionRed");

    //For Green
    //laGreenUniform = gl.getUniformLocation(shaderProgramObject, "u_LaGreen");
    ldUniformGreen = gl.getUniformLocation(shaderProgramObject, "u_LdGreen");
    lsUniformGreen = gl.getUniformLocation(shaderProgramObject, "u_LsGreen");

    lightGreenPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_positionGreen");

    //For Blue
    //laBlueUniform = gl.getUniformLocation(shaderProgramObject, "u_LaBlue");
    ldUniformBlue = gl.getUniformLocation(shaderProgramObject, "u_LdBlue");
    lsUniformBlue = gl.getUniformLocation(shaderProgramObject, "u_LsBlue");

    lightBluePositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_positionBlue");


    kaUniform = gl.getUniformLocation(shaderProgramObject, "u_Ka");
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_Kd");
    ksUniform = gl.getUniformLocation(shaderProgramObject, "u_Ks");

    materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");

    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    //gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.CULL_FACE);
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    } else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width / canvas.height), 0.1, 100.0);

    gl.viewport(0, 0, canvas.width, canvas.height);
}
function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(shaderProgramObject);

    //lighting details
    if (gbOnLKeyLight1) {
        gl.uniform1i(onLKeyUniform, 1);
        gl.uniform3fv(laUniform, light_ambient);
        gl.uniform3fv(ldUniformRed, light_diffuseRed);
        gl.uniform3fv(lsUniformRed, light_specularRed);
        gl.uniform3fv(ldUniformGreen, light_diffuseGreen);
        gl.uniform3fv(lsUniformGreen, light_specularGreen);

        gl.uniform3fv(ldUniformBlue, light_diffuseBlue);
        gl.uniform3fv(lsUniformBlue, light_specularBlue);

        var angleInRadian = degreeToRadian(gAngle);
        var lightPosition = new Float32Array([0.0, 0.0, 0.0, 1.0]);
        lightPosition[0] = RADIUS * Math.cos(gAngle);
        lightPosition[2] = RADIUS * Math.sin(gAngle);
        gl.uniform4fv(lightRedPositionUniform, lightPosition);
        lightPosition[0] = 0.0;
        lightPosition[2] = 0.0;

        //light1
        lightPosition[1] = RADIUS * Math.cos(gAngle);
        lightPosition[2] = RADIUS * Math.sin(gAngle);
        gl.uniform4fv(lightGreenPositionUniform, lightPosition);
        lightPosition[1] = 0.0;
        lightPosition[2] = 0.0;

        //light2
        lightPosition[0] = RADIUS * Math.cos(gAngle);
        lightPosition[1] = RADIUS * Math.sin(gAngle);
        gl.uniform4fv(lightBluePositionUniform, lightPosition);
        lightPosition[0] = 0.0;
        lightPosition[2] = 0.0;


        //set material properties
        gl.uniform3fv(kaUniform, material_ambient);
        gl.uniform3fv(kdUniform, material_diffuse);
        gl.uniform3fv(ksUniform, material_specular);
        gl.uniform1f(materialShininessUniform, material_shininess);

    } else {
        gl.uniform1i(onLKeyUniform, 0);
    }
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();

    var angleInRadian = degreeToRadian(gAngle);
    mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -4.0]);
    //mat4.multiply(modelViewMatrix, modelViewMatrix, modelMatrix);
    gl.uniformMatrix4fv(modelMatrixUniform, false, modelMatrix);
    gl.uniformMatrix4fv(viewMatrixUniform, false, viewMatrix);
    gl.uniformMatrix4fv(projectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphere.draw();

    gl.useProgram(null);
    update();
    displayAnimationRequestId = requestAnimationFrame(draw);
}
function toggleFullScreen() {
    //code
    var fullScreen_element =
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    //if not full screen
    if (fullScreen_element == null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
    }
    else //restore from fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();

    }
    resize();
}

function keydown(event) {
    switch (event.keyCode) {
        case 27://Esc
            uninitialize();
            window.close();
            break;
        case 76:
        case 108:
            if (gbOnLKeyLight1)
                gbOnLKeyLight1 = false;
            else
                gbOnLKeyLight1 = true;
            break;
        case 70: //for 'F' or 'f'
            if (bFullscreen == true)
                bFullscreen = false;
            else
                bFullscreen = true;
            toggleFullScreen();
            break;
    }
}
function update() {
    if (gAngle >= 360.0)
        gAngle = 0.0;
    else
        gAngle = gAngle + 0.05;
}
function degreeToRadian(angleInDegree) {
    return (angleInDegree * Math.PI / 180);
}
function mouseDown() {

}

function uninitialize() {
    if (sphere) {
        sphere.deallocate();
        sphere = null;
    }

    if (shaderProgramObject) {
        if (fragmentShaderObject) {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            fragmentShaderObject = null;
        }

        if (vertexShaderObject) {
            gl.detachShader(shaderProgramObject, vertexShaderObject);
            vertexShaderObject = null;
        }
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}


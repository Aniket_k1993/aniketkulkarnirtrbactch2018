//global variables
var canvas = null;
var gl = null; //for webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
    AMC_ATTRIBUTE_VERTEX: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var MediaPlayer = mediaPlayer;

var  startTime = 1;
var finalTime = 1;
//For I
var vao = null;
var vbo_Position = null;
var vbo_Color = null;

//For N
var vaoN = null;
var vboN = null;
var vboColorN = null;

//For D
var vaoD = null;
var vboD = null;
var vboColorD = null;

// For Second I
var vaoSecondI = null;
var vboSecondI = null;
var vboColorSecondI = null;

// For A
var vaoA = null;
var vboA = null;
var vboColorA = null;

//For Animation To Translate One By One 
// For Animation
var x_For_fi = 3.0;
var x_For_fsi = 8.6;
var a_For_fn = 6.3;
var y_For_fa = 4.4;
var Colr_D = 0.0;
var ForWhite = 10.6;
var ForOrange = 10.6;
var ForGreen = 10.6;

// Vao&vbo
var vao_Airoplain = null;
var vbo_Airoplain_Position = null;
var vbo_Airoplain_Color = null;

// For Animation
var Airoplain_Upper_X = -4.0;
var Airoplain_Upper_Y = 3.0;
var Airoplain_Middle_X = -4.0;
var Airoplain_Middle_Y = 0.0;
var Airoplain_Lower_X = -4.0;
var Airoplain_Lower_Y = -3.0;

var gfUFlagOX = -1.0;
var gfUFlagOY = 0.03;
var gfUFlagWX = -1.0;
var gfUFlagWY = 0.0;
var gfUFlagGX = -1.0;
var gfUFlagGY = -0.03;

var gfMFlagOX = -1.0;
var gfMFlagOY = 0.03;
var gfMFlagWX = -1.0;
var gfMFlagWY = 0.0;
var gfMFlagGX = -1.0;
var gfMFlagGY = -0.03;

var gfLFlagOX = -1.0;
var gfLFlagOY = 0.03;
var gfLFlagWX = -1.0;
var gfLFlagWY = 0.0;
var gfLFlagGX = -1.0;
var gfLFlagGY = -0.03;
var Flag = 0.0;



var mvpUniform;
var gAngle = 0.0;

var perspectiveProjectionMatrix;

//To start animation
var requestAnimationFrame = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

//To stop animation
var cancelAnimationFrage =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame;

//on body load function
function main() {
    //get canvas elementFromPoint
    canvas = document.getElementById("amc");
    if (!canvas)
        console.log("Obtaining canvas from main document failed\n");
    else
        console.log("Obtaining canvas from main document succeeded\n");
    //print obtained canvas width and height on console
    console.log("Canvas width:" + canvas.width + " height:" + canvas.height);
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;
    mediaPlayer = MediaPlayer.create(drawingContext, R.raw.song);

    //register keyboard and mouse event with window class
    window.addEventListener("keydown", keydown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    init();
    resize();
    draw();
}

function init() {
    //Get OpenGL context
    gl = canvas.getContext("webgl2");
    if (gl == null)
        console.log("Obtaining 2D webgl2 failed\n");
    else
        console.log("Obtaining 2D webgl2 succeeded\n");

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex shaderProgramObject
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec4 vColor;" +
        "uniform mat4 u_mvp_matrix;" +
        "out vec4 out_color;" +
        "void main(void)" +
        "{" +
        "gl_Position = u_mvp_matrix * vPosition;" +
        "out_color = vColor;" +
        "}";
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    //fragmentShader
    var fragmentShaderSource =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 out_color;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "FragColor = out_color;" +
        "}";
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSource);
    gl.compileShader(fragmentShaderObject);
    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //shader program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    //pre-link binidng of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");
    //linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //get MVP uniform
    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

    // Vertices, Colors, Shader attributes, vbo, vao initializations:
    var forIVertices = new Float32Array([
        // Perspective triangle
        -1.0, 1.0, 0.0,		// Apex
        -1.0, -1.0, 0.0]);

    var forIColor = new Float32Array([
        1.0, 0.0, 0.0,		// Apex
        0.0, 1.0, 0.0]);

    var forNVertices = new Float32Array([
        -0.5, -1.0, 0.0,		// Apex
        -0.5, 1.0, 0.0,		// Let bottom
        -0.5, 1.0, 0.0,		// Let bottom
        0.5, -1.0, 0.0,		// Apex
        0.5, -1.0, 0.0,
        0.5, 1.0, 0.0]);

    var forNColor = new Float32Array([
        0.0, 1.0, 0.0,		// Apex
        1.0, 0.0, 0.0,		// Let bottom
        1.0, .0, 0.0,		// Apex
        0.0, 1.0, 0.0,		// Let bottom
        0.0, 1.0, 0.0,		// Apex
        1.0, 0.0, 0.0]);

    var forDVertices = new Float32Array([
        -0.5, 1.0, 0.0,		// Apex
        0.5, 1.0, 0.0,		// Let bottom
        0.5, 1.0, 0.0,		// Let bottom
        0.5, -1.0, 0.0,		// Apex
        0.5, -1.0, 0.0,
        -0.5, -1.0, 0.0,
        -0.4, -1.0, 0.0,
        -0.4, 1.0, 0.0]);

    var forSecondIVertices = new Float32Array([
        1.0, 1.0, 0.0,		// Apex
        1.0, -1.0, 0.0]);

    var forSecondIColor = new Float32Array([
        1.0, 0.0, 0.0,		// Apex
        0.0, 1.0, 0.0]);

    var forAVertices = new Float32Array([
        0.5, -1.0, 0.0,
        0.0, 1.0, 0.0,
        -0.5, -1.0, 0.0]);

    var forAColor = new Float32Array([
        0.0, 1.0, 0.0,
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0]);


    //var RectangleVertices = new Float32Array([1.0, 1.0, 0.0, -1.0, 1.0, 0.0, -1.0, -1.0, 0.0, 1.0, -1.0, 0.0]);

    //var RectangleColor = new Float32Array([0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0]);

    // First I
    vao = gl.createVertexArray();
    gl.bindVertexArray(vao);

    vbo_Position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Position);
    gl.bufferData(gl.ARRAY_BUFFER, forIVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color);
    gl.bufferData(gl.ARRAY_BUFFER, forIColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // For N
    vaoN = gl.createVertexArray();
    gl.bindVertexArray(vaoN);

    vboN = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboN);
    gl.bufferData(gl.ARRAY_BUFFER, forNVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vboColorN = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboColorN);
    gl.bufferData(gl.ARRAY_BUFFER, forNColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    //For D
    vaoD = gl.createVertexArray();
    gl.bindVertexArray(vaoD);

    vboD = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboD);
    gl.bufferData(gl.ARRAY_BUFFER, forDVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vboColorD = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboColorD);
    gl.bufferData(gl.ARRAY_BUFFER, [], gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    //For Second I
    vaoSecondI = gl.createVertexArray();
    gl.bindVertexArray(vaoSecondI);

    vboSecondI = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboSecondI);
    gl.bufferData(gl.ARRAY_BUFFER, forSecondIVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vboColorSecondI = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboColorSecondI);
    gl.bufferData(gl.ARRAY_BUFFER, forSecondIColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    //For A
    vaoA = gl.createVertexArray();
    gl.bindVertexArray(vaoA);

    vboA = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboA);
    gl.bufferData(gl.ARRAY_BUFFER, forAVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vboColorA = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboColorA);
    gl.bufferData(gl.ARRAY_BUFFER, forAColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // ***************** For Airoplain
    vao_Airoplain = gl.createVertexArray();
    gl.bindVertexArray(vao_Airoplain);

    vbo_Airoplain_Position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, 4 * 2, gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Airoplain_Color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, 4 * 2, gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);



    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    } else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width / canvas.height), 0.1, 100.0);

    gl.viewport(0, 0, canvas.width, canvas.height);
}
function draw() {
    mediaPlayer.start();
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(shaderProgramObject);

    // For I Only
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [-x_For_fi, 0.0, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vao);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.LINES, 0, 2);

    gl.bindVertexArray(null);
    //console.log("UnBind Vao Sucess");

    // For N 
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [-1.399, a_For_fn, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoN);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.LINE_STRIP, 0, 6);

    gl.bindVertexArray(null);

    // For D
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoD);
    //console.log("Bind Vao Sucess");

    var forDColor = new Float32Array([
        Colr_D, 0.0, 0.0,		// Apex
        Colr_D, 0.0, 0.0,		// Let bottom
        Colr_D, 0.0, 0.0,		// Apex
        0.0, Colr_D, 0.0,		// Let bottom
        0.0, Colr_D, 0.0,		// Apex
        0.0, Colr_D, 0.0,		// Let bottom
        0.0, Colr_D, 0.0,		// Apex
        Colr_D, 0.0, 0.0]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vboColorD);
    gl.bufferData(gl.ARRAY_BUFFER, forDColor, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.drawArrays(gl.LINE_STRIP, 0, 8);

    gl.bindVertexArray(null);

    //For Second I
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [0.1, -x_For_fsi, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoSecondI);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.LINES, 0, 2);

    gl.bindVertexArray(null);

    // For A
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [y_For_fa, 0.0, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoA);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.LINE_STRIP, 0, 3);

    gl.bindVertexArray(null);

    // Red Line For A
    gl.bindVertexArray(vao_Airoplain);

    DrawFlag(-0.255, 0.0, 0.255, 0.0, Flag, 0.0, 0.0);
    gl.drawArrays(gl.LINES,
        0,
        2);

    gl.bindVertexArray(null);


    // White Line For A
    gl.bindVertexArray(vao_Airoplain);

    DrawFlag(-0.255, -0.02, 0.255, -0.02, Flag, Flag, Flag);
    gl.drawArrays(gl.LINES,
        0,
        2);

    gl.bindVertexArray(null);

    // Green Line For A
    gl.bindVertexArray(vao_Airoplain);

    DrawFlag(-0.255, -0.04, 0.255, -0.04, 0.0, Flag, 0.0);
    gl.drawArrays(gl.LINES,
        0,
        2);

    gl.bindVertexArray(null);



    // *************************** For Airoplain
    // Upper Airoplain
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [Airoplain_Upper_X, Airoplain_Upper_Y, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    airoplain();
    
    // Middle Airoplain
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [Airoplain_Middle_X, Airoplain_Middle_Y, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    airoplain();

    // Loawer Airoplain
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [Airoplain_Lower_X, Airoplain_Lower_Y, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    airoplain();



    gl.useProgram(null);

    update();


    requestAnimationFrame(draw, canvas);
}

function airoplain() {
    // ****************** Airoplain First Part


    gl.bindVertexArray(vao_Airoplain);

    var A_FirstPart_Position = new Float32Array([
        0.6, 0.0, 0.0,
        0.1, 0.1, 0.0,
        0.1, -0.1, 0.0]);

    var A_FirstPart_Color = new Float32Array([
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, A_FirstPart_Position, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, A_FirstPart_Color, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.TRIANGLES, 0, 3);

    gl.bindVertexArray(null);


    // ***************** Second Part Airoploin
    gl.bindVertexArray(vao_Airoplain);

    var A_SecondPart_Position = new Float32Array([
        0.1, 0.1, 0.0,
        -0.3, 0.1, 0.0,
        -0.3, -0.1, 0.0,
        0.1, -0.1, 0.0]);

    var A_SecondPart_Color = new Float32Array([
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,
        0.128, 0.226, 0.238]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, A_SecondPart_Position, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, A_SecondPart_Color, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

    gl.bindVertexArray(null);

    //************************* Third Part Airoplain
    gl.bindVertexArray(vao_Airoplain);

    var A_ThirdPart_Position = new Float32Array([
        0.1, 0.0, 0.0,
        -0.2, 0.3, 0.0,
        -0.1, 0.0, 0.0]);

    var A_ThirdPart_Color = new Float32Array([
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
    ]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, A_ThirdPart_Position, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, A_ThirdPart_Color, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.TRIANGLES, 0, 3);

    gl.bindVertexArray(null);

    //******************************** Forth Part in Airoplain
    gl.bindVertexArray(vao_Airoplain);

    var A_FourthPart_Position = new Float32Array([
        0.1, -0.0, 0.0,
        -0.2, -0.3, 0.0,
        -0.1, 0.0, 0.0]);

    var A_FourthPart_Color = new Float32Array([
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
    ]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, A_FourthPart_Position, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, A_FourthPart_Color, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.TRIANGLES, 0, 3);

    gl.bindVertexArray(null);

    //******************************** Fifth Part In Airoplain
    gl.bindVertexArray(vao_Airoplain);

    var A_FifthPart_Position = new Float32Array([
        -0.1, 0.0, 0.0,
        -0.4, 0.2, 0.0,
        -0.4, -0.2, 0.0]);

    var A_FifthPart_Color = new Float32Array([
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
    ]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, A_FifthPart_Position, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, A_FifthPart_Color, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.TRIANGLES, 0, 3);

    gl.bindVertexArray(null);

    //Orange
    gl.bindVertexArray(vao_Airoplain);
    DrawFlag(gfMFlagOX, gfMFlagOY, -0.6, gfMFlagOY, 1.0, 0.6, 0.2);
    gl.drawArrays(gl.LINES,
        0,
        2);
    gl.bindVertexArray(null);
    //White
    gl.bindVertexArray(vao_Airoplain);
    DrawFlag(gfMFlagWX, gfMFlagWY, -0.6, gfMFlagWY, 1.0, 1.0, 1.0);
    gl.drawArrays(gl.LINES,
        0,
        2);
    gl.bindVertexArray(null);
    //Green
    gl.bindVertexArray(vao_Airoplain);
    DrawFlag(gfMFlagGX, gfMFlagGY, -0.6, gfMFlagGY, 0.07, 0.53, 0.02);
    gl.drawArrays(gl.LINES,
        0,
        2);
    gl.bindVertexArray(null);


    gl.bindVertexArray(null);

}

function DrawFlag(x1, y1, x2, y2, r, g, b) {
    var fLineVertices = new Float32Array([
        x1, y2, 0.0,
        x2, y2, 0.0]);

    // Color
    var fLineColor = new Float32Array([
        r, g, b,
        r, g, b]);

    // Line Vertices
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, fLineVertices, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, fLineColor, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
}


function toggleFullScreen() {
    //code
    var fullScreen_element =
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    //if not full screen
    if (fullScreen_element == null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
    }
    else //restore from fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
    }
}

function keydown(event) {
    switch (event.keyCode) {
        case 27://Esc
            uninitialize();
            window.close();
            break;
        case 70: //for 'F' or 'f'
            toggleFullScreen();
            break;
    }
}
function update() {
    if (x_For_fi >= 1.5) {
        x_For_fi = x_For_fi - 0.007;
    }
    //////// FOR MOVING A
    if (y_For_fa >= 2.0) {
        y_For_fa = y_For_fa - 0.002;
    }
    /////// FOR MOVING 
    if (a_For_fn >= 0.0) {
        a_For_fn = a_For_fn - 0.002;
    }
    ////// FOR MOVING SECOND I
    if (x_For_fsi >= 0.0) {
        x_For_fsi = x_For_fsi - 0.002;
    }
    else
    {
        if (Colr_D <= 1.0) {
            Colr_D = Colr_D + 0.0007;
        }
        else
        {
            if (Airoplain_Upper_X <= 2.0) {
                Airoplain_Upper_X += 0.005;
                gfUFlagOX -= 0.005;
                gfUFlagWX -= 0.005;
                gfUFlagGX -= 0.005;
            }
            if (Airoplain_Upper_Y >= 0.0) {
                Airoplain_Upper_Y -= 0.005;
            }
            else if (Airoplain_Upper_X >= 2.0) {
                if (Airoplain_Upper_X >= -1.5) {
                    Airoplain_Upper_X += 0.005;
                    Airoplain_Upper_Y -= 0.005;
                }
            }

            if (Airoplain_Middle_X <= 2.0) {
                Airoplain_Middle_X += 0.005;
                gfMFlagOX -= 0.005;
                gfMFlagWX -= 0.005;
                gfMFlagGX -= 0.005;
            }
            if (Airoplain_Lower_X <= 2.0) {
                Airoplain_Lower_X += 0.005;
                gfLFlagOX -= 0.005;
                gfLFlagWX -= 0.005;
                gfLFlagGX -= 0.005;
            }
            else {
                gfMFlagOX += 0.005;
                gfMFlagWX += 0.005;
                gfMFlagGX += 0.005;

            }
            if (Airoplain_Lower_Y <= 0.0) {
                Airoplain_Lower_Y += 0.005;
            }
            else if (Airoplain_Lower_X >= 2.0) {
                if (Airoplain_Middle_X <= 4.0) {
                    Airoplain_Middle_X += 0.005;
                    gfMFlagOX += 0.005;
                    gfMFlagWX += 0.005;
                    gfMFlagGX += 0.005;
                }
                else {
                    if (Flag <= 1.0) {
                        Flag = Flag + 0.005;
                    }

                }
                if (Airoplain_Lower_Y <= 2.0) {
                    Airoplain_Lower_X += 0.005;
                    Airoplain_Lower_Y += 0.005;
                }
            }
        }
    }

    //if (ForWhite >= -4.01) {
    //    ForWhite = ForWhite - 0.002;
    //}
}
function degreeToRadian(angleInDegree) {
    return (angleInDegree * Math.PI / 180);
}
function mouseDown() {
    alert("Mouse is clicked");
}

function uninitialize() {
    if (vao_triangle) {
        gl.deleteVertexArray(vao_triangle);
        vao_triangle = null;
    }
    if (vao_square) {
        gl.deleteVertexArray(vao_square);
        vao_square = null;
    }
    if (vbo_position) {
        gl.deleteBuffer(vbo_position);
        vbo_position = null;
    }

    if (vbo_color) {
        gl.deleteBuffer(vbo_color);
        vbo_color = null;
    }

    if (shaderProgramObject) {
        if (fragmentShaderObject) {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            fragmentShaderObject = null;
        }

        if (vertexShaderObject) {
            gl.detachShader(shaderProgramObject, vertexShaderObject);
            vertexShaderObject = null;
        }
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}


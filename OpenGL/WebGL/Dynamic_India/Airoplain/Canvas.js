//global variables
var canvas = null;
var gl = null; //for webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
    AMC_ATTRIBUTE_VERTEX: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

// *************** For Airoplain ***************
// Vao&vbo
var vao_Airoplain = null;
var vbo_Airoplain_Position = null;
var vbo_Airoplain_Color = null;

// For Animation
var Airoplain_Upper_X = -4.0;
var Airoplain_Upper_Y = 3.0;
var Airoplain_Middle_X = -4.0;
var Airoplain_Middle_Y = 0.0;
var Airoplain_Lower_X = -4.0;
var Airoplain_Lower_Y = -3.0;

var gfUFlagOX = -1.0;
var gfUFlagOY = 0.03;
var gfUFlagWX = -1.0;
var gfUFlagWY = 0.0;
var gfUFlagGX = -1.0;
var gfUFlagGY = -0.03;

var gfMFlagOX = -1.0;
var gfMFlagOY = 0.03;
var gfMFlagWX = -1.0;
var gfMFlagWY = 0.0;
var gfMFlagGX = -1.0;
var gfMFlagGY = -0.03;

var gfLFlagOX = -1.0;
var gfLFlagOY = 0.03;
var gfLFlagWX = -1.0;
var gfLFlagWY = 0.0;
var gfLFlagGX = -1.0;
var gfLFlagGY = -0.03;
var Flag = 0.0;


var mvpUniform;
var gAngle = 0.0;

var perspectiveProjectionMatrix;

//To start animation
var requestAnimationFrame = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

//To stop animation
var cancelAnimationFrage =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame;

//on body load function
function main() {
    //get canvas elementFromPoint
    canvas = document.getElementById("amc");
    if (!canvas)
        console.log("Obtaining canvas from main document failed\n");
    else
        console.log("Obtaining canvas from main document succeeded\n");
    //print obtained canvas width and height on console
    console.log("Canvas width:" + canvas.width + " height:" + canvas.height);
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;


    //register keyboard and mouse event with window class
    window.addEventListener("keydown", keydown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    init();
    resize();
    draw();
}

function init() {
    //Get OpenGL context
    gl = canvas.getContext("webgl2");
    if (gl == null)
        console.log("Obtaining 2D webgl2 failed\n");
    else
        console.log("Obtaining 2D webgl2 succeeded\n");

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex shaderProgramObject
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec4 vColor;" +
        "uniform mat4 u_mvp_matrix;" +
        "out vec4 out_color;" +
        "void main(void)" +
        "{" +
        "gl_Position = u_mvp_matrix * vPosition;" +
        "out_color = vColor;" +
        "}";
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    //fragmentShader
    var fragmentShaderSource =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 out_color;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "FragColor = out_color;" +
        "}";
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSource);
    gl.compileShader(fragmentShaderObject);
    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //shader program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    //pre-link binidng of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");
    //linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //get MVP uniform
    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

    // First I
    vao_Airoplain = gl.createVertexArray();
    gl.bindVertexArray(vao_Airoplain);

    vbo_Airoplain_Position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, 4 * 2, gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Airoplain_Color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, 4 * 2, gl.DYNAMIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    } else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width / canvas.height), 0.1, 100.0);

    gl.viewport(0, 0, canvas.width, canvas.height);
}
function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(shaderProgramObject);

    // Upper Airoplain
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [Airoplain_Upper_X, Airoplain_Upper_Y, -4.5]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    airoplain();

    // Middle Airoplain
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [Airoplain_Middle_X, Airoplain_Middle_Y, -4.5]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    airoplain();

    // Loawer Airoplain
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [Airoplain_Lower_X, Airoplain_Lower_Y, -4.5]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    airoplain();

    gl.useProgram(null);

    update();


    requestAnimationFrame(draw, canvas);
}

function airoplain()
{
    // ****************** Airoplain First Part

    
    gl.bindVertexArray(vao_Airoplain);

    var A_FirstPart_Position = new Float32Array([
            0.6, 0.0, 0.0,
            0.1, 0.1, 0.0,
            0.1, -0.1, 0.0]);

    var A_FirstPart_Color = new Float32Array([
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, A_FirstPart_Position, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, A_FirstPart_Color, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.TRIANGLES, 0, 3);

    gl.bindVertexArray(null);


    // ***************** Second Part Airoploin
    gl.bindVertexArray(vao_Airoplain);

    var A_SecondPart_Position = new Float32Array([
        0.1, 0.1, 0.0,
        -0.3, 0.1, 0.0,
        -0.3, -0.1, 0.0,
        0.1, -0.1, 0.0]);

    var A_SecondPart_Color = new Float32Array([
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,
        0.128, 0.226, 0.238]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, A_SecondPart_Position, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, A_SecondPart_Color, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

    gl.bindVertexArray(null);

    //************************* Third Part Airoplain
    gl.bindVertexArray(vao_Airoplain);

    var A_ThirdPart_Position = new Float32Array([
        0.1, 0.0, 0.0,
        -0.2, 0.3, 0.0,
        -0.1, 0.0, 0.0]);

    var A_ThirdPart_Color = new Float32Array([
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
        ]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, A_ThirdPart_Position, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, A_ThirdPart_Color, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.TRIANGLES, 0, 3);

    gl.bindVertexArray(null);

    //******************************** Forth Part in Airoplain
    gl.bindVertexArray(vao_Airoplain);

    var A_FourthPart_Position = new Float32Array([
        0.1, -0.0, 0.0,
        -0.2, -0.3, 0.0,
        -0.1, 0.0, 0.0]);

    var A_FourthPart_Color = new Float32Array([
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
    ]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, A_FourthPart_Position, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, A_FourthPart_Color, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.TRIANGLES, 0, 3);

    gl.bindVertexArray(null);

    //******************************** Fifth Part In Airoplain
    gl.bindVertexArray(vao_Airoplain);

    var A_FifthPart_Position = new Float32Array([
        -0.1, 0.0, 0.0,
        -0.4, 0.2, 0.0,
        -0.4, -0.2, 0.0]);

    var A_FifthPart_Color = new Float32Array([
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
        0.128, 0.226, 0.238,		// Apex
    ]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, A_FifthPart_Position, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, A_FifthPart_Color, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.TRIANGLES, 0, 3);

    gl.bindVertexArray(null);

    //Orange
    gl.bindVertexArray(vao_Airoplain);
    DrawFlag(gfMFlagOX, gfMFlagOY, -0.6, gfMFlagOY, 1.0, 0.6, 0.2);
    gl.drawArrays(gl.LINES,
        0,
        2);
    gl.bindVertexArray(null);
    //White
    gl.bindVertexArray(vao_Airoplain);
    DrawFlag(gfMFlagWX, gfMFlagWY, -0.6, gfMFlagWY, 1.0, 1.0, 1.0);
    gl.drawArrays(gl.LINES,
        0,
        2);
    gl.bindVertexArray(null);
    //Green
    gl.bindVertexArray(vao_Airoplain);
    DrawFlag(gfMFlagGX, gfMFlagGY, -0.6, gfMFlagGY, 0.07, 0.53, 0.02);
    gl.drawArrays(gl.LINES,
        0,
        2);
    gl.bindVertexArray(null);


    gl.bindVertexArray(null);

}

function DrawFlag(x1, y1, x2, y2, r, g, b)
{
    var fLineVertices = new Float32Array([
        x1, y2, 0.0,
        x2, y2, 0.0]);

    // Color
    var fLineColor = new Float32Array([
        r, g, b,
        r, g, b]);
    
    // Line Vertices
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Position);
    gl.bufferData(gl.ARRAY_BUFFER, fLineVertices, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    //console.log("Bind Vao Sucess");

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Airoplain_Color);
    gl.bufferData(gl.ARRAY_BUFFER, fLineColor, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

}
function toggleFullScreen() {
    //code
    var fullScreen_element =
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    //if not full screen
    if (fullScreen_element == null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
    }
    else //restore from fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
    }
}

function keydown(event) {
    switch (event.keyCode) {
        case 27://Esc
            uninitialize();
            window.close();
            break;
        case 70: //for 'F' or 'f'
            toggleFullScreen();
            break;
    }
}
function update()
{
    if (Airoplain_Upper_X <= 2.0)
    {
        Airoplain_Upper_X += 0.005;
        gfUFlagOX -= 0.005;
        gfUFlagWX -= 0.005;
        gfUFlagGX -= 0.005;
    }
    if (Airoplain_Upper_Y >= 0.0)
    {
        Airoplain_Upper_Y -= 0.005;
    }
			else if (Airoplain_Upper_X >= 2.0)
            {
                if (Airoplain_Upper_X >= -1.5)
                {
                    Airoplain_Upper_X += 0.005;
                    Airoplain_Upper_Y -= 0.005;
                }
            }

    if (Airoplain_Middle_X <= 2.0)
    {
        Airoplain_Middle_X += 0.005;
        gfMFlagOX -= 0.005;
        gfMFlagWX -= 0.005;
        gfMFlagGX -= 0.005;
    }
    if (Airoplain_Lower_X <= 2.0)
    {
        Airoplain_Lower_X += 0.005;
        gfLFlagOX -= 0.005;
        gfLFlagWX -= 0.005;
        gfLFlagGX -= 0.005;
    }
			else
            {
                gfMFlagOX += 0.005;
                gfMFlagWX += 0.005;
                gfMFlagGX += 0.005;

            }
    if (Airoplain_Lower_Y <= 0.0)
    {
        Airoplain_Lower_Y += 0.005;
    }
    else if (Airoplain_Lower_X >= 2.0)
    {
        if (Airoplain_Middle_X <= 4.0)
        {
            Airoplain_Middle_X += 0.005;
            gfMFlagOX += 0.005;
            gfMFlagWX += 0.005;
            gfMFlagGX += 0.005;
        }
        else
        {
            if (Flag <= 1.0)
            {
                Flag = Flag + 0.005;
            }

        }
        if (Airoplain_Lower_Y <= 2.0)
        {
            Airoplain_Lower_X += 0.005;
            Airoplain_Lower_Y += 0.005;
        }
    }
}
function degreeToRadian(angleInDegree) {
    return (angleInDegree * Math.PI / 180);
}
function mouseDown() {
    alert("Mouse is clicked");
}

function uninitialize() {
    if (vao_Airoplain) {
        gl.deleteVertexArray(vao_square);
        vao_square = null;
    }
    if (vbo_Airoplain_Position) {
        gl.deleteBuffer(vbo_position);
        vbo_position = null;
    }

    if (vbo_Airoplain_Color) {
        gl.deleteBuffer(vbo_color);
        vbo_color = null;
    }

    if (shaderProgramObject) {
        if (fragmentShaderObject) {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            fragmentShaderObject = null;
        }

        if (vertexShaderObject) {
            gl.detachShader(shaderProgramObject, vertexShaderObject);
            vertexShaderObject = null;
        }
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}


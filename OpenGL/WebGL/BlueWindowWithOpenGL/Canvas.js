//onload function
var canvas = null;
//var context = null;
var gl = null;
var bfullscreen = false;
var canvas_orignal_width;
var canvas_orignal_height;
var prespectiveprojectionmatrix;

// to start animation to have requestanimationframe() to called "cross-browser compatible
var requestanimationframe = window.requestanimationframe || window.webkitrequestanimationframe
    || window.mozrequestanimationframe || window.orequestanimationframe || window.msrequestanimationframe;

//to stop animation to have cancleanimationframe() to be called "crossbrowser" compat
var cancelanimationframe = window.cancelanimationframe ||
    window.webkitcancelrequestanimationframe || window.webkitcancelanimationframe ||
    window.mozcancelrequestanimationframe || window.mozcancelanimationframe ||
    window.ocancelrequestanimationframe || window.ocancelanimationframe ||
    window.mscancelrequestanimationframe || window.mscancelanimationframe;


function main()
{
    //get the canvas element
    canvas = document.getElementById("amc");
    if (!canvas)
        console.log("canvas was get failed\n");
    else
        console.log("canvase was get sucessfully\n");

    console.log("canvas width : " + canvas.width + "and canvas height " + canvas.height);

    canvas_orignal_width = canvas.width;
    canvas_orignal_height = canvas.height;

    window.addEventListener("keydown", onKeyDown, false);
    window.addEventListener("click", onmousedown, false);
    window.addEventListener("resize", onResize, false);

    //intialize webgl
    init();

    ////start drawing here as warning-up
    //resize(resize(window.innerWidth, window.innerHeight);
    //draw();
}

function onResize(event) {
    if (bfullscreen) {
        resize(window.innerWidth, window.innerHeight);
    } else {
        resize(canvas_orignal_width, canvas_orignal_height);
    }
}

function togglefullscreen()
{
    var fullscreenElement =
        document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;

    if (!fullscreenElement) {
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen();
        } else if (canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        } else if (canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        } else if (canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }

        bfullscreen = true;
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }

        bfullscreen = false;
    }
}

function init()
{
    //code
    //get webgl 2.0 context
    gl = canvas.getContext("webgl2");
    if (!gl)
    {
        console.log("failed  to get the rendering context for webgl");
        return;
    }

    console.log("Success to get the rendering context for webgl");
    gl.viewportwidth = canvas.width;
    gl.viewportheight = canvas.height;

    //vertex shader


    //fragment shader

    //set a clear color
    gl.clearColor(0.0, 0.0, 1.0, 1.0);

    //set tje deptj test alwayes be enable
    gl.enable(gl.DEPTH_TEST);

    // depth test to do 
    gl.depthFunc(gl.LEQUAL);

    // we will always cull back face better performence
    //gl.enable(gl.CULL_FACE);

    //Clear Depth
    gl.clearDepth(1.0);

    // Hear Can Initialize the Mat4 

    resize(canvas_orignal_width, canvas_orignal_height);
    draw();
}

function resize(width, height) {
    //code
 
    if (height === 0) {
        height = 1;
    }
        canvas.width = width;
        canvas.height = height;
  
    gl.viewport(0, 0, width, height);
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    requestAnimationFrame(draw, canvas);
}

function onKeyDown(event)
{
    switch (event.key)
    {
        case "F":
        case "f":
            togglefullscreen();
            console.log("Sucess The Full Screen");
            break;
    }
}

function mousedown()
{
    //code
    
}

function unintialize() {
    //code
}
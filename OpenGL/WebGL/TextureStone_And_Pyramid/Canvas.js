var canvas = null;
var gl = null;
var isEscapeKeyPressed = false;
var isFullscreen = false;
var canvasOriginalWidth = 1;
var canvasOriginalHeight = 1;
var displayAnimationRequestId = 0;
var requestAnimationFrame =
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

var cancelAnimationFrame =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame;

const WebGLMacros = {
    AMC_ATTRIBUTE_POSITION: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3
};

var angleCube = 0.0;
var anglePyramid = 0.0;
var speed = 0.3;

var vertexShaderObject = null;
var fragmentShaderObject = null;
var shaderProgramObject = null;

var vaoPyramid = null;
var vboPyramidPosition = null;
var vboPyramidTexture = null;
var vaoCube = null;
var vboCubePosition = null;
var vboCubeTexture = null;
var mvpUniform = null;
var textureSamplerUniform = null;
var textureKundali = null;
var textureStone = null;

var perspectiveProjectionMatrix = null;

function main() {
    canvas = document.getElementById("amc");

    if (!canvas) {
        console.log("CG | Error | Not able to get canvas.");
        return;
    }

    console.log("CG | Info | canvas found.");
    console.log("CG | Info | canvas width:", canvas.width, "canvas height:", canvas.height);

    canvasOriginalWidth = canvas.width;
    canvasOriginalHeight = canvas.height;

    window.addEventListener("keydown", onKeyDown, false);
    window.addEventListener("click", onMouseDown, false);
    window.addEventListener("resize", onResize, false);

    initialize();
    display();
}

function onKeyDown(event) {
    if (event.repeat) {
        return;
    }

    switch (event.key) {
        case "F":
        case "f":
            toggleFullscreen();
            break;

        case "Escape":
            if (!isEscapeKeyPressed) {
                isEscapeKeyPressed = true;
                cleanUp();
                alert("Rendering is stopped.");
            }
            break;

        default:
            console.log("Keydown not handled:", event.key);
            break;
    }
}

function onMouseDown(event) { }

function onResize(event) {
    if (isFullscreen) {
        resize(window.innerWidth, window.innerHeight);
    } else {
        resize(canvasOriginalWidth, canvasOriginalHeight);
    }
}

function initialize() {
    gl = canvas.getContext("webgl2");

    if (!gl) {
        console.log("CG | Error | Not able to get WebGL-2 context.");
        return;
    }

    console.log("CG | Info | WebGL2 context found.");

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    var vertexShaderCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vertexPosition;" +
        "in vec2 vertexTextureCoordinate0;" +
        "\n" +
        "out vec2 outVertexTextureCoordinate0;" +
        "\n" +
        "uniform mat4 mvpMatrix;" +
        "\n" +
        "void main(void)" +
        "{" +
        "   gl_Position = mvpMatrix * vertexPosition;" +
        "   outVertexTextureCoordinate0 = vertexTextureCoordinate0;" +
        "}";

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderCode);
    gl.compileShader(vertexShaderObject);

    if (!gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(vertexShaderObject);

        if (error.length > 0) {
            alert(error);
            cleanUp();
        }
    }
    var fragmentShaderCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "\n" +
        "in vec2 outVertexTextureCoordinate0;" +
        "\n" +
        "out vec4 fragmentColor;" +
        "\n" +
        "uniform sampler2D textureSampler0;" +
        "\n" +
        "void main(void)" +
        "{" +
        "   fragmentColor = texture(textureSampler0, outVertexTextureCoordinate0);" +
        "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderCode);
    gl.compileShader(fragmentShaderObject);

    if (!gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);

        if (error.length > 0) {
            alert(error);
            cleanUp();
        }
    }
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_POSITION, "vertexPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vertexTextureCoordinate0");
    gl.linkProgram(shaderProgramObject);

    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);

        if (error.length > 0) {
            alert(error);
            cleanUp();
        }
    }

    mvpUniform = gl.getUniformLocation(shaderProgramObject, "mvpMatrix");
    textureSamplerUniform = gl.getUniformLocation(shaderProgramObject, "textureSampler0");

    var pyramidVertices = new Float32Array([
        // Front face
        0.0,1.0,0.0,
        -1.0,-1.0,1.0,
        1.0,-1.0,1.0,

        // Right face
        0.0,1.0,0.0,
        1.0,-1.0,1.0,
        1.0,-1.0,-1.0,

        // Back face
        0.0,1.0,0.0,
        1.0,-1.0,-1.0,
        -1.0,-1.0,-1.0,

        // Left face
        0.0,1.0,0.0,
        -1.0,-1.0,-1.0,
        -1.0,-1.0,1.0
    ]);

    var pyramidTextureCoordinates = new Float32Array([
        // Front face
        0.5,1.0,
        0.0,0.0,
        1.0,0.0,

        // Right face
        0.5,1.0,
        1.0,0.0,
        0.0,0.0,

        // Back face
        0.5,1.0,
        1.0,0.0,
        0.0,0.0,

        // Left face
        0.5,1.0,
        0.0,0.0,
        1.0,0.0
    ]);

    vaoPyramid = gl.createVertexArray();
    gl.bindVertexArray(vaoPyramid);

    vboPyramidPosition = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboPyramidPosition);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vboPyramidTexture = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboPyramidTexture);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidTextureCoordinates, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // ********************** Cube  *****************
    var cubeVertices = new Float32Array([
        // Top face
        1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,

        // Bottom face
        1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,

        // Front face
        1.0, 1.0, 1.0,
        -1.0, 1.0, 1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,

        // Back face
        1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,

        // Right face
        1.0, 1.0, -1.0,
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,

        // Left face
        -1.0, 1.0, 1.0,
        -1.0, 1.0, -1.0,
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0
    ]);

    var cubeTextureCoordinates = new Float32Array([
        // Top face
        0.0, 1.0,
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,

        // Bottom face
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,
        1.0, 0.0,

        // Front face
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,

        // Back face
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,

        // right face
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,

        // left face
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0
    ]);

    vaoCube = gl.createVertexArray();
    gl.bindVertexArray(vaoCube);

    vboCubePosition = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboCubePosition);
    gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vboCubeTexture = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboCubeTexture);
    gl.bufferData(gl.ARRAY_BUFFER, cubeTextureCoordinates, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);


    perspectiveProjectionMatrix = mat4.create();

    textureKundali = loadGLTextures("resources/vijay_kundali.png");
    textureStone = loadGLTextures("resources/stone.png");

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    resize(canvasOriginalWidth, canvasOriginalHeight);
}

function initializeVertexShaderObject() {

}

function initializeFragmentShaderObject() {
    
}

function initializeShaderProgramObject() {
   
}

function initializePyramidBuffers() {

}

function initializeCubeBuffers() {
    
}

function update() {
    angleCube -= 0.5;
    anglePyramid += 0.5;

    if (angleCube <= -360.0) {
        angleCube = 0.0;
    }

    if (anglePyramid >= 360.0) {
        anglePyramid = 0.0;
    }
}

function display() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(shaderProgramObject);

    drawPyramid();
    drawCube();

    gl.useProgram(null);

    update();
    displayAnimationRequestId = requestAnimationFrame(display);
}

function drawPyramid() {
    var modelViewMatrix = mat4.create();
    var rotationMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5, 0.0, -6.0]);
    mat4.rotateY(rotationMatrix, rotationMatrix, degreeToRadian(anglePyramid));

    mat4.multiply(modelViewMatrix, modelViewMatrix, rotationMatrix);
    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoPyramid);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, textureStone);
    gl.uniform1i(textureSamplerUniform, 0);
    gl.drawArrays(gl.TRIANGLES, 0, 12);

    gl.bindVertexArray(null);
}

function drawCube() {
    var modelViewMatrix = mat4.create();
    var rotationMatrix = mat4.create();
    var scaleMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.scale(scaleMatrix, scaleMatrix, [0.75, 0.75, 0.75]);
    mat4.translate(modelViewMatrix, modelViewMatrix, [1.5, 0.0, -6.0]);
    mat4.rotateX(rotationMatrix, rotationMatrix, degreeToRadian(angleCube));
    mat4.rotateY(rotationMatrix, rotationMatrix, degreeToRadian(angleCube));
    mat4.rotateZ(rotationMatrix, rotationMatrix, degreeToRadian(angleCube));

    mat4.multiply(modelViewMatrix, modelViewMatrix, scaleMatrix);
    mat4.multiply(modelViewMatrix, modelViewMatrix, rotationMatrix);
    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoCube);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, textureKundali);
    gl.uniform1i(textureSamplerUniform, 0);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

    gl.bindVertexArray(null);
}

function loadGLTextures(resourcePath) {
    var texture = gl.createTexture();

    var pixel = new Uint8Array([0, 0, 0, 0]);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, pixel);

    var image = new Image();
    image.src = resourcePath;
    image.onload = function () {
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, image.width, image.height, 0, gl.RGB, gl.UNSIGNED_BYTE, image);
        gl.generateMipmap(gl.TEXTURE_2D);
        gl.bindTexture(gl.TEXTURE_2D, null);
    };

    gl.bindTexture(gl.TEXTURE_2D, null);
    return texture;
}

function toggleFullscreen() {
    var fullscreenElement =
        document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;

    if (!fullscreenElement) {
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen();
        } else if (canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        } else if (canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        } else if (canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }

        isFullscreen = true;
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }

        isFullscreen = false;
    }
}

function resize(width, height) {
    if (height == 0) {
        height = 1;
    }

    canvas.width = width;
    canvas.height = height;

    gl.viewport(0, 0, width, height);
    mat4.perspective(perspectiveProjectionMatrix, 45.0, width / height, 1.0, 100.0);
}

function cleanUp() {
    cancelAnimationFrame(displayAnimationRequestId);

    if (vaoPyramid) {
        gl.devareVertexArray(vaoPyramid);
        vaoPyramid = null;
    }

    if (vboPyramidPosition) {
        gl.devareBuffer(vboPyramidPosition);
        vboPyramidPosition = null;
    }

    if (vboPyramidTexture) {
        gl.devareBuffer(vboPyramidTexture);
        vboPyramidTexture = null;
    }

    if (vaoCube) {
        gl.devareVertexArray(vaoCube);
        vaoCube = null;
    }

    if (vboCubePosition) {
        gl.devareBuffer(vboCubePosition);
        vboCubePosition = null;
    }

    if (vboCubeTexture) {
        gl.devareBuffer(vboCubeTexture);
        vboCubeTexture = null;
    }

    if (shaderProgramObject) {
        if (vertexShaderObject) {
            gl.detachShader(shaderProgramObject, vertexShaderObject);
        }

        if (fragmentShaderObject) {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
        }
    }

    if (vertexShaderObject) {
        gl.devareShader(vertexShaderObject);
        vertexShaderObject = null;
    }

    if (fragmentShaderObject) {
        gl.devareShader(fragmentShaderObject);
        fragmentShaderObject = null;
    }

    if (shaderProgramObject) {
        gl.devareProgram(shaderProgramObject);
        shaderProgramObject = null;
    }

    gl.useProgram(null);

    if (textureStone) {
        gl.devareTexture(textureStone);
        textureStone = null;
    }

    if (textureKundali) {
        gl.devareTexture(textureKundali);
        textureKundali = null;
    }
}

function degreeToRadian(degree) {
    return degree * Math.PI / 180.0;
}

function isPowerOf2(value) {
    return (value & (value - 1)) == 0;
}

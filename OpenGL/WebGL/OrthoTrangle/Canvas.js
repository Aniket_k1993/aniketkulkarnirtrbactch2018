var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_Original_Width;
var canvas_Original_Height;

var isEscapeKeyPressed = false;
var displayAnimationRequestId = 0;

const WebGLMacros = {
    AMC_ATTRIBUTE_POSITION: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3
};


var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoTriangle;
var vboTriangle;

var mvpUniform;

var orthographicProjectionMatrix = null;


var requestAnimationFrame =
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

var cancelAnimationFrame =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame;

function main() {
    canvas = document.getElementById("amc");

    if (!canvas) {
        console.log("AMC | Error | Not able to get canvas.");
        return;
    }

    console.log("AMC | Info | Surface found.");
    console.log("AMC | Info | Surface width:", canvas.width, "canvas height:", canvas.height);

    canvas_Original_Width = canvas.width;
    canvas_Original_Height = canvas.height;

    window.addEventListener("keydown", onKeyDown, false);
    window.addEventListener("click", onMouseDown, false);
    window.addEventListener("resize", resize, false);

    init();

    resize();
    display();
}

function toggleFullscreen() {
    var fullscreenElement =
        document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement || null;

    if (!fullscreenElement) {
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen();
        } else if (canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        } else if (canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        } else if (canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }

        bFullscreen = true;
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }

        bFullscreen = false;
    }
}


function onKeyDown(event) {
    switch (event.key) {
        case "F":
        case "f":
            if (!event.repeat) {
                toggleFullscreen();
            }
            break;

        case "Escape":
            if (!event.repeat && !isEscapeKeyPressed) {
                isEscapeKeyPressed = true;
                unintialize();
                alert("Rendering is stopped.");
            }
            break;

        default:
            console.log("Keydown not handled:", event.key);
            break;
    }
}

function onMouseDown(event) { }

function init() {
    gl = canvas.getContext("webgl2");

    if (!gl) {
        console.log("AMC | Error | Not able to get WebGL-2 context.");
        return;
    }

    console.log("AMC | Info | 2D gl found.");

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    var vertexShaderCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "uniform mat4 mvpMatrix;" +
        "\n" +
        "void main(void)" +
        "{" +
        "   gl_Position = mvpMatrix * vPosition;" +
        "}";

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderCode);
    gl.compileShader(vertexShaderObject);

    if (!gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(vertexShaderObject);

        if (error.length > 0) {
            alert(error);
            unintialize();
        }
    }

    var fragmentShaderCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "out vec4 fragmentColor;" +
        "\n" +
        "void main(void)" +
        "{" +
        "   fragmentColor = vec4(1.0, 1.0, 0.0, 1.0);" +
        "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderCode);
    gl.compileShader(fragmentShaderObject);

    if (!gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);

        if (error.length > 0) {
            alert(error);
            unintialize();
        }
    }

    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
    gl.linkProgram(shaderProgramObject);

    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);

        if (error.length > 0) {
            alert(error);
            unintialize();
        }
    }

    mvpUniform = gl.getUniformLocation(shaderProgramObject, "mvpMatrix");
    var triangleVertices = new Float32Array([0.0, 50.0, 0.0, -50.0, -50.0, 0.0, 50.0, -50.0, 0.0]);

    vaoTriangle = gl.createVertexArray();
    gl.bindVertexArray(vaoTriangle);

    vboTriangle = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboTriangle);
    gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);
    
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    orthographicProjectionMatrix = mat4.create();
    //resize(canvas_Original_Width, canvas_Original_Height);
}

function resize(width, height) {
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
         canvas.width = canvas_Original_Width;
         canvas.height = canvas_Original_Height;
    }
   
    gl.viewport(0, 0, canvas.width, canvas.height);

    if (canvas.width <= canvas.height) {
        mat4.ortho(orthographicProjectionMatrix, -100.0, 100.0, (100.0 * (canvas.height / canvas.width)), (100.0 * (canvas.height / canvas.width)), -100.0, 100.0);
    } else {
        mat4.ortho(orthographicProjectionMatrix, (-100.0 * (canvas.width / canvas.height)), (100.0 * (canvas.width / canvas.height)), -100.0, 100.0, -100.0, 100.0);
    }
}


function display() {
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.useProgram(shaderProgramObject);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.multiply(modelViewProjectionMatrix, orthographicProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoTriangle);

    gl.drawArrays(gl.TRIANGLES, 0, 3);

    gl.bindVertexArray(null);
    gl.useProgram(null);

    requestAnimationFrame(display, canvas);
}

function unintialize() {
    cancelAnimationFrame(displayAnimationRequestId);

    if (vaoTriangle) {
        gl.devareVertexArray(vaoTriangle);
        vaoTriangle = null;
    }

    if (vboTriangle) {
        gl.devareBuffer(vboTriangle);
        vboTriangle = null;
    }

    if (shaderProgramObject) {
        if (vertexShaderObject) {
            gl.detachShader(shaderProgramObject, vertexShaderObject);
        }

        if (fragmentShaderObject) {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
        }
    }

    if (vertexShaderObject) {
        gl.devareShader(vertexShaderObject);
        vertexShaderObject = null;
    }

    if (fragmentShaderObject) {
        gl.devareShader(fragmentShaderObject);
        fragmentShaderObject = null;
    }

    if (shaderProgramObject) {
        gl.devareProgram(shaderProgramObject);
        shaderProgramObject = null;
    }

    gl.useProgram(null);
}

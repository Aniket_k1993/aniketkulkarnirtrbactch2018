//global variables
var canvas = null;
var gl = null; //for webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
    AMC_ATTRIBUTE_POSITION: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

//For I
var vao = null;
var vbo_Position = null;
var vbo_Color = null;

//For N
var vaoN = null;
var vboN = null;
var vboColorN = null;

//For D
var vaoD = null;
var vboD = null;
var vboColorD = null;

// For Second I
var vaoSecondI = null;
var vboSecondI = null;
var vboColorSecondI = null;

// For A
var vaoA = null;
var vboA = null;
var vboColorA = null;

var mvpUniform;
var gAngle = 0.0;

var perspectiveProjectionMatrix;

//To start animation
var requestAnimationFrame = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

//To stop animation
var cancelAnimationFrage =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame;

//on body load function
function main() {
    //get canvas elementFromPoint
    canvas = document.getElementById("amc");
    if (!canvas)
        console.log("Obtaining canvas from main document failed\n");
    else
        console.log("Obtaining canvas from main document succeeded\n");
    //print obtained canvas width and height on console
    console.log("Canvas width:" + canvas.width + " height:" + canvas.height);
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;


    //register keyboard and mouse event with window class
    window.addEventListener("keydown", keydown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    init();
    resize();
    draw();
}

function init() {
    //Get OpenGL context
    gl = canvas.getContext("webgl2");
    if (gl == null)
        console.log("Obtaining 2D webgl2 failed\n");
    else
        console.log("Obtaining 2D webgl2 succeeded\n");

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex shaderProgramObject
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec4 vColor;" +
        "uniform mat4 u_mvp_matrix;" +
        "out vec4 out_color;" +
        "void main(void)" +
        "{" +
        "gl_Position = u_mvp_matrix * vPosition;" +
        "out_color = vColor;" +
        "}";
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    //fragmentShader
    var fragmentShaderSource =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 out_color;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "FragColor = out_color;" +
        "}";
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSource);
    gl.compileShader(fragmentShaderObject);
    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //shader program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    //pre-link binidng of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");
    //linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //get MVP uniform
    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

    var forIVertices = new Float32Array([
        // Perspective triangle
        -1.0, 1.0, 0.0,		// Apex
        -1.0, -1.0, 0.0]);

    var forIColor = new Float32Array([
        1.0, 0.0, 0.0,		// Apex
        0.0, 1.0, 0.0]);

    var forNVertices = new Float32Array([
        -0.5, -1.0, 0.0,		// Apex
        -0.5, 1.0, 0.0,		// Let bottom
        -0.5, 1.0, 0.0,		// Let bottom
        0.5, -1.0, 0.0,		// Apex
        0.5, -1.0, 0.0,
        0.5, 1.0, 0.0]);

    var forNColor = new Float32Array([
        0.0, 1.0, 0.0,		// Apex
        1.0, 0.0, 0.0,		// Let bottom
        1.0, .0, 0.0,		// Apex
        0.0, 1.0, 0.0,		// Let bottom
        0.0, 1.0, 0.0,		// Apex
        1.0, 0.0, 0.0]);

    var forDVertices = new Float32Array([
        -0.5, 1.0, 0.0,		// Apex
        0.5, 1.0, 0.0,		// Let bottom
        0.5, 1.0, 0.0,		// Let bottom
        0.5, -1.0, 0.0,		// Apex
        0.5, -1.0, 0.0,
        -0.5, -1.0, 0.0,
        -0.4, -1.0, 0.0,
        -0.4, 1.0, 0.0]);

    var forDColor = new Float32Array([
        1.0, 0.0, 0.0,		// Apex
        1.0, 0.0, 0.0,		// Let bottom
        1.0, 0.0, 0.0,		// Apex
        0.0, 1.0, 0.0,		// Let bottom
        0.0, 1.0, 0.0,		// Apex
        0.0, 1.0, 0.0,		// Let bottom
        0.0, 1.0, 0.0,		// Apex
        1.0, 0.0, 0.0]);

    var forSecondIVertices = new Float32Array([
        1.0, 1.0, 0.0,		// Apex
        1.0, -1.0, 0.0]);

    var forSecondIColor = new Float32Array([
        1.0, 0.0, 0.0,		// Apex
        0.0, 1.0, 0.0]);

    var forAVertices = new Float32Array([
        0.5, -1.0, 0.0,
        0.0, 1.0, 0.0,
        -0.5, -1.0, 0.0]);

    var forAColor = new Float32Array([
        0.0, 1.0, 0.0,
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0]);


    //var RectangleVertices = new Float32Array([1.0, 1.0, 0.0, -1.0, 1.0, 0.0, -1.0, -1.0, 0.0, 1.0, -1.0, 0.0]);

    //var RectangleColor = new Float32Array([0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0]);

    // First I
    vao = gl.createVertexArray();
    gl.bindVertexArray(vao);

    vbo_Position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Position);
    gl.bufferData(gl.ARRAY_BUFFER, forIVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_Color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_Color);
    gl.bufferData(gl.ARRAY_BUFFER, forIColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    // For N
    vaoN = gl.createVertexArray();
    gl.bindVertexArray(vaoN);

    vboN = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboN);
    gl.bufferData(gl.ARRAY_BUFFER, forNVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vboColorN = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboColorN);
    gl.bufferData(gl.ARRAY_BUFFER, forNColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    //For D
    vaoD = gl.createVertexArray();
    gl.bindVertexArray(vaoD);

    vboD = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboD);
    gl.bufferData(gl.ARRAY_BUFFER, forDVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vboColorD = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboColorD);
    gl.bufferData(gl.ARRAY_BUFFER, forDColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    //For Second I
    vaoSecondI = gl.createVertexArray();
    gl.bindVertexArray(vaoSecondI);

    vboSecondI = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboSecondI);
    gl.bufferData(gl.ARRAY_BUFFER, forSecondIVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vboColorSecondI = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboColorSecondI);
    gl.bufferData(gl.ARRAY_BUFFER, forSecondIColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    //For A
    vaoA = gl.createVertexArray();
    gl.bindVertexArray(vaoA);

    vboA = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboA);
    gl.bufferData(gl.ARRAY_BUFFER, forAVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vboColorA = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboColorA);
    gl.bufferData(gl.ARRAY_BUFFER, forAColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);


    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    } else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width / canvas.height), 0.1, 100.0);

    gl.viewport(0, 0, canvas.width, canvas.height);
}
function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(shaderProgramObject);

    // For I Only
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5, 0.0, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vao);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.LINES, 0, 2);

    gl.bindVertexArray(null);
    //console.log("UnBind Vao Sucess");

    // For N 
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [-1.399, 0.0, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoN);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.LINE_STRIP, 0, 6);

    gl.bindVertexArray(null);

    // For D
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoD);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.LINE_STRIP, 0, 8);

    gl.bindVertexArray(null);

    //For Second I
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [0.1, 0.0, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoSecondI);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.LINES, 0, 2);

    gl.bindVertexArray(null);

    // For A
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.translate(modelViewMatrix, modelViewMatrix, [2.0, 0.0, -4.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoA);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.LINE_STRIP, 0, 3);

    gl.bindVertexArray(null);

    gl.useProgram(null);
    requestAnimationFrame(draw, canvas);
}
function toggleFullScreen() {
    //code
    var fullScreen_element =
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    //if not full screen
    if (fullScreen_element == null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
    }
    else //restore from fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
    }
}

function keydown(event) {
    switch (event.keyCode) {
        case 27://Esc
            uninitialize();
            window.close();
            break;
        case 70: //for 'F' or 'f'
            toggleFullScreen();
            break;
    }
}
function update() {
    if (gAngle >= 360.0)
        gAngle = 0.0;
    else
        gAngle = gAngle + 1.0;
}
function degreeToRadian(angleInDegree) {
    return (angleInDegree * Math.PI / 180);
}
function mouseDown() {
    alert("Mouse is clicked");
}

function uninitialize() {
    if (vao_triangle) {
        gl.deleteVertexArray(vao_triangle);
        vao_triangle = null;
    }
    if (vao_square) {
        gl.deleteVertexArray(vao_square);
        vao_square = null;
    }
    if (vbo_position) {
        gl.deleteBuffer(vbo_position);
        vbo_position = null;
    }

    if (vbo_color) {
        gl.deleteBuffer(vbo_color);
        vbo_color = null;
    }

    if (shaderProgramObject) {
        if (fragmentShaderObject) {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            fragmentShaderObject = null;
        }

        if (vertexShaderObject) {
            gl.detachShader(shaderProgramObject, vertexShaderObject);
            vertexShaderObject = null;
        }
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}


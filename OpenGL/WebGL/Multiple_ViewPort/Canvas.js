//global variables
var canvas = null;
var gl = null; //for webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros =
{
    AMC_ATTRIBUTE_VERTEX: 0,
    AMC_ATTRIBUTE_COLOR: 1,
    AMC_ATTRIBUTE_NORMAL: 2,
    AMC_ATTRIBUTE_TEXTURE0: 3
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoTriangle = null;
var vboTriangle = null;
var vboTriangleColor = null;
var flag = 0;

var mvpUniform;
var gAngle = 0.0;

var perspectiveProjectionMatrix;

//To start animation
var requestAnimationFrame = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

//To stop animation
var cancelAnimationFrage =
    window.cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame ||
    window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame ||
    window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame ||
    window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame ||
    window.msCancelAnimationFrame;

//on body load function
function main() {
    //get canvas elementFromPoint
    canvas = document.getElementById("amc");
    if (!canvas)
        console.log("Obtaining canvas from main document failed\n");
    else
        console.log("Obtaining canvas from main document succeeded\n");
    //print obtained canvas width and height on console
    console.log("Canvas width:" + canvas.width + " height:" + canvas.height);
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;


    //register keyboard and mouse event with window class
    window.addEventListener("keydown", keydown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    init();
    resize();
    draw();
}

function init() {
    //Get OpenGL context
    gl = canvas.getContext("webgl2");
    if (gl == null)
        console.log("Obtaining 2D webgl2 failed\n");
    else
        console.log("Obtaining 2D webgl2 succeeded\n");

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex shaderProgramObject
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec4 vColor;" +
        "uniform mat4 u_mvp_matrix;" +
        "out vec4 out_color;" +
        "void main(void)" +
        "{" +
        "gl_Position = u_mvp_matrix * vPosition;" +
        "out_color = vColor;" +
        "}";
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    //fragmentShader
    var fragmentShaderSource =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 out_color;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "FragColor = out_color;" +
        "}";
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSource);
    gl.compileShader(fragmentShaderObject);
    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //shader program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    //pre-link binidng of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");
    //linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //get MVP uniform
    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

    var triangleVertices = new Float32Array([0.0, 0.3, 0.0, -0.3, -0.3, 0.0, 0.3, -0.3, 0.0]);

    var traingleColor = new Float32Array([1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]);

    // Trangle
    vaoTriangle = gl.createVertexArray();
    gl.bindVertexArray(vaoTriangle);
    console.log("in vao Trangle Sucess");

    vboTriangle = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboTriangle);
    gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
    console.log("in vbo Position Sucees");

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vboTriangleColor = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboTriangleColor);
    gl.bufferData(gl.ARRAY_BUFFER, traingleColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    console.log("in vbo color Sucees");

    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    } else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    canvas.width = canvas_original_width;
    canvas.height = canvas_original_height;

    gl.viewport(0, 0, canvas.width, canvas.height);

    if (flag == 1) {
        gl.viewport(0, 0, canvas.width / 2, canvas.height / 2);
    }
    ///////// For Right Bottom
    else if (flag == 2) {
        gl.viewport(canvas.width / 2, 0, canvas.width / 2, canvas.height / 2);
    }
    ///////// For Left Top 
    else if (flag == 3) {
        gl.viewport(0, canvas.height / 2, canvas.width / 2, canvas.height / 2);
    }
    ///////// For Right Top
    else if (flag == 4) {
        gl.viewport(canvas.width / 2, canvas.height / 2, canvas.width / 2, canvas.height / 2);
    }
    ///////////
    else if (flag == 5) {
        gl.viewport(0, 0, canvas.width / 2, canvas.height);
    }
    else if (flag == 6) {
        gl.viewport(canvas.width / 2, 0, canvas.width / 2, canvas.height);
    }
    else if (flag == 7) {
        gl.viewport(0, canvas.height / 2, canvas.width / 2, canvas.height);
    }
    else if (flag == 8) {
        gl.viewport(canvas.width, canvas.height / 2, canvas.width / 2, canvas.height / 2);
    }
    else if (flag == 9) {
        gl.viewport(canvas.width * 0.5, canvas.height * 0.5, canvas.width * 0.5, canvas.height * 0.5);
    }

}
function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.useProgram(shaderProgramObject);

    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    ////mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5, 0.0, -6.0]);
    //console.log("in Translate Sucessfull");

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    gl.bindVertexArray(vaoTriangle);
    //console.log("Bind Vao Sucess");

    gl.drawArrays(gl.TRIANGLES, 0, 3);

    gl.bindVertexArray(null);

    gl.useProgram(null);


    requestAnimationFrame(draw, canvas);
}
function toggleFullScreen() {
    //code
    var fullScreen_element =
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    //if not full screen
    if (fullScreen_element == null) {
        if (canvas.requestFullscreen)
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
    }
    else //restore from fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
    }
}

function keydown(event) {
    switch (event.keyCode) {
        case 27://Esc
            uninitialize();
            window.close();
            break;
        case 49:
            alert("clicked 1");
            flag = 1;
            resize(canvas_original_width, canvas_original_height);
            break;

        case 50:
            alert("clicked 2");

            flag = 2;
            resize(canvas_original_width, canvas_original_height);
            break;

        case 51:
            alert("clicked 3");

            flag = 3;
            resize(canvas_original_width, canvas_original_height);
            break;

        case 52:
            alert("clicked 4");

            flag = 4;
            resize(canvas_original_width, canvas_original_height);
            break;

        case 53:
            alert("clicked 5");

            flag = 5;
            resize(canvas_original_width, canvas_original_height);
            break;

        case 54:
            alert("clicked 6");

            flag = 6;
            resize(canvas_original_width, canvas_original_height);
            break;

        case 55:
            alert("clicked 7");

            flag = 7;
            resize(canvas_original_width, canvas_original_height);
            break;

        case 56:
            alert("clicked 8");

            flag = 8;
            resize(canvas_original_width, canvas_original_height);
            break;

        case 57:
            alert("clicked 9");

            flag = 9;
            resize(canvas_original_width, canvas_original_height);
            break;
        case 70: //for 'F' or 'f'
            toggleFullScreen();
            break;
    }
}
function update() {
    if (gAngle >= 360.0)
        gAngle = 0.0;
    else
        gAngle = gAngle + 1.0;
}
function degreeToRadian(angleInDegree) {
    return (angleInDegree * Math.PI / 180);
}
function mouseDown() {
    alert("Mouse is clicked");
}

function uninitialize() {
    if (vao_triangle) {
        gl.deleteVertexArray(vao_triangle);
        vao_triangle = null;
    }
    if (vao_square) {
        gl.deleteVertexArray(vao_square);
        vao_square = null;
    }
    if (vbo_position) {
        gl.deleteBuffer(vbo_position);
        vbo_position = null;
    }

    if (vbo_color) {
        gl.deleteBuffer(vbo_color);
        vbo_color = null;
    }

    if (shaderProgramObject) {
        if (fragmentShaderObject) {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            fragmentShaderObject = null;
        }

        if (vertexShaderObject) {
            gl.detachShader(shaderProgramObject, vertexShaderObject);
            vertexShaderObject = null;
        }
        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}


//Headers:
#include <iostream>	
#include <stdio.h>	//For print();
#include <stdlib.h>	//For exit();
#include <memory.h>	//For memset();

//XWindows server headers:
#include <X11/Xlib.h>	//Analogous to windows.h
#include <X11/Xutil.h>	//Used for XVisualInfo struct and associated APIs ie visuals
#include <X11/XKBlib.h>	//Keyboard library
#include <X11/keysym.h>	//For key symbols 

// GLEW Headers
#include <GL/glew.h>

//OpenGL headers:
#include <GL/gl.h>
#include <GL/glx.h>	//Bridging API

#include "vmath.h"

// For Texture 
#include <SOIL/SOIL.h>

#define KUNDALI_BMP_PATH "vijay_kundali.png"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Namespaces:
using namespace std;
using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUT_TEXTURE0
};

//Global variable declarations:
bool bFullscreen = false;		//Fullscreen variable
Display *gpDisplay = NULL;		//Global pointer
XVisualInfo *gpXVisualInfo = NULL;	
Colormap gColormap;
Window gWindow;

typedef GLXContext (*glXCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;			//Rendering context. Parallel to HGLRC

FILE *gpFile = NULL;

GLuint VertexShaderObject;
GLuint FragmentShaderObject;
GLuint ShaderProgramObject;

GLuint VaoRectangle;
GLuint VboRectangle;
GLuint VboRectangleColor;

GLuint textureSamplerUniform = 0;
GLuint textureKundali = 0;
GLuint textureStone = 0;

bool loadGLTextures(GLuint *texture, const char *resourcePath);

GLuint MVP_Matrix;

GLfloat AngleTrangle = 0.0f;
GLfloat AngleRectangle = 0.0f;

//***********************************  For Only Changes_With_Light&....
bool gbLight;
GLfloat Cube_Angle = 0.0f;

// For InterLeved Array Only
bool bIsLKeyPressed = false;
GLuint modelMatrixUniform = 0;
GLuint viewMatrixUniform = 0;
GLuint projectionMatrixUniform = 0;
GLuint laUniform = 0;
GLuint ldUniform = 0;
GLuint lsUniform = 0;
GLuint kaUniform = 0;
GLuint kdUniform = 0;
GLuint ksUniform = 0;
GLuint materialShininessUniform = 0;
GLuint lightPositionUniform = 0;
GLuint isLightingEnabledUniform = 0;
GLuint textureMarble = 0;

GLfloat lightAmbient[] = { 0.25f, 0.25f, 0.25f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f };

GLfloat materialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
GLfloat materialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialShininess = 128.0f;


mat4 PrespectiveProjectionMatrix;
//entry point fuction:
int main(void)		
{
	//Function prototypes:
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void Update(void);
	void resize(int, int);
	void uninitialize(void);

	bool bDone = false;
	char ascii[26];		//For key press funtionality
	//Code:
	//Create log file:
	gpFile = fopen("Log_Programmable Pipeline.txt", "w");
	if(gpFile == NULL)
	{
		printf("Unable to create log file.\nExiting...\n");
		exit(1);
	}
	else
	{
		fprintf(gpFile, "Log file is successfully opened.\n");
	}
	
	
	//Create window:
	CreateWindow();		//Function to create a window
	
	//Initialize():
	initialize();
	
	//Game loop:
	XEvent event;		//To store the message/event. Parallel to MSG structure
	KeySym keysym;		
	int winWidth;
	int winHeight;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);		//Get next message. Fill 'event' with the address of next event. Parallel to GetMessage()
			switch(event.type)			//Switch on event type. Parallel to iMsg
			{
				case MapNotify:			//Parallel to WM_CREATE
					break;
				case KeyPress:			//Parallel to WM_KEYDOWN
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:			//Exit on Esc key press
							bDone = true;
						default:
							break;
					}
					XLookupString(&event.xkey, ascii, sizeof(ascii), NULL, NULL);
					switch(ascii[0])
					{
						case 'f':
						case 'F':
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
						case 'l':
						case 'L':
							
							if (bIsLKeyPressed == false) 
							{
								gbLight = true;
								bIsLKeyPressed = true;
							}
							else 
							{
								gbLight = false;
								bIsLKeyPressed = false;
							}
							break;

						default:
							break;
					}		
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:			//Left mouse button
							break;
						case 2:			//Middle mouse button
							break;	
						case 3:			//Right mouse button
							break;
						default:
							break;
					}
					break;
				case MotionNotify:			//Mouse move
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:				//Painting of window
					break;
				case DestroyNotify:			
					break;
				case 33:				//Close press
					bDone = true;
					break;
				default:
					break;
			}
		}
		
		display();
		Update();
		//Spin call here
	}
	uninitialize();
	return(0);
}
				
void CreateWindow(void)
{
	//Function Prototypes:
	void uninitialize(void);
	
	//Variable declarations:
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfigs = NULL;	//Array pointer to store the returned visuals
	GLXFBConfig bestGLXFBConfig;		//Store the best visual
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs = 0;
	int styleMask;
	int i;
	
	//1. Our own specified Frame Buffer
	static int frameBufferAttributes[] = 
	{
		GLX_X_RENDERABLE, True,			//We want video rendering and not image rendering
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,	//Window is drawable
		GLX_RENDER_TYPE, GLX_RGBA_BIT,		//Frame is of RGBA type
		GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,	//Equivalent to giving TrueColor in XMatchVisualInfo
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DEPTH_SIZE, 24,
		GLX_STENCIL_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		//GLX_SAMPLE_BUFFERS, 1,
		//GLX_SAMPLES, 4,
		None			//As a stopper for array
	};
	
	//Code:
	gpDisplay = XOpenDisplay(NULL);		//Pointer to struct is the return value. It returns default display pointer.
	if(gpDisplay == NULL)
	{
		printf("ERROR: Unable to open X Display.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	//2. Get a new frame buffer config that meets our attrib requirements:
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay, DefaultScreen(gpDisplay), frameBufferAttributes, &iNumFBConfigs);
	if(pGLXFBConfigs == NULL)
	{
		printf("Failed to get valid FrameBuffer Config.\nExiting now...\n");
		uninitialize();
		exit(1);
	}
	
	//3. As we are not asking the system to choose or match a visual, we get multiple visuals to choose from	
	printf("%d Matching FB Configs found.\n", iNumFBConfigs);
	
	//4. Pick that FB Config/Visual with the most samples per pixel
	int bestFramebufferConfig = -1, worstFramebufferConfig = -1, bestNumberOfSamples = -1, worstNumberOfSamples = 999;
	
	for(i = 0; i < iNumFBConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfigs[i]);
		if(pTempXVisualInfo)
		{
			int sampleBuffer, samples;
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffer);
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);
			printf("Matching Framebuffer Config = %d : Visual ID = 0x%lu : Sample_BUFFERS = %d : SAMPLES = %d\n", i, pTempXVisualInfo->visualid, sampleBuffer, samples);
			if(bestFramebufferConfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferConfig = i;
				bestNumberOfSamples = samples;
			}
			if(worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree(pTempXVisualInfo);
	}
	
	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferConfig];
	
	//Set global GLXConfig
	gGLXFBConfig = bestGLXFBConfig;
	
	//Free the FBConfig list allocated by glXChooseFBConfig()
	XFree(pGLXFBConfigs);
	
	//5. 
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);
	printf("Choose Visual ID = 0x%lu\n", gpXVisualInfo->visualid);
		
	winAttribs.border_pixel = 0;		//Take default value for color
	winAttribs.border_pixmap = 0;		//Take default pattern
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;
	
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPress | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),	//Parent window handle
				0,				//X
				0,				//Y
				WIN_WIDTH,			//W
				WIN_HEIGHT,			//H
				0,				//Thickness of border
				gpXVisualInfo->depth,		
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);
	if(!gWindow)
	{
		printf("ERROR: Failed to create Main Window.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay, gWindow, "First OpenGL X-Window");
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);	//33 turned on after this protocol
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);		//ShowWindow(); UpdateWindow(); SetFocus(), etc
}

	
void ToggleFullscreen(void)
{
	//Variable declarations:
	Atom wm_state;		//To save state of normal screen
	Atom fullscreen;	//TO save state of full screen
	XEvent xev = {0};
	
	//Code:
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);	//Network compliant. 
	memset(&xev, 0, sizeof(xev));		//Memset to default values
	
	xev.type = ClientMessage;		//Custom message
	xev.xclient.window = gWindow;		//Window for which client message is going(predefined message)
	xev.xclient.message_type = wm_state;	//Type of message that you are sending(created by you)
	xev.xclient.format = 32;		//byte size = 32
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
	
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),	//Propagates to your window
		False,						//If the message be propagated to your children
		StructureNotifyMask,
		&xev);
		
}

void initialize(void)
{
	
	void resize(int,int);
	void uninitialize(void);
	//Code:
	//6. Create a new GL context 4.5 for rendering:
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
	GLint attribs[] = 
	{
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 5,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
		0
	};
	
	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);
	if(!gGLXContext)	//On failure, fall back to old style 2.x context
	{
		GLint attribs[] = 
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0
		};
		printf("Failed to create GLX 4.5 context. Hence, using old style GLX Context. \n");
		gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);
	}
	else	//Successfully created 4.x context
	{
		printf("OpenGL Context 4.5 is created.\n");
	}
	
	//Verifying that context is a direct context
	if(!glXIsDirect(gpDisplay, gGLXContext))
	{
		printf("Indirect GLX rendering Context Obtained\n");
	}
	else
	{
		printf("Direct GLX Rendering Context Obtained\n");
	}

	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	// GLEW initialization code for GLSL. It must be here, ie after creating OpenGL context but before using any OpenGL function
	GLenum glew_error = glewInit();
	if(glew_error != GLEW_OK)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
		gGLXContext = NULL;
		
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}

	// *** VERTEX SHADER ***
	// 1. Create the shader (object is created and assigned)
	VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vertexPosition;" \
		"in vec3 vertexNormal;" \
		"in vec4 vertexColor;" \
		"in vec2 vertexTextureCoordinate0;" \
		"\n" \
		"out vec4 outVertexColor;" \
		"out vec3 tNormal;" \
		"out vec3 source;" \
		"out vec3 viewVector;" \
		"out vec2 outVertexTextureCoordinate0;" \
		"\n" \
		"uniform mat4 modelMatrix;" \
		"uniform mat4 viewMatrix;" \
		"uniform mat4 projectionMatrix;" \
		"uniform vec4 lightPosition;" \
		"uniform int isLightingEnabled;" \
		"\n" \
		"void main(void)" \
		"{" \
		"   if(isLightingEnabled == 1)" \
		"   {" \
		"       vec4 eyeCoordinates = viewMatrix * modelMatrix * vertexPosition;" \
		"       tNormal = mat3(viewMatrix * modelMatrix) * vertexNormal;" \
		"       source = vec3(lightPosition) - eyeCoordinates.xyz;" \
		"       viewVector = -eyeCoordinates.xyz;" \
		"   }" \
		"\n" \
		"   gl_Position = projectionMatrix * viewMatrix * modelMatrix * vertexPosition;" \
		"   outVertexColor = vertexColor;" \
		"   outVertexTextureCoordinate0 = vertexTextureCoordinate0;" \
		"}";

			fprintf(gpFile, "Log in Vertex Shader.\n");
	glShaderSource(VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// 4. Compile the shader - Dynamic compilation by the driver
	glCompileShader(VertexShaderObject);

	// ERROR CHECKING:
	GLint iInfoLogLength = 0;			// Length of the log created on failure to compile
	GLint iShaderCompiledStatus = 0;	// Stores shader compilation status
	char *szInfoLog = NULL;				// String to store the log

	glGetShaderiv(VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// Create the shader
	FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// FragColor is our name for the Fragment
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 outVertexColor;" \
		"in vec3 tNormal;" \
		"in vec3 source;" \
		"in vec3 viewVector;" \
		"in vec2 outVertexTextureCoordinate0;" \
		"\n" \
		"out vec4 fragmentColor;" \
		"\n" \
		"uniform int isLightingEnabled;" \
		"uniform vec3 la;" \
		"uniform vec3 ld;" \
		"uniform vec3 ls;" \
		"uniform vec3 ka;" \
		"uniform vec3 kd;" \
		"uniform vec3 ks;" \
		"uniform float materialShininess;" \
		"\n" \
		"uniform sampler2D textureSampler0;"
		"\n" \
		"void main(void)" \
		"{" \
		"   vec3 phongAdsColor;" \
		"   if(isLightingEnabled == 1)" \
		"   {" \
		"       vec3 normalizedTNormal = normalize(tNormal);" \
		"       vec3 normalizedSource = normalize(source);" \
		"       vec3 normalizedViewVector = normalize(viewVector);" \
		"       float tNormalDotLightDirection = max(dot(normalizedTNormal, normalizedSource), 0.0);" \
		"       vec3 ambient = la * ka;" \
		"       vec3 diffuse = ld * kd * tNormalDotLightDirection;" \
		"       vec3 reflectionVector = reflect(-normalizedSource, normalizedTNormal);" \
		"       vec3 specular = ls * ks * pow(max(dot(reflectionVector, normalizedViewVector), 0.0), materialShininess);" \
		"       phongAdsColor = ambient + diffuse + specular;"
		"   }" \
		"   else" \
		"   {" \
		"       phongAdsColor = vec3(1.0, 1.0, 1.0);" \
		"   }" \
		"\n" \
		"   fragmentColor = texture(textureSampler0, outVertexTextureCoordinate0) * outVertexColor * vec4(phongAdsColor, 1.0);" \
		"}";

		fprintf(gpFile, "Log in fragment Shader.\n");
	glShaderSource(FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compiler the shader
	glCompileShader(FragmentShaderObject);
	
	// Error checking for compilation done here
	glGetShaderiv(FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	// *** SHADER PROGRAM ***
	// Create
	ShaderProgramObject = glCreateProgram();		// It can link ALL the shaders. Hence, no parameter

	glAttachShader(ShaderProgramObject, VertexShaderObject);

	glAttachShader(ShaderProgramObject, FragmentShaderObject);
	
	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vertexNormal");
	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vertexColor");
	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUT_TEXTURE0, "vertexTextureCoordinate0");


	// Link the shader
	glLinkProgram(ShaderProgramObject);
	
		fprintf(gpFile, "linked Seccessfll The ShaderProgramObject.\n");
	// Error checking for linking
	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(ShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)	// Failure to link
	{
		glGetProgramiv(ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetProgramInfoLog(ShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program shader link log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// Get MVP uniform location
	modelMatrixUniform = glGetUniformLocation(ShaderProgramObject, "modelMatrix");
	viewMatrixUniform = glGetUniformLocation(ShaderProgramObject, "viewMatrix");
	projectionMatrixUniform = glGetUniformLocation(ShaderProgramObject, "projectionMatrix");

	isLightingEnabledUniform = glGetUniformLocation(ShaderProgramObject, "isLightingEnabled");

	laUniform = glGetUniformLocation(ShaderProgramObject, "la");
	ldUniform = glGetUniformLocation(ShaderProgramObject, "ld");
	lsUniform = glGetUniformLocation(ShaderProgramObject, "ls");
	kaUniform = glGetUniformLocation(ShaderProgramObject, "ka");
	kdUniform = glGetUniformLocation(ShaderProgramObject, "kd");
	ksUniform = glGetUniformLocation(ShaderProgramObject, "ks");
	lightPositionUniform = glGetUniformLocation(ShaderProgramObject, "lightPosition");
	materialShininessUniform = glGetUniformLocation(ShaderProgramObject, "materialShininess");

	const GLfloat cubeVertices[] = 
	{	
			-1.0f, 1.0f, 1.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 0.0f, 0.0f,
		-1.0f, -1.0f, 1.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,0.0f,0.0f, 0.0f,0.0f,1.0f, 0.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 0.0f,1.0f,0.0f, 1.0f,0.0f,0.0f, 0.0f, 0.0f,
		1.0f, -1.0f, 1.0f, 0.0f,1.0f,0.0f, 1.0f,0.0f,0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f, 0.0f,1.0f,0.0f, 1.0f,0.0f,0.0f, 1.0f, 1.0f,
		1.0f, 1.0f, -1.0f, 0.0f,1.0f,0.0f, 1.0f,0.0f,0.0f, 0.0f, 1.0f,
		1.0f, 1.0f, -1.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f,-1.0f, 0.0f, 0.0f,
		1.0f, -1.0f, -1.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f,-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f,-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f, 0.0f,0.0f,1.0f, 0.0f,0.0f,-1.0f, 0.0f, 1.0f,
		-1.0f, 1.0f, -1.0f, 0.0f,1.0f,1.0f, -1.0f,0.0f,0.0f, 0.0f, 0.0f,
		-1.0f, -1.0f, -1.0f, 0.0f,1.0f,1.0f, -1.0f,0.0f,0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f, 0.0f,1.0f,1.0f, -1.0f,0.0f,0.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f, 0.0f,1.0f,1.0f, -1.0f,0.0f,0.0f, 0.0f, 1.0f,
		-1.0f, 1.0f, -1.0f, 1.0f,0.5f,0.0f, 0.0f,1.0f,0.0f, 0.0f, 0.0f,
		-1.0f, 1.0f, 1.0f, 1.0f,0.5f,0.0f, 0.0f,1.0f,0.0f,1.0f, 0.0f,
		1.0f, 1.0f, 1.0f, 1.0f,0.5f,0.0f, 0.0f,1.0f,0.0f, 	1.0f, 1.0f,
		1.0f, 1.0f, -1.0f, 1.0f,0.5f,0.0f, 0.0f,1.0f,0.0f, 0.0f, 1.0f,
		-1.0f, -1.0f, -1.0f, 0.0f,0.7f,1.0f, 0.0f,-1.0f,0.0f, 0.0f, 0.0f,
		-1.0f, -1.0f, 1.0f, 0.0f,0.7f,1.0f, 0.0f,-1.0f,0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f, 0.0f,0.7f,1.0f, 0.0f,-1.0f,0.0f, 1.0f, 1.0f,
		1.0f, -1.0f, -1.0f, 0.0f,0.7f,1.0f, 0.0f,-1.0f,0.0f, 0.0f, 1.0f
	};

	// B. BLOCK FOR SQUARE:
	glGenVertexArrays(1, &VaoRectangle);
	glBindVertexArray(VaoRectangle);

		fprintf(gpFile, "Geting The Vao Cube.\n");
	// A. BUFFER BLOCK FOR VERTICES:
	glGenBuffers(1, &VboRectangle);						// Buffer to store vertex position
	glBindBuffer(GL_ARRAY_BUFFER, VboRectangle);		// Find that named object in memory
	glBufferData(GL_ARRAY_BUFFER, 24 * 11 * sizeof(float), cubeVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);


	glVertexAttribPointer(AMC_ATTRIBUT_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(9 * sizeof(float)));
	glEnableVertexAttribArray(AMC_ATTRIBUT_TEXTURE0);

	// Release the buffer for vertices:
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	glBindVertexArray(0); 			// Unbind
	
	// Background
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);	
       glClearDepth(1.0f);

       glEnable(GL_DEPTH_TEST);
       glEnable(GL_TEXTURE_2D);

       // Disable face culling to see back side of object when rotated.
       // glEnable(GL_CULL_FACE);

       glDepthFunc(GL_LEQUAL);
       glShadeModel(GL_SMOOTH);
       glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);


	// Set PerspectiveMatrix to identity matrix
	PrespectiveProjectionMatrix = mat4::identity();
		
	loadGLTextures(&textureKundali, KUNDALI_BMP_PATH);

	resize(WIN_WIDTH, WIN_HEIGHT);
} 

bool loadGLTextures(GLuint *texture, const char *resourcePath)
{
	bool textureLoaded = false;
    int width = 0;
    int height = 0;

    unsigned char *imageData = SOIL_load_image(resourcePath, &width, &height, 0, SOIL_LOAD_RGB);

    if (imageData != NULL)
	{
		textureLoaded = true;
		glGenTextures(1, texture);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		// Generate the mipmapped texture
        // For SOIL we need GL_RGB instead of GL_BGR_EXT
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, (GLsizei)width, (GLsizei)height, 0, GL_RGB, GL_UNSIGNED_BYTE, (GLvoid *)imageData);
		glGenerateMipmap(GL_TEXTURE_2D);
        SOIL_free_image_data(imageData);

        // Unbind the texture else the last loaded texture will be shown in display
        // if we fo not forgot to specify to which texture to bind in display.
        glBindTexture(GL_TEXTURE_2D, 0);
	}

	return textureLoaded;
}

void display(void)
{
	//Code:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	

	glUseProgram(ShaderProgramObject);	
	if (gbLight == true) 
	{	
		glUniform1i(isLightingEnabledUniform, 1);
		//setting light's properties
		glUniform3fv(laUniform, 1, lightAmbient);
		glUniform3fv(ldUniform, 1, lightDiffuse);
		glUniform3fv(lsUniform, 1, lightSpecular);

		glUniform4fv(lightPositionUniform, 1, lightPosition);
		
		//set material properties
		glUniform3fv(kaUniform, 1, materialAmbient);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);
		glUniform1f(materialShininessUniform, materialShininess);

		//setting materia properties
	}
	else 
	{
		glUniform1i(isLightingEnabledUniform, 0);
	}

	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	modelMatrix = translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = rotate(AngleRectangle, 1.0f, 0.0f, 0.0f);
	rotationMatrix *= rotate(AngleRectangle, 0.0f, 1.0f, 0.0f);
	rotationMatrix *= rotate(AngleRectangle, 0.0f, 0.0f, 1.0f);
	modelMatrix = modelMatrix * rotationMatrix;
	// Pass above matrices to shaders
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, PrespectiveProjectionMatrix);


    	glActiveTexture(GL_TEXTURE0);
    	glBindTexture(GL_TEXTURE_2D, textureKundali);
    	glUniform1i(textureSamplerUniform, 0);
	glEnable(GL_TEXTURE_2D);


	// BIND vao
	glBindVertexArray(VaoRectangle);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);	
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);	
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);	
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);	
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);	
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);	

	// Unbind vao
	glBindVertexArray(0);

	glUseProgram(0);

	glXSwapBuffers(gpDisplay, gWindow);
}

void Update(void)
{

	 AngleRectangle += 0.02f;
         if(AngleRectangle >= 360.0f)
         {
             AngleRectangle = 0.0f;
         }

}
void resize(int width, int height)
{
	//Code:
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	PrespectiveProjectionMatrix = perspective(45.0f,((GLfloat)width / (GLfloat)height),0.1f,100.0f);	
}
	
void uninitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
       // Destroy vao
    
    if(VaoRectangle)
    {
        glDeleteVertexArrays(1, &VaoRectangle);
        VaoRectangle = 0;
    }

    if(VboRectangle)
    {
        glDeleteBuffers(1, &VboRectangle);
        VboRectangle= 0;
    }

    if(VboRectangleColor)
    {
        glDeleteBuffers(1, &VboRectangleColor);
        VboRectangleColor= 0;
    }

    if(ShaderProgramObject)
    {
        if(VertexShaderObject)
        {
            glDetachShader(ShaderProgramObject, VertexShaderObject);
        }

        if(FragmentShaderObject)
        {
            glDetachShader(ShaderProgramObject, FragmentShaderObject);
        }
    }

    if(VertexShaderObject)
    {
        glDeleteShader(VertexShaderObject);
        VertexShaderObject = 0;
    }

    if(FragmentShaderObject)
    {
        glDeleteShader(FragmentShaderObject);
        FragmentShaderObject = 0;
    }

    if(ShaderProgramObject)
    {
        glDeleteProgram(ShaderProgramObject);
        ShaderProgramObject = 0;
    }

    glUseProgram(0);

    if (textureStone)
	{
		glDeleteTextures(1, &textureStone);
		textureStone = 0;
	}

	if (textureKundali)
	{
		glDeleteTextures(1, &textureKundali);
		textureKundali = 0;
    }
 
       if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
       {
             glXMakeCurrent(gpDisplay, 0, 0);
       }
 
       if(gGLXContext)
       {
             glXDestroyContext(gpDisplay, gGLXContext);
       }
 

	if(gpFile)
	{
		fprintf(gpFile, "Log file is successfully closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
		





















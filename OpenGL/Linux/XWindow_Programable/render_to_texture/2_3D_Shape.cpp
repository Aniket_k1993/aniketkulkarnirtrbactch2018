//GLXContext with Frame Buffer Configurations
//Code for programmable pipeline
//Code for Vulcan programming

//Headers:
#include <iostream>	
#include <stdio.h>	//For print();
#include <stdlib.h>	//For exit();
#include <memory.h>	//For memset();

//XWindows server headers:
#include <X11/Xlib.h>	//Analogous to windows.h
#include <X11/Xutil.h>	//Used for XVisualInfo struct and associated APIs ie visuals
#include <X11/XKBlib.h>	//Keyboard library
#include <X11/keysym.h>	//For key symbols 

// GLEW Headers
#include <GL/glew.h>

//OpenGL headers:
#include <GL/gl.h>
#include <GL/glx.h>	//Bridging API

#include "vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Namespaces:
using namespace std;
using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUT_TEXTURE0
};

//Global variable declarations:
bool bFullscreen = false;		//Fullscreen variable
Display *gpDisplay = NULL;		//Global pointer
XVisualInfo *gpXVisualInfo = NULL;	
Colormap gColormap;
Window gWindow;

typedef GLXContext (*glXCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;			//Rendering context. Parallel to HGLRC

FILE *gpFile = NULL;

GLuint vao_pyramid;
GLuint vao_cube;

GLuint vbo_position_pyramid;
GLuint vbo_position_cube;

GLuint vbo_color_pyramid;
GLuint vbo_color_cube;

//For Texture Code
GLuint TextureKundali;			// Kundali texture object
GLfloat anglePyramid = 0.0;
GLfloat angleCube = 0.0;

//Shader Program Objects
GLint gShaderProgramObject;
GLint gVertexShaderObject;
GLint gFragementShaderObject;

//Addition
GLint gShaderProgramObject_FBO;
GLuint mvpUniform_FBO;
GLuint samplerUniform_FBO;
GLuint vbo_texCoord_Cube;
GLuint FBO;
GLuint texture_FBO;
GLuint fbo_depth;
GLuint TextureSamplerUniform;	// to catch uniform of sampler
GLint gVertexShaderObject_FBO;
GLint gFragementShaderObject_FBO;

int windowWidth;
int windowHeight;


GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;

//entry point fuction:
int main(void)		
{
	//Function prototypes:
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void Update(void);
	void resize(int, int);
	void uninitialize(void);

	bool bDone = false;
	char ascii[26];		//For key press funtionality
	//Code:
	//Create log file:
	gpFile = fopen("Log_Programmable Pipeline.txt", "w");
	if(gpFile == NULL)
	{
		printf("Unable to create log file.\nExiting...\n");
		exit(1);
	}
	else
	{
		fprintf(gpFile, "Log file is successfully opened.\n");
	}
	
	
	//Create window:
	CreateWindow();		//Function to create a window
	
	//Initialize():
	initialize();
	
	//Game loop:
	XEvent event;		//To store the message/event. Parallel to MSG structure
	KeySym keysym;		
	int winWidth;
	int winHeight;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);		//Get next message. Fill 'event' with the address of next event. Parallel to GetMessage()
			switch(event.type)			//Switch on event type. Parallel to iMsg
			{
				case MapNotify:			//Parallel to WM_CREATE
					break;
				case KeyPress:			//Parallel to WM_KEYDOWN
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:			//Exit on Esc key press
							bDone = true;
						default:
							break;
					}
					XLookupString(&event.xkey, ascii, sizeof(ascii), NULL, NULL);
					switch(ascii[0])
					{
						case 'f':
						case 'F':
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
						default:
							break;
					}		
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:			//Left mouse button
							break;
						case 2:			//Middle mouse button
							break;	
						case 3:			//Right mouse button
							break;
						default:
							break;
					}
					break;
				case MotionNotify:			//Mouse move
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:				//Painting of window
					break;
				case DestroyNotify:			
					break;
				case 33:				//Close press
					bDone = true;
					break;
				default:
					break;
			}
		}
		
		display();
		Update();
		//Spin call here
	}
	uninitialize();
	return(0);
}
				
void CreateWindow(void)
{
	//Function Prototypes:
	void uninitialize(void);
	
	//Variable declarations:
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfigs = NULL;	//Array pointer to store the returned visuals
	GLXFBConfig bestGLXFBConfig;		//Store the best visual
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs = 0;
	int styleMask;
	int i;
	
	//1. Our own specified Frame Buffer
	static int frameBufferAttributes[] = 
	{
		GLX_X_RENDERABLE, True,			//We want video rendering and not image rendering
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,	//Window is drawable
		GLX_RENDER_TYPE, GLX_RGBA_BIT,		//Frame is of RGBA type
		GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,	//Equivalent to giving TrueColor in XMatchVisualInfo
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DEPTH_SIZE, 24,
		GLX_STENCIL_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		//GLX_SAMPLE_BUFFERS, 1,
		//GLX_SAMPLES, 4,
		None			//As a stopper for array
	};
	
	//Code:
	gpDisplay = XOpenDisplay(NULL);		//Pointer to struct is the return value. It returns default display pointer.
	if(gpDisplay == NULL)
	{
		printf("ERROR: Unable to open X Display.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	//2. Get a new frame buffer config that meets our attrib requirements:
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay, DefaultScreen(gpDisplay), frameBufferAttributes, &iNumFBConfigs);
	if(pGLXFBConfigs == NULL)
	{
		printf("Failed to get valid FrameBuffer Config.\nExiting now...\n");
		uninitialize();
		exit(1);
	}
	
	//3. As we are not asking the system to choose or match a visual, we get multiple visuals to choose from	
	printf("%d Matching FB Configs found.\n", iNumFBConfigs);
	
	//4. Pick that FB Config/Visual with the most samples per pixel
	int bestFramebufferConfig = -1, worstFramebufferConfig = -1, bestNumberOfSamples = -1, worstNumberOfSamples = 999;
	
	for(i = 0; i < iNumFBConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfigs[i]);
		if(pTempXVisualInfo)
		{
			int sampleBuffer, samples;
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffer);
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);
			printf("Matching Framebuffer Config = %d : Visual ID = 0x%lu : Sample_BUFFERS = %d : SAMPLES = %d\n", i, pTempXVisualInfo->visualid, sampleBuffer, samples);
			if(bestFramebufferConfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferConfig = i;
				bestNumberOfSamples = samples;
			}
			if(worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree(pTempXVisualInfo);
	}
	
	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferConfig];
	
	//Set global GLXConfig
	gGLXFBConfig = bestGLXFBConfig;
	
	//Free the FBConfig list allocated by glXChooseFBConfig()
	XFree(pGLXFBConfigs);
	
	//5. 
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);
	printf("Choose Visual ID = 0x%lu\n", gpXVisualInfo->visualid);
		
	winAttribs.border_pixel = 0;		//Take default value for color
	winAttribs.border_pixmap = 0;		//Take default pattern
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;
	
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPress | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),	//Parent window handle
				0,				//X
				0,				//Y
				WIN_WIDTH,			//W
				WIN_HEIGHT,			//H
				0,				//Thickness of border
				gpXVisualInfo->depth,		
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);
	if(!gWindow)
	{
		printf("ERROR: Failed to create Main Window.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay, gWindow, "First OpenGL X-Window");
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);	//33 turned on after this protocol
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);		//ShowWindow(); UpdateWindow(); SetFocus(), etc
}

	
void ToggleFullscreen(void)
{
	//Variable declarations:
	Atom wm_state;		//To save state of normal screen
	Atom fullscreen;	//TO save state of full screen
	XEvent xev = {0};
	
	//Code:
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);	//Network compliant. 
	memset(&xev, 0, sizeof(xev));		//Memset to default values
	
	xev.type = ClientMessage;		//Custom message
	xev.xclient.window = gWindow;		//Window for which client message is going(predefined message)
	xev.xclient.message_type = wm_state;	//Type of message that you are sending(created by you)
	xev.xclient.format = 32;		//byte size = 32
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
	
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),	//Propagates to your window
		False,						//If the message be propagated to your children
		StructureNotifyMask,
		&xev);
		
}

void initialize(void)
{
	
	void resize(int,int);
	void uninitialize(void);
	//Code:
	//6. Create a new GL context 4.5 for rendering:
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
	GLint attribs[] = 
	{
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 5,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
		0
	};
	
	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);
	if(!gGLXContext)	//On failure, fall back to old style 2.x context
	{
		//When a context version below 3.0 is requested, implementations will return the newest context version
		//compatible with openGL versions less than version 3.0
		GLint attribs[] = 
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0
		};
		printf("Failed to create GLX 4.5 context. Hence, using old style GLX Context. \n");
		gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);
	}
	else	//Successfully created 4.x context
	{
		printf("OpenGL Context 4.5 is created.\n");
	}
	
	//Verifying that context is a direct context
	if(!glXIsDirect(gpDisplay, gGLXContext))
	{
		printf("Indirect GLX rendering Context Obtained\n");
	}
	else
	{
		printf("Direct GLX Rendering Context Obtained\n");
	}

	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	// GLEW initialization code for GLSL. It must be here, ie after creating OpenGL context but before using any OpenGL function
	GLenum glew_error = glewInit();
	if(glew_error != GLEW_OK)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
		gGLXContext = NULL;
		
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}

	gVertexShaderObject_FBO = glCreateShader(GL_VERTEX_SHADER);

	//Vertex Shader Code
	const GLchar* vertextShaderSourceCodeN =
		"#version 430 core " \
		"\n " \
		"in vec4 vPosition;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec2 out_TexCoord;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix * vPosition;" \
		"out_TexCoord=vTexCoord;"\
		"} ";

	//"in vec4 vColor;" \
	//"out vec4 out_color;" \
		//"out_color=vColor;"\

	//Shader object and source code mapping
	glShaderSource(gVertexShaderObject_FBO, 1, &vertextShaderSourceCodeN, NULL);

	//Compile Shader
	glCompileShader(gVertexShaderObject_FBO);

	//Error Checking
	GLint iShaderCompileStatusN = 0;
	GLint iInfoLogLengthN = 0;
	GLchar * szInfoLogN = NULL;

	glGetShaderiv(gVertexShaderObject_FBO, GL_COMPILE_STATUS, &iShaderCompileStatusN);

	if (iShaderCompileStatusN == GL_FALSE) {
		glGetShaderiv(gVertexShaderObject_FBO, GL_INFO_LOG_LENGTH, &iInfoLogLengthN);
		if (iInfoLogLengthN > 0) {
			szInfoLogN = (GLchar *)malloc(iInfoLogLengthN);
			if (szInfoLogN != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_FBO,
					iInfoLogLengthN,
					&written,
					szInfoLogN);
				fprintf(gpFile, "Vertex Shader Compilation Log %s\n", szInfoLogN);
				free(szInfoLogN);
				uninitialize();
				exit(0);
			}
		}
	}

	//Fragement Shader
	//Define Shader Object
	gFragementShaderObject_FBO = glCreateShader(GL_FRAGMENT_SHADER);

	//Fragement Shader Code
	const GLchar* fragementShaderSourceCodeN =
		"#version 430 core " \
		"\n " \
		"in vec2 out_TexCoord;"
		"uniform sampler2D u_sampler;"
		"out vec4 fragColor;"
		"void main(void)" \
		"{" \
		"fragColor=texture(u_sampler,out_TexCoord);" \
		"} ";


	//"in vec4 out_color;"\
	//"fragColor=out_color;" \
		
	//Shader object and source code mapping
	glShaderSource(gFragementShaderObject_FBO, 1, &fragementShaderSourceCodeN, NULL);

	//Compile Shader
	glCompileShader(gFragementShaderObject_FBO);

	//Error Checking
	iShaderCompileStatusN = 0;
	iInfoLogLengthN = 0;
	szInfoLogN = NULL;

	glGetShaderiv(gFragementShaderObject_FBO, GL_COMPILE_STATUS, &iShaderCompileStatusN);

	if (iShaderCompileStatusN == GL_FALSE) {
		glGetShaderiv(gFragementShaderObject_FBO, GL_INFO_LOG_LENGTH, &iInfoLogLengthN);
		if (iInfoLogLengthN > 0) {
			szInfoLogN = (GLchar *)malloc(iInfoLogLengthN);
			if (szInfoLogN != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gFragementShaderObject_FBO,
					iInfoLogLengthN,
					&written,
					szInfoLogN);
				fprintf(gpFile, "Fragement Shader Compilation Log %s\n", szInfoLogN);
				free(szInfoLogN);
				uninitialize();
				exit(0);
			}
		}
	}

	//Program Object
	gShaderProgramObject_FBO = glCreateProgram();
	//Attach Shaders
	glAttachShader(gShaderProgramObject_FBO, gVertexShaderObject_FBO);
	glAttachShader(gShaderProgramObject_FBO, gFragementShaderObject_FBO);

	//Prelinking
	//Binding to attribute
	glBindAttribLocation(gShaderProgramObject_FBO,
		AMC_ATTRIBUTE_POSITION,
		"vPosition");

	glBindAttribLocation(gShaderProgramObject_FBO,
	AMC_ATTRIBUT_TEXTURE0,
		"vTexCoord");
	/*glBindAttribLocation(gShaderProgramObject_FBO,
		AMC_ATTRIBUTE_COLOR,
		"vColor");*/

		//Link Shader Program
	glLinkProgram(gShaderProgramObject_FBO);

	//Error Checking
	GLint iShaderLinkStatusN = 0;

	glGetProgramiv(gShaderProgramObject_FBO, GL_LINK_STATUS, &iShaderLinkStatusN);

	if (iShaderLinkStatusN == GL_FALSE) {
		glGetProgramiv(gShaderProgramObject_FBO, GL_INFO_LOG_LENGTH, &iInfoLogLengthN);
		if (iInfoLogLengthN > 0) {
			szInfoLogN = (GLchar *)malloc(iInfoLogLengthN);
			if (szInfoLogN != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_FBO,
					iInfoLogLengthN,
					&written,
					szInfoLogN);
				fprintf(gpFile, "Program Link Log %s\n", szInfoLogN);
				free(szInfoLogN);
				uninitialize();
				exit(0);
			}
		}
	}

	//Post Linking
	//Retriving Uniform Location
	mvpUniform_FBO = glGetUniformLocation(gShaderProgramObject_FBO,
		"u_mvp_matrix");

	samplerUniform_FBO = glGetUniformLocation(gShaderProgramObject_FBO,
		"u_sampler");


	//Vertex Shader
	//Define Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//Vertex Shader Code
	const GLchar* vertextShaderSourceCode =
		"#version 430 core " \
		"\n " \
		"in vec4 vPosition;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec2 out_TexCoord;" \
		"void main(void)" \
		"{" \
		"gl_Position=u_mvp_matrix * vPosition;" \
		"out_TexCoord=vTexCoord;"\
		"} ";

	//Shader object and source code mapping
	glShaderSource(gVertexShaderObject, 1, &vertextShaderSourceCode, NULL);

	//Compile Shader
	glCompileShader(gVertexShaderObject);

	//Error Checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar * szInfoLog = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE) {
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Fragement Shader
	//Define Shader Object
	gFragementShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//Fragement Shader Code
	const GLchar* fragementShaderSourceCode =
		"#version 430 core " \
		"\n " \
		"in vec2 out_TexCoord;"
		"uniform sampler2D u_sampler;"
		"out vec4 fragColor;"
		"void main(void)" \
		"{" \
		"fragColor=texture(u_sampler,out_TexCoord);" \
		"} ";


	//Shader object and source code mapping
	glShaderSource(gFragementShaderObject, 1, &fragementShaderSourceCode, NULL);

	//Compile Shader
	glCompileShader(gFragementShaderObject);

	//Error Checking
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(gFragementShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE) {
		glGetShaderiv(gFragementShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetShaderInfoLog(gFragementShaderObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf(gpFile, "Fragement Shader Compilation Log %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Program Object
	gShaderProgramObject = glCreateProgram();
	//Attach Shaders
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragementShaderObject);

	//Prelinking
	//Binding to attribute
	glBindAttribLocation(gShaderProgramObject,
		AMC_ATTRIBUTE_POSITION,
		"vPosition");

	glBindAttribLocation(gShaderProgramObject,
		AMC_ATTRIBUT_TEXTURE0,
		"vTexCoord");

	//Link Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error Checking
	GLint iShaderLinkStatus = 0;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderLinkStatus);

	if (iShaderLinkStatus == GL_FALSE) {
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0) {
			szInfoLog = (GLchar *)malloc(iInfoLogLength);
			if (szInfoLog != NULL) {
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject,
					iInfoLogLength,
					&written,
					szInfoLog);
				fprintf(gpFile, "Program Link Log %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	//Post Linking
	//Retriving Uniform Location
	mvpUniform = glGetUniformLocation(gShaderProgramObject,
		"u_mvp_matrix");

	TextureSamplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");

	//Vertices
	const GLfloat pyramidVertices[] = {
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f
	};

	const GLfloat cubeVertices[] = {
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f
	};

	const GLfloat pyramidColors[] = {
		0.0f, 1.0f,1.0f,
		0.0f, 0.0f,1.0f,
		1.0f, 0.0f,1.0f,
		1.0f, 1.0f,1.0f,
		1.0f, 1.0f,1.0f,
		0.0f, 1.0f,1.0f,
		0.0f, 0.0f,1.0f,
		1.0f, 0.0f,1.0f,
		0.0f, 0.0f,1.0f,
		1.0f, 0.0f,1.0f,
		1.0f, 1.0f,1.0f,
		0.0f, 1.0f,1.0f,
		1.0f, 0.0f,1.0f,
		1.0f, 1.0f,1.0f,
		0.0f, 1.0f,1.0f,
		0.0f, 0.0f,1.0f,
		1.0f, 0.0f,1.0f,
		1.0f, 1.0f,1.0f,
		0.0f, 1.0f,1.0f,
		0.0f, 0.0f,1.0f,
		0.0f, 0.0f,1.0f,
		1.0f, 0.0f,1.0f,
		1.0f, 1.0f,1.0f,
		0.0f, 1.0f,1.0f,
	};

	const GLfloat cubeTexCoord[] =
	{
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f 
	};


	//Create vao
	//Save everying in single set
	glGenVertexArrays(1, &vao_pyramid);

	glBindVertexArray(vao_pyramid);


	//TRIANGLE
	//Generate Buffer
	glGenBuffers(1, &vbo_position_pyramid);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_position_pyramid);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(pyramidVertices),
		pyramidVertices,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &vbo_color_pyramid);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_color_pyramid);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(pyramidColors),
		pyramidColors,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind array
	glBindVertexArray(0);

	//RECTANGLE
	glGenVertexArrays(1, &vao_cube);

	glBindVertexArray(vao_cube);

	//Generate Buffer
	glGenBuffers(1, &vbo_position_cube);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_position_cube);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(cubeVertices),
		cubeVertices,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	////Generate Buffer
	//glGenBuffers(1, &vbo_color_cube);
	////Bind Generated Buffer
	//glBindBuffer(GL_ARRAY_BUFFER,
	//	vbo_color_cube);
	////Fill Buffer
	//glBufferData(GL_ARRAY_BUFFER,
	//	sizeof(cubeColors),
	//	cubeColors,
	//	GL_STATIC_DRAW);
	////Set Vertex Attrib Pointer
	//glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
	//	3,
	//	GL_FLOAT,
	//	GL_FALSE,
	//	0,
	//	NULL);
	////Enable Vertex Attrib Array
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	////Unbind Buffer
	//glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Generate Buffer
	glGenBuffers(1, &vbo_texCoord_Cube);
	//Bind Generated Buffer
	glBindBuffer(GL_ARRAY_BUFFER,
		vbo_texCoord_Cube);
	//Fill Buffer
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(cubeTexCoord),
		cubeTexCoord,
		GL_STATIC_DRAW);
	//Set Vertex Attrib Pointer
	glVertexAttribPointer(AMC_ATTRIBUT_TEXTURE0,
		2,
		GL_FLOAT,
		GL_FALSE,
		0,
		NULL);
	//Enable Vertex Attrib Array
	glEnableVertexAttribArray(AMC_ATTRIBUT_TEXTURE0);
	//Unbind Buffer
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind array
	glBindVertexArray(0);

	//Now from here onward add Frame Buffer Facility
	glGenFramebuffers(1,&FBO);
	glBindFramebuffer(GL_FRAMEBUFFER,FBO);//Read/Draw
	
	glGenTextures(1,&texture_FBO);
	glBindTexture(GL_TEXTURE_2D,texture_FBO);

	glTexStorage2D(GL_TEXTURE_2D,1,GL_RGBA8,1024,1024);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0 /*| GL_DEPTH_ATTACHMENT*/,texture_FBO,0);
	//Turn Off Mipmaps 4th 0

	static const GLenum draw_Buffers[] = { GL_COLOR_ATTACHMENT0 };

	//Changes
	glGenBuffers(1, &fbo_depth);
	glBindRenderbuffer(GL_RENDERBUFFER, fbo_depth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 1024, 1024);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, fbo_depth);

	glDrawBuffers(1,draw_Buffers);

	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	
	glBindVertexArray(0); 			// Unbind
	
	glClearDepth(1.0f);				//clear depth buffer
	glEnable(GL_DEPTH_TEST);		//enable the depth
	glDepthFunc(GL_LEQUAL);			//less than or equal to 1.0f in far. Uses ray tracing algorithm

	glShadeModel(GL_SMOOTH);		//to remove aliasing
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	//to remove distortion
	//glEnable(GL_CULL_FACE);		// Disable culling

	// Background
	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);	

	// Set PerspectiveMatrix to identity matrix
	perspectiveProjectionMatrix = mat4::identity();
		
	resize(WIN_WIDTH, WIN_HEIGHT);
} 

void display(void)
{
	//Code:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
glUseProgram(gShaderProgramObject);

	//Declaration of Matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 scaleMatrix;

	//Pyramid
	//Initialize matrices

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();


	glBindFramebuffer(GL_FRAMEBUFFER, FBO);
	glViewport(0, 0, 1024, 1024);
	glClearColor(0.0, 0.0, 1.0, 0.0);
	glEnable(GL_DEPTH_TEST);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	translationMatrix = translate(0.0f, 0.15f, -6.0f);
	scaleMatrix = scale(0.75f, 0.75f, 0.75f);
	rotationMatrix = rotate(anglePyramid, 0.0f, 1.0f, 0.0f);

	//Matrix Multiplication
	modelViewMatrix = translationMatrix * rotationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms
	glUniformMatrix4fv(mvpUniform_FBO,//Changed
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, TextureKundali);
	glUniform1i(TextureSamplerUniform, 0);


	//Bind with vao
	glBindVertexArray(vao_pyramid);

	//Bind with textures if any
	glDrawArrays(GL_TRIANGLE_FAN,
		0,
		4);
	glDrawArrays(GL_TRIANGLE_FAN,
		4,
		4);
	glDrawArrays(GL_TRIANGLE_FAN,
		8,
		4);
	glDrawArrays(GL_TRIANGLE_FAN,
		12,
		4);
	glDrawArrays(GL_TRIANGLE_FAN,
		16,
		4);
	glDrawArrays(GL_TRIANGLE_FAN,
		20,
		4);


	//Unbind vao

	glBindVertexArray(0);


	glBindFramebuffer(GL_FRAMEBUFFER, 0);//Test

	glUseProgram(0);



	glClearColor(0.0, 0.0, 1.0, 0.0);
	glViewport(0, 0, (GLsizei)windowWidth, (GLsizei)windowHeight);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject_FBO);
	//CUBE
	//Initialize matrices

	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	//Transformation

	translationMatrix = translate(0.0f, 0.0f, -5.0f);
	//scaleMatrix = scale(0.75f, 0.75f, 0.75f);
	rotationMatrix = rotate(angleCube,angleCube,angleCube);

	//Matrix Multiplication

	modelViewMatrix = translationMatrix * scaleMatrix * rotationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

	//Send necessary matrices to shader in resp. Uniforms

	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	glUniform1i(samplerUniform_FBO, 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,texture_FBO);
	//Bind with vao

	glBindVertexArray(vao_cube);

	//Bind with textures if any

	//Draw
	//6Faces
	glDrawArrays(GL_TRIANGLE_FAN,
		0,
		4);
	glDrawArrays(GL_TRIANGLE_FAN,
		4,
		4);
	glDrawArrays(GL_TRIANGLE_FAN,
		8,
		4);
	glDrawArrays(GL_TRIANGLE_FAN,
		12,
		4);
	glDrawArrays(GL_TRIANGLE_FAN,
		16,
		4);
	glDrawArrays(GL_TRIANGLE_FAN,
		20,
		4);
	//Unbind vao

	glBindVertexArray(0);

	glBindTexture(GL_TEXTURE_2D, 0);

	glUseProgram(0);


	glXSwapBuffers(gpDisplay, gWindow);
}

void Update(void)
{
	anglePyramid = anglePyramid + 0.02f;
	if (anglePyramid > 360.0f)
		anglePyramid = 0.0f;
	angleCube = angleCube + 0.02f;
	if (angleCube > 360.0f)
		angleCube = 0.0f;

}
void resize(int width, int height)
{
	//Code:
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,((GLfloat)width / (GLfloat)height),0.1f,100.0f);	
}
	
void uninitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
       // Destroy vao
       if (vao_pyramid)
       {
             glDeleteVertexArrays(1, &vao_pyramid);
             vao_pyramid = 0;
       }
 
       if (vao_cube)
       {
             glDeleteVertexArrays(1, &vao_cube);
             vao_cube = 0;
       }

       // Destroy vbo
       if (vbo_position_pyramid)
       {
             glDeleteBuffers(1, &vbo_position_pyramid);
             vbo_position_pyramid = 0;
       }
 
       if (vbo_position_cube)
       {
             glDeleteBuffers(1, &vbo_position_cube);
             vbo_position_cube = 0;
       }

       // Destroy vbo
       if (vbo_color_pyramid)
       {
             glDeleteBuffers(1, &vbo_color_pyramid);
             vbo_color_pyramid = 0;
       }
 
       if (vbo_color_cube)
       {
             glDeleteBuffers(1, &vbo_color_cube);
             vbo_color_cube = 0;
       }


       // Detach the shaders first before deleting
       // Detach vertex shader from shader program object
       glDetachShader(gShaderProgramObject,  gVertexShaderObject);
       // Detach fragment shader from shader program object
       glDetachShader(gShaderProgramObject, gFragementShaderObject);
 
       // Delete vertex shader object
       glDeleteShader(gVertexShaderObject);
       gVertexShaderObject = 0;
       // Delete fragment shader object
       glDeleteShader(gFragementShaderObject);
       gFragementShaderObject = 0;
 
       // Delete shader program object. It has no objects attached
       glDeleteProgram(gShaderProgramObject);
       gShaderProgramObject = 0;
 
       // Unlink shader program
       glUseProgram(0); // Stray call
 
       if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
       {
             glXMakeCurrent(gpDisplay, 0, 0);
       }
 
       if(gGLXContext)
       {
             glXDestroyContext(gpDisplay, gGLXContext);
       }
 

	if(gpFile)
	{
		fprintf(gpFile, "Log file is successfully closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
		





















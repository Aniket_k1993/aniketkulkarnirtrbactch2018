//GLXContext with Frame Buffer Configurations
//Code for programmable pipeline
//Code for Vulcan programming

//Headers:
#include <iostream>	
#include <stdio.h>	//For print();
#include <stdlib.h>	//For exit();
#include <memory.h>	//For memset();

//XWindows server headers:
#include <X11/Xlib.h>	//Analogous to windows.h
#include <X11/Xutil.h>	//Used for XVisualInfo struct and associated APIs ie visuals
#include <X11/XKBlib.h>	//Keyboard library
#include <X11/keysym.h>	//For key symbols 

// GLEW Headers
#include <GL/glew.h>

//OpenGL headers:
#include <GL/gl.h>
#include <GL/glx.h>	//Bridging API

#include "vmath.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Namespaces:
using namespace std;
using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUT_TEXTURE0
};

//Global variable declarations:
bool bFullscreen = false;		//Fullscreen variable
Display *gpDisplay = NULL;		//Global pointer
XVisualInfo *gpXVisualInfo = NULL;	
Colormap gColormap;
Window gWindow;

typedef GLXContext (*glXCreateContextAttribsARBProc) (Display*, GLXFBConfig, GLXContext, Bool, const int*);
glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;			//Rendering context. Parallel to HGLRC

FILE *gpFile = NULL;

GLuint VertexShaderObject;
GLuint FragmentShaderObject;
GLuint ShaderProgramObject;

GLuint Vao;
GLuint Vbo;
GLuint VboColor;

bool Animatation = false;

// *************************** Light's
bool LkeyIsPressed = false;
GLuint Model_Matrix_Uniform;
GLuint View_Matrix_Uniform;
GLuint Projection_Matrix_Uniform;

// Light uniforms:
GLuint La_UniformRed;				// Uniform for ambient component of light
GLuint Ld_UniformRed;				// Uniform for diffuse component of light
GLuint Ls_UniformRed;				// Uniform for specular component of light
GLuint Light_Position_UniformRed;	// Uniform for light position

// Matrial uniforms:
GLuint Ka_Uniform;				// Uniform for ambient component of material
GLuint Kd_Uniform;				// Uniform for diffuse component of material
GLuint Ks_Uniform;				// Uniform for specular component of material
GLuint Material_Shininess_Uniform;	// Uniform for shininess component of material

// Light uniforms:
GLuint La_UniformBlue;				// Uniform for ambient component of light
GLuint Ld_UniformBlue;				// Uniform for diffuse component of light
GLuint Ls_UniformBlue;				// Uniform for specular component of light
GLuint Light_Position_UniformBlue;	// Uniform for light position

GLuint LKeyPressedUniform;

// Animate and Light variables:
bool gbAnimate;
bool gbLight;

GLfloat lightAmbientRed[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat lightDiffuseRed[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat lightSpecularRed[] = { 1.0f,0.0f,0.0f,0.0f };
GLfloat lightPositionRed[] = { 2.0f,1.0f,1.0f,0.0f };

GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,0.0f };
GLfloat materialShininess= 128.0f;


GLfloat lightAmbientBlue[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat lightDiffuseBlue[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat lightSpecularBlue[] = { 0.0f,0.0f,1.0f,0.0f };
GLfloat lightPositionBlue[] = { -2.0f,1.0f,1.0f,0.0f };

// **********************************************************************

mat4 PrespectiveProjectionMatrix;

//For Rotation
GLfloat AngleTrangle = 0.0f;
GLfloat AngleRectangle = 0.0f;

//entry point fuction:
int main(void)		
{
	//Function prototypes:
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void Update(void);
	void resize(int, int);
	void uninitialize(void);

	bool bDone = false;
	char ascii[26];		//For key press funtionality
	//Code:
	//Create log file:
	gpFile = fopen("Log_Programmable Pipeline.txt", "w");
	if(gpFile == NULL)
	{
		printf("Unable to create log file.\nExiting...\n");
		exit(1);
	}
	else
	{
		fprintf(gpFile, "Log file is successfully opened.\n");
	}
	
	
	//Create window:
	CreateWindow();		//Function to create a window
	
	//Initialize():
	initialize();
	
	//Game loop:
	XEvent event;		//To store the message/event. Parallel to MSG structure
	KeySym keysym;		
	int winWidth;
	int winHeight;

	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);		//Get next message. Fill 'event' with the address of next event. Parallel to GetMessage()
			switch(event.type)			//Switch on event type. Parallel to iMsg
			{
				case MapNotify:			//Parallel to WM_CREATE
					break;
				case KeyPress:			//Parallel to WM_KEYDOWN
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:			//Exit on Esc key press
							bDone = true;
						default:
							break;
					}
					XLookupString(&event.xkey, ascii, sizeof(ascii), NULL, NULL);
					switch(ascii[0])
					{
						case 'f':
						case 'F':
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
						case 'a':
						case 'A':	
							if(Animatation==false)
							{
								Animatation = true;
							}
							else
							{	
								Animatation = false;		
							}					
							break;
						case 'l': // for 'L' or 'l'
						case 'L':
							if (LkeyIsPressed == false)
							{
								gbLight = true;
								LkeyIsPressed = true;
							}
							else
							{
								gbLight = false;
								LkeyIsPressed = false;
							}
							break;
						default:
							break;
					}		
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:			//Left mouse button
							break;
						case 2:			//Middle mouse button
							break;	
						case 3:			//Right mouse button
							break;
						default:
							break;
					}
					break;
				case MotionNotify:			//Mouse move
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:				//Painting of window
					break;
				case DestroyNotify:			
					break;
				case 33:				//Close press
					bDone = true;
					break;
				default:
					break;
			}
		}
		
		display();
		Update();
		//Spin call here
	}
	uninitialize();
	return(0);
}
				
void CreateWindow(void)
{
	//Function Prototypes:
	void uninitialize(void);
	
	//Variable declarations:
	XSetWindowAttributes winAttribs;
	GLXFBConfig *pGLXFBConfigs = NULL;	//Array pointer to store the returned visuals
	GLXFBConfig bestGLXFBConfig;		//Store the best visual
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNumFBConfigs = 0;
	int styleMask;
	int i;
	
	//1. Our own specified Frame Buffer
	static int frameBufferAttributes[] = 
	{
		GLX_X_RENDERABLE, True,			//We want video rendering and not image rendering
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,	//Window is drawable
		GLX_RENDER_TYPE, GLX_RGBA_BIT,		//Frame is of RGBA type
		GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,	//Equivalent to giving TrueColor in XMatchVisualInfo
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
		GLX_DEPTH_SIZE, 24,
		GLX_STENCIL_SIZE, 8,
		GLX_DOUBLEBUFFER, True,
		//GLX_SAMPLE_BUFFERS, 1,
		//GLX_SAMPLES, 4,
		None			//As a stopper for array
	};
	
	//Code:
	gpDisplay = XOpenDisplay(NULL);		//Pointer to struct is the return value. It returns default display pointer.
	if(gpDisplay == NULL)
	{
		printf("ERROR: Unable to open X Display.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	//2. Get a new frame buffer config that meets our attrib requirements:
	pGLXFBConfigs = glXChooseFBConfig(gpDisplay, DefaultScreen(gpDisplay), frameBufferAttributes, &iNumFBConfigs);
	if(pGLXFBConfigs == NULL)
	{
		printf("Failed to get valid FrameBuffer Config.\nExiting now...\n");
		uninitialize();
		exit(1);
	}
	
	//3. As we are not asking the system to choose or match a visual, we get multiple visuals to choose from	
	printf("%d Matching FB Configs found.\n", iNumFBConfigs);
	
	//4. Pick that FB Config/Visual with the most samples per pixel
	int bestFramebufferConfig = -1, worstFramebufferConfig = -1, bestNumberOfSamples = -1, worstNumberOfSamples = 999;
	
	for(i = 0; i < iNumFBConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, pGLXFBConfigs[i]);
		if(pTempXVisualInfo)
		{
			int sampleBuffer, samples;
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLE_BUFFERS, &sampleBuffer);
			glXGetFBConfigAttrib(gpDisplay, pGLXFBConfigs[i], GLX_SAMPLES, &samples);
			printf("Matching Framebuffer Config = %d : Visual ID = 0x%lu : Sample_BUFFERS = %d : SAMPLES = %d\n", i, pTempXVisualInfo->visualid, sampleBuffer, samples);
			if(bestFramebufferConfig < 0 || sampleBuffer && samples > bestNumberOfSamples)
			{
				bestFramebufferConfig = i;
				bestNumberOfSamples = samples;
			}
			if(worstFramebufferConfig < 0 || !sampleBuffer || samples < worstNumberOfSamples)
			{
				worstFramebufferConfig = i;
				worstNumberOfSamples = samples;
			}
		}
		XFree(pTempXVisualInfo);
	}
	
	bestGLXFBConfig = pGLXFBConfigs[bestFramebufferConfig];
	
	//Set global GLXConfig
	gGLXFBConfig = bestGLXFBConfig;
	
	//Free the FBConfig list allocated by glXChooseFBConfig()
	XFree(pGLXFBConfigs);
	
	//5. 
	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, bestGLXFBConfig);
	printf("Choose Visual ID = 0x%lu\n", gpXVisualInfo->visualid);
		
	winAttribs.border_pixel = 0;		//Take default value for color
	winAttribs.border_pixmap = 0;		//Take default pattern
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;
	
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPress | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),	//Parent window handle
				0,				//X
				0,				//Y
				WIN_WIDTH,			//W
				WIN_HEIGHT,			//H
				0,				//Thickness of border
				gpXVisualInfo->depth,		
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);
	if(!gWindow)
	{
		printf("ERROR: Failed to create Main Window.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay, gWindow, "First OpenGL X-Window");
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);	//33 turned on after this protocol
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);		//ShowWindow(); UpdateWindow(); SetFocus(), etc
}

	
void ToggleFullscreen(void)
{
	//Variable declarations:
	Atom wm_state;		//To save state of normal screen
	Atom fullscreen;	//TO save state of full screen
	XEvent xev = {0};
	
	//Code:
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);	//Network compliant. 
	memset(&xev, 0, sizeof(xev));		//Memset to default values
	
	xev.type = ClientMessage;		//Custom message
	xev.xclient.window = gWindow;		//Window for which client message is going(predefined message)
	xev.xclient.message_type = wm_state;	//Type of message that you are sending(created by you)
	xev.xclient.format = 32;		//byte size = 32
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
	
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),	//Propagates to your window
		False,						//If the message be propagated to your children
		StructureNotifyMask,
		&xev);
		
}

void initialize(void)
{
	
	void resize(int,int);
	void uninitialize(void);
	//Code:
	//6. Create a new GL context 4.5 for rendering:
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
	GLint attribs[] = 
	{
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 5,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB,
		0
	};
	
	gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);
	if(!gGLXContext)	//On failure, fall back to old style 2.x context
	{
		//When a context version below 3.0 is requested, implementations will return the newest context version
		//compatible with openGL versions less than version 3.0
		GLint attribs[] = 
		{
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1,
			GLX_CONTEXT_MINOR_VERSION_ARB, 0,
			0
		};
		printf("Failed to create GLX 4.5 context. Hence, using old style GLX Context. \n");
		gGLXContext = glXCreateContextAttribsARB(gpDisplay, gGLXFBConfig, 0, True, attribs);
	}
	else	//Successfully created 4.x context
	{
		printf("OpenGL Context 4.5 is created.\n");
	}
	
	//Verifying that context is a direct context
	if(!glXIsDirect(gpDisplay, gGLXContext))
	{
		printf("Indirect GLX rendering Context Obtained\n");
	}
	else
	{
		printf("Direct GLX Rendering Context Obtained\n");
	}

	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	
	// GLEW initialization code for GLSL. It must be here, ie after creating OpenGL context but before using any OpenGL function
	GLenum glew_error = glewInit();
	if(glew_error != GLEW_OK)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
		gGLXContext = NULL;
		
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}

	// *** VERTEX SHADER ***
	// 1. Create the shader (object is created and assigned)
	VertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// 2. Provide the source code to shader
	// '\' indicates a single string and not 3 separate strings. Source code as array of characters.
	// Instead of keeping inside a file (eg vertexshader.vsh), we keep it as a string inside our source file
	
	// GLSL version number 1.3 (1.3 * 100 = 130). default = 1.1
	// vPosition and u_mvp_matrix are user defined names for matrices for transformation
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_positionRed;" \
		"uniform vec4 u_light_positionBlue;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_directionRed;" \
		"out vec3 light_directionBlue;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_directionRed = vec3(u_light_positionRed) - eye_coordinates.xyz;" \
		"light_directionBlue = vec3(u_light_positionBlue) - eye_coordinates.xyz;" \

		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	
	// 3. Pass the object as the shader source. glVertexShaderObject is our specialist
	// 1 - 1program, so only 1 string. NULL - Pass length of string if not NUL terminated
	// (const GLchar **) - Pass the address of the program by casting (as array is used)
	glShaderSource(VertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// 4. Compile the shader - Dynamic compilation by the driver
	glCompileShader(VertexShaderObject);

	// ERROR CHECKING:
	GLint iInfoLogLength = 0;			// Length of the log created on failure to compile
	GLint iShaderCompiledStatus = 0;	// Stores shader compilation status
	char *szInfoLog = NULL;				// String to store the log

	glGetShaderiv(VertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(VertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(VertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// Create the shader
	FragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// Pass the source code to shader
	// FragColor is our name for the Fragment
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core"	\
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_directionRed;" \
		"in vec3 light_directionBlue;" \
		"in vec3 viewer_vector;" \
		"uniform vec3 u_LaRed;" \
		"uniform vec3 u_LdRed;" \
		"uniform vec3 u_LsRed;" \
		"uniform vec3 u_LaBlue;" \
		"uniform vec3 u_LdBlue;" \
		"uniform vec3 u_LsBlue;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \

		"vec3 normalized_light_directionRed=normalize(light_directionRed);" \
		"vec3 ambientRed = u_LaRed * u_Ka;" \
		"float tn_dot_ldRed = max(dot(normalized_transformed_normals, normalized_light_directionRed),0.0);" \
		"vec3 diffuseRed = u_LdRed * u_Kd * tn_dot_ldRed;" \
		"vec3 reflection_vectorRed = reflect(-normalized_light_directionRed, normalized_transformed_normals);" \
		"vec3 specularRed = u_LsRed * u_Ks * pow(max(dot(reflection_vectorRed, normalized_viewer_vector), 0.0), u_material_shininess);" \

		"vec3 normalized_light_directionBlue=normalize(light_directionBlue);" \
		"float tn_dot_ldBlue = max(dot(normalized_transformed_normals, normalized_light_directionBlue),0.0);" \
		"vec3 ambientBlue = u_LaBlue * u_Ka;" \
		"vec3 diffuseBlue = u_LdBlue * u_Kd * tn_dot_ldBlue;" \
		"vec3 reflection_vectorBlue = reflect(-normalized_light_directionBlue, normalized_transformed_normals);" \
		"vec3 specularBlue = u_LsBlue * u_Ks * pow(max(dot(reflection_vectorBlue, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambientRed + diffuseRed + specularRed + ambientBlue + diffuseBlue + specularBlue;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";


	glShaderSource(FragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compiler the shader
	glCompileShader(FragmentShaderObject);
	
	// Error checking for compilation done here
	glGetShaderiv(FragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)	// Failure to compile
	{
		glGetShaderiv(FragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetShaderInfoLog(FragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader compilation log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}
	// *** SHADER PROGRAM ***
	// Create
	ShaderProgramObject = glCreateProgram();		// It can link ALL the shaders. Hence, no parameter

	glAttachShader(ShaderProgramObject, VertexShaderObject);

	glAttachShader(ShaderProgramObject, FragmentShaderObject);
	
	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(ShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link the shader
	glLinkProgram(ShaderProgramObject);
	
	// Error checking for linking
	GLint iShaderProgramLinkStatus = 0;

	glGetProgramiv(ShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)	// Failure to link
	{
		glGetProgramiv(ShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;		// Stores number of characters written
				glGetProgramInfoLog(ShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program shader link log : %s\n", szInfoLog);
				free(szInfoLog);
				szInfoLog = NULL;
				uninitialize();
				exit(0);
			}
		}
	}

	// Get MVP uniform location
	Model_Matrix_Uniform = glGetUniformLocation(ShaderProgramObject, "u_model_matrix");
	View_Matrix_Uniform = glGetUniformLocation(ShaderProgramObject, "u_view_matrix");
	Projection_Matrix_Uniform = glGetUniformLocation(ShaderProgramObject, "u_projection_matrix");
	
	// If L key is Pressed or not:
	LKeyPressedUniform = glGetUniformLocation(ShaderProgramObject, "u_lighting_enabled");

	// Uniforms for Light:
	// Ambient color intensity of light:
	La_UniformRed = glGetUniformLocation(ShaderProgramObject, "u_LaRed");
	// Diffuse color intensity of light:
	Ld_UniformRed = glGetUniformLocation(ShaderProgramObject, "u_LdRed");
	// Specular color intensity of light:
	Ls_UniformRed = glGetUniformLocation(ShaderProgramObject, "u_LsRed");

	La_UniformBlue = glGetUniformLocation(ShaderProgramObject, "u_LaBlue");
	// Diffuse color intensity of light:
	Ld_UniformBlue = glGetUniformLocation(ShaderProgramObject, "u_LdBlue");
	// Specular color intensity of light:
	Ls_UniformBlue = glGetUniformLocation(ShaderProgramObject, "u_LsBlue");
	// Position of light:
	Light_Position_UniformRed = glGetUniformLocation(ShaderProgramObject, "u_light_positionRed");
	Light_Position_UniformBlue = glGetUniformLocation(ShaderProgramObject, "u_light_positionBlue");

	// Uniforms for material:
	// ambient reflective color intensity of material
	Ka_Uniform = glGetUniformLocation(ShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_Uniform = glGetUniformLocation(ShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_Uniform = glGetUniformLocation(ShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	Material_Shininess_Uniform = glGetUniformLocation(ShaderProgramObject, "u_material_shininess");;

	
	// Vertices, Colors, Shader attributes, vbo, vao initializations:
	float pyramidVertices[] = 
	{	
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
	};

	float pyramidColors[] = 
	{						
		// Front face
		   0.0f, 0.447214f, 0.894427f,
		   0.0f, 0.447214f, 0.894427f,
		   0.0f, 0.447214f, 0.894427f,

		   // Right face
		   0.894427f, 0.447214f, 0.0f,
		   0.894427f, 0.447214f, 0.0f,
		   0.894427f, 0.447214f, 0.0f,

		   // Back face
		   0.0f, 0.447214f, -0.894427f,
		   0.0f, 0.447214f, -0.894427f,
		   0.0f, 0.447214f, -0.894427f,

		   // Left face
		   -0.894427f, 0.447214f, 0.0f,
		   -0.894427f, 0.447214f, 0.0f,
		   -0.894427f, 0.447214f, 0.0f
	};
	// A. BLOCK FOR TRIANGLE:
	glGenVertexArrays(1, &Vao);
	glBindVertexArray(Vao);

	// A. BUFFER BLOCK FOR VERTICES:
	glGenBuffers(1, &Vbo);					// Buffer to store vertex position
	glBindBuffer(GL_ARRAY_BUFFER, Vbo);		// Find that named object in memory
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);	// Takes data from CPU to GPU

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	// Release the buffer for vertices:
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// B. BUFFER BLOCK FOR COLORS:
	glGenBuffers(1, &VboColor);						// Buffer to store vertex colors
	glBindBuffer(GL_ARRAY_BUFFER, VboColor);		// Find that named object in memory
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidColors), pyramidColors, GL_STATIC_DRAW);	// Takes data from CPU to GPU

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	// Release the buffer for colors:
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);				// Unbind 
	
	glClearDepth(1.0f);				//clear depth buffer
	glEnable(GL_DEPTH_TEST);		//enable the depth
	glDepthFunc(GL_LEQUAL);			//less than or equal to 1.0f in far. Uses ray tracing algorithm

	glShadeModel(GL_SMOOTH);		//to remove aliasing
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	//to remove distortion
	//glEnable(GL_CULL_FACE);		// Disable culling

	// Background
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);	

	// Set PerspectiveMatrix to identity matrix
	PrespectiveProjectionMatrix = mat4::identity();
		
	resize(WIN_WIDTH, WIN_HEIGHT);
} 

void display(void)
{
	//Code:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	

	glUseProgram(ShaderProgramObject);	

	if (gbLight == true)
	{
		// Set the 'u_lighting_enabled' uniform:
		glUniform1i(LKeyPressedUniform, 1);

		// Set the light's properties:
		glUniform3fv(La_UniformRed, 1, lightAmbientRed);
		glUniform3fv(Ld_UniformRed, 1, lightDiffuseRed);
		glUniform3fv(Ls_UniformRed, 1, lightSpecularRed);
		// Set the light's properties:
		glUniform3fv(La_UniformBlue, 1, lightAmbientBlue);
		glUniform3fv(Ld_UniformBlue, 1, lightDiffuseBlue);
		glUniform3fv(Ls_UniformBlue, 1, lightSpecularBlue);

		glUniform4fv(Light_Position_UniformRed, 1, lightPositionRed);
		glUniform4fv(Light_Position_UniformBlue, 1, lightPositionBlue);

		// Set the material's properties:
		glUniform3fv(Ka_Uniform, 1, materialAmbient);
		glUniform3fv(Kd_Uniform, 1, materialDiffuse);
		glUniform3fv(Ks_Uniform, 1, materialSpecular);
		glUniform1f(Material_Shininess_Uniform, materialShininess);
	}
	else
	{
		// Set the 'u_lighting_enabled' uniform:
		glUniform1i(LKeyPressedUniform, 0);
	}


	//For Cube
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 RotationMatrix = mat4::identity();

	// Translate the modelViewMatrix along the z axis
	modelMatrix = translate(0.0f, 0.0f, -6.0f);

	RotationMatrix = rotate(AngleTrangle, 0.0f, -1.0f, 0.0f);

	modelMatrix = modelMatrix * RotationMatrix;

	glUniformMatrix4fv(Model_Matrix_Uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(View_Matrix_Uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(Projection_Matrix_Uniform, 1, GL_FALSE, PrespectiveProjectionMatrix);


	glBindVertexArray(Vao);
	//Draw

	glDrawArrays(GL_TRIANGLES, 0, 12);

	glBindVertexArray(0);


	glUseProgram(0);

	glXSwapBuffers(gpDisplay, gWindow);
}

void Update(void)
{
	if(Animatation == true)
	{
		AngleTrangle += 0.02f;
		if(AngleTrangle >= 360.0f)
		{
			AngleTrangle = 0.0f;	
		}
	}
}
void resize(int width, int height)
{
	//Code:
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	PrespectiveProjectionMatrix = perspective(45.0f,((GLfloat)width / (GLfloat)height),0.1f,100.0f);	
}
	
void uninitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
       // Destroy vao
       if (Vao)
       {
             glDeleteVertexArrays(1, &Vao);
             Vao = 0;
       }
 
       // Destroy vbo
       if (Vbo)
       {
             glDeleteBuffers(1, &Vbo);
             Vbo = 0;
       }
 
       // Detach the shaders first before deleting
       // Detach vertex shader from shader program object
       glDetachShader(ShaderProgramObject,  VertexShaderObject);
       // Detach fragment shader from shader program object
       glDetachShader(ShaderProgramObject, FragmentShaderObject);
 
       // Delete vertex shader object
       glDeleteShader(VertexShaderObject);
       VertexShaderObject = 0;
       // Delete fragment shader object
       glDeleteShader(FragmentShaderObject);
       FragmentShaderObject = 0;
 
       // Delete shader program object. It has no objects attached
       glDeleteProgram(ShaderProgramObject);
       ShaderProgramObject = 0;
 
       // Unlink shader program
       glUseProgram(0); // Stray call
 
       if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
       {
             glXMakeCurrent(gpDisplay, 0, 0);
       }
 
       if(gGLXContext)
       {
             glXDestroyContext(gpDisplay, gGLXContext);
       }
 

	if(gpFile)
	{
		fprintf(gpFile, "Log file is successfully closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
		





















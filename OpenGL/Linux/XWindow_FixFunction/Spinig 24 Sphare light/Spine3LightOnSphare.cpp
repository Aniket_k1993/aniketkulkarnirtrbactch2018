//XWindows first program

#include <iostream>	
#include <stdio.h>
#include <stdlib.h>	//For exit();
#include <memory.h>	//For memset();

//XWindows headers:

#include <X11/Xlib.h>
#include <X11/Xutil.h>	//Used for XVisualInfo struct and associated APIs
#include <X11/XKBlib.h>	//Keyboard library
#include <X11/keysym.h>	//For key symbols 

//OpenGL headers:
#include <GL/gl.h>
#include <GL/glx.h>	//Bridging API
#include <GL/glu.h>

//Namespaces:
using namespace std;

//Global variable declarations:
bool bFullscreen = false;		//Fullscreen variable
Display *gpDisplay = NULL;		//Global pointer
XVisualInfo *gpXVisualInfo = NULL;	
Colormap gColormap;
Window gWindow;

int giWindowWidth = 800;
int giWindowHeight = 600;
bool Blighting = false;
GLUquadric *Quadric = NULL;

GLfloat angle_x_Light = 0;
GLfloat angle_y_Light = 0;
GLfloat angle_z_Light = 0;

//int x_rotation = 0;
//int y_rotation = 0;
//int z_rotation = 0;

bool gbLighting = false;
GLUquadric *quadric[24];
GLfloat KeyisPress = 0.0f;
//Lighting arrays:
//LIGHT0: GLOBAL WHITE LIGHT
GLfloat light_ambient[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };	//White colored light
//GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat light_position[] = { 1.0f, 1.0f, 1.0f, 0.0f };	//Position of light

GLfloat light_model_ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
GLfloat light_model_local_viewer[] = { 0.0f };
GLXContext gGLXContext;			//Rendering context

//entry point fuction:
int main(void)		
{
	//Function prototypes:
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void resize(int, int);
	void uninitialize(void);
	void update(void);
	
	//Variable declarations:
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;
	char ascii[26];		//For key press funtionality
	//Code:
	CreateWindow();		//Function to create a window
	
	//Initialize():
	initialize();
	
	XEvent event;		//To store the message/event
	KeySym keysym;		
	
	//Game loop
	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);		//Get next message. Fill 'event' with the address of next event.
			switch(event.type)			//Switch on event type
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:			//Exit on Esc key press
							bDone = true;				
						default:
							break;
					}
					XLookupString(&event.xkey, ascii, sizeof(ascii), NULL, NULL);
					switch(ascii[0])
					{
						case 'l':	//For 'l' or 'L'
						case 'L':
							if (gbLighting == false)
							{
								gbLighting = true;
								glEnable(GL_LIGHTING);
							}
							else
							{
								gbLighting = false;
								glDisable(GL_LIGHTING);
							}
							break;
						case 'f':
						case 'F':
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
						case 'x':
						case 'X':
						KeyisPress = 1;
							angle_x_Light = 0.0f;
							break;

						case 'y':
						case 'Y':
							KeyisPress = 2;
							angle_y_Light = 0.0f;
							break;

						case 'z':
						case 'Z':
							KeyisPress = 3;
							angle_z_Light = 0.0f;
							break;

						default:
							break;
					}		
					break;
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:			//Left mouse button
							break;
						case 2:			//Middle mouse button
							break;	
						case 3:			//Right mouse button
							break;
						default:
							break;
					}
					break;
				case MotionNotify:			//Mouse move
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:				//Painting of window
					break;
				case DestroyNotify:			
					break;
				case 33:				//Close press
					bDone = true;
					break;
				default:
					break;
			}
		}
		
		display();
		//Spin call here
		update();
	}
	uninitialize();
	return(0);
}
				
void CreateWindow(void)
{
	//Function Prototypes:
	void uninitialize(void);
	
	//Variable declarations:
	XSetWindowAttributes winAttribs;
	int defaultScreen;	//To get the default screen
	int defaultDepth;	//To get the default depth
	int styleMask;
	
	static int frameBufferAttributes[] = 
	{
		GLX_DOUBLEBUFFER, True,
		GLX_RGBA,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_DEPTH_SIZE, 24,
		None			//As a stopper for array
	};
	
	//Code:
	gpDisplay = XOpenDisplay(NULL);		//Pointer to struct is the return value. It returns default display pointer.
	if(gpDisplay == NULL)
	{
		printf("ERROR: Unable to open X Display.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen = XDefaultScreen(gpDisplay);
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	//We have created a visual to create a new context
		
	winAttribs.border_pixel = 0;		//Take default value for color
	winAttribs.border_pixmap = 0;		//Take default pattern
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);
	//Colormap is a h/w resource and is shared by the RootWindow. 
	//gpXVisualInfo->screen - We need visual screen and not default screen as rendering is done on this visual screen.
	//AllocNone - Do not allocate memory for color map. We need it only when there are child windows that share the color map of the parent.
	gColormap = winAttribs.colormap;
	
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPress | KeyPressMask | PointerMotionMask | StructureNotifyMask;
	//ExposureMask - Expose
	//VisibilityChangeMask - MapNotify 
	//ButtonPress - ButtonPress
	//KeyPressMask - KeyPress 
	//PointerMotionMask - MotionNotify
	//StructureNotifyMask - ConfigureNotify
	
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),	//Parent window handle
				0,				//X
				0,				//Y
				giWindowWidth,			//W
				giWindowHeight,			//H
				0,				//Thickness of border
				gpXVisualInfo->depth,		
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);
	if(!gWindow)
	{
		printf("ERROR: Failed to create Main Window.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay, gWindow, "2 3D rotating shapes");
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);	//33 turned on after this protocol
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);		//ShowWindow(); UpdateWindow(); SetFocus(), etc
}

	
void ToggleFullscreen(void)
{
	//Variable declarations:
	Atom wm_state;		//To save state of normal screen
	Atom fullscreen;	//TO save state of full screen
	XEvent xev = {0};
	
	//Code:
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);	//Network compliant. 
	memset(&xev, 0, sizeof(xev));		//Memset to default values
	
	xev.type = ClientMessage;		//Custom message
	xev.xclient.window = gWindow;		//Window for which client message is going(predefined message)
	xev.xclient.message_type = wm_state;	//Type of message that you are sending(created by you)
	xev.xclient.format = 32;		//byte size = 32
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
	
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),	//Propagates to your window
		False,						//If the message be propagated to your children
		StructureNotifyMask,
		&xev);
		
}

void initialize(void)
{
	//Function protoypes:
	void resize(int, int);
	void uninitialize(void);
	
	//Code:
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	if(gGLXContext == NULL)
	{
		printf("ERROR: Failed to create Rendering Context.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);		//clear depth buffer
	glEnable(GL_DEPTH_TEST);	//enable the depth
	glDepthFunc(GL_LEQUAL);		//less than or equal to 1.0f in far. Uses ray tracing algorithm

	glShadeModel(GL_SMOOTH);	//to remove aliasing
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	//to remove distortion

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

	//Lighting calls
	//LIGHT0:
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	//glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	//Enable LIGHT0
	glEnable(GL_LIGHT0);

	for (int i = 0; i <= 24; i++)
	{
		quadric[i] = gluNewQuadric();
	}

	//Create a quadric for the sphere
	//quadric = gluNewQuadric();
	//Create a quadric for the sphere
	//quadric = gluNewQuadric();
	resize(giWindowWidth, giWindowHeight);		//Stray resize
} 

void display(void)
{

	//code
	void draw24sphare(void);
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	draw24sphare();


	glXSwapBuffers(gpDisplay, gWindow);

}

void draw24sphare(void)
{

	if (KeyisPress == 1)
	{
		glPushMatrix();
		glRotatef(angle_x_Light, 1.0f, 0.0f, 0.0f);		//Light rotating about X-axis
		light_position[2] = angle_x_Light;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		glPopMatrix();
	}
	else if (KeyisPress == 2)
	{
		glPushMatrix();
		glRotatef(angle_y_Light, 0.0f, 1.0f, 0.0f);		//Light rotating about Y-axis
		light_position[0] = angle_y_Light;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		glPopMatrix();
	}
	else if (KeyisPress == 3)
	{
		glPushMatrix();
		glRotatef(angle_z_Light, 0.0f, 0.0f, 1.0f);		//Light rotating about Z-axis
		light_position[1] = angle_z_Light;
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		glPopMatrix();
	}

	GLfloat material_ambiant[4];
	GLfloat material_Diffuse[4];
	GLfloat material_Specular[4];
	GLfloat material_shininess[1];
	//COLUMN - 1
	//1st sphere on 1st column
	//Material:

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	material_ambiant[0] = 0.0215f;
	material_ambiant[1] = 0.1745f;
	material_ambiant[2] = 0.215f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.7568f;
	material_Diffuse[1] = 0.61424f;
	material_Diffuse[2] = 0.7568f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.633f;
	material_Specular[1] = 0.727811f;
	material_Specular[2] = 0.633f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.6 * 128.0f;

	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);


	glPushMatrix();
	glTranslatef(-7.5f, 12.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[0], 0.75f, 30, 30);
	glPopMatrix();

	//2nd sphere on 1st column
	//Material:
	material_ambiant[0] = 0.135f;
	material_ambiant[1] = 0.2225f;
	material_ambiant[2] = 0.1575f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.54f;
	material_Diffuse[1] = 0.89f;
	material_Diffuse[2] = 0.63f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.316228f;
	material_Specular[1] = 0.316228f;
	material_Specular[2] = 0.316228f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.1 * 128.0f;

	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);


	glPushMatrix();
	glTranslatef(-7.5f, 10.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[1], 0.75f, 30, 30);
	glPopMatrix();

	//3rd sphere on 1st column
	//Material:
	material_ambiant[0] = 0.05375f;
	material_ambiant[1] = 0.05f;
	material_ambiant[2] = 0.06625f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.18257f;
	material_Diffuse[1] = 0.17f;
	material_Diffuse[2] = 0.22525f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.332741f;
	material_Specular[1] = 0.328634f;
	material_Specular[2] = 0.346435f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.3 * 128.0f;

	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-7.5f, 8.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[2], 0.75f, 30, 30);
	glPopMatrix();

	//4th sphere on 1st column
	//Material:
	material_ambiant[0] = 0.25f;
	material_ambiant[1] = 0.20725f;
	material_ambiant[2] = 0.20725f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 1.0f;
	material_Diffuse[1] = 0.829f;
	material_Diffuse[2] = 0.829f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.296648f;
	material_Specular[1] = 0.296648f;
	material_Specular[2] = 0.296648f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.088 * 128.0f;

	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-7.5f, 6.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[3], 0.75f, 30, 30);
	glPopMatrix();

	//5th sphere on 1st column
	//Material:
	material_ambiant[0] = 0.1745f;
	material_ambiant[1] = 0.01175f;
	material_ambiant[2] = 0.01175f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.61424f;
	material_Diffuse[1] = 0.04136f;
	material_Diffuse[2] = 0.04136f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.727811f;
	material_Specular[1] = 0.626959f;
	material_Specular[2] = 0.626959f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.6 * 128.0f;

	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-7.5f, 4.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[4], 0.75f, 30, 30);
	glPopMatrix();

	//6th sphere on 1st column
	//Material:
	material_ambiant[0] = 0.1f;
	material_ambiant[1] = 0.18725;
	material_ambiant[2] = 0.1745f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.396f;
	material_Diffuse[1] = 0.74151f;
	material_Diffuse[2] = 0.69102f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.297254;
	material_Specular[1] = 0.30829f;
	material_Specular[2] = 0.306678f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.1 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-7.5f, 2.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[5], 0.75f, 30, 30);
	glPopMatrix();

	//COLUMN - 2
	//1st sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.329412f;
	material_ambiant[1] = 0.223529f;
	material_ambiant[2] = 0.027451f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.780392f;
	material_Diffuse[1] = 0.568627f;
	material_Diffuse[2] = 0.113725f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.992157f;
	material_Specular[1] = 0.941176f;
	material_Specular[2] = 0.807843f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.21794872 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 12.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[6], 0.75f, 30, 30);
	glPopMatrix();

	//2nd sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.2125f;
	material_ambiant[1] = 0.1275f;
	material_ambiant[2] = 0.054f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.714f;
	material_Diffuse[1] = 0.4284f;
	material_Diffuse[2] = 0.18144f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.393549f;
	material_Specular[1] = 0.271906f;
	material_Specular[2] = 0.166721f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.2 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 10.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[7], 0.75f, 30, 30);
	glPopMatrix();

	//3rd sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.25f;
	material_ambiant[1] = 0.25f;
	material_ambiant[2] = 0.25f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.4f;
	material_Diffuse[1] = 0.4f;
	material_Diffuse[2] = 0.4f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.774597f;
	material_Specular[1] = 0.774597f;
	material_Specular[2] = 0.774597f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.6 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 8.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[8], 0.75f, 30, 30);
	glPopMatrix();

	//4th sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.19125f;
	material_ambiant[1] = 0.0735f;
	material_ambiant[2] = 0.0225f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.7038f;
	material_Diffuse[1] = 0.27048f;
	material_Diffuse[2] = 0.0828f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.256777f;
	material_Specular[1] = 0.137622f;
	material_Specular[2] = 0.086014f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.1 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 6.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[9], 0.75f, 30, 30);
	glPopMatrix();

	//5th sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.24752f;
	material_ambiant[1] = 0.1995f;
	material_ambiant[2] = 0.0745f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.75164f;
	material_Diffuse[1] = 0.60648f;
	material_Diffuse[2] = 0.22648f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.628281f;
	material_Specular[1] = 0.555802f;
	material_Specular[2] = 0.366065f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.4 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 4.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[10], 0.75f, 30, 30);
	glPopMatrix();

	//6th sphere on 2nd column
	//Material:
	material_ambiant[0] = 0.19225f;
	material_ambiant[1] = 0.19225f;
	material_ambiant[2] = 0.19225f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.50754f;
	material_Diffuse[1] = 0.50754f;
	material_Diffuse[2] = 0.50754f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.508273f;
	material_Specular[1] = 0.508273f;
	material_Specular[2] = 0.508273f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.4 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-9.5f, 2.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[11], 0.75f, 30, 30);
	glPopMatrix();

	//COLUMN - 3
	//1st sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.01f;
	material_Diffuse[1] = 0.01f;
	material_Diffuse[2] = 0.01f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.50f;
	material_Specular[1] = 0.50f;
	material_Specular[2] = 0.50f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 12.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[12], 0.75f, 30, 30);
	glPopMatrix();

	//2nd sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.1f;
	material_ambiant[2] = 0.06f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.0f;
	material_Diffuse[1] = 0.50980392f;
	material_Diffuse[2] = 0.50980392f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.50196078f;
	material_Specular[1] = 0.50196078f;
	material_Specular[2] = 0.50196078f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 10.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[13], 0.75f, 30, 30);
	glPopMatrix();

	//3rd sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.1f;
	material_Diffuse[1] = 0.35f;
	material_Diffuse[2] = 0.1f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.45f;
	material_Specular[1] = 0.55f;
	material_Specular[2] = 0.45f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 8.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[14], 0.75f, 30, 30);
	glPopMatrix();

	//4th sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.5f;
	material_Diffuse[1] = 0.0f;
	material_Diffuse[2] = 0.0f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.7f;
	material_Specular[1] = 0.6f;
	material_Specular[2] = 0.6f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 6.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[15], 0.75f, 30, 30);
	glPopMatrix();

	//5th sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.55f;
	material_Diffuse[1] = 0.55f;
	material_Diffuse[2] = 0.55f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.70f;
	material_Specular[1] = 0.70f;
	material_Specular[2] = 0.70f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 4.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[16], 0.75f, 30, 30);
	glPopMatrix();

	//6th sphere on 3rd column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.5f;
	material_Diffuse[1] = 0.5f;
	material_Diffuse[2] = 0.0f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.60f;
	material_Specular[1] = 0.60f;
	material_Specular[2] = 0.50f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.25 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-11.5f, 2.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[17], 0.75f, 30, 30);
	glPopMatrix();

	//COLUMN - 4
	//1st sphere on 4th column
	//Material:
	material_ambiant[0] = 0.02f;
	material_ambiant[1] = 0.02f;
	material_ambiant[2] = 0.02f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.01f;
	material_Diffuse[1] = 0.01f;
	material_Diffuse[2] = 0.01f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.4f;
	material_Specular[1] = 0.4f;
	material_Specular[2] = 0.4f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.078125 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 12.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[18], 0.75f, 30, 30);
	glPopMatrix();

	//2nd sphere on 4th column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.05f;
	material_ambiant[2] = 0.05f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.4f;
	material_Diffuse[1] = 0.5f;
	material_Diffuse[2] = 0.5f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.04f;
	material_Specular[1] = 0.7f;
	material_Specular[2] = 0.7f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.078125 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 10.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[19], 0.75f, 30, 30);
	glPopMatrix();

	//3rd sphere on 4th column
	//Material:
	material_ambiant[0] = 0.0f;
	material_ambiant[1] = 0.05f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.4f;
	material_Diffuse[1] = 0.5f;
	material_Diffuse[2] = 0.4f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.04f;
	material_Specular[1] = 0.7f;
	material_Specular[2] = 0.04f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.1 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 8.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[20], 0.75f, 30, 30);
	glPopMatrix();

	//4th sphere on 4th column
	//Material:
	material_ambiant[0] = 0.05f;
	material_ambiant[1] = 0.0f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.5f;
	material_Diffuse[1] = 0.4f;
	material_Diffuse[2] = 0.4f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.7f;
	material_Specular[1] = 0.04f;
	material_Specular[2] = 0.04f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.078125 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 6.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[21], 0.75f, 30, 30);
	glPopMatrix();

	//5th sphere on 4th column
	//Material:
	material_ambiant[0] = 0.05f;
	material_ambiant[1] = 0.05f;
	material_ambiant[2] = 0.05f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.5f;
	material_Diffuse[1] = 0.5f;
	material_Diffuse[2] = 0.5f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.7f;
	material_Specular[1] = 0.7f;
	material_Specular[2] = 0.7f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.78125 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 4.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[22], 0.75f, 30, 30);
	glPopMatrix();

	//6th sphere on 4th column
	//Material:
	material_ambiant[0] = 0.05f;
	material_ambiant[1] = 0.05f;
	material_ambiant[2] = 0.0f;
	material_ambiant[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_AMBIENT, material_ambiant);

	material_Diffuse[0] = 0.5f;
	material_Diffuse[1] = 0.5f;
	material_Diffuse[2] = 0.4f;
	material_Diffuse[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_DIFFUSE, material_Diffuse);

	material_Specular[0] = 0.7f;
	material_Specular[1] = 0.7f;
	material_Specular[2] = 0.04f;
	material_Specular[3] = 1.0f;

	glMaterialfv(GL_FRONT, GL_SPECULAR, material_Specular);

	material_shininess[0] = 0.1 * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, material_shininess);

	glPushMatrix();
	glTranslatef(-13.5f, 2.5f, 0.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	gluSphere(quadric[23], 0.75f, 30, 30);
	glPopMatrix();

}

void update(void)
{

	//code	
	//Rotate the light about x-axis
	angle_x_Light = ((angle_x_Light - 0.5) - 360);

	//Rotate the light about y-axis
	angle_y_Light = ((angle_y_Light - 0.5) - 360);
	
	//Rotate the light about z-axis
	angle_z_Light = ((angle_z_Light - 0.5) - 360);
	
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
		if (width <= height)
	{
		glOrtho(0.0f, 15.5f, 0, (10.0f *(GLfloat)height / width), -10.0f, 15.f);
	}
	else
	{
		glOrtho(0.0f, (-15.5f * (GLfloat)width / height), 0.0f, 15.5f, -10.0f, 15.f);
	}

}
	
void uninitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}
		



















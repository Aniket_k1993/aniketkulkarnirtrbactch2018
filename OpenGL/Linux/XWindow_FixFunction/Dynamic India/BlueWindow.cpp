//XWindows Texture mapping

#include <iostream>	
#include <stdio.h>
#include <stdlib.h>	//For exit();
#include <memory.h>	//For memset();
#define _USE_MATH_DEFINES
#include <math.h>
#include <time.h>
//XWindows headers:

#include <X11/Xlib.h>
#include <X11/Xutil.h>	//Used for XVisualInfo struct and associated APIs
#include <X11/XKBlib.h>	//Keyboard library
#include <X11/keysym.h>	//For key symbols 

//OpenGL headers:
#include <GL/gl.h>
#include <GL/glx.h>	//Bridging API
#include <GL/glu.h>

//Namespaces:
using namespace std;

//Global variable declarations:
bool bFullscreen = false;		//Fullscreen variable
Display *gpDisplay = NULL;		//Global pointer
XVisualInfo *gpXVisualInfo = NULL;	
Colormap gColormap;
Window gWindow;

int giWindowWidth = 800;		//Window width
int giWindowHeight = 600;		//Window height

// For Rotatation 
float TrAngle =0.0f;
float RectAngle=0.0f;

//For Dynamic India 
void FORI(void);
void FORN(void);
void FORD(void);
void FORSECONDI(void);
void FORA(void);
void airoplain(void);
void tiranga(void);

GLfloat x_For_fi = -3.0, y_For_fa = 5.0, a_For_fn = -7.0, b_For_si = 10.0, r = 0.0f, s = 0.0f, t = 0.0f, i = 0.0f, d = 0.0f, k = 0.0f, f = 12;
GLfloat m = -13.0f, n = -14.5f;
GLfloat v = 0.0f, y = 0.0f, a = -2.0f;
GLfloat v_For_Bottom = 0.0f, y_For_Bottom = 0.0f, a_For_Sec_Mov = -2.0f;
GLfloat angle_For_Top = M_PI;
GLfloat angle_For_Bottom = M_PI;
GLfloat angle = 45.0f;
GLfloat angle_exit = 0.0f;

long int Ver = 1;
GLfloat orange = -3.5f;
GLfloat white = -3.5f;
GLfloat green = -3.5f;
GLfloat D_fade = 0.0f;
GLfloat Red = 0.0f;
GLfloat Green1 = 0.0f;
GLfloat Green2 = 0.0f;
GLfloat Black = 1.0f;
GLfloat Black1 = 0.5f;
GLfloat Black2 = 0.0f;
GLfloat orange_for_tiranga = 0.0f;
GLfloat white_for_tiranga = 0.0f;
GLfloat green_for_tiranga = 0.0f;
GLfloat x_move_tiranga = 0.0f;

GLfloat tricolor_movement = -3.0f;



GLXContext gGLXContext;			//Rendering context

//entry point fuction:
int main(void)		
{
	//Function prototypes:
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void Update(void);
	void resize(int, int);
	void uninitialize(void);
	
	//Variable declarations:
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;
	char ascii[26];		//For key press funtionality
	//Code:
	CreateWindow();		//Function to create a window
	
	//Initialize():
	initialize();
	
	XEvent event;		//To store the message/event
	KeySym keysym;		
	
	//Game loop
	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);		//Get next message. Fill 'event' with the address of next event.
			switch(event.type)					//Switch on event type
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:			//Exit on Esc key press
							bDone = true;
						default:
							break;
					}
					XLookupString(&event.xkey, ascii, sizeof(ascii), NULL, NULL);
					switch(ascii[0])
					{
						case 'f':				//Toggle Fullscreen on keypress
						case 'F':
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
						default:
							break;
					}		
					break;
				//Declarations for future use
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:				//Left mouse button
							break;
						case 2:				//Middle mouse button
							break;	
						case 3:				//Right mouse button
							break;
						default:
							break;
					}
					break;
				case MotionNotify:			//Mouse move
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:				//Painting of window
					break;
				case DestroyNotify:			
					break;
				case 33:					//Close press
					bDone = true;
					break;
				default:
					break;
			}
		}
		
		display();
		//Spin calls here
		Update();	
	}
	uninitialize();
	return(0);
}

//Create XWindows window				
void CreateWindow(void)
{
	//Function Prototypes:
	void uninitialize(void);
	
	//Variable declarations:
	XSetWindowAttributes winAttribs;
	int defaultScreen;	//To get the default screen
	int defaultDepth;	//To get the default depth
	int styleMask;
	
	//Set the depth parameters
	static int frameBufferAttributes[] = 
	{
		GLX_DOUBLEBUFFER, True,
		GLX_RGBA,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_DEPTH_SIZE, 24,
		None			//As a stopper for array
	};
	
	//Code:
	gpDisplay = XOpenDisplay(NULL);		//Pointer to struct is the return value. It returns default display pointer.
	if(gpDisplay == NULL)				//Error handling
	{
		printf("ERROR: Unable to open X Display.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen = XDefaultScreen(gpDisplay);
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	//We have created a visual to create a new context
		
	winAttribs.border_pixel = 0;		//Take default value for color
	winAttribs.border_pixmap = 0;		//Take default pattern
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;
	
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPress | KeyPressMask | PointerMotionMask | StructureNotifyMask;
	
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),	//Parent window handle
				0,						//X
				0,						//Y
				giWindowWidth,			//W
				giWindowHeight,			//H
				0,						//Thickness of border
				gpXVisualInfo->depth,		
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);
	if(!gWindow)						//Error handling
	{
		printf("ERROR: Failed to create Main Window.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay, gWindow, "OpenGL 3D textures");
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);	//33 turned on after this protocol
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);		//ShowWindow(); UpdateWindow(); SetFocus(), etc
}

//Toggle fullscreen	
void ToggleFullscreen(void)
{
	//Variable declarations:
	Atom wm_state;		//To save state of normal screen
	Atom fullscreen;	//TO save state of full screen
	XEvent xev = {0};
	
	//Code:
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);	//Network compliant. 
	memset(&xev, 0, sizeof(xev));		//Memset to default values
	
	xev.type = ClientMessage;		//Custom message
	xev.xclient.window = gWindow;		//Window for which client message is going(predefined message)
	xev.xclient.message_type = wm_state;	//Type of message that you are sending(created by you)
	xev.xclient.format = 32;		//byte size = 32
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
	
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),	//Propagates to your window
		False,						//If the message be propagated to your children
		StructureNotifyMask,
		&xev);
		
}

//Initialization
void initialize(void)
{
	//Function protoypes:
	void resize(int, int);
	void uninitialize(void);
	int LoadGLTexture(GLuint *, char *);	//Load texture function prototype
	
	//Code:
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	if(gGLXContext == NULL)
	{
		printf("ERROR: Failed to create Rendering Context.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);									//clear depth buffer
	glEnable(GL_DEPTH_TEST);							//enable the depth
	glDepthFunc(GL_LEQUAL);								//less than or equal to 1.0f in far. Uses ray tracing algorithm

	glShadeModel(GL_SMOOTH);							//to remove aliasing
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	//to remove distortion

	//enable texture mapping

	resize(giWindowWidth, giWindowHeight);					//Stray resize
	ToggleFullscreen();
} 

//Function to load the texture

void resize(int width, int height)
{
	//Code:
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

	
//Free all resources
void uninitialize(void)
{

	// code

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}




//Drawing
void display(void)
{
/*void FORI(void);
void FORN(void);
void FORD(void);
void FORSECONDI(void);
void FORA(void);
void airoplain(void);*/
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glClear(GL_COLOR_BUFFER_BIT);
glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if(Ver >= 1)
	{ 
		glTranslatef(x_For_fi, 0.0f, -4.2f);
		FORI();
		if (x_For_fi <= -1.5f)
			Ver = 2;
	}
	////////////// FOR A
	if (Ver >= 2)
	{ 
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(y_For_fa, 0.0f, -4.2f);
		FORA();
		if (y_For_fa >= 1.5f)
			Ver = 3;
	}
	////////////// for N 
	if(Ver >= 3)
	{ 
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-1.2f, -a_For_fn, -4.2f);
		FORN();
		if (a_For_fn <= 0.0f)
			Ver = 4;
	}
	/////////////// FOR I
	if(Ver >= 4)
	{ 
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(1.02f, -b_For_si, -4.2f);
		FORSECONDI();
		if (b_For_si >= 0.0f)
			Ver = 5;
	}
	///////////// FOR D
	if(Ver >= 5)
	{ 
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(-0.2f, 0.0f, -4.2f);
		FORD();
		if (Red >= 1.0f)	//To initiate next animation of I after fade in effect sets in
			Ver = 6;
	}
	///////////////////Tirang and Airoplain
	//if (vary >= 6)
	//{
	if (Ver >= 6)
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPointSize(7.0f);
		glTranslatef(-3.0f, 4.5f, -3.0f);
		if (angle_For_Top < (3 * (M_PI / 2)))
		{
			v = 4.5f * (cos(angle_For_Top));
			y = 4.5f * (sin(angle_For_Top));
			glTranslatef(v, y, -6.0f);
			glRotatef(angle, 0.0f, 0.0f, -1.0f);
			airoplain();
			tiranga();

		}

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPointSize(7.0f);
		glTranslatef(-3.0f, -4.5f, -3.0f);
		if (angle_For_Top < (3 * (M_PI / 2)))
		{
			v = 4.5f * (cos(angle_For_Top));
			y = 4.5f * (sin(-angle_For_Top));
			glTranslatef(v, y, -6.0f);
			glRotatef(angle, 0.0f, 0.0f, 1.0f);
			airoplain();
			tiranga();
		}

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPointSize(7.0f);
		glTranslatef(-3.0f, -0.0f, -3.0f);
		if (v <= 10.0f)
		{
			glTranslatef(v, 0.0f, -6.0f);
			airoplain();
		}
		tiranga();
		//if (Black <= 0.0f)
		//time = 7;
		if (v >= 7.7)
		{
			Ver = 7;
		}
	}
	
	if (Ver >= 7)
	{
		/////////////////// For Right Top


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPointSize(7.0f);
		glTranslatef(6.7f, 0.0f, -3.0f);
		if (angle_For_Bottom < (3 * (M_PI / 2)))
		{
			v_For_Bottom = 4.0f * (cos(angle_For_Bottom) / 2);
			y_For_Bottom = 4.0f * (-sin(angle_For_Bottom) / 2);
			//glRotatef(angle, v_For_Bottom, y_For_Bottom, 0.0f);
			glTranslatef(v_For_Bottom, y_For_Bottom, -6.0f);
			glRotatef(angle_exit, 0.0f, 0.0f, 1.0f);
			airoplain();
			//tiranga();
			//tiranga();
		}
		//glTranslatef(v_For_Bottom, y_For_Bottom, -6.0f);


		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glPointSize(7.0f);
		glTranslatef(6.7f, 0.0f, -3.0f);
		if (angle_For_Bottom < (3 * (M_PI / 2)))
		{
			v_For_Bottom = 4.0f * (cos(angle_For_Bottom)/2);
			y_For_Bottom = 4.0f * (sin(angle_For_Bottom)/2);
			//glRotatef(angle, v_For_Bottom, y_For_Bottom, 0.0f);
			glTranslatef(v_For_Bottom, y_For_Bottom, -6.0f);
			glRotatef(angle_exit, 0.0f, 0.0f, -1.0f);
			airoplain();
			//tiranga();
		}
		if (angle_exit <= 45.0f)
			Ver = 8;
	}
   
	// double bufferring
	glXSwapBuffers(gpDisplay, gWindow);


}

void Update(void)
{
////////  MOVIG FIRST I	
	if (Ver >= 1 && x_For_fi <= -1.5)
	{
		x_For_fi = x_For_fi + 0.002f;
	}
//////// FOR MOVING A
	if (Ver >= 2 && y_For_fa >= 1.5f)
	{
		y_For_fa = y_For_fa - 0.002f;
	}
/////// FOR MOVING 
	if (Ver >= 3 && a_For_fn <= 0.0f)
	{
		a_For_fn = a_For_fn + 0.002f;
	}
////// FOR MOVING SECOND I
	if (Ver >= 4 && b_For_si >= 0.0f)
	{
		b_For_si = b_For_si - 0.002f;

	}
	else
	{ 
		if (Ver >= 5)						//For animation of D
		{
			if (Red <= 1.0f)		//For Orange fade in
				Red = Red + 0.0007f;
			if (Green1 <= 1.0f)		//For Green fade in
				Green1 = Green1 + 0.0007f;
			if (Green2 <= 0.5f)		//For Orange fade in
				Green2 = Green2 + 0.0007f;
		}
	}
	
	/////////////// FOR AiroPlain

	if (Ver >= 6)
	{
		if (angle >= 0.0f)
		{
			angle = angle - 0.007f;
		}


		angle_For_Top = angle_For_Top + 0.002;
		//v = v + 0.0002f;
		if (v <= 0.0)
		{
			v = v + 0.005;
			//airoplain();
		}
		else if (v <= 10.7f)
		{
			v = v + 0.005;
		}
		//if (angle_For_Bottom <= 45)
		//{
		//	if (Black <= 1.0f)
		//		Black = 1.0f;
		//	if (Black1 <= 0.5f)
		//		Black1 = 0.0f;
		//	if (Black2 <= 1.0f)
		//		Black2 = 1.0f;
		//}
		//else
		//{
		//	if (Black >= 0.0f)
		//		Black = Black - 0.0007f;
		//	if (Black1 >= 0.0f)
		//		Black1 = Black1 - 0.0007f;
		//	if (Black2 >= 0.0f)
		//		Black2 = Black2 - 0.0007f;
		//}
		if (tricolor_movement <= -2.175f)
			tricolor_movement = tricolor_movement + 0.005f;
		if (tricolor_movement >= 1.175f)
		{
			if (orange <= -0.35f)
				orange = orange + 0.007f;
			if (white <= -0.435f)
				white = white + 0.007f;
			if (green <= -0.36f)
				green = green + 0.007f;
		}

	}
	
	if (Ver >= 7)
	{
		angle_For_Bottom = angle_For_Bottom + 0.002;
		if (angle_exit <= 45.0f)
		{
			angle_exit = angle_exit + 0.002f;
		}
	}
	if (Ver >= 8)
	{
		if (Black >= 0.0f)
			Black = Black - 0.007f;
		if (Black1 >= 0.0f)
			Black1 = Black1 - 0.007f;
		if (Black2 >= 0.0f)
			Black2 = Black2 - 0.007f;

		if (orange_for_tiranga <= 1.0f)
		{
			orange_for_tiranga = orange_for_tiranga + 0.007f;
		}
		if (white_for_tiranga <= 0.5f)
		{
			white_for_tiranga = white_for_tiranga + 0.007f;
		}
		if (x_move_tiranga <= 15.0f)
		{
			x_move_tiranga = x_move_tiranga + 0.005f;
		}
	}


}

void airoplain(void)
{
	//glTranslatef(0.0f, 0.0f, -4.4f);
	glColor3f(0.128f, 0.226f, 0.238f);
	glBegin(GL_TRIANGLES);
	glVertex3f(0.6f, 0.0f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.1f, -0.1f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(-0.3f, -0.1f, 0.0f);
	glVertex3f(0.1f, -0.1f, 0.0f);
	glEnd();
	////////////// UP PAN
	glBegin(GL_TRIANGLES);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(-0.2f, 0.3f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glEnd();
	////////////// DOWN PAN
	glBegin(GL_TRIANGLES);
	glVertex3f(0.1f, -0.0f, 0.0f);
	glVertex3f(-0.2f, -0.3f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glEnd();
	///////// BACK PANAL
	glBegin(GL_TRIANGLES);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glVertex3f(-0.4f, -0.2f, 0.0f);
	glEnd();

	//////////   IAF
	///////////////FOR I
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(-0.3f, 0.1f, 0.0f);
	glVertex3f(-0.3f, -0.1f, 0.0f);
	glEnd();

	glBegin(GL_LINE_STRIP);
	glVertex3f(-0.2f, -0.1f, 0.0f);
	glVertex3f(-0.1f, 0.1f, 0.0f);
	glVertex3f(0.0f, -0.1f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.1f, -0.1f, 0.0f);
	glVertex3f(0.1f, 0.08f, 0.0f);
	glVertex3f(0.2f, 0.08f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glEnd();
	
	//////////////////////////// Tiranga
	//glTranslatef(tricolor_movement, 0.0f, -4.5f);

	////glMatrixMode(GL_MODELVIEW);
	////glLoadIdentity();

}

void tiranga(void)
{
	glLineWidth(5.0f);
	glBegin(GL_LINES);

	if (tricolor_movement >= 1.375f)
	{
		//
		//Center Line of A
		//Orange line
		//glColor3f(1.0f, 0.5f, 0.0f);		//Orange color
		glColor3f(Black, Black1, 0.0f);		//Orange color
											//glVertex3f(0.528f, 0.02f, 0.0f);	//Original co-ordinates
											//glVertex3f(0.974, 0.02f, 0.0f);
		glVertex3f(orange, 0.05f, 0.0f);
		glVertex3f(-1.7f, 0.05f, 0.0f);

		//White line
		//glColor3f(1.0f, 1.0f, 1.0f);		//White
		glColor3f(Black, Black, Black);		//Orange color
											//glVertex3f(0.52f, 0.0f, 0.0f);	//Original co-ordinates
											//glVertex3f(0.98f, 0.0f, 0.0f);
		glVertex3f(white, 0.02f, 0.0f);
		glVertex3f(-1.7f, 0.02f, 0.0f);

		//Green line
		//glColor3f(0.0f, 1.0f, 0.0f);		//Green color
		glColor3f(0.0f, Black, 0.0f);		//Orange color
											//glVertex3f(0.513f, -0.02f, 0.0f);	//Original Co-ordinates
											//glVertex3f(0.985f, -0.02f, 0.0f);
		glVertex3f(green, -0.01f, 0.0f);
		glVertex3f(-1.7f, -0.01f, 0.0f);
	}
	else
	{
		//Center Line of A
		//Orange line
		//glColor3f(1.0f, 0.5f, 0.0f);		//Orange color
		glColor3f(Black, Black1, 0.0f);
		//glVertex3f(0.528f, 0.02f, 0.0f);	//Original co-ordinates
		//glVertex3f(0.974, 0.02f, 0.0f);
		glVertex3f(-9.5f, 0.05f, 0.0f);
		glVertex3f(-0.4f, 0.05f, 0.0f);

		//White line
		//glColor3f(1.0f, 1.0f, 1.0f);		//White
		glColor3f(Black, Black, Black);		//White
											//glVertex3f(0.52f, 0.0f, 0.0f);	//Original co-ordinates
											//glVertex3f(0.98f, 0.0f, 0.0f);
		glVertex3f(-9.5f, 0.02f, 0.0f);
		glVertex3f(-0.4f, 0.02f, 0.0f);

		//Green line
		//glColor3f(0.0f, 1.0f, 0.0f);		//Green color
		glColor3f(0.0f, Black, 0.0f);		//Green color
											//glVertex3f(0.513f, -0.02f, 0.0f);	//Original Co-ordinates
											//glVertex3f(0.985f, -0.02f, 0.0f);
		glVertex3f(-9.5f, -0.01f, 0.0f);
		glVertex3f(-0.4f, -0.01f, 0.0f);
	}
	glEnd();

}


void FORI(void)
{
		///////////////// FOR I  
	glLineWidth(5.5);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.5f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.5f, -1.0f, 0.0f);
	glEnd();


}
void FORN(void)
{
	glLineWidth(5.5);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.3f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.3f, -1.0f, 0.0f);
	
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.3f, -1.0f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.3f, 1.0f, 0.0f);

	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.3f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.3f, -1.0f, 0.0f);

	glEnd();

}

void FORD(void)
{
	
	glLineWidth(5.5);
	glBegin(GL_LINES);
	glColor3f(Red, Green2, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(0.4f, 1.0f, 0.0f);
	glColor3f(Red, Green2, 0.0f);
	glVertex3f(0.4f, 1.0f, 0.0f);
	glColor3f(0.0f, Green1, 0.0f);
	glVertex3f(0.4f, -1.0f, 0.0f);
	glColor3f(0.0f, Green1, 0.0f);
	glVertex3f(0.4f, -1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glColor3f(0.0f, Green1, 0.0f);
	glVertex3f(-0.3f, -1.0f, 0.0f);
	glColor3f(Red, Green2, 0.0f);
	glVertex3f(-0.3f, 1.0f, 0.0f);
	glEnd();

}

void FORSECONDI(void)
{
	glLineWidth(5.5);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(-0.5f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.5f, -1.0f, 0.0f);
	glEnd();

}
void FORA(void)
{
	glLineWidth(5.5);
	glBegin(GL_LINE_STRIP);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glColor3f(1.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.5f, -1.0f, 0.0f);
	glEnd();


	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glColor3f(orange_for_tiranga, white_for_tiranga, green_for_tiranga);		//Orange color
	glVertex3f(-0.22, 0.05f, 0.0f);
	glVertex3f(0.22, 0.05f, 0.0f);

	glColor3f(orange_for_tiranga, orange_for_tiranga, orange_for_tiranga);		//White
	glVertex3f(-0.22, 0.02f, 0.0f);
	glVertex3f(0.22, 0.02f, 0.0f);

	glColor3f(0.0f, orange_for_tiranga, 0.0f);		//Green color
	glVertex3f(-0.22, 0.01f, 0.0f);
	glVertex3f(0.22, 0.01f, 0.0f);
	glEnd();

}



		






















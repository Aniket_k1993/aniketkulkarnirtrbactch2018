//XWindows Texture mapping

#include <iostream>	
#include <stdio.h>
#include <stdlib.h>	//For exit();
#include <memory.h>	//For memset();

//XWindows headers:

#include <X11/Xlib.h>
#include <X11/Xutil.h>	//Used for XVisualInfo struct and associated APIs
#include <X11/XKBlib.h>	//Keyboard library
#include <X11/keysym.h>	//For key symbols 

//OpenGL headers:
#include <GL/gl.h>
#include <GL/glx.h>	//Bridging API
#include <GL/glu.h>

//Namespaces:
using namespace std;

//Global variable declarations:
bool bFullscreen = false;		//Fullscreen variable
Display *gpDisplay = NULL;		//Global pointer
XVisualInfo *gpXVisualInfo = NULL;	
Colormap gColormap;
Window gWindow;


// For View Port Program Only
//int width, height;
int flag = 0;
////

int giWindowWidth = 800;		//Window width
int giWindowHeight = 600;		//Window height

// For Rotatation 
float TrAngle =0.0f;
float RectAngle=0.0f;

GLXContext gGLXContext;			//Rendering context

//entry point fuction:
int main(void)		
{
	//Function prototypes:
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void Update(void);
	void resize(int, int);
	void uninitialize(void);
	
	//Variable declarations:
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;

	bool bDone = false;
	char ascii[26];		//For key press funtionality
	//Code:
	CreateWindow();		//Function to create a window
	
	//Initialize():
	initialize();
	
	XEvent event;		//To store the message/event
	KeySym keysym;		
	
	//Game loop
	while(bDone == false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay, &event);		//Get next message. Fill 'event' with the address of next event.
			switch(event.type)					//Switch on event type
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:			//Exit on Esc key press
							bDone = true;
					/*int giWindowWidth = 800;		//Window width
int giWindowHeight = 600;		//Window height*/
					case 0x30:
						flag = 0;
						resize(giWindowWidth, giWindowHeight);
						break;
					case 0x31:
						flag = 1;
						resize(giWindowWidth, giWindowHeight);
						break;
					case 0x32:
						flag = 2;
						resize(giWindowWidth, giWindowHeight);
						break;
					case 0x33:
						flag = 3;
						resize(giWindowWidth, giWindowHeight);
						break;
					case 0x34:
						flag = 4;
						resize(giWindowWidth, giWindowHeight);
						break;
					case 0x35:
						flag = 5;
						resize(giWindowWidth, giWindowHeight);
						break;
					case 0x36:
						flag = 6;
						resize(giWindowWidth, giWindowHeight);
						break;
					case 0x37:
						flag = 7;
						resize(giWindowWidth, giWindowHeight);
						break;
					case 0x38:
						flag = 8;
						resize(giWindowWidth, giWindowHeight);
						break;
///////////////////////////////////						break;
		
					default:
						break;
					}
					XLookupString(&event.xkey, ascii, sizeof(ascii), NULL, NULL);
					switch(ascii[0])
					{
						case 'f':				//Toggle Fullscreen on keypress
						case 'F':
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;
						default:
							break;
					}		
					break;
				//Declarations for future use
				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:				//Left mouse button
							break;
						case 2:				//Middle mouse button
							break;	
						case 3:				//Right mouse button
							break;
						default:
							break;
					}
					break;
				case MotionNotify:			//Mouse move
					break;
				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;
				case Expose:				//Painting of window
					break;
				case DestroyNotify:			
					break;
				case 33:					//Close press
					bDone = true;
					break;
				default:
					break;
			}
		}
		
		display();
		//Spin calls here
//		Update();
	}
	uninitialize();
	return(0);
}

//Create XWindows window				
void CreateWindow(void)
{
	//Function Prototypes:
	void uninitialize(void);
	
	//Variable declarations:
	XSetWindowAttributes winAttribs;
	int defaultScreen;	//To get the default screen
	int defaultDepth;	//To get the default depth
	int styleMask;
	
	//Set the depth parameters
	static int frameBufferAttributes[] = 
	{
		GLX_DOUBLEBUFFER, True,
		GLX_RGBA,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_DEPTH_SIZE, 24,
		None			//As a stopper for array
	};
	
	//Code:
	gpDisplay = XOpenDisplay(NULL);		//Pointer to struct is the return value. It returns default display pointer.
	if(gpDisplay == NULL)				//Error handling
	{
		printf("ERROR: Unable to open X Display.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen = XDefaultScreen(gpDisplay);
	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	//We have created a visual to create a new context
		
	winAttribs.border_pixel = 0;		//Take default value for color
	winAttribs.border_pixmap = 0;		//Take default pattern
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay, RootWindow(gpDisplay, gpXVisualInfo->screen), gpXVisualInfo->visual, AllocNone);

	gColormap = winAttribs.colormap;
	
	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPress | KeyPressMask | PointerMotionMask | StructureNotifyMask;
	
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	gWindow = XCreateWindow(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),	//Parent window handle
				0,						//X
				0,						//Y
				giWindowWidth,			//W
				giWindowHeight,			//H
				0,						//Thickness of border
				gpXVisualInfo->depth,		
				InputOutput,
				gpXVisualInfo->visual,
				styleMask,
				&winAttribs);
	if(!gWindow)						//Error handling
	{
		printf("ERROR: Failed to create Main Window.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay, gWindow, "OpenGL 3D textures");
	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);	//33 turned on after this protocol
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);		//ShowWindow(); UpdateWindow(); SetFocus(), etc
}

//Toggle fullscreen	
void ToggleFullscreen(void)
{
	//Variable declarations:
	Atom wm_state;		//To save state of normal screen
	Atom fullscreen;	//TO save state of full screen
	XEvent xev = {0};
	
	//Code:
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);	//Network compliant. 
	memset(&xev, 0, sizeof(xev));		//Memset to default values
	
	xev.type = ClientMessage;		//Custom message
	xev.xclient.window = gWindow;		//Window for which client message is going(predefined message)
	xev.xclient.message_type = wm_state;	//Type of message that you are sending(created by you)
	xev.xclient.format = 32;		//byte size = 32
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;
	
	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),	//Propagates to your window
		False,						//If the message be propagated to your children
		StructureNotifyMask,
		&xev);
		
}

//Initialization
void initialize(void)
{
	//Function protoypes:
	void resize(int, int);
	void uninitialize(void);
	int LoadGLTexture(GLuint *, char *);	//Load texture function prototype
	
	//Code:
	gGLXContext = glXCreateContext(gpDisplay, gpXVisualInfo, NULL, GL_TRUE);
	if(gGLXContext == NULL)
	{
		printf("ERROR: Failed to create Rendering Context.\n Exiting...\n");
		uninitialize();
		exit(1);
	}
	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepth(1.0f);									//clear depth buffer
	glEnable(GL_DEPTH_TEST);							//enable the depth
	glDepthFunc(GL_LEQUAL);								//less than or equal to 1.0f in far. Uses ray tracing algorithm

	glShadeModel(GL_SMOOTH);							//to remove aliasing
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	//to remove distortion

	//enable texture mapping

	resize(giWindowWidth, giWindowHeight);					//Stray resize
} 

//Function to load the texture

//Drawing
void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glBegin(GL_TRIANGLES);
	//////////////////////////////////////////////////////////////////
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.5f, -0.5f, 0.0f);
		glVertex3f(0.5f, -0.5f, 0.0f);
	glEnd();
    
	// double bufferring
	glXSwapBuffers(gpDisplay, gWindow);


}




void resize(int width, int height)
{
	//Code:
	if(height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
/////////For Left Bottom
	if (flag == 0)
	{
		glViewport(0,0,(float)width,(float)height);
	}
	else if (flag == 1)
	{
		glViewport(0, 0, width/2, height/2);
	}
	///////// For Right Bottom
	else if (flag == 2)
	{
		glViewport(width/2, 0, width/2, height/2);
	}
	///////// For Left Top 
	else if (flag == 3)
	{
		glViewport(0, height/2, width/2, height/2);
	}
	///////// For Right Top
	else if (flag == 4)
	{
		glViewport(width/2, height/2, width/2, height/2);
	}
	///////////
	else if (flag == 5)
	{
		glViewport(0, 0, width/2, height);
	}
	else if (flag == 6)
	{
		glViewport(width/2, 0, width/2, height);
	}
	else if (flag == 7)
	{
		glViewport(0, height/2, width/2, height);
	}
	else if (flag == 8)
	{
		glViewport(width, height/2, width/2, height/2);
	}
	else if (flag == 9)
	{
		glViewport(width*0.5, height*0.5, width*0.5, height*0.5);
	}	
	//if (flag == 0)
////	{
	//	
	//}
//	else if (flag == 1)
//	{
		
//	}
//}	
}

	
//Free all resources
void uninitialize(void)
{

	// code

	GLXContext currentGLXContext;
	currentGLXContext = glXGetCurrentContext();
	
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}
		





















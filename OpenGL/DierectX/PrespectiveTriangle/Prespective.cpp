#include<windows.h>
#include<stdio.h>

//****Dierect X Changes****
#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning(disable: 4838)
#include "XNAMath/xnamath.h"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"D3dCompiler.lib")
//  ********  //

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

enum
{
	ANK_ATTRIBUTE_POSITION,
	ANK_ATTRIBUTE_COLOR,
	ANK_ATTRIBUTE_NORMAL,
	ANK_ATTRIBUTE_TEXTURE0,
};
//global Variable Declaration
FILE *gpFile = NULL;
char gszlogFileName[] = "log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool isFullscreen = false;
bool gbisActive = false;
bool isEscapeKeyPressed = false;

float clearColor[4]; //RGBA
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;

struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix;
};

XMMATRIX gPrespectivegraphicsProjectionMatrix;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) 
{
	HRESULT initialize(void);
	void uninitialize(void);
	void Display(void);

	WNDCLASSEX wndClass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("MyDierctX_First_Program");
	bool bDone = false;

	if (fopen_s(&gpFile, gszlogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created\nExisting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Create SucessFull");
		fclose(gpFile);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = WndProc;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;

	if (!RegisterClassEx(&wndClass))
	{
		MessageBox(NULL, TEXT("Cannot register class."), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(EXIT_FAILURE);
	}
	DWORD styleExtra = WS_EX_APPWINDOW;
	dwStyle = WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE;


	hwnd = CreateWindow(szClassName,
		TEXT("DierectX First Program"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{

		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "intialize() Failed. Exisiting NOW...\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "initialize() SucessFully.\n");
		fclose(gpFile);
	}

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//render
			Display();

			if (gbisActive)
			{
				if (isEscapeKeyPressed)
				{
					bDone = true;
				}
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	HRESULT resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize();

	//variable Declaration
	HRESULT hr;

	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0) //if 0, windows is active
		{
			gbisActive = true;
		}
		else //if not 0 , windows is not active
		{
			gbisActive = false;
		}
		break;

	case WM_ERASEBKGND:
		return (0);

	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{

				fopen_s(&gpFile, gszlogFileName, "a+");
				fprintf_s(gpFile, "resize() Failed. Exisiting NOW...\n");
				fclose(gpFile);
			}
			else
			{
				fopen_s(&gpFile, gszlogFileName, "a+");
				fprintf_s(gpFile, "resize() SucessFully.\n");
				fclose(gpFile);
			}  
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (isEscapeKeyPressed == false)
			{
				isEscapeKeyPressed = true;
			}
			break;
		case 0x46:
			if (isFullscreen == false)
			{
				ToggleFullScreen();
				isFullscreen = true;
			}
			else
			{
				ToggleFullScreen();
				isFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (isFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	void uninitialize();
	HRESULT resize(int, int);

	//variable declaration
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{ D3D_DRIVER_TYPE_HARDWARE,
	  D3D_DRIVER_TYPE_WARP,
	  D3D_DRIVER_TYPE_REFERENCE, };
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // Default, lowest 

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1; // based upon  d3dFeaturelevel_requierd
	
	//code
	numDriverTypes = sizeof(d3dDriverType) / sizeof(d3dDriverTypes[0]);
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}
	if (FAILED(hr))
	{

		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDevicesAndSwapChain() Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDevicesAndSwapChain() Sucess...\n");
		fprintf_s(gpFile, "The Chosen Driver Is Of");

		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "Warp type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference type. \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type. \n");
		}
	
		fprintf_s(gpFile, "The Supported Heighest  Feature Level Is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0 \n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpFile, "10.1 \n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0 \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown \n");
		}
		fclose(gpFile);
	}

	const char  *VertexShaderSourceCode =
	"cbuffer ConstantBuffer" \
	"{" \
	"float4x4 worldViewProjectionMatrix;"  \
	"}" \
	"float4 main(float4 pos: POSITION): SV_POSITION" \
	"{" \
	"	float4 position = mul(worldViewProjectionMatrix, pos);" \
	"   return(position);" \
	"}";

	ID3DBlob *pID3DBlob_VertexShaderSourceCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(VertexShaderSourceCode,
		lstrlenA(VertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderSourceCode,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszlogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() Failed For Vertex Shader : %s.\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&gpFile, gszlogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() Failed For Vertex Shade  Sucess...\n");
			fprintf_s(gpFile, "The Chosen Driver Is Of");
		}
	}

	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderSourceCode->GetBufferPointer(),
		pID3DBlob_VertexShaderSourceCode->GetBufferSize(),
		NULL,
		&gpID3D11VertexShader);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "CreateShader: Faild \n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "Create Shader Sucess...\n");
		fclose(gpFile);

	}
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);

	//*********************** PIXEL SHADER ******************
	const char  *PixelShaderSourceCode =
		"float4 main(void): SV_TARGET" \
		"{" \
		"   return float4(1.0f, 1.0f, 0.0f, 1.0f);" \
		"}";

	ID3DBlob *pID3DBlob_PixelShaderSourceCode = NULL;
	//ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(PixelShaderSourceCode,
		lstrlenA(PixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderSourceCode,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszlogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() Failed For Pixel Shader : %s.\n", (char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
		else
		{
			fopen_s(&gpFile, gszlogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() Failed For Pixel Shade  Sucess...\n");
			fprintf_s(gpFile, "The Chosen Driver Is Of");
		}
	}

	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderSourceCode->GetBufferPointer(),
		pID3DBlob_PixelShaderSourceCode->GetBufferSize(),
		NULL,
		&gpID3D11PixelShader
	);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "CreateShader: Faild \n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "Create Shader Sucess...\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);
	pID3DBlob_PixelShaderSourceCode->Release();
	pID3DBlob_PixelShaderSourceCode = NULL;
	// ******************** Initialize Buffer ***********************

	const float triangleVertices[] = {
	0.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	-1.0f, -1.0f, 0.0f
	};

	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory((void *)&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = sizeof(float) * ARRAYSIZE(triangleVertices);
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	hr = gpID3D11Device ->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_VertexBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device: CreateBuffer() Faild for Vertex buffer \n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device: CreateBuffer() sucess for Vertex buffer \n");
		fclose(gpFile);

	}


	D3D11_MAPPED_SUBRESOURCE mappedSubresource = {};
	ZeroMemory((void *)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &mappedSubresource);
	memcpy(mappedSubresource.pData, triangleVertices, sizeof(triangleVertices));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer, NULL);

	//Create and Set  input  Layout
	D3D11_INPUT_ELEMENT_DESC inputElementDesc;
	
	inputElementDesc.SemanticName = "POSITION";
	inputElementDesc.SemanticIndex = 0;
	inputElementDesc.Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc.InputSlot = 0;
	inputElementDesc.AlignedByteOffset = 0;
	inputElementDesc.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc.InstanceDataStepRate = 0;

	hr = gpID3D11Device->CreateInputLayout(&inputElementDesc, 
		1, 
		pID3DBlob_VertexShaderSourceCode->GetBufferPointer(), 
		pID3DBlob_VertexShaderSourceCode->GetBufferSize(), 
		&gpID3D11InputLayout);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "Create Input Layout() Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "Create Input Layout() Success. \n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
	pID3DBlob_VertexShaderSourceCode->Release();
	pID3DBlob_VertexShaderSourceCode = NULL;

	//define and set  the  constant buffer
	D3D11_BUFFER_DESC bufferDesc_constBuffer;
	ZeroMemory((void *)&bufferDesc_constBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_constBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_constBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_constBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_constBuffer, NULL, &gpID3D11Buffer_ConstantBuffer);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "Create Constant Buffer() Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "Create Constant Buffer() Success. \n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->VSSetConstantBuffers(0,1, &gpID3D11Buffer_ConstantBuffer);

	//d3d Clear color
	//blue
	clearColor[0] = 0.0f;
	clearColor[1] = 0.0f;
	clearColor[2] = 1.0f;
	clearColor[3] = 1.0f;

	gPrespectivegraphicsProjectionMatrix = XMMatrixIdentity();

	hr = resize(WIN_WIDTH, WIN_HEIGHT); 
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "resize() Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "resize() Success. \n");
		fclose(gpFile);
	}
	return(S_OK);
}

HRESULT resize(int Width, int Height)
{
	HRESULT hr = S_OK;

	if (gpID3D11RenderTargetView != NULL)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	//resize Swap Chain buffer accordingly
	gpIDXGISwapChain->ResizeBuffers(1, Width, Height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	//again get back  from swap buffer chain
	ID3D11Texture2D *ID3D11Texture2D_backBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID *)&ID3D11Texture2D_backBuffer);

	//again  get  render target  view  from  d3d11  device using above  back  buffer 
	hr = gpID3D11Device->CreateRenderTargetView(ID3D11Texture2D_backBuffer, NULL, &gpID3D11RenderTargetView);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Failed...\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszlogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Success. \n");
		fclose(gpFile);
	}
	ID3D11Texture2D_backBuffer->Release();
	ID3D11Texture2D_backBuffer =  NULL;

	//Set render target view as render target

	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

	//Set viewport
	D3D11_VIEWPORT d3dViewPort = {};
	ZeroMemory((void *)&d3dViewPort, sizeof(D3D11_VIEWPORT));
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)Width;
	d3dViewPort.Height = (float)Height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;

	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);
	if (Height == 0)
		Height = 1;
	gPrespectivegraphicsProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f),
		(float)Width / (float)Height,
		0.1f,
		100.0f);
	return (hr);
}

void Display(void) 
{
	//code
	//clear render target view  to a chosen  color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, clearColor);

	//Select which Vertex Buffer to display
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;

	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer, &stride, &offset);

	//Select Geometry Primetive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//translate is cincerned with  world view matrix transformation
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 6.0f);

	worldMatrix = translationMatrix;
	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * gPrespectivegraphicsProjectionMatrix;

	//Load the  data info  the constant buffer
	CBUFFER constantBuffer;
//	ZeroMemory((void*)&constantBuffer, sizeof(constantBuffer));
	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->Draw(3, 0);

	gpIDXGISwapChain->Present(0, 0);
}

void uninitialize(void)
{
	if (isFullscreen)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
/*ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;
*/
	if (gpID3D11VertexShader != NULL)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11PixelShader != NULL)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer != NULL)
	{
		gpID3D11Buffer_VertexBuffer->Release();
		gpID3D11Buffer_VertexBuffer = NULL;
	}

	if (gpID3D11InputLayout != NULL)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_ConstantBuffer != NULL)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11RenderTargetView != NULL)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain != NULL)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext != NULL)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device != NULL)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	DestroyWindow(ghwnd);
	ghwnd = NULL;

	fprintf_s(gpFile,"---------- ANK: DirectX Debug Logs End ----------\n");
	fclose(gpFile);
}


